#ifndef mkserv_logging_H
#define mkserv_logging_H

namespace mkserv {

  void set_application(const char *name);

  void error(const char* filename,
	     unsigned linenumber,
	     const char* text);
}

#endif /* mkserv_logging_H */
