/* gcc -c mkserv_main.cpp */
/* gcc -o mkserv mkserv_main.cpp mkserv_message.cpp mkserv_server.cpp mkserv_socket.cpp mkserv_logging.cpp -lstdc++ -lpthread -lm */
/* gcc -o mkserv_spectra mkserv_main.cpp mkserv_message.cpp mkserv_server.cpp mkserv_socket.cpp mkserv_logging.cpp -lstdc++ -lpthread -lm */
/* gcc -o mkserv_samples mkserv_main.cpp mkserv_message.cpp mkserv_server.cpp mkserv_socket.cpp mkserv_logging.cpp -lstdc++ -lpthread -lm */
#include <stdio.h>
#include <iostream>     // std::cout

#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <thread>
#include <atomic>
#include <vector>

//#define SIMULATION
#define RINGBUFFER

#ifdef RINGBUFFER
#include "boost/program_options.hpp"

#include "psrdada_cpp/cli_utils.hpp"
#include "psrdada_cpp/multilog.hpp"
#include "psrdada_cpp/simple_file_writer.hpp"
#include "psrdada_cpp/dada_input_stream.hpp"
#include "psrdada_cpp/dada_output_stream.hpp"
#include "psrdada_cpp/dada_client_base.hpp"
#include "psrdada_cpp/dada_null_sink.hpp"
#include "psrdada_cpp/common.hpp"

const size_t SUCCESS = 0;
const size_t ERROR_IN_COMMAND_LINE = 1;
const size_t ERROR_UNHANDLED_EXCEPTION = 2;

namespace po = boost::program_options;

template<typename T>
static po::typed_value<T> *make_opt(T &var)
{
  return po::value<T>(&var);
}

#endif /* RINGBUFFER */

#include "mkserv_server.h"

typedef struct {
  int          port;         // server port (default is mkserv::PORT)
  int          mode;         // operating mode
  // properties when the server provides spectra
  int          nspectra;     // number of spectra stored in one ringbuffer slot
  int          nchannels;    // number of spectral channels
  int          ncounters;    // number of additional couters (nreceived, nsaturated)
  float        bandwidth;    // bandwidth of the spectrum in MHz
  // properties when the server provides samples
  int          nfeeds;       // number of parallel data feeds (e.g. number of ADC's)
  int          nblocks;      // number of sample blocks in one feed stored in one ringbuffer slot (total of nblocks*nfeeds in one slot)
  int          nsamples;     // number of samples in one block;
  int          nbits;        // number of bits per sample
  int          nscis;        // number of side channel items
} setup_t;

static void mkservSigChild(int signo)
{
  pid_t   pid;
  int     stat;

  while ((pid = waitpid(-1, &stat, WNOHANG)) > 0) {
    //printf("child %d terminated\n", pid);
  }
  return;
}

#ifdef RINGBUFFER
class ExtractSpectrum
{
public:
  explicit ExtractSpectrum(setup_t *setup);
  ExtractSpectrum(ExtractSpectrum const&) = delete;
  ~ExtractSpectrum();
  void init(psrdada_cpp::RawBytes&);
  bool operator()(psrdada_cpp::RawBytes&);
private:
  setup_t      *_setup;
};

ExtractSpectrum::ExtractSpectrum(setup_t *setup)
{
  _setup = setup;
}

ExtractSpectrum::~ExtractSpectrum()
{
}

void ExtractSpectrum::init(psrdada_cpp::RawBytes& block)
{
}

bool ExtractSpectrum::operator()(psrdada_cpp::RawBytes& block)
{
  mkserv::server::raw_spectrum((int8_t*)(block.ptr()), _setup->nchannels);
  return false;
}

class ExtractSamples
{
public:
  explicit ExtractSamples(setup_t *setup);
  ExtractSamples(ExtractSamples const&) = delete;
  ~ExtractSamples();
  void init(psrdada_cpp::RawBytes&);
  bool operator()(psrdada_cpp::RawBytes&);
private:
  setup_t      *_setup;
  size_t        _data_length;
  size_t        _sci_offset;
  size_t        _sci_length;
};

ExtractSamples::ExtractSamples(setup_t *setup)
{
  _setup       = setup;
  _data_length = setup->nfeeds*setup->nblocks*setup->nsamples*setup->nbits/8;
  _sci_length  = sizeof(uint64_t)*setup->nfeeds*setup->nblocks*setup->nscis;
}

ExtractSamples::~ExtractSamples()
{
}

void ExtractSamples::init(psrdada_cpp::RawBytes& block)
{
}

bool ExtractSamples::operator()(psrdada_cpp::RawBytes& block)
{
  // int8_t *data, size_t data_length, size_t sci_offset, size_t sci_length
  size_t slot_nheaps = block.total_bytes()/(_setup->nsamples*_setup->nbits/8 + _setup->nscis*sizeof(uint64_t));
  _sci_offset = block.total_bytes() - slot_nheaps*sizeof(uint64_t)*_setup->nscis;
  mkserv::server::raw_samples((int8_t*)(block.ptr()), _data_length, _sci_offset, _sci_length);
  return false;
}

#endif /* RINGBUFFER */

static void do_server(setup_t *setup)
{
  mkserv::socket               sock;
  std::vector<mkserv::server*> servers;
  std::vector<std::thread*>    threads;

  if (sock.listen(setup->port) == -1) exit(1);
  signal(SIGCHLD, mkservSigChild);
  while (true) {
    if (!sock.accept()) break;
    std::cout << "mkserv: accept() -> " << sock.cfd() << std::endl;
    mkserv::server *s = new mkserv::server(sock.cfd());
    s->mode(setup->mode);
    s->nspectra(setup->nspectra);
    s->nchannels(setup->nchannels);
    s->ncounters(setup->ncounters);
    s->bandwidth(setup->bandwidth);
    s->nfeeds(setup->nfeeds);
    s->nblocks(setup->nblocks);
    s->nsamples(setup->nsamples);
    s->nbits(setup->nbits);
    s->nscis(setup->nscis);
    std::thread    *t = new std::thread([s] () { s->process_requests(); });
    servers.push_back(s);
    threads.push_back(t);
  }
  for (auto t : threads) {
    t->join();
  }
}

#ifdef SIMULATION
static int8_t gSimSpectrum[2*mkserv::RAW_SPECTRUM_SIZE*sizeof(float) + 2*sizeof(int32_t)];
static int8_t gSimSamples[mkserv::MAX_NFEEDS*mkserv::MAX_NBLOCKS*mkserv::RAW_SAMPLES_SIZE*sizeof(int8_t) + mkserv::MAX_NFEEDS*mkserv::MAX_NBLOCKS*mkserv::MAX_NSCIS*sizeof(uint64_t)];
#endif /* SIMULATION */

int main(int argc, char *argv[])
{
  mkserv::set_application("mkserv");
  static setup_t    setup;
  //setup.port      = mkserv::PORT;
  setup.port      = mkserv::PORT + 1;
  setup.mode      = mkserv::DUPLICATES_MODE;         // operating mode
  // properties when the server provides spectra
  setup.nspectra  = 0;      // number of spectra stored in one ringbuffer slot
  setup.nchannels = 65536;  // number of spectral channels
  setup.ncounters = 2;      // number of additional couters (nreceived, nsaturated)
  setup.bandwidth = 1350.0; // bandwidth of the spectrum in MHz
  // properties when the server provides samples
  setup.nfeeds    = 2;      // number of parallel data feeds (e.g. number of ADC's)
  setup.nblocks   = 4;      // number of sample blocks in one feed stored in one ringbuffer slot (total of nblocks*nfeeds in one slot)
  setup.nsamples  = 4096;   // number of samples in one block
  setup.nbits     = 8;      // number of bits per sample
  setup.nscis     = 2;      // number of side channel items
#ifdef RINGBUFFER
  // We read always spectra from an input PSR DADA ringbuffer
  key_t   input_key;

  // program option declaration
  namespace po = boost::program_options;
  po::options_description desc("Options");
  desc.add_options()("help,h", "Print help messages");
  desc.add_options()(
		     "input_key,i",
		     po::value<std::string>()->default_value("dada")->notifier([&input_key](std::string in) { input_key = psrdada_cpp::string_to_key(in); }),
		     "The shared memory key for the dada buffer to connect to (hex string)");
  desc.add_options()("port",      make_opt(setup.port), "The server port");
  desc.add_options()("mode",      make_opt(setup.mode), "The operating mode");
  desc.add_options()("nspectra",  make_opt(setup.nspectra), "The number of spectra in one ringbuffer slot");
  desc.add_options()("nchannels", make_opt(setup.nchannels), "The number of channels in the input spectra");
  desc.add_options()("ncounters", make_opt(setup.ncounters), "The number of additional couters (nreceived, nsaturated)");
  desc.add_options()("bandwidth", make_opt(setup.bandwidth), "The bandwidth of the spectrum in MHz");
  desc.add_options()("nfeeds",    make_opt(setup.nfeeds), "The number of parallel data feeds (e.g. number of ADC's)");
  desc.add_options()("nblocks",   make_opt(setup.nblocks), "The number of sample blocks in one feed (total of nblocks*nfeeds in one slot)");
  desc.add_options()("nsamples",  make_opt(setup.nsamples), "The number of samples in one block");
  desc.add_options()("nbits",     make_opt(setup.nbits), "The number of bits per sample");
  desc.add_options()("nscis",     make_opt(setup.nscis), "The number of side channel items");
  po::variables_map vm;
  try {
    po::store(po::parse_command_line(argc, argv, desc), vm);
    if (vm.count("help")) {
      std::cout << "Read PFB spectra from an input DADA ringbuffer and provide access via a socket connection."
                << std::endl
                << desc << std::endl;
      return SUCCESS;
    }
    po::notify(vm);
  } catch (po::error &e) {
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
    std::cerr << desc << std::endl;
    return ERROR_IN_COMMAND_LINE;
  }
#endif /* RINGBUFFER */
  if (setup.nspectra != 0) {
    // #################### Spectra ####################
    std::thread server_thread = std::thread(do_server, &setup);
#ifdef SIMULATION
    float   *sdata = (float*)gSimSpectrum;
    int32_t *idata = (int32_t*)(sdata + 2*mkserv::RAW_SPECTRUM_SIZE);
    int ipeak = 1;
    do {
      for (int i = 0; i < 2*mkserv::RAW_SPECTRUM_SIZE; i++) {
	if ((i == 2*ipeak) || (i == 2*ipeak+1)) {
	  sdata[i] = 10.0;
	} else {
	  sdata[i] = 1.0;
	}
      }
      idata[0] = ipeak;
      idata[1] = 13;
      ipeak = (ipeak + 1)%mkserv::RAW_SPECTRUM_SIZE;
      std::cout << "spectrum: ipeak " << ipeak << std::endl;
      mkserv::server::raw_spectrum(gSimSpectrum, mkserv::RAW_SPECTRUM_SIZE);
      usleep(1000000);
    } while (true);
#endif /* SIMULATION */
#ifdef RINGBUFFER
    psrdada_cpp::MultiLog log("MKSERV");
    ExtractSpectrum sink(&setup);
    psrdada_cpp::DadaInputStream<decltype(sink)> istream(input_key, log, sink);
    istream.start();
#endif /* RINGBUFFER */
    server_thread.join();  // <== This statement must be here, otherwise sink and istream are deleted immediately!
  } else if (setup.nfeeds != 0) {
    // #################### Samples ####################
    std::thread server_thread = std::thread(do_server, &setup);
#ifdef SIMULATION
    int8_t   *data        = gSimSamples;
    int32_t   data_length = setup.nfeeds*setup.nblocks*setup.nsamples*setup.nbits/8;
    uint64_t *scis        = (uint64_t*)(data + data_length);
    int32_t   sci_offset  = data_length;
    int32_t   sci_count   = setup.nfeeds*setup.nblocks*setup.nscis;
    int8_t    samp        = 0;
    uint32_t  sci         = 1;
    do {
      for (int i = 0; i < data_length; i++) {
	data[i] = samp;
	samp = (samp + 1)%105;
      }
      for (int i = 0; i < sci_count;   i++) {
	scis[i] = sci++;
      }
      mkserv::server::raw_samples(data, data_length, sci_offset, sizeof(uint64_t)*sci_count);
      usleep(100000);
    } while (true);
#endif /* SIMULATION */
#ifdef RINGBUFFER
    psrdada_cpp::MultiLog log("MKSERV");
    ExtractSamples sink(&setup);
    psrdada_cpp::DadaInputStream<decltype(sink)> istream(input_key, log, sink);
    istream.start();
#endif /* RINGBUFFER */
    server_thread.join();  // <== This statement must be here, otherwise sink and istream are deleted immediately!
  } else {
  }
  exit(0);
}
