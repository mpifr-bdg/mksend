#include <iostream>
#include <utility>
#include <chrono>
#include <cstdint>
#include <cstdlib>
#include <vector>
#include <string>
#include <memory>
#include <signal.h>
#include <atomic>
#include <thread>
#include <sched.h>
#include <unistd.h>

#include <spead2/common_thread_pool.h>
#include <spead2/send_udp.h>
#if SPEAD2_USE_IBV
# include <spead2/send_udp_ibv.h>
#endif

#include "psrdada_cpp/cli_utils.hpp"

#include "mksend_sender.h"

namespace mksend
{

  mksend::sender *sender::instance = NULL;

  static int          g_stopped = 0;
  static std::thread  g_stop_thread;

  void signal_handler(int signalValue)
  {
    std::cout << "received signal " << signalValue << std::endl;
    g_stopped++;
    // 1. Set a flag on the memory allocator (we want to finish the current slot or we have to terminate (not sending!) the current slot)
    // 2. If the current slot is finished or terminated, terminate the streams (one for each multicast group)
    g_stop_thread = std::thread(mksend::sender::request_stop);
    if (g_stopped > 1)
      {
	exit(-1);
      }
  }

  sender::sender() : mlog("send")
  {
    instance = this;
    opts = NULL;
    //allocator = NULL;
    thread_pool = NULL;
  }

  sender::~sender()
  {
  }

  //############################## spead2::send::udp_stream
  /**
   * Constructor with multicast hop count and outgoing interface address
   * (IPv4 only).
   *
   * @param io_service   I/O service for sending data
   * @param endpoint     Multicast group and port
   * @param config       Stream configuration
   * @param buffer_size  Socket buffer size (0 for OS default)
   * @param ttl          Maximum number of hops
   * @param interface_address   Address of the outgoing interface
   *
   * @throws std::invalid_argument if @a endpoint is not an IPv4 multicast address
   * @throws std::invalid_argument if @a interface_address is not an IPv4 address
   */
  /*
  udp_stream(
	     io_service_ref io_service,
	     const boost::asio::ip::udp::endpoint &endpoint,
	     const stream_config &config,
	     std::size_t buffer_size,
	     int ttl,
	     const boost::asio::ip::address &interface_address);
  */
  
  //############################## spead2::send::udp_ibv_stream
  /**
   * Constructor.
   *
   * @param io_service   I/O service for sending data
   * @param endpoint     Multicast group and port
   * @param config       Stream configuration
   * @param interface_address   Address of the outgoing interface
   * @param buffer_size  Socket buffer size (0 for OS default)
   * @param ttl          Maximum number of hops
   * @param comp_vector  Completion channel vector (interrupt) for asynchronous operation, or
   *                     a negative value to poll continuously. Polling
   *                     should not be used if there are other users of the
   *                     thread pool. If a non-negative value is provided, it
   *                     is taken modulo the number of available completion
   *                     vectors. This allows a number of readers to be
   *                     assigned sequential completion vectors and have them
   *                     load-balanced, without concern for the number
   *                     available.
   * @param max_poll     Maximum number of times to poll in a row, without
   *                     waiting for an interrupt (if @a comp_vector is
   *                     non-negative) or letting other code run on the
   *                     thread (if @a comp_vector is negative).
   *
   * @throws std::invalid_argument if @a endpoint is not an IPv4 multicast address
   * @throws std::invalid_argument if @a interface_address is not an IPv4 address
   */
  /*
  udp_ibv_stream(
		 io_service_ref io_service,
		 const boost::asio::ip::udp::endpoint &endpoint,
		 const stream_config &config,
		 const boost::asio::ip::address &interface_address,
		 std::size_t buffer_size = default_buffer_size,
		 int ttl = 1,
		 int comp_vector = 0,
		 int max_poll = default_max_poll);
  */

#if defined(SPEAD2_MAJOR) && (SPEAD2_MAJOR == 3)

  std::unique_ptr<spead2::send::stream> sender::make_stream(std::vector<std::string>::iterator destination)
  {
    using asio::ip::udp;

    udp::resolver                         resolver(thread_pool->get_io_service());
    udp::resolver::query                  query((*destination), opts->port);
    std::unique_ptr<spead2::send::stream> stream;

    try {
    if (opts->network_mode == NO_NETWORK_MODE) return stream;
#if SPEAD2_USE_IBV
    if (opts->ibv_if != "")
      {
	//std::cout << "creating an IBV UDP stream at " << opts->ibv_if << std::endl;
	spead2::send::udp_ibv_config  ibv_config;
	ibv_config.add_endpoint(*resolver.resolve(query));
	ibv_config.set_interface_address(boost::asio::ip::address::from_string(opts->ibv_if));
	ibv_config.set_buffer_size(opts->buffer);
	ibv_config.set_comp_vector(opts->ibv_comp_vector);
	ibv_config.set_max_poll(opts->ibv_max_poll);
	ibv_config.set_ttl(opts->nhops);
	//std::cout << "  endpoint = " << ibv_config.get_endpoints()[0] << std::endl;
	//std::cout << "  interface = " << ibv_config.get_interface_address() << std::endl;
	//std::cout << "  buffer size = " << ibv_config.get_buffer_size() << std::endl;
	//std::cout << "  completion vector = " << ibv_config.get_comp_vector() << std::endl;
	//std::cout << "  max poll = " << ibv_config.get_max_poll() << std::endl;
	//std::cout << "  nhops = " << (int)(ibv_config.get_ttl()) << std::endl;
        stream.reset(new spead2::send::udp_ibv_stream(spead2::io_service_ref(thread_pool), *sconfig, ibv_config));
      }
    else
#endif
    if (opts->udp_if != "")
      {
        std::vector<boost::asio::ip::udp::endpoint> endpoints;
	endpoints.push_back(*resolver.resolve(query));
        boost::asio::ip::address interface_address = boost::asio::ip::address::from_string(opts->udp_if);
        stream.reset(new spead2::send::udp_stream(spead2::io_service_ref(thread_pool),
                                                  endpoints,
                                                  *sconfig,
                                                  opts->buffer,
                                                  opts->nhops,
                                                  interface_address));
      }
    else
      {
        std::vector<boost::asio::ip::udp::endpoint> endpoints;
        endpoints.push_back(*resolver.resolve(query));
        stream.reset(new spead2::send::udp_stream(spead2::io_service_ref(thread_pool),
                                                  endpoints,
                                                  *sconfig,
                                                  opts->buffer,
                                                  opts->nhops));
      }
    } catch (std::runtime_error &e) {
            std::cerr << "ERROR (make_stream()): " << e.what() << std::endl;
    }
    return stream;
  }

#else

  std::unique_ptr<spead2::send::stream> sender::make_stream(std::vector<std::string>::iterator destination)
  {
    using asio::ip::udp;

    udp::resolver          resolver(thread_pool->get_io_service());
    udp::resolver::query   query((*destination), opts->port);
    udp::endpoint          endpoint = *resolver.resolve(query);
    std::unique_ptr<spead2::send::stream> stream;

    try {
    if (opts->network_mode == NO_NETWORK_MODE) return stream;
#if SPEAD2_USE_IBV
    if (opts->ibv_if != "")
      {
        boost::asio::ip::address interface_address = boost::asio::ip::address::from_string(opts->ibv_if);
        stream.reset(new spead2::send::udp_ibv_stream(spead2::io_service_ref(thread_pool),
                                                      endpoint,
                                                      *sconfig,
                                                      interface_address,
                                                      opts->buffer,
                                                      opts->nhops,
                                                      opts->ibv_comp_vector,
                                                      opts->ibv_max_poll));
      }
    else
#endif
    if (opts->udp_if != "")
      {
        boost::asio::ip::address interface_address = boost::asio::ip::address::from_string(opts->udp_if);
        stream.reset(new spead2::send::udp_stream(spead2::io_service_ref(thread_pool),
                                                  endpoint,
                                                  *sconfig,
                                                  opts->buffer,
                                                  opts->nhops,
                                                  interface_address));
      }
    else
      {
        stream.reset(new spead2::send::udp_stream(spead2::io_service_ref(thread_pool),
                                                  endpoint,
						  *sconfig,
                                                  opts->buffer,
                                                  opts->nhops));
      }
    } catch (std::runtime_error &e) {
            std::cerr << "ERROR (make_stream()): " << e.what() << std::endl;
    }
    return stream;
  }

#endif /* defined(SPEAD2_MAJOR) && (SPEAD2_MAJOR == 3) */


  int sender::execute(int argc, const char **argv)
  {
    try
      {
        std::vector<int> affinity;

	opts = create_options();
	opts->parse_args(argc, argv);
	std::cout << opts->header << std::endl;
	if (opts->quiet)
        {
            psrdada_cpp::set_log_level("warning");
        }
	spead2::bug_compat_mask    bug_compat = opts->pyspead ? spead2::BUG_COMPAT_PYSPEAD_0_5_2 : 0;
	spead2::flavour            f(spead2::maximum_version, 64, 48, bug_compat);
	cpu_set_t cpuset;
	bool      lst = false;
	sched_getaffinity(0, sizeof(cpuset), &cpuset);
	for (int i = 0; i < CPU_SETSIZE; i++) {
	  if (CPU_ISSET(i, &cpuset)) {
	    std::cout << "CPU/CORE " << i << " used\n";
	    if (!lst) {
	      lst = true;
	    } else {
	      affinity.push_back(i);
	    }
	  }
	}
	thread_pool.reset(new spead2::thread_pool(opts->threads, affinity));
#if defined(SPEAD2_MAJOR) && (SPEAD2_MAJOR == 3)
	spead2::send::stream_config *cfg = new spead2::send::stream_config();
	cfg->set_max_packet_size(opts->packet);
	cfg->set_rate(opts->nwrate);
	cfg->set_burst_size(opts->burst_size);
	cfg->set_max_heaps(opts->heaps);
	cfg->set_burst_rate_ratio(opts->burst_ratio);
	sconfig.reset(cfg);
#else
	sconfig.reset(new spead2::send::stream_config(opts->packet,
						      opts->nwrate, 
						      opts->burst_size,
						      opts->heaps,
                                                      opts->burst_ratio));
#endif
	//std::cout << "max packet size = " << sconfig->get_max_packet_size() << std::endl;
	//std::cout << "rate = " << sconfig->get_rate() << std::endl;
	//std::cout << "burst size = " << sconfig->get_burst_size() << std::endl;
	//std::cout << "max heaps = " << sconfig->get_max_heaps() << std::endl;
	//std::cout << "burst rate ratio = " << sconfig->get_burst_rate_ratio() << std::endl;
	spead2::item_pointer_t  next   = (spead2::item_pointer_t)(opts->heap_id_start);
	spead2::item_pointer_t  offset = (spead2::item_pointer_t)(opts->heap_id_offset);
	spead2::item_pointer_t  step   = (spead2::item_pointer_t)(opts->heap_id_step);
	for (auto it = opts->destinations.begin(); it != opts->destinations.end(); ++it)
	  {
	    streams.push_back(std::unique_ptr<mksend::stream>(new mksend::stream(make_stream(it), opts->heaps)));
	    streams.at(streams.size() - 1)->set_cnt_sequence(next, step);
	    streams.at(streams.size() - 1)->create_heaps(f);
	    next += offset;
	  }
	ringbuffer = create_ringbuffer();
	signal(SIGINT, signal_handler);
	/*
	  std::int64_t n_complete = 0;
	  for (const auto &ptr : streams)
	  {
	  auto &stream = dynamic_cast<mksend::stream &>(*ptr);
	  n_complete += stream.join();
	  }
	*/
	if (opts->dada_mode == FULL_DADA_MODE)
	  {
	    ringbuffer->start();
	  }
	else
	  {
	    ringbuffer->simulate();
	  }
	//std::cout << "Sent " << n_complete << " heaps\n";
      }
    catch (std::runtime_error &e)
      {
	std::cerr << "ERROR (execute(): " << e.what() << std::endl;
	std::cerr << "Cannot connect to DADA Ringbuffer " << opts->dada_key << " exiting..." << std::endl;
	exit(0);
      }
    if (opts->dada_mode == FULL_DADA_MODE) g_stop_thread.join();
    return 0;
  }

  std::shared_ptr<mksend::options> sender::create_options()
  {
    return std::shared_ptr<mksend::options>(new mksend::options());
  }

  std::shared_ptr<mksend::ringbuffer> sender::create_ringbuffer()
  {
    return std::shared_ptr<mksend::ringbuffer>(new mksend::ringbuffer(psrdada_cpp::string_to_key(opts->dada_key), mlog, opts, streams));
  }

  /*
  std::unique_ptr<mksend::stream> sender::create_stream()
  {
    spead2::bug_compat_mask bug_compat = opts->pyspead ? spead2::BUG_COMPAT_PYSPEAD_0_5_2 : 0;

    return std::unique_ptr<mksend::stream>(new mksend::stream(opts, thread_pool, bug_compat, opts->heaps));
  }
  */

  void sender::request_stop()
  {
    instance->ringbuffer->stop();
    /*
    // request a stop from the allocator (fill the current ringbuffer slot)
    instance->allocator->request_stop();
    // test if the last ringbuffer slot was filled and the ringbuffer closed
    do {
      if (instance->allocator->is_stopped()) break;
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
    } while (1);
    // And now stop all streams
    for (const auto &ptr : instance->streams)
      {
	auto &stream = dynamic_cast<mksend::stream &>(*ptr);
	stream.stop();
      }
    */
  }

}
