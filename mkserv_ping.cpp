/* gcc -c mkserv_ping.cpp */
/* gcc -o mkping mkserv_ping.cpp mkserv_message.cpp mkserv_socket.cpp mkserv_logging.cpp -lstdc++ -lpthread */
/* ./mkping localhost */

#include <stdio.h>
#include <iostream>     // std::cout

#include <thread>
#include <atomic>
#include <vector>

#include "mkserv_logging.h"
#include "mkserv_message.h"

static void send_ping(mkserv::socket &sock)
{
  mkserv::message_header msg_hdr;
  mkserv::ping_request   ping_req(&msg_hdr);
  mkserv::ping_reply     ping_rep(&msg_hdr);

  ping_req.create();
  if (!ping_req.send_message(sock)) exit(2);
  if (!msg_hdr.receive_header(sock)) exit(3);
  if (msg_hdr.tag() == mkserv::ERROR_TAG) {
    mkserv::error_reply  err_rep(&msg_hdr);
    if (!err_rep.receive_body(sock)) exit(4);
    std::cerr << "ERROR: cannot send a PING message, received an ERROR reply: " << err_rep.err_code() << ":" << err_rep.err_flags() << std::endl;
    return;
  }
  if (!ping_rep.receive_body(sock)) exit(4);
  std::cout << "reply: " << msg_hdr.tag() << msg_hdr.code() << " " << msg_hdr.payload() << std::endl;
}

static void send_exit(mkserv::socket &sock)
{
  mkserv::message_header      msg_hdr;

  msg_hdr.create(mkserv::EXIT_TAG, mkserv::REQUEST_CODE, 0);
  if (!msg_hdr.send_header(sock)) exit(2);
  if (!msg_hdr.receive_header(sock)) exit(3);
  std::cout << "reply: " << msg_hdr.tag() << msg_hdr.code() << " " << msg_hdr.payload() << std::endl;
}

int main(int argc, char *argv[])
{
  mkserv::set_application("mkping");

  mkserv::socket               sock;

  if (!sock.connect(argv[1], atoi(argv[2]))) exit(1);
  std::cout << "sock fd = " << sock.cfd() << std::endl;
  send_ping(sock);
  send_exit(sock);
  sock.finish_client();
}
