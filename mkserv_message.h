#ifndef mkserv_message_h
#define mkserv_message_h


#include "mkserv_socket.h"

namespace mkserv {

  const int MAX_SPECTRUM_SIZE = 128*1024;
  const int MAX_SAMPLES       = 4096;
  const int RAW_SPECTRUM_SIZE = 256*256;
  const int RAW_SAMPLES_SIZE  = 6144;
  const int MAX_NFEEDS        = 16;
  const int MAX_NBLOCKS       = 4;
  const int MAX_NSCIS         = 16;

  /*
   * using the message based system needs some explanations:
   *
   * creating and sending a request for a spectrum on the client side:
   *    socket             sock;
   *    message_request    msg_req;
   *    spectrum_request   spec_req;
   *    spec_req.create(frequency, count, binning);
   *    spec_req.send(sock, msg_request);
   *
   * getting a request and handle a specific type on the server side:
   *    socket             sock;
   *    message_request    msg_req;
   *    msg_req.receive(sock);
   *    if (msg_req.tag() == SPECTRUM_REQ) {
   *       spectrum_request   spec_req;
   *       spec_req.receive(sock, msg_req); // get the remaining parameters (frequency, count, binning)
   *       spectrum_reply     spec_rep;
   *       ... build the spectrum reply
   *       spec_rep.send(sock);
   *    }
   *
   * getting a reply on the client side:
   *    socket             sock;
   *    message_reply      msg_rep;
   *    spectrum_reply     spec_rep;
   *    spec_rep.receive(sock, msg_rep);
   *    ... work with the contents in spec_rep
   *
   * a request has the following structure:
   *    I2 tag
   *    I4 length
   *    [ length bytes payload ]
   * a reply has the following structure:
   *    I2 tag
   *    I2 err
   *    I4 length
   *    [ length bytes payload ]
   *
   * all payload data (request and reply) are handled in the specific receive classes
   */

  const char ERROR_TAG    = '?';
  const char PING_TAG     = 'P';
  const char EXIT_TAG     = 'E';
  const char FEATURES_TAG = 'F';
  const char SPECTRUM_TAG = 'S';
  const char SAMPLES_TAG  = 'R';

  const char REQUEST_CODE = '?';
  const char REPLY_CODE   = '!';

  const int16_t DUPLICATES_MODE  = 0;
  const int16_t CONTINUOUS_MODE  = 1;
  const int16_t NEWEST_MODE      = 2;

  // #######################################################
  // # class message_block
  // # provides the basic message block class
  // #######################################################
  class message_block {
  protected:
    int32_t   _length;  // the length of the message block
  public:
    message_block();
    virtual ~message_block();
    inline int32_t length() { return _length; };
    void add(size_t size);
    void add(size_t size, size_t count);
    void init();
    virtual int write(socket &sock);
    virtual int read(socket &sock);
  };
  
  // #######################################################
  // # class message_header
  // # provides the basic message header class
  // #######################################################
  class message_header : public message_block {
  protected:
    char      _msg_tag;        // each message has a specific tag (ping, spectrum, exit)
    char      _msg_code;       // each message has a specific code (request, reply)
    int32_t   _payload;        // number of bytes in the payload (after _payload element)
  public:
    message_header();
    ~message_header();
    inline char    tag()     { return _msg_tag;  };
    inline char    code()    { return _msg_code; };
    inline int32_t payload() { return _payload;  };
    void create(char tag, char code, int32_t payload);
    int write(socket &sock);
    int read(socket &sock);
    int send_header(socket &sock);
    int receive_header(socket &sock);
  };

  // #######################################################
  // # class message_body
  // #######################################################
  class message_body : public message_block {
  protected:
    message_header  *_hdr;
  public:
    message_body(message_header *hdr);
    ~message_body();
    int send_message(socket &sock);
    int receive_body(socket &sock);
  };

  // #######################################################
  // # class error_reply
  // #######################################################
  class error_reply : public message_body {
  protected:
    int32_t          _err_code;    // error code
    int32_t          _err_flags;   // error flags
  public:
    error_reply(message_header *hdr);
    ~error_reply();
    inline int32_t  err_code()           { return _err_code;     };
    inline int32_t  err_flags()          { return _err_flags;    };
    void create(int32_t err_code, int32_t err_flags);
    int write(socket &sock);
    int read(socket &sock);
  };

  // #######################################################
  // # class ping_request
  // #######################################################
  class ping_request : public message_body {
  public:
    ping_request(message_header *hdr);
    ~ping_request();
    void create();
  };

  // #######################################################
  // # class ping_reply
  // #######################################################
  class ping_reply : public message_body {
  public:
    ping_reply(message_header *hdr);
    ~ping_reply();
    void create();
  };

  // #######################################################
  // # class features_request
  // #######################################################
  class features_request : public message_body {
  protected:
    int16_t          _mode;         // operating mode
  public:
    features_request(message_header *hdr);
    ~features_request();
    inline int16_t mode() { return _mode; };
    void create(int16_t mode);
    int write(socket &sock);
    int read(socket &sock);
  };

  // #######################################################
  // # class features_reply
  // #######################################################
  class features_reply : public message_body {
  protected:
    // behaviour
    int16_t          _mode;         // operating mode
    // properties when the server provides spectra
    int32_t          _nspectra;     // number of spectra stored in one ringbuffer slot
    int32_t          _nchannels;    // number of spectral channels
    int32_t          _ncounters;    // number of additional couters (nreceived, nsaturated)
    float            _bandwidth;    // bandwidth of the spectrum in MHz
    // properties when the server provides samples
    int32_t          _nfeeds;       // number of parallel data feeds (e.g. number of ADC's)
    int32_t          _nblocks;      // number of sample blocks in one feed stored in one ringbuffer slot (total of nblocks*nfeeds in one slot)
    int32_t          _nsamples;     // number of samples in one block;
    int32_t          _nbits;        // number of bits per sample
    int32_t          _nscis;        // number of side channel items
  public:
    features_reply(message_header *hdr);
    ~features_reply();
    inline void     mode(int16_t m)      { _mode = m;      };
    inline void     nspectra(int32_t n)  { _nspectra = n;  };
    inline void     nchannels(int32_t n) { _nchannels = n; };
    inline void     ncounters(int32_t n) { _ncounters = n; };
    inline void     bandwidth(float b)   { _bandwidth = b; };
    inline void     nfeeds(int32_t n)    { _nfeeds = n;    };
    inline void     nblocks(int32_t n)   { _nblocks = n;   };
    inline void     nsamples(int32_t n)  { _nsamples = n;  };
    inline void     nbits(int32_t n)     { _nbits = n;     };
    inline void     nscis(int32_t n)     { _nscis = n;     };
    inline int16_t  mode()      { return _mode;      };
    inline int32_t  nspectra()  { return _nspectra;  };
    inline int32_t  nchannels() { return _nchannels; };
    inline int32_t  ncounters() { return _ncounters; };
    inline float    bandwidth() { return _bandwidth; };
    inline int32_t  nfeeds()    { return _nfeeds;    };
    inline int32_t  nblocks()   { return _nblocks;   };
    inline int32_t  nsamples()  { return _nsamples;  };
    inline int32_t  nbits()     { return _nbits;     };
    inline int32_t  nscis()     { return _nscis;     };
    void create(int16_t mode, int32_t nspectra, int32_t nchannels, int32_t ncounters, float bandwidth, int32_t nfeeds, int32_t nblocks, int32_t nsamples, int32_t nbits, int32_t nscis);
    void create();
    int write(socket &sock);
    int read(socket &sock);
  };


  // #######################################################
  // # class spectrum_request
  // #######################################################
  class spectrum_request : public message_body {
  protected:
    float            _frequency;
    int32_t          _count;
    int32_t          _binning;
  public:
    spectrum_request(message_header *hdr);
    ~spectrum_request();
    inline float   frequency() { return _frequency; };
    inline int32_t count()     { return _count;     };
    inline int32_t binning()   { return _binning;   };
    void create(float frequency, int32_t count, int32_t binning);
    int write(socket &sock);
    int read(socket &sock);
  };

  // #######################################################
  // # class spectrum_reply
  // #######################################################
  class spectrum_reply : public message_body {
  protected:
    int32_t          _nreceived;    // number of received heaps (4K samples)
    int32_t          _nsaturated;   // number of saturated samples
    float            _freq_low;     // the lowest frequency (spectrum[0])
    float            _freq_high;    // the highest frequency (spectrum[count-1])
    int32_t          _count;        // count contains the number of channels in the processed raw data
    int32_t          _binning;      // the number of raw data channels used to compute one sample
    float            _spectrum[MAX_SPECTRUM_SIZE]; // processed (mapped) raw data
  public:
    spectrum_reply(message_header *hdr);
    ~spectrum_reply();
    inline int32_t  nreceived()           { return _nreceived;     };
    inline int32_t  nsaturated()          { return _nsaturated;    };
    inline float    freq_low()            { return _freq_low;      };
    inline float    freq_high()           { return _freq_high;     };
    inline int32_t  count()               { return _count;         };
    inline int32_t  binning()             { return _binning;       };
    inline float   &spectrum(int32_t idx) { return _spectrum[idx]; };
    void create(int32_t nreceived, int32_t nsaturated, float freq_low, float freq_high, int32_t count, int32_t binning);
    int write(socket &sock);
    int read(socket &sock);
  };


  // #######################################################
  // # class samples_request
  // #######################################################
  class samples_request : public message_body {
  protected:
    int16_t          _mode;
    int32_t          _feed;
  public:
    samples_request(message_header *hdr);
    ~samples_request();
    inline int16_t mode() { return _mode;   };
    inline int32_t feed() { return _feed;   };
    void create(int16_t mode, int32_t feed);
    int write(socket &sock);
    int read(socket &sock);
  };

  // #######################################################
  // # class samples_reply
  // #######################################################
  class samples_reply : public message_body {
  protected:
    int32_t          _feed;                 // feed number, same as samples_request
    int32_t          _block;                // current block in a sequence of sample blocks of one feed (start with 0)
    int32_t          _nsamples;             // count contains the number of samples
    int32_t          _nscis;                // count contains the number of side-channel-items
    int16_t          _samples[MAX_SAMPLES]; // samples
    uint64_t         _scis[MAX_NSCIS];      // side-channel-items
  public:
    samples_reply(message_header *hdr);
    ~samples_reply();
    inline int32_t   feed()               { return _feed;        };
    inline int32_t   block()              { return _block;       };
    inline int32_t   nsamples()           { return _nsamples;     };
    inline int16_t  &samples(int32_t idx) { return _samples[idx]; };
    inline int32_t   nscis()              { return _nscis;        };
    inline uint64_t &scis(int32_t idx)    { return _scis[idx];    };
    void create(int32_t feed, int32_t block, int32_t nsamples, int32_t nscis);
    int write(socket &sock);
    int read(socket &sock);
  };

}

#endif /* mkserv_message_h */
