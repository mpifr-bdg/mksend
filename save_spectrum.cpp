/* Save spectra from a PSRDADA ringbuffer into a file. */

// EDD PFB program puts spectra into a PRSDADA ringbuffer which contains two numbers (summed up squared real and imaginary values) for each channel and two counters (number of used SPEAD heaps and number of heaps containing saturated samples).

#include <iostream>
#include <fstream>
#include <iomanip>
#include "boost/program_options.hpp"
#include <ctime>
#include <memory>

#include "psrdada_cpp/cli_utils.hpp"
#include "psrdada_cpp/multilog.hpp"
#include "psrdada_cpp/simple_file_writer.hpp"
#include "psrdada_cpp/dada_input_stream.hpp"
#include "psrdada_cpp/dada_output_stream.hpp"
#include "psrdada_cpp/dada_client_base.hpp"
#include "psrdada_cpp/dada_null_sink.hpp"
#include "psrdada_cpp/common.hpp"

const size_t SUCCESS = 0;
const size_t ERROR_IN_COMMAND_LINE = 1;
const size_t ERROR_UNHANDLED_EXCEPTION = 2;

class ExtractSpectrumChannels
{
public:
  explicit ExtractSpectrumChannels(std::string filename, int spectra_size, int binning_mode, int selected_channels, float scale);
  ExtractSpectrumChannels(ExtractSpectrumChannels const&) = delete;
  ~ExtractSpectrumChannels();
  void init(psrdada_cpp::RawBytes&);
  bool operator()(psrdada_cpp::RawBytes&);
private:
  std::ofstream _outfile;
  int      _spectra_size;
  int      _binning_mode;
  int      _selected_channels;
  float    _scale;
  int      _step;
  float   *_spectra;
};

ExtractSpectrumChannels::ExtractSpectrumChannels(std::string filename, int spectra_size, int binning_mode, int selected_channels, float scale)
{
  _outfile.open(filename.c_str(),std::ifstream::out);
  if (_outfile.is_open()) {
    BOOST_LOG_TRIVIAL(debug) << "Opened file " << filename;
  } else {
    std::stringstream stream;
    stream << "Could not open file " << filename;
    throw std::runtime_error(stream.str().c_str());
  }
  _spectra_size = spectra_size;
  _binning_mode = binning_mode;
  _selected_channels = selected_channels;
  _scale             = scale;
  _step              = (_spectra_size/_binning_mode)/(_selected_channels+1);
  _spectra = new float[_spectra_size/_binning_mode];
}

ExtractSpectrumChannels::~ExtractSpectrumChannels()
{
  _outfile.close();
  delete _spectra;
}

void ExtractSpectrumChannels::init(psrdada_cpp::RawBytes& block)
{
  // do nothing
}

bool ExtractSpectrumChannels::operator()(psrdada_cpp::RawBytes& block)
{
  // create binned spectrum out of an input spectrum (beware, the input spectrum does not contain the zero frequency!)
  int i, j;

  float *in  = (float*)(block.ptr());
  float *out = _spectra;
  for (i = 0; i < _spectra_size/_binning_mode; i++) {
    *out = 0.0;
    for (j = 0; j < 2*_binning_mode; j++) {
      *out += *in++;
    }
    out++;
  }
  //  ch(i) := trunc(4096/11) * (i+1), i=0..9
  _outfile << (_spectra[_step]/_scale);
  for (i = 1; i < _selected_channels; i++) {
    _outfile << " " << (_spectra[_step*(i+1)]/_scale);
  }
  int32_t  *ptr = (int32_t*)(block.ptr() + block.total_bytes()) - 2;
  _outfile << " " << ptr[0] << " " << ptr[1];
  _outfile << std::endl;
  return false;
}


int main(int argc, char** argv)
{
  // We read always spectra from an input PSR DADA ringbuffer
  key_t   input_key;
  // Spectra size, binning mode and channel selection
  int     spectra_size      = 65536;
  int     binning_mode      = 16;
  int     selected_channels = 10;
  float   scale             = 1.0;
  // We always store the result in a file
  std::string file_name = "channels.ascii";

  // program option declaration
  namespace po = boost::program_options;
  po::options_description desc("Options");
  desc.add_options()("help,h", "Print help messages");
  desc.add_options()(
		     "input_key,i",
		     po::value<std::string>()->default_value("dada")->notifier([&input_key](std::string in) { input_key = psrdada_cpp::string_to_key(in); }),
		     "The shared memory key for the dada buffer to connect to (hex string)");
  desc.add_options()("spectra_size,n", po::value<int>(&spectra_size)->default_value(spectra_size),
		     "The number of channels in the input spectra");
  desc.add_options()("binning_mode,b", po::value<int>(&binning_mode)->default_value(binning_mode),
		     "The number of input channels which are binned into an output channel");
  desc.add_options()("selected_channels,c", po::value<int>(&selected_channels)->required(),
		     "The number of output channels which go into the output file");
  desc.add_options()("scale,s", po::value<float>(&scale)->default_value(scale),
		     "An optional scale factor for rescaling the selected channels (avoid huge numbers)");
  desc.add_options()("output_file,o", po::value<std::string>(&file_name)->default_value(file_name),
		     "The name of the output file to write the selected channels into");
  po::variables_map vm;
  try {
    po::store(po::parse_command_line(argc, argv, desc), vm);
    if (vm.count("help")) {
      std::cout << "Read PFB spectra from an input DADA ringbuffer, applies binning and store selected channels in a file."
                << std::endl
                << desc << std::endl;
      return SUCCESS;
    }
    po::notify(vm);
  } catch (po::error &e) {
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
    std::cerr << desc << std::endl;
    return ERROR_IN_COMMAND_LINE;
  }
  psrdada_cpp::MultiLog log("ALLAN");
  ExtractSpectrumChannels sink(file_name, spectra_size, binning_mode, selected_channels, scale);
  psrdada_cpp::DadaInputStream<decltype(sink)> istream(input_key, log, sink);
  istream.start();

  return SUCCESS;
}
