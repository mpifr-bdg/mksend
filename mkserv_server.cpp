/* gcc -c mkserv_server.cpp */
#include <stdio.h>
#include <iostream>     // std::cout
#include <string.h>
#include <math.h>

#include "mkserv_logging.h"
#include "mkserv_socket.h"
#include "mkserv_message.h"
#include "mkserv_server.h"


namespace mkserv {

  std::mutex  server::graw_mutex;
  int32_t     server::graw_channels = 0;
  float       server::graw_spectrum[RAW_SPECTRUM_SIZE];
  int32_t     server::graw_nheaps = 0;
  int32_t     server::graw_nsaturated = 0;
  int8_t      server::graw_samples_data[MAX_NFEEDS*MAX_NBLOCKS*RAW_SAMPLES_SIZE];
  uint64_t    server::graw_samples_scis[MAX_NFEEDS*MAX_NBLOCKS*MAX_NSCIS];
  int32_t     server::graw_samples_block[MAX_NFEEDS];

  server::server(int cfd) :
    _sock(cfd),
    _err_rep(&_msg_hdr),
    _ping_req(&_msg_hdr),
    _ping_rep(&_msg_hdr),
    _feat_req(&_msg_hdr),
    _feat_rep(&_msg_hdr),
    _spec_req(&_msg_hdr),
    _spec_rep(&_msg_hdr),
    _samp_req(&_msg_hdr),
    _samp_rep(&_msg_hdr)
  {
  }

  void server::handle_error(int32_t err_code, int32_t err_flags)
  {
    _err_rep.create(err_code, err_flags);
    _err_rep.send_message(_sock);
  }

  void server::handle_exit()
  {
    _msg_hdr.create(EXIT_TAG, REPLY_CODE, 0);
    _msg_hdr.send_header(_sock);
  }

  void server::handle_ping()
  {
    _ping_rep.create();
    _ping_rep.send_message(_sock);
  }

  void server::handle_features()
  {
    if (!_feat_req.receive_body(_sock)) {
      _err_rep.create(1, 0);
      _err_rep.send_message(_sock);
      return;
    }
    int16_t mode     = _feat_req.mode();
    std::cout << "   features: " << mode << std::endl;
    _feat_rep.create();
    _feat_rep.send_message(_sock);
  }

  void server::handle_spectrum()
  {
    if (!_spec_req.receive_body(_sock)) {
      _err_rep.create(2, 0);
      _err_rep.send_message(_sock);
      return;
    }
    float   frequency = _spec_req.frequency();
    int32_t count     = _spec_req.count();
    int32_t binning   = _spec_req.binning();
    map_spectrum(1350.0, frequency, count, binning);
    std::cout << "   spectrum: " << frequency << " " << _count << " " << _binning << " " << _freq_low << " " << _freq_high << std::endl;
    _spec_rep.create(_nheaps, _nsaturated, _freq_low, _freq_high, _count, _binning);
    for (int32_t i = 0; i < _count; i++) _spec_rep.spectrum(i) = _spectrum[i];
    _spec_rep.send_message(_sock);
  }

  void server::handle_samples()
  {
    if (!_samp_req.receive_body(_sock)) {
      _err_rep.create(3, 0);
      _err_rep.send_message(_sock);
      return;
    }
    int16_t mode     = _samp_req.mode();
    int32_t feed     = _samp_req.feed();
    if (map_samples(mode, feed)) {
      std::cout << "   samples: " << mode << " " << _feed << " " << _block << " " << _feat_rep.nsamples() << " " << _feat_rep.nscis() << std::endl;
      _samp_rep.create(_feed, _block, _feat_rep.nsamples(), _feat_rep.nscis());
      for (int32_t i = 0; i < _feat_rep.nsamples(); i++) _samp_rep.samples(i) = _samples[i];
      for (int32_t i = 0; i < _feat_rep.nscis();    i++) _samp_rep.scis(i)    = _scis[i];
    } else {
      _samp_rep.create(feed, 0, 0, 0);
    }
    _samp_rep.send_message(_sock);
  }

  void server::process_requests()
  {
    std::cout << "server::process_requests()" << std::endl;
    while (true) {
      if (!_msg_hdr.receive_header(_sock)) break;
      std::cout << "  receive: " << _msg_hdr.tag() << _msg_hdr.code() << " " << _msg_hdr.payload() << std::endl;
      switch (_msg_hdr.tag()) {
      case EXIT_TAG:     handle_exit();     return;
      case PING_TAG:     handle_ping();     break;
      case FEATURES_TAG: handle_features(); break;
      case SPECTRUM_TAG: handle_spectrum(); break;
      case SAMPLES_TAG:  handle_samples();  break;
      default:
	handle_error(1, 0);
	return;
      }
    }
  }

  void server::raw_spectrum(int8_t *data, int32_t channels)
  {
    std::unique_lock<std::mutex> lck(graw_mutex);
    std::cout << "raw(.., " << channels << ")" << std::endl;
    float *sdata = (float*)data;
    float *pdata = graw_spectrum;
    for (int32_t i = 0; i < channels; i++) {
      *pdata++ = *sdata++ + *sdata++; // add up real and imaginary numbers
    }
    graw_channels = channels;
    int32_t *iptr = (int32_t*)sdata;
    graw_nheaps     = *iptr++;
    graw_nsaturated = *iptr++;
  }

  void server::map_spectrum(float bandwidth, float frequency, int32_t count, int32_t binning)
  {
  int     channels;  // [N]
  double  channelwidth;
  int     start;
  int     i, j;

  _count   = count;
  _binning = binning;
  channels = graw_channels;
  channelwidth = bandwidth/(double)channels;
  // 1. Es wird der Wert fuer das Binning auf den maximal moeglichen Wert (channels/count) korrigiert
  if (channels/_count < _binning) _binning = channels/_count;
  if (binning < 1) binning = 1;
  // 2. Es wird die Anzahl Ergebniswerten auf die maximal moegliche Anzahl beschraenkt
  if (_count*_binning > channels) _count = channels/_binning;
  if (_count < 1) _count = 1;
  // 3. Es wird sichergestellt, dass das Fenster innerhalb des Rohspektrums liegt
  start = (int)floor(frequency/channelwidth) - _count/2*_binning;
  if (start < 0) start = 0;
  if ((start + _count*_binning - 1) > (channels - 1)) start = channels - 1 - _count*_binning;
  /*
  printf("monMapSpectrum(): raw = %f MHz, %d channels, filter = %f MHz, %d count, %d binning\n",
	 bandwidth, channels, request->frequency, request->count, request->binning);
  printf("  -> filter = %d count, %d binning, %d start\n",
	 count, binning, start);
  */
  std::cout << "map(): count=" << _count << " binning=" << _binning << std::endl;
  // Nun wird das Rohspektrum auf das Ergebnis gemapped
  {
    std::unique_lock<std::mutex> lck(graw_mutex);
    for (i = 0; i < _count; i++) {
      double sum = 0.0;
      double freq = 0.0;
      for (j = 0; j < _binning; j++) {
	//printf("raw[%d] = %f\n", first_sample, raw->dat[first_sample]);
	sum   += graw_spectrum[start];
	freq  += channelwidth*(double)start;
	start++;
      }
      if (i == 0) {
	_freq_low = freq/(double)_binning;
      } else if (i == (count - 1)) {
	_freq_high = freq/(double)_binning;
      }
      _spectrum[i] = sum/(double)_binning;
    }
    _nheaps     = graw_nheaps;
    _nsaturated = graw_nsaturated;
  }
  /*
  printf("  -> freq = [%f MHz .. %f MHz]\n",
	 freq_low, freq_high);
  */
    
  }

  void server::raw_samples(int8_t *data, size_t data_length, size_t sci_offset, size_t sci_length)
  {
    std::unique_lock<std::mutex> lck(graw_mutex);
    std::cout << "samples(.., " << data_length << "," << sci_offset << "," << sci_length << ")" << std::endl;
    memcpy(graw_samples_data, data, data_length);
    memcpy(graw_samples_scis, data + sci_offset, sci_length);
    for (int i = 0; i < MAX_NFEEDS; i++) graw_samples_block[i] = 0;
  }

  bool server::map_samples(int16_t mode, int32_t feed)
  {
    // get the requested samples and scis from the raw data (blocking access using a mutex)
    std::unique_lock<std::mutex> lck(graw_mutex);
    if (mode == CONTINUOUS_MODE) {
      // check that we have a block of samples which were not transferred yet
      if (graw_samples_block[feed] == _feat_rep.nblocks()) return false;
    }
    size_t    nwords = _feat_rep.nsamples()*_feat_rep.nbits()/64;
    uint64_t *buffer = (uint64_t*)graw_samples_data + (_feat_rep.nfeeds()*graw_samples_block[feed] + feed)*nwords;
    int16_t  *dest   = _samples;
    size_t    iWord  = 0;
    uint64_t  val64, rest64;
    if (_feat_rep.nbits() == 8) {
      while (iWord < nwords) {
	val64 = be64toh(buffer[iWord++]);
	*dest++ = (int16_t)((int64_t)((0xFF00000000000000 & val64) <<  0) >> 56);
	*dest++ = (int16_t)((int64_t)((0x00FF000000000000 & val64) <<  8) >> 56);
	*dest++ = (int16_t)((int64_t)((0x0000FF0000000000 & val64) << 16) >> 56);
	*dest++ = (int16_t)((int64_t)((0x000000FF00000000 & val64) << 24) >> 56);
	*dest++ = (int16_t)((int64_t)((0x00000000FF000000 & val64) << 32) >> 56);
	*dest++ = (int16_t)((int64_t)((0x0000000000FF0000 & val64) << 40) >> 56);
	*dest++ = (int16_t)((int64_t)((0x000000000000FF00 & val64) << 48) >> 56);
	*dest++ = (int16_t)((int64_t)((0x00000000000000FF & val64) << 56) >> 56);
      }
    } else if (_feat_rep.nbits() == 10) {
      while (iWord < nwords) {
	// 1st: 64-bit word -> 6 samples and 4 bits rest
	val64 = be64toh(buffer[iWord++]);
	*dest++ = (int16_t)((int64_t)(( 0xFFC0000000000000 & val64) <<  0) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x003FF00000000000 & val64) << 10) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x00000FFC00000000 & val64) << 20) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x00000003FF000000 & val64) << 30) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x0000000000FFC000 & val64) << 40) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x0000000000003FF0 & val64) << 50) >> 54);
	rest64 =                      ( 0x000000000000000F & val64) << 60; // 4 bits rest.
	// 2nd: 64-bit word -> 6 samples and 8 bits rest
	val64 = be64toh(buffer[iWord++]);
	*dest++ = (int16_t)((int64_t)(((0xFC00000000000000 & val64) >> 4) | rest64) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x03FF000000000000 & val64) <<  6) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x0000FFC000000000 & val64) << 16) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x0000003FF0000000 & val64) << 26) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x000000000FFC0000 & val64) << 36) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x000000000003FF00 & val64) << 46) >> 54);
	rest64 =                      ( 0x00000000000000FF & val64) << 56; // 8 bits rest.    
	// 3rd: 64-bit word -> 7 samples and 2 bits rest
	val64 = be64toh(buffer[iWord++]);
	*dest++ = (int16_t)((int64_t)(((0xC000000000000000 & val64) >> 8) | rest64) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x3FF0000000000000 & val64) <<  2) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x000FFC0000000000 & val64) << 12) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x000003FF00000000 & val64) << 22) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x00000000FFC00000 & val64) << 32) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x00000000003FF000 & val64) << 42) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x0000000000000FFC & val64) << 52) >> 54);
	rest64 =                      ( 0x0000000000000003 & val64) << 62; // 2 bits rest.
	// 4th: 64-bit word -> 6 samples and 6 bits rest
	val64 = be64toh(buffer[iWord++]);
	*dest++ = (int16_t)((int64_t)(((0xFF00000000000000 & val64) >> 2) | rest64) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x00FFC00000000000 & val64) <<  8) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x00003FF000000000 & val64) << 18) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x0000000FFC000000 & val64) << 28) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x0000000003FF0000 & val64) << 38) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x000000000000FFC0 & val64) << 48) >> 54);
	rest64 =                      ( 0x000000000000003F & val64) << 58; // 6 bits rest.
	// 5th: 64-bit word -> 7 samples and no rest
	val64 = be64toh(buffer[iWord++]);
	*dest++ = (int16_t)((int64_t)(((0xF000000000000000 & val64) >> 6) | rest64) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x0FFC000000000000 & val64) <<  4) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x0003FF0000000000 & val64) << 14) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x000000FFC0000000 & val64) << 24) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x000000003FF00000 & val64) << 34) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x00000000000FFC00 & val64) << 44) >> 54);
	*dest++ = (int16_t)((int64_t)(( 0x00000000000003FF & val64) << 54) >> 54);
      }
    } else if (_feat_rep.nbits() == 12) {
      while (iWord < nwords) {
	// 1st: 64-bit word -> 5 samples and 4 bits rest
	val64 = be64toh(buffer[iWord++]);
	*dest++ = (int16_t)((int64_t)(( 0xFFF0000000000000 & val64) <<  0) >> 52);
	*dest++ = (int16_t)((int64_t)(( 0x000FFF0000000000 & val64) << 12) >> 52);
	*dest++ = (int16_t)((int64_t)(( 0x000000FFF0000000 & val64) << 24) >> 52);
	*dest++ = (int16_t)((int64_t)(( 0x000000000FFF0000 & val64) << 36) >> 52);
	*dest++ = (int16_t)((int64_t)(( 0x000000000000FFF0 & val64) << 48) >> 52);
	rest64 =                      ( 0x000000000000000F & val64) << 60; // 4 bits rest.
	// 2nd: 64-bit word -> 5 samples and 8 bits rest
	val64 = be64toh(buffer[iWord++]);
	*dest++ = (int16_t)((int64_t)(((0xFF00000000000000 & val64) >> 4) | rest64) >> 52);
	*dest++ = (int16_t)((int64_t)(( 0x00FFF00000000000 & val64) <<  8) >> 52);
	*dest++ = (int16_t)((int64_t)(( 0x00000FFF00000000 & val64) << 20) >> 52);
	*dest++ = (int16_t)((int64_t)(( 0x00000000FFF00000 & val64) << 32) >> 52);
	*dest++ = (int16_t)((int64_t)(( 0x00000000000FFF00 & val64) << 44) >> 52);
	rest64 =                      ( 0x00000000000000FF & val64) << 56; // 8 bits rest.
	// 3rd: 64-bit word -> 6 samples and no rest
	val64 = be64toh(buffer[iWord++]);
	*dest++ = (int16_t)((int64_t)(((0xF000000000000000 & val64) >> 8) | rest64) >> 52);
	*dest++ = (int16_t)((int64_t)(( 0x0FFF000000000000 & val64) <<  4) >> 52);
	*dest++ = (int16_t)((int64_t)(( 0x0000FFF000000000 & val64) << 16) >> 52);
	*dest++ = (int16_t)((int64_t)(( 0x0000000FFF000000 & val64) << 28) >> 52);
	*dest++ = (int16_t)((int64_t)(( 0x0000000000FFF000 & val64) << 40) >> 52);
	*dest++ = (int16_t)((int64_t)(( 0x0000000000000FFF & val64) << 52) >> 52);
      }
    } else {
      std::cerr << "ERROR: cannot handle " << _feat_rep.nbits() << " bits per sample" << std::endl;
      return false;
    }
    uint64_t *scis = graw_samples_scis + (_feat_rep.nfeeds()*graw_samples_block[feed] + feed)*_feat_rep.nscis();
    _feed  = feed;
    _block = graw_samples_block[feed];
    for (int32_t i = 0; i < _feat_rep.nscis(); i++) _scis[i] = *scis++;
    if (mode == CONTINUOUS_MODE) graw_samples_block[feed]++;
    return true;
  }

}
