#ifndef mksend_heap_h
#define mksend_heap_h

#include <iostream>
#include <cstdint>
#include <cstdlib>
#include <vector>

#include <spead2/common_endian.h>
#include <spead2/common_thread_pool.h>
#include <spead2/common_defines.h>
#include <spead2/common_flavour.h>
#include <spead2/send_heap.h>

namespace mksend
{
  
  class heap
  {
  protected:
    const spead2::flavour             &_flavour;
    static spead2::s_item_pointer_t    _mask;
    spead2::send::heap                *_heap = NULL;
    int                                _nbitems = 0;
    spead2::s_item_pointer_t           _ids[32];
    spead2::send::heap::item_handle    _handles[32];
  public:
    heap(const spead2::flavour &f);
    ~heap();
    inline spead2::send::heap &get_heap() { return *_heap; };
    void set_item(spead2::s_item_pointer_t id, spead2::s_item_pointer_t value);
    void set_item(spead2::s_item_pointer_t id, const std::uint8_t  *ptr, std::size_t length);
  protected:
    void create_heap();
  };

}


#endif /* mksend_heap_h */
