/* checks the slots on a PSRDADA ringbuffer about missing packets (needs side-channel-items). */

#include <iostream>
#include <fstream>
#include <iomanip>
#include "boost/program_options.hpp"
#include <ctime>
#include <memory>

#include "psrdada_cpp/cli_utils.hpp"
#include "psrdada_cpp/multilog.hpp"
#include "psrdada_cpp/simple_file_writer.hpp"
#include "psrdada_cpp/dada_input_stream.hpp"
#include "psrdada_cpp/dada_output_stream.hpp"
#include "psrdada_cpp/dada_client_base.hpp"
#include "psrdada_cpp/dada_null_sink.hpp"
#include "psrdada_cpp/common.hpp"

const size_t SUCCESS                   = 0;
const size_t ERROR_IN_COMMAND_LINE     = 1;
const size_t ERROR_UNHANDLED_EXCEPTION = 2;

class CheckMissingPackets
{
public:
  explicit CheckMissingPackets(std::string filename, int packet_count, int sci_count);
  CheckMissingPackets(CheckMissingPackets const&) = delete;
  ~CheckMissingPackets();
  void init(psrdada_cpp::RawBytes&);
  bool operator()(psrdada_cpp::RawBytes&);
private:
  std::ofstream _outfile;
  int           _packet_count;
  int           _sci_count;
  int           _slot_index;
};

CheckMissingPackets::CheckMissingPackets(std::string filename, int packet_count, int sci_count)
{
  _outfile.open(filename.c_str(),std::ifstream::out);
  if (_outfile.is_open()) {
    BOOST_LOG_TRIVIAL(debug) << "Opened file " << filename;
  } else {
    std::stringstream stream;
    stream << "Could not open file " << filename;
    throw std::runtime_error(stream.str().c_str());
  }
  _packet_count = packet_count;
  _sci_count    = sci_count;
  _slot_index   = 0;
}

CheckMissingPackets::~CheckMissingPackets()
{
  _outfile.close();
}

void CheckMissingPackets::init(psrdada_cpp::RawBytes& block)
{
  // do nothing
}

bool CheckMissingPackets::operator()(psrdada_cpp::RawBytes& block)
{
  // output the slot number
  _outfile << _slot_index;
  if (_slot_index < 20) {
    _outfile << std::endl;
    _slot_index++;
    return false;
  }
  uint64_t  *sci = (uint64_t*)(block.ptr() + block.total_bytes()) - _packet_count*_sci_count;
  for (int i = 0; i < _packet_count; i++) {
    if (sci[i*_sci_count] & (1uL << 63)) {
      int first = i;
      int last = i;
      for (int j = i+1; j < _packet_count; j++) {
	if (sci[j*_sci_count] & (1uL << 63)) {
	  last = j;
	} else {
	  break;
	}
      }
      if (first == last) {
	_outfile << " " << first;
      } else {
	_outfile << " " << first << "-" << last;
      }
      i = last;
    }
  }
  _outfile << std::endl;
  _slot_index++;
  return false;
}


int main(int argc, char** argv)
{
  // We read always packet from an input PSR DADA ringbuffer
  key_t   input_key;
  // Number of packets in a slot and number of side-channel-items per packet
  int     packet_count  = 65536;
  int     sci_count     = 1;
  // We always store the result in a file
  std::string file_name = "missing.txt";

  // program option declaration
  namespace po = boost::program_options;
  po::options_description desc("Options");
  desc.add_options()("help,h", "Print help messages");
  desc.add_options()(
		     "input_key,i",
		     po::value<std::string>()->default_value("dada")->notifier([&input_key](std::string in) { input_key = psrdada_cpp::string_to_key(in); }),
		     "The shared memory key for the dada buffer to connect to (hex string)");
  desc.add_options()("packet_count,n", po::value<int>(&packet_count)->default_value(packet_count),
		     "The number of packets in a slot");
  desc.add_options()("sci_count,s", po::value<int>(&sci_count)->default_value(sci_count),
		     "The number of side-channel-items per packet");
  desc.add_options()("output_file,o", po::value<std::string>(&file_name)->default_value(file_name),
		     "The name of the output file to write the selected channels into");
  po::variables_map vm;
  try {
    po::store(po::parse_command_line(argc, argv, desc), vm);
    if (vm.count("help")) {
      std::cout << "Read slots with raw data input DADA ringbuffer, detect and report the missing packets."
                << std::endl
                << desc << std::endl;
      return SUCCESS;
    }
    po::notify(vm);
  } catch (po::error &e) {
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
    std::cerr << desc << std::endl;
    return ERROR_IN_COMMAND_LINE;
  }
  psrdada_cpp::MultiLog log("ALLAN");
  CheckMissingPackets sink(file_name, packet_count, sci_count);
  psrdada_cpp::DadaInputStream<decltype(sink)> istream(input_key, log, sink);
  istream.start();

  return SUCCESS;
}
