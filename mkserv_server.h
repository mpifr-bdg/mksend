#ifndef mkserv_server_h
#define mkserv_server_h

#include <atomic>
#include <mutex>

#include "mkserv_logging.h"
#include "mkserv_socket.h"
#include "mkserv_message.h"

namespace mkserv {


  class server {
  protected:
    socket             _sock;
    message_header     _msg_hdr;
    error_reply        _err_rep;
    ping_request       _ping_req;
    ping_reply         _ping_rep;
    features_request   _feat_req;
    features_reply     _feat_rep;
    spectrum_request   _spec_req;
    spectrum_reply     _spec_rep;
    samples_request    _samp_req;
    samples_reply      _samp_rep;
    float              _freq_low;
    float              _freq_high;
    int32_t            _count;
    int32_t            _binning;
    float              _spectrum[RAW_SPECTRUM_SIZE];
    int32_t            _nheaps;
    int32_t            _nsaturated;
    int32_t            _feed;
    int32_t            _block;
    int16_t            _samples[4096];
    uint64_t           _scis[MAX_NSCIS];
    // guard mutex for raw data access
    static std::mutex  graw_mutex;
    // raw spectrum and counters
    static int32_t     graw_channels;
    static float       graw_spectrum[RAW_SPECTRUM_SIZE];
    static int32_t     graw_nheaps;
    static int32_t     graw_nsaturated;
    // raw samples and side-channel-items
    static int8_t      graw_samples_data[MAX_NFEEDS*MAX_NBLOCKS*RAW_SAMPLES_SIZE];
    static uint64_t    graw_samples_scis[MAX_NFEEDS*MAX_NBLOCKS*MAX_NSCIS];
    static int32_t     graw_samples_block[MAX_NFEEDS];
  public:
    server(int cfd);
    inline void     mode(int16_t m)      { _feat_rep.mode(m);      };
    inline void     nspectra(int32_t n)  { _feat_rep.nspectra(n);  };
    inline void     nchannels(int32_t n) { _feat_rep.nchannels(n); };
    inline void     ncounters(int32_t n) { _feat_rep.ncounters(n); };
    inline void     bandwidth(float b)   { _feat_rep.bandwidth(b); };
    inline void     nfeeds(int32_t n)    { _feat_rep.nfeeds(n);    };
    inline void     nblocks(int32_t n)   { _feat_rep.nblocks(n);   };
    inline void     nsamples(int32_t n)  { _feat_rep.nsamples(n);  };
    inline void     nbits(int32_t n)     { _feat_rep.nbits(n);     };
    inline void     nscis(int32_t n)     { _feat_rep.nscis(n);     };
    inline int16_t  mode()      { return _feat_rep.mode();      };
    inline int32_t  nspectra()  { return _feat_rep.nspectra();  };
    inline int32_t  nchannels() { return _feat_rep.nchannels(); };
    inline int32_t  ncounters() { return _feat_rep.ncounters(); };
    inline float    bandwidth() { return _feat_rep.bandwidth(); };
    inline int32_t  nfeeds()    { return _feat_rep.nfeeds();    };
    inline int32_t  nblocks()   { return _feat_rep.nblocks();   };
    inline int32_t  nsamples()  { return _feat_rep.nsamples();  };
    inline int32_t  nbits()     { return _feat_rep.nbits();     };
    inline int32_t  nscis()     { return _feat_rep.nscis();     };
    void process_requests();
    static void raw_spectrum(int8_t *data, int32_t channels);
    void map_spectrum(float bandwidth, float frequency, int32_t count, int32_t binning);
    static void raw_samples(int8_t *data, size_t data_length, size_t sci_offset, size_t sci_length);
    bool map_samples(int16_t mode, int32_t feed);
  protected:
    void handle_error(int32_t err_code, int32_t err_flags);
    void handle_exit();
    void handle_ping();
    void handle_features();
    void handle_spectrum();
    void handle_samples();
  };

}

#endif /* mkserv_server_h */
