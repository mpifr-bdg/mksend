/* gcc -c mkserv_features.cpp */
/* gcc -o mkfeatures mkserv_features.cpp mkserv_message.cpp mkserv_socket.cpp mkserv_logging.cpp -lstdc++ -lpthread */
/* ./mkfeatures -h localhost -p 6000 -f 0 */

#include <stdio.h>
#include <string.h>
#include <iostream>     // std::cout

#include <thread>
#include <atomic>
#include <vector>

#include "mkserv_logging.h"
#include "mkserv_message.h"

//#define SIMULATION
#define RINGBUFFER

#ifdef RINGBUFFER
#include "boost/program_options.hpp"
#include "boost/lexical_cast.hpp"

namespace po = boost::program_options;

const size_t SUCCESS = 0;
const size_t ERROR_IN_COMMAND_LINE = 1;
const size_t ERROR_UNHANDLED_EXCEPTION = 2;

template<typename T>
static po::typed_value<T> *make_opt(T &var)
{
  return po::value<T>(&var);
}

#endif /* RINGBUFFER */

typedef struct {
  std::string  host;
  int          port;         // server port (default is mkserv::PORT)
  int          mode;
} setup_t;

static void send_features(mkserv::socket &sock, setup_t *setup)
{
  mkserv::message_header      msg_hdr;
  mkserv::features_request    feat_req(&msg_hdr);
  mkserv::features_reply      feat_rep(&msg_hdr);

  feat_req.create(setup->mode);
  if (!feat_req.send_message(sock)) exit(2);
  if (!msg_hdr.receive_header(sock)) exit(3);
  if (msg_hdr.tag() == mkserv::ERROR_TAG) {
    mkserv::error_reply  err_rep(&msg_hdr);
    if (!err_rep.receive_body(sock)) exit(4);
    std::cerr << "ERROR: cannot send a FEATURES message, received an ERROR reply: " << err_rep.err_code() << ":" << err_rep.err_flags() << std::endl;
    return;
  }
  if (!feat_rep.receive_body(sock)) exit(5);
  std::cout << "reply: " << msg_hdr.tag() << msg_hdr.code() << " " << msg_hdr.payload() << std::endl;
  std::cout << "mode: " << feat_rep.mode() << std::endl;
  std::cout << "nspectra: " << feat_rep.nspectra() << std::endl;
  std::cout << "nchannels: " << feat_rep.nchannels() << std::endl;
  std::cout << "ncounters: " << feat_rep.ncounters() << std::endl;
  std::cout << "bandwidth: " << feat_rep.bandwidth() << std::endl;
  std::cout << "nfeed:s " << feat_rep.nfeeds() << std::endl;
  std::cout << "nblocks: " << feat_rep.nblocks() << std::endl;
  std::cout << "nsamples: " << feat_rep.nsamples() << std::endl;
  std::cout << "nbits: " << feat_rep.nbits() << std::endl;
  std::cout << "nscis: " << feat_rep.nscis() << std::endl;
}

static void send_exit(mkserv::socket &sock)
{
  mkserv::message_header      msg_hdr;

  msg_hdr.create(mkserv::EXIT_TAG, mkserv::REQUEST_CODE, 0);
  if (!msg_hdr.send_header(sock)) exit(2);
  if (!msg_hdr.receive_header(sock)) exit(3);
  std::cout << "reply: " << msg_hdr.tag() << msg_hdr.code() << " " << msg_hdr.payload() << std::endl;
}

int main(int argc, char *argv[])
{
  mkserv::set_application("mkfeatures");

  static setup_t    setup;
  mkserv::socket    sock;

  setup.host = "localhost";
  setup.port = mkserv::PORT;
  setup.mode = mkserv::DUPLICATES_MODE;

#ifdef RINGBUFFER
  // program option declaration
  namespace po = boost::program_options;
  po::options_description desc("Options");
  desc.add_options()("help", "Print help messages");
  desc.add_options()("host", make_opt(setup.host), "The server host");
  desc.add_options()("port", make_opt(setup.port), "The server port");
  po::variables_map vm;
  try {
    po::store(po::parse_command_line(argc, argv, desc), vm);
    if (vm.count("help")) {
      std::cout << "Read raw features from a server." << std::endl << desc << std::endl;
      return SUCCESS;
    }
    po::notify(vm);
  } catch (po::error &e) {
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
    std::cerr << desc << std::endl;
    return ERROR_IN_COMMAND_LINE;
  }
#endif /* RINGBUFFER */

  if (!sock.connect(setup.host.c_str(), setup.port)) exit(1);
  std::cout << "sock fd = " << sock.cfd() << std::endl;
  send_features(sock, &setup);
  send_exit(sock);
  sock.finish_client();
}
