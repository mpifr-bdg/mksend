# project name and specify the minumum cmake version.
project (mksend CXX)
cmake_minimum_required (VERSION 3.5)

# @mhein3:
# cmake -DCMAKE_BUILD_TYPE=DEBUG -DCMAKE_INSTALL_PREFIX=~ .
# cmake -D CMAKE_INSTALL_PREFIX=~ .

# The version number.
set (mksend_VERSION_MAJOR 0)
set (mksend_VERSION_MINOR 1)

# cmake setup.
list(INSERT CMAKE_MODULE_PATH 0 ${CMAKE_SOURCE_DIR}/cmake)

# default installtion locations (note these will be prefixed by ${CMAKE_INSTALL_PREFIX})
if(NOT LIBRARY_INSTALL_DIR)
    set(LIBRARY_INSTALL_DIR "lib")
endif(NOT LIBRARY_INSTALL_DIR)

if(NOT INCLUDE_INSTALL_DIR)
    set(INCLUDE_INSTALL_DIR "include/${CMAKE_PROJECT_NAME}")
endif(NOT INCLUDE_INSTALL_DIR)

if(NOT MODULES_INSTALL_DIR)
    set (MODULES_INSTALL_DIR "share/${CMAKE_PROJECT_NAME}")
endif(NOT MODULES_INSTALL_DIR)

if(NOT BINARY_INSTALL_DIR)
    set(BINARY_INSTALL_DIR "bin/")
endif(NOT BINARY_INSTALL_DIR)

# get project dependencies and compiler settings.
include(dependencies)
find_package(CUDA)

# define the cheetah libraries
set(MKSEND_LIBRARIES ${DEPENDENCY_LIBRARIES})

include_directories(..)
include_directories(${PROJECT_BINARY_DIR})

set(mksend_src
  mksend_main.cpp
  mksend_sender.cpp
  mksend_options.cpp
  mksend_ringbuffer.cpp
  mksend_heap.cpp
  mksend_stream.cpp
#  mksend_allocator.cpp
#  mksend_destination.cpp
#  mksend_stream.cpp
)

set(mkserv_src
  mkserv_main.cpp
  mkserv_logging.cpp
  mkserv_socket.cpp
  mkserv_message.cpp
  mkserv_server.cpp
)

set(mkping_src
  mkserv_ping.cpp
  mkserv_logging.cpp
  mkserv_socket.cpp
  mkserv_message.cpp
  mkserv_server.cpp
)

set(mkspectrum_src
  mkserv_spectrum.cpp
  mkserv_logging.cpp
  mkserv_socket.cpp
  mkserv_message.cpp
  mkserv_server.cpp
)

set(mkfeatures_src
  mkserv_features.cpp
  mkserv_logging.cpp
  mkserv_socket.cpp
  mkserv_message.cpp
  mkserv_server.cpp
)

set(mksamples_src
  mkserv_samples.cpp
  mkserv_logging.cpp
  mkserv_socket.cpp
  mkserv_message.cpp
  mkserv_server.cpp
)


#set(mksend_inc
#)

# === Print build options summary.
set(DEBUG_PRINT ON)
if (DEBUG_PRINT)
    message(STATUS "")
    message(STATUS "****************************************************************************")
    message(STATUS " name: ${CMAKE_PROJECT_NAME} version: ${PROJECT_VERSION}")
    message(STATUS "")
    message(STATUS "")
    message(STATUS " Compiler Options:")
    message(STATUS "  Build type: ${CMAKE_BUILD_TYPE}")
    message(STATUS "  C++ Compiler: ${CMAKE_CXX_COMPILER}")
    if (${CMAKE_BUILD_TYPE} MATCHES release)
        message(STATUS "  C++ flags: ${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_RELEASE}")
    elseif (${CMAKE_BUILD_TYPE} MATCHES debug)
        message(STATUS "  C++ flags: ${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_DEBUG}")
    elseif (${CMAKE_BUILD_TYPE} MATCHES relwithdebinfo)
        message(STATUS "  C++ flags: ${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_RELWITHDEBINFO}")
    elseif (${CMAKE_BUILD_TYPE} MATCHES minsizerel)
        message(STATUS "  C++ flags: ${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_MINSIZEREL}")
    endif ()
    message(STATUS "")
    message(STATUS "Install locations: (make install)")
    message(STATUS "  Libraries: ${CMAKE_INSTALL_PREFIX}/lib")
    message(STATUS "  Inclues:   ${CMAKE_INSTALL_PREFIX}/${INCLUDE_INSTALL_DIR}")
    message(STATUS "  Binaries:  ${CMAKE_INSTALL_PREFIX}/${BINARY_INSTALL_DIR}")
    if(ENABLE_DOC)
        message(STATUS "  Doc:       ${CMAKE_INSTALL_PREFIX}/${DOC_INSTALL_DIR}")
    else(ENABLE_DOC)
        message(STATUS "  Doc:       Disabled (-DENABLE_DOC=true to enable)")
    endif(ENABLE_DOC)
    message(STATUS "  Other:     ${CMAKE_INSTALL_PREFIX}/${MODULES_INSTALL_DIR}")
    message(STATUS "****************************************************************************")
    message(STATUS "")
endif (DEBUG_PRINT)

#mksend
add_executable(mksend ${mksend_src})
target_link_libraries(mksend ${MKSEND_LIBRARIES})

#mkserv
add_executable(mkserv ${mkserv_src})
target_link_libraries(mkserv ${MKSEND_LIBRARIES})

#mkping
add_executable(mkping ${mkping_src})
target_link_libraries(mkping ${MKSEND_LIBRARIES})

#mkspektrum
add_executable(mkspectrum ${mkspectrum_src})
target_link_libraries(mkspectrum ${MKSEND_LIBRARIES})

#mksamples
add_executable(mksamples ${mksamples_src})
target_link_libraries(mksamples ${MKSEND_LIBRARIES})

#mkfeatures
add_executable(mkfeatures ${mkfeatures_src})
target_link_libraries(mkfeatures ${MKSEND_LIBRARIES})

add_executable(send_heaps send_heaps.cpp mksend_heap.cpp)
target_link_libraries(send_heaps ${MKSEND_LIBRARIES})

add_executable(save_spectrum save_spectrum.cpp)
target_link_libraries(save_spectrum ${MKSEND_LIBRARIES})

add_executable(check_slots check_slots.cpp)
target_link_libraries(check_slots ${MKSEND_LIBRARIES})

install (TARGETS mksend mkserv mkping mkspectrum mksamples mkfeatures send_heaps save_spectrum check_slots
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib)
#install (FILES MathFunctions.h DESTINATION include)

install(FILES ${mksend_inc} DESTINATION include/mksend)
