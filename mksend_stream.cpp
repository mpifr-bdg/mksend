
#include "mksend_stream.h"

namespace mksend
{

  stream::stream(std::unique_ptr<spead2::send::stream> stream, std::size_t nbheaps) : _stream(std::move(stream)), _nbheaps(nbheaps), _sem(nbheaps)
  {
  }

  stream::~stream()
  {
  }

  void stream::set_cnt_sequence(spead2::item_pointer_t next, spead2::item_pointer_t step)
  {
    _id_next = next;
    _id_step = step;
    if (_stream)
      {
	_stream->set_cnt_sequence(next, step);
      }
  }

  spead2::send::stream &stream::get_stream()
  {
    return *_stream;
  }

  void stream::create_heaps(const spead2::flavour &f)
  {
    std::size_t  i;

    for (i = 0; i < _nbheaps; i++)
      {
	_heaps.push_back(std::unique_ptr<mksend::heap>(new heap(f)));
	_used.push_back(false);
      }
    _head = 0;
  }

  std::size_t stream::get_free_heap()
  {
    std::size_t  i;

    _sem.wait();
    {
      std::lock_guard<std::mutex> lock(_mutex);
      for (i = 0; i < _nbheaps; i++)
	{
	  std::size_t nr = (_head + i)%_nbheaps;
	  if (!_used.at(nr))
	    {
	      _used.at(nr) = true;
	      _head = (nr + 1)%_nbheaps;
	      return nr;
	    }
	}
    }
    return NO_HEAP_AVAILABLE;
  }

  void stream::set_item(std::size_t nr, spead2::s_item_pointer_t id, spead2::s_item_pointer_t value)
  {
    _heaps.at(nr)->set_item(id, value);
  }

  void stream::set_item(std::size_t nr, spead2::s_item_pointer_t id, const std::uint8_t  *ptr, std::size_t length)
  {
    _heaps.at(nr)->set_item(id, ptr, length);
  }

  bool stream::send_heap(std::size_t nr)
  {
    if (_stream)
      {
	return _stream->async_send_heap(_heaps.at(nr)->get_heap(),
					[this, nr] (const boost::system::error_code &ec, spead2::item_pointer_t bytes_transferred)
					{
					  (void)bytes_transferred;
					  if (ec)
					    std::cout << ec.message() << std::endl;
					  //else
					  //  std::cout << "Sent " << bytes_transferred << " bytes in heap" << std::endl;
					  this->heap_finished(nr);
					});
      }
    else
      {
	//std::cout << "Sending heap with id " << _id_next << std::endl;
	_id_next += _id_step;
	heap_finished(nr);
      }
    return true;
  }

  void stream::heap_finished(std::size_t nr)
  {
    {
      std::lock_guard<std::mutex> lock(_mutex);
      _used.at(nr) = false;
    }
    _sem.post();
  }


  void stream::flush()
  {
    if (_stream)
      {
	_stream->flush();
      }
  }
}
