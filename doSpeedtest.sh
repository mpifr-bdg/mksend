#!/bin/bash

#
# ./mksend --quiet --header header_speedtest.cfg \
#          --dada-mode 0 --heap-group 1 \
#          --nslots 10 --ngroups-data $NGROUPS_DATA \
#          --rate $RATE \
#          --nheaps $NHEAPS \
#          --ibv-if 192.168.2.90 239.11.1.1+15 \
#          --nthreads $NTHREADS \
#          --ibv-vector -1 \
#          --burst-size BURST_SIZE \
#          --heap-size $HEAP_SIZE
# relevant options of mksend:
PACKET_SIZE=8800         #  --packet-size arg     Maximum packet size to use for UDP
#BUFFER_SIZE=8388608      #  --buffer-size arg     Socket buffer size
BUFFER_SIZE=16777216
NTHREADS=16              #  --nthreads arg        Number of worker threads
NHEAPS=36                #  --nheaps arg          Maximum number of active heaps
#  --memcpy-nt           Use non-temporal memcpy
#  --dada-mode arg       dada mode (0 = artificial data, 1 = data from dada ringbuffer)
#  --dada-key arg        PSRDADA ring buffer key
NGROUPS_DATA=131072      #  --ngroups-data arg    Number of groups (heaps with the same timestamp) going into the data space.
NSLOTS=100                #  --nslots arg          Number of slots send via network during simulation, 0 means infinite.
NETWORK_MODE=1           #  --network-mode arg    network mode (0 = no network, 1 = full network support)
IBV_IF=192.168.2.90      #  --ibv-if arg          Interface address for ibverbs
UDP_IF=${IBV_IF}
IBV_VECTOR=-1            #  --ibv-vector arg      Interrupt vector (-1 for polled)
IBV_MAX_POLL=10          #  --ibv-max-poll arg    Maximum number of times to poll in a row
#  --udp-if arg          UDP interface
PORT=7809                #  --port arg            Network port number
NHOPS=2                  #  --nhops arg           Maximum number of hops
RATE=40                  #  --rate arg            Network use rate
BURST_SIZE=131072        #  --burst-size arg      Size of a network burst [Bytes]
BURST_RATIO=1.05         #  --burst-ratio arg     I do not know what this means.
HEAP_SIZE=8192           #  --heap-size arg       The heap size used for checking incomming heaps.
HEAP_GROUP=1             #  --heap-group arg      number of consecutive heaps going into the same stream
NSCI=0                   #  --nsci arg            Number of item pointers in the side-channel
NITEMS=4                 #  --nitems arg          Number of item pointers in a SPEAD heap
SYNC_EPOCH=0.0           #  --sync-epoch arg      the ADC sync epoch
SAMPLE_CLOCK=1750000000.0 #  --sample-clock arg    virtual sample clock used for calculations
SAMPLE_START=0           #  --sample-start arg    first timestamp of the first SPEAD heap in the first



SLOT_SIZE=2147483648
GROUP_NHEAPS=2
MCAST_DEST="225.0.0.64"

HEAP_SIZE=8192
NHEAPS=128
RATE=1000
NTHREADS=16
BURST_SIZE=131072
NMCAST=16

IBVORUDP="I"
NRUNS="1"
NTHREADS_LIST="1 2 4 8 16"
NMCAST_LIST="1 2 4 8 16"
HEAP_SIZE_LIST="4096 8192 16384 32768 65536 131072 262144"
TOTAL_RATE=12800

while [[ "x$1" != "x" ]]
do
  case $1 in
    ("-nrun")
      shift
      NRUNS=$1
      shift
      ;;
    ("-nslot")
      shift
      NSLOTS=$1
      shift
      ;;
    ("-nthread")
      shift
      NTHREADS_LIST="$1"
      shift
      ;;
    ("-nmcast")
      shift
      NMCAST_LIST="$1"
      shift
      ;;
    ("-mcast")
      shift;
      MCAST_DEST="$1"
      shift
      ;;
    ("-heap-size")
      shift
      HEAP_SIZE_LIST="$1"
      shift
      ;;
    ("-rate")
      shift
      TOTAL_RATE="$1"
      shift
      ;;
    ("-udp")
      shift
      IBVORUDP="V"
      UDP_IF="$1"
      shift
      ;;
    ("-ibv")
      shift
      IBVORUDP="I"
      IBV_IF="$1"
      shift
      ;;
    (*)
      echo "unrecognized option $1"
      shift
      ;;
  esac
done


echo "heap size, packet size, burst size, buffer size, slot size, nthreads, nmcast, nslots, ngroups, nheaps, rate, burst ratio, open heaps, payload, tutal duration, total send, total flush"

function doSave () {
  OLD_PACKET_SIZE=$PACKET_SIZE
  OLD_BUFFER_SIZE=$BUFFER_SIZE
  OLD_NTHREADS=$NTHREADS
  OLD_NHEAPS=$NHEAPS
  OLD_NSLOTS=$NSLOTS
  OLD_RATE=$RATE
  OLD_BURST_SIZE=$BURST_SIZE
  OLD_BURST_RATIO=$BURST_RATIO
  OLD_HEAP_SIZE=$HEAP_SIZE
}

function doRestore () {
  PACKET_SIZE=$OLD_PACKET_SIZE
  BUFFER_SIZE=$OLD_BUFFER_SIZE
  NTHREADS=$OLD_NTHREADS
  NHEAPS=$OLD_NHEAPS
  NSLOTS=$OLD_NSLOTS
  RATE=$OLD_RATE
  BURST_SIZE=$OLD_BURST_SIZE
  BURST_RATIO=$OLD_BURST_RATIO
  HEAP_SIZE=$OLD_HEAP_SIZE
}

function doTest () {
  local NGROUPS_DATA=$(( $SLOT_SIZE / $HEAP_SIZE / $GROUP_NHEAPS ))
  local MAX_IP=$(( $NMCAST - 1 ))
  local BASIC_ARGS="--header header_speedtest.cfg --dada-mode 0 --heap-group 1 --nthreads $NTHREADS"
  local DATA_ARGS="--nheaps $NHEAPS --ngroups-data $NGROUPS_DATA --nslots $NSLOTS --heap-size $HEAP_SIZE --sample-start $SAMPLE_START --nsci $NSCI --nitems $NITEMS"
  local NETWORK_ARGS="--packet-size $PACKET_SIZE --buffer-size $BUFFER_SIZE --network-mode $NETWORK_MODE --port $PORT --nhops $NHOPS --rate ${RATE}000000 --burst-size $BURST_SIZE --burst-ratio $BURST_RATIO"
  if [[ $IBVORUDP == "I" ]]
  then
    NETWORK_ARGS="$NETWORK_ARGS --ibv-if $IBV_IF --ibv-vector $IBV_VECTOR --ibv-max-poll"
  else
    NETWORK_ARGS="$NETWORK_ARGS --udp-if $UDP_IF"
  fi
  echo "./mksend --quiet $BASIC_ARGS $DATA_ARGS $NETWORK_ARGS ${MCAST_DEST}+${MAX_IP}"
  ./mksend --quiet $BASIC_ARGS $DATA_ARGS $NETWORK_ARGS ${MCAST_DEST}+${MAX_IP} | tail -n 1 > test.log
  PAYLOAD=$( cat test.log | awk '{ print $5; }' )
  TOTAL_DURATION=$( cat test.log | awk '{ print $6; }' )
  TOTAL_SEND=$( cat test.log | awk '{ print $7; }' )
  TOTAL_FLUSH=$( cat test.log | awk '{ print $8; }' )
  echo "$HEAP_SIZE,$PACKET_SIZE,$BURST_SIZE,$BUFFER_SIZE,$SLOT_SIZE,$NTHREADS,$NMCAST,$NSLOTS,$NGROUPS_DATA,$GROUP_NHEAPS,$RATE,$BURST_RATIO,$NHEAPS,$PAYLOAD,$TOTAL_DURATION,$TOTAL_SEND,$TOTAL_FLUSH"
}

##############
# rate and heap size
#
#RATE_LIST="10 20 50 100 200 500 1000"
#RATE_LIST="200 300 400 500 600 700 750 800"
#HEAP_SIZE_LIST="4096 8192 16384 32768 65536 131072 262144 524288"
#doSave
#for RATE in $RATE_LIST
#do
#  for HEAP_SIZE in $HEAP_SIZE_LIST
#  do
#    doTest
#  done
#done
#doRestore

##############
# rate and multicast groups
#
#NMCAST_LIST="1 2 4 8 16"
#RATE_LIST="3200 2800 6400 8000 9600 11200 12000 12800"
#doSave
#for NMCAST in $NMCAST_LIST
#do
#  for HRATE in $RATE_LIST
#  do
#    RATE=$(( $HRATE / $NMCAST ))
#    doTest
#  done
#done
#doRestore

##############
# burst ratio
#
#NMCAST=1
#RATE=12500
#BURST_RATIO_LIST="1.05 1.2 1.5 1.7 2.0"
#for BURST_RATIO in $BURST_RATIO_LIST
#do
#  doTest
#done
#doRestore

##############
# number of threads and multicast groups
#
doSave
for NMCAST in $NMCAST_LIST
do
  RATE=$(( $TOTAL_RATE / $NMCAST ))
  for NTHREADS in $NTHREADS_LIST
  do
    if (( $NTHREADS > $NMCAST ))
    then
      continue
    fi
    for HEAP_SIZE in $HEAP_SIZE_LIST
    do
      for (( IRUN=0 ; $IRUN < $NRUNS ; IRUN++ ))
      do
        doTest
      done
    done
  done
done
doRestore

