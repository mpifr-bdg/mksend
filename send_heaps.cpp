#include <iostream>
#include <utility>
#include <boost/asio.hpp>
#include <spead2/common_endian.h>
#include <spead2/common_thread_pool.h>
#include <spead2/common_defines.h>
#include <spead2/common_flavour.h>
#include <spead2/send_heap.h>
#include <spead2/send_udp.h>
#include <spead2/send_stream.h>

#include "mksend_heap.h"

#define NTHREADS_DEF       8
#define PACKET_DEF         spead2::send::stream_config::default_max_packet_size
#define BUFFER_DEF         512*1024
#define RATE_DEF           0.0
#define NHEAPS_DEF         spead2::send::stream_config::default_max_heaps
#define UDP_IF_DEF         "10.10.1.10"
#define NHOPS_DEF          2
#define PORT_DEF           "7149"
#define DESTINATION_DEF    "226.2.1.150"
#define HEAP_SIZE_DEF      0x100000
#define NITEMS_DEF         4

using boost::asio::ip::udp;

typedef struct {
  // flags/control
  bool                      pyspead;
  int                       nthreads;
  // networking
  std::string               udp_if;
  std::string               port;
  std::string               destination;
  std::size_t               packet;
  std::size_t               buffer;
  double                    rate;
  std::size_t               nheaps;
  int                       nhops;
  // heaps
  std::size_t               nitems;
  std::size_t               heap_size;
  // heap contents
  spead2::s_item_pointer_t   timestamp;
  spead2::s_item_pointer_t   ts_step;
  spead2::s_item_pointer_t   items[32];
  char                      *payload;
} send_context;

static void usage(char *argv0)
{
  printf("usage: %s { <options> } <destination>\n", argv0);
  printf("  -pyspead\n");
  printf("     enable spead2::BUG_COMPAT_PYSPEAD_0_5_2\n");
  printf("  -nthreads <nthreads>\n");
  printf("     sets the number of threads in the threadpool [%d]\n", NTHREADS_DEF);
  printf("  -udp-if <ip>\n");
  printf("     sets the IP of the network interface [%s]\n", UDP_IF_DEF);
  printf("  -port <nr>\n");
  printf("     sets the port number [%s]\n", PORT_DEF);
  printf("  -packet <size>\n");
  printf("     sets the packet size (not payload size) [%ld]\n", PACKET_DEF);
  printf("  -buffer <size>\n");
  printf("     sets the buffer size [%d]\n", BUFFER_DEF);
  printf("  -rate <rate>\n");
  printf("     sets the number of bytes per second [%g]\n", RATE_DEF);
  printf("  -nheaps <count>\n");
  printf("     sets the number of heaps in the stream queue [%ld]\n", NHEAPS_DEF);
  printf("  -nhops <count>\n");
  printf("     sets the maximum number of multicast hops [%d]\n", NHOPS_DEF);
  printf("  -nitems <count>\n");
  printf("     sets the number of item pointer values per heap, first contains a timestamp, last the payload [%d]\n", NITEMS_DEF);
  printf("  -heap-size <size>\n");
  printf("     sets the payload size [%d]\n", HEAP_SIZE_DEF);
}

static void parse_args(send_context *context, int argc, char *argv[])
{
  int idx = 1;

  // flags/control
  context->pyspead         = false;
  context->nthreads        = NTHREADS_DEF;
  // networking
  context->udp_if          = UDP_IF_DEF;
  context->port            = PORT_DEF;
  context->destination     = DESTINATION_DEF;
  context->packet          = PACKET_DEF;
  context->buffer          = BUFFER_DEF;
  context->rate            = RATE_DEF;
  context->nheaps          = NHEAPS_DEF;
  context->nhops           = NHOPS_DEF;
  // heaps
  context->nitems          = NITEMS_DEF;
  context->heap_size       = HEAP_SIZE_DEF;
  // heap contents
  context->timestamp = 0;
  context->ts_step = 0x200000;
  context->payload = NULL;
  do {
    // flags/control
    if (strcmp(argv[idx], "-pyspead") == 0) {
      context->pyspead = true;
      idx++;
      continue;
    } 
    if (strcmp(argv[idx], "-nthreads") == 0) {
      idx++;
      context->nthreads = atoi(argv[idx]);
      idx++;
      continue;
    }
    // networking
    if (strcmp(argv[idx], "-udp-if") == 0) {
      idx++;
      context->udp_if = argv[idx];
      idx++;
      continue;
    }
    if (strcmp(argv[idx], "-port") == 0) {
      idx++;
      context->port = argv[idx];
      idx++;
      continue;
    }
    if (strcmp(argv[idx], "-packet") == 0) {
      idx++;
      context->packet = atoi(argv[idx]);
      idx++;
      continue;
    }
    if (strcmp(argv[idx], "-buffer") == 0) {
      idx++;
      context->buffer = atoi(argv[idx]);
      idx++;
      continue;
    }
    if (strcmp(argv[idx], "-rate") == 0) {
      idx++;
      context->rate = atof(argv[idx]);
      idx++;
      continue;
    }
    if (strcmp(argv[idx], "-nheaps") == 0) {
      idx++;
      context->nheaps = atoi(argv[idx]);
      idx++;
      continue;
    }
    if (strcmp(argv[idx], "-nhops") == 0) {
      idx++;
      context->nhops = atoi(argv[idx]);
      idx++;
      continue;
    }
    // heaps
    if (strcmp(argv[idx], "-nitems") == 0) {
      idx++;
      context->nitems = atoi(argv[idx]);
      idx++;
      continue;
    }
    if (strcmp(argv[idx], "-heap-size") == 0) {
      idx++;
      context->heap_size = atoi(argv[idx]);
      idx++;
      continue;
    }
    break;
  } while (1);
  if (idx < argc) {
    context->destination = argv[idx];
  }
}

static void send_heaps(send_context *context)
{
  using boost::asio::ip::udp;

  spead2::thread_pool          tp(context->nthreads);
  udp::resolver                resolver(tp.get_io_service());
  udp::resolver::query         query(context->destination, context->port);
#if defined(SPEAD2_MAJOR) && (SPEAD2_MAJOR == 3)
  std::vector<boost::asio::ip::udp::endpoint> endpoints;
  endpoints.push_back(*resolver.resolve(query));
#else
  udp::endpoint                endpoint = *resolver.resolve(query);
#endif
  spead2::bug_compat_mask      bug_compat = context->pyspead ? spead2::BUG_COMPAT_PYSPEAD_0_5_2 : 0;
  spead2::flavour              f(spead2::maximum_version, 64, 48, bug_compat);
  boost::asio::ip::address     interface_address = boost::asio::ip::address::from_string(context->udp_if);
  spead2::send::stream_config  sconfig;
  sconfig.set_max_packet_size(context->packet);
  sconfig.set_rate(context->rate);
  sconfig.set_burst_size(spead2::send::stream_config::default_burst_size);
  sconfig.set_max_heaps(context->nheaps);

  spead2::send::udp_stream     stream(spead2::io_service_ref(tp),
#if defined(SPEAD2_MAJOR) && (SPEAD2_MAJOR == 3)
				      endpoints,
#else
				      endpoint,
#endif
				      sconfig,
				      context->buffer,
				      context->nhops,
				      interface_address);
  std::size_t                  i;
  mksend::heap                 hc(f);

  std::cout << "allocating memory for payload " << context->heap_size << std::endl;
  context->payload = (char*)calloc(context->heap_size, sizeof(char));
  std::cout << "initializing payload" << std::endl;
  for (i = 0; i < context->heap_size; i++) {
    context->payload[i] = (char)(i%128);
  }
  std::cout << "initializing items" << std::endl;
  for (i = 0; i < context->nitems; i++) {
    context->items[i] = 13*(i+1);
  }
  do {
    //spead2::send::heap h(f);
    context->items[0] = context->timestamp;
    context->timestamp += context->ts_step;
    for (i = 0; i < context->nitems - 1; i++) {
      // h.add_item(0x1600 + i, &(context->items[i]), sizeof(context->items[i]), true);
      //h.add_item(0x1600 + i, context->items[i]);
      hc.set_item(0x1600 + i, context->items[i]);
    }
    // h.add_item(0x1600 + context->nitems, context->payload, context->heap_size, true);
    //h.add_item(0x1600 + context->nitems, context->payload, context->heap_size, false);
    hc.set_item(0x1600 + context->nitems, (std::uint8_t *)(context->payload), context->heap_size);
    std::cout << "sending heap with timespamt " << context->items[0] << std::endl;
    stream.async_send_heap(hc.get_heap(), [] (const boost::system::error_code &ec, spead2::item_pointer_t bytes_transferred)
    {
        if (ec)
            std::cerr << ec.message() << '\n';
        else
            std::cout << "Sent " << bytes_transferred << " bytes in heap\n";
    });
    stream.flush();
  } while (true);

}

int main(int argc, char *argv[])
{
  send_context context;

  if (argc <= 2) {
    usage(argv[0]);
    return 0;
  } else {
    parse_args(&context, argc, argv);
  }
  send_heaps(&context);
}
