package mk;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.util.Arrays;
import java.util.Vector;

import java.text.*;

public class FilterSamplesPanel extends Panel implements MouseListener {

    protected ReplyFeatures       features     = null;
    protected ReplySamples        samples      = null;
    protected int                 nFeeds       = 0;
    protected int                 feed         = 1;

    protected JTextField          tfFeed       = null;
    protected JLabel              lFeedCount   = null;

    public FilterSamplesPanel() {
	super("Filter Samples");
    }

    public void createGUI(boolean withTouch) {
	super.createGUI();
	setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
	tfFeed = new JTextField("" + feed);
	tfFeed.setColumns(2);
	if (withTouch) {
	    tfFeed.setEditable(false);
	    tfFeed.addMouseListener(this);
	} else {
	    tfFeed.addActionListener(this);
	}
	lFeedCount = new JLabel("[0 .." + (nFeeds-1) + "]");
	// Feed
	add(new JLabel("Feed:"));
	add(tfFeed);
	//add(lFeedCount);
    }

    public int getFeedCount() { return nFeeds; }

    public void setFeedCount(int nFeeds) {
	this.nFeeds = nFeeds;
	lFeedCount.setText("[0 .." + (nFeeds-1) + "]");
    }

    public int getFeed() { return feed; }

    public void setFeed(int feed) {
	this.feed = feed;
	tfFeed.setText("" + feed);
    }

    public void actionPerformed(ActionEvent e) {
	if (e.getSource() == tfFeed) {
	    try {
		feed = Short.parseShort(tfFeed.getText());
	    } catch (Exception ex) {}
	}
    }

    public void mouseEntered(MouseEvent me) { }

    public void mouseExited(MouseEvent me) { }

    public void mouseClicked(MouseEvent me) {
	if (me.getSource() == tfFeed) {
	    int b = NumberDialog.enterFixnum("Feed number", tfFeed.getText());
	    tfFeed.setText("" + b);
	    this.feed = (short)b;
	}
    }

    public void mousePressed(MouseEvent me) { }

    public void mouseReleased(MouseEvent me) { }

    public void setFeatures(ReplyFeatures features) {
	this.features = features;
	setFeedCount(features.nfeeds);
    }

    public void setSamples(ReplySamples samples) {
	this.samples = samples;
    }


}
