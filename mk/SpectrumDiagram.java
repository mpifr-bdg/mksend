package mk;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.util.Arrays;
import java.util.Vector;

public class SpectrumDiagram extends Diagram {

    //protected static final float OFFSET       = 165.0F;

    protected FilterSpectrumPanel  filterPanel  = null;
    protected Color                curColor     = new Color(255,   0, 0, 255);
    protected Color                refColor     = new Color(  0, 255, 0, 255);
    protected JToggleButton        btnAutoscale = null;
    protected JToggleButton        btnRef       = null;
    protected JToggleButton        btnDiff      = null;
    protected JButton              btnXRange    = null;
    protected JButton              btnYRange    = null;
    protected JButton              btnYZero     = null;
    protected boolean              autoscale    = true;
    protected boolean              autozero     = false;
    protected boolean              showdiff     = false;
    protected JTextField           tfPowerRef   = null;
    protected JTextField           tfPowerMin   = null;
    protected JTextField           tfPowerMax   = null;
    protected float                powerRef     = 165.0F;
    protected double               powerMin     = -10.0;
    protected double               powerMax     =  10.0;
    protected float                powerBase    = (float)(10.0*Math.log10(0.1)); // replacement value if raw/ref spectrum[i] is equal with 0.0
    protected float[]              rawSpectrum  = null;
    protected float[]              refSpectrum  = null;

    public SpectrumDiagram(int width, int height, int left, int right, int bottom, int top) {
	super(width, height, left, right, bottom, top);
	btnAutoscale = createToggleButton("A", autoscale);
	btnRef       = createToggleButton("R", false);
	btnDiff      = createToggleButton("D", showdiff);
	btnXRange    = createSimpleButton("X");
	btnYRange    = createSimpleButton("Y");
	btnYZero     = createSimpleButton("0");
	createLabel("Ref:");
	tfPowerRef  = createTextField("" + powerRef, true, 6);
	createLabel("Min:");
	tfPowerMin  = createTextField("" + powerMin, true, 6);
	createLabel("Max:");
	tfPowerMax  = createTextField("" + powerMax, true, 6);
	createInputField();
	xRangeFlag = true;
	yRangeFlag = true;
    }

    public void setFilterPanel(FilterSpectrumPanel panel) { filterPanel = panel; }

    public boolean getAutoscale() { return autoscale; }

    public void setAutoscale(boolean flag) {
	autoscale = flag;
    }

    public void clearReference() {
	refSpectrum = null;
    }

    public void takeReference() {
	refSpectrum = Arrays.copyOf(rawSpectrum, rawSpectrum.length);
    }

    public boolean getShowDiff() { return showdiff; }

    public void setShowDiff(boolean flag) {
	showdiff = flag;
    }

    public void setSpectrum(ReplySpectrum spectrum) {
	int    i;
	double val;
	if ((refSpectrum != null) && (refSpectrum.length != spectrum.count)) refSpectrum = null;
	if ((rawSpectrum == null) || (rawSpectrum.length != spectrum.count)) {
	    rawSpectrum = Arrays.copyOf(spectrum.spectrum, spectrum.count);
	} else {
	    for (i = 0; i < rawSpectrum.length; i++) {
		rawSpectrum[i] = spectrum.spectrum[i];
	    }
	}
	for (i = 0; i < rawSpectrum.length; i++) {
	    if (rawSpectrum[i] == 0.0) {
		rawSpectrum[i] = powerBase;
	    } else {
		rawSpectrum[i] = (float)(10.0*Math.log10(rawSpectrum[i]));
	    }
	}
	clearShapes();
	clearColors();
	setXRange(spectrum.freq_low, spectrum.freq_high);
	// If autoscale is used, recalculate the lowest and highest y-values.
	if (autoscale || autozero) {
	    double yminr = 0.0, ymaxr = 0.0;
	    boolean isFirst = true;
	    // two cases:
	    if (showdiff && (refSpectrum != null)) {
		// 1. show the difference between a reference spectrum with the current raw spectrum
		for (i = 0; i < rawSpectrum.length; i++) {
		    if (rawSpectrum[i] == powerBase) continue; // current raw spectrum is 0.0 at this channel -> ignore this channel
		    if (refSpectrum[i] == powerBase) continue; // reference spectrum is 0.0 at this channel -> ignore this channel
		    val = rawSpectrum[i] - refSpectrum[i];
		    if (isFirst) {
			ymaxr = val;
			yminr = val;
			isFirst = false;
		    } else {
			if (val > ymaxr) ymaxr = val;
			if (val < yminr) yminr = val;
		    }
		}
	    } else {
		// 2. show the current raw spectrum and optional the reference spectrum
		for (i = 0; i < rawSpectrum.length; i++) {
		    if (rawSpectrum[i] == powerBase) continue; // current raw spectrum is 0.0 at this channel -> ignore this channel
		    val = rawSpectrum[i];
		    if (isFirst) {
			ymaxr = val;
			yminr = val;
			isFirst = false;
		    } else {
			if (val > ymaxr) ymaxr = val;
			if (val < yminr) yminr = val;
		    }
		}
		if (refSpectrum != null) {
		    for (i = 0; i < refSpectrum.length; i++) {
			if (refSpectrum[i] == powerBase) continue; // reference spectrum is 0.0 at this channel -> ignore this channel
			val = refSpectrum[i];
			if (isFirst) {
			    ymaxr = val;
			    yminr = val;
			    isFirst = false;
			} else {
			    if (val > ymaxr) ymaxr = val;
			    if (val < yminr) yminr = val;
			}
		    }
		}
	    }
	    if (isFirst) return; // all channels are 0.0 -> no display!
	    if (autoscale) {
		if (yminr == ymaxr) {
		    setYRange(yminr - powerRef - 1, ymaxr - powerRef + 1);
		} else {
		    setYRange(yminr - powerRef,     ymaxr - powerRef);
		}
		powerMin = yMin;
		powerMax = yMax;
		tfPowerMin.setText(numFormatD1.format((float)powerMin));
		tfPowerMax.setText(numFormatD1.format((float)powerMax));
	    }
	    if (autozero) {
		powerRef = (float)ymaxr;
		powerMin = yminr - powerRef - 3.0;
		powerMax = ymaxr - powerRef + 3.0;
		setYRange(powerMin, powerMax);
		tfPowerRef.setText(numFormatD2.format(powerRef));
		tfPowerMin.setText(numFormatD2.format((float)powerMin));
		tfPowerMax.setText(numFormatD2.format((float)powerMax));
		autozero = false;
	    }
	}
	// calculate the scaling parameters
	double xscale = (double)width/(double)rawSpectrum.length;
	double yoff   = yMin;
	double yscale = (double)height/(yMax - yMin);
	GeneralPath p;
	// two cases:
	if (showdiff && (refSpectrum != null)) {
	    // 1. show the difference between a reference spectrum with the current raw spectrum
	    // Create the raw spectrum plot.
	    boolean isFirst = true;
	    p = new GeneralPath();
	    for (i = 0; i < rawSpectrum.length; i++) {
		if (rawSpectrum[i] == powerBase) continue; // current raw spectrum is 0.0 at this channel -> ignore this channel
		if (refSpectrum[i] == powerBase) continue; // reference spectrum is 0.0 at this channel -> ignore this channel
		val = rawSpectrum[i] - refSpectrum[i] - powerRef;
		if (isFirst) {
		    p.moveTo(xscale*(double)i, yscale*(val - yoff));
		    isFirst = false;
		} else {
		    p.lineTo(xscale*(double)i, yscale*(val - yoff));
		}
	    }
	} else {
	    // 2. show the current raw spectrum and optional the reference spectrum
	    // Create the optinal reference spectrum plot.
	    if (refSpectrum != null) {
		boolean isFirst = true;
		p = new GeneralPath();
		for (i = 0; i < refSpectrum.length; i++) {
		    if (refSpectrum[i] == powerBase) continue; // reference spectrum is 0.0 at this channel -> ignore this channel
		    val = refSpectrum[i] - powerRef;
		    if (isFirst) {
			p.moveTo(xscale*(double)i, yscale*(val - yoff));
			isFirst = false;
		    } else {
			p.lineTo(xscale*(double)i, yscale*(val - yoff));
		    }
		}
		addShape(p);
		addColor(refColor);
	    }
	    // Create the raw spectrum plot.
	    boolean isFirst = true;
	    p = new GeneralPath();
	    for (i = 0; i < rawSpectrum.length; i++) {
		if (rawSpectrum[i] == powerBase) continue; // current raw spectrum is 0.0 at this channel -> ignore this channel
		val = rawSpectrum[i] - powerRef;
		if (isFirst) {
		    p.moveTo(xscale*(double)i, yscale*(val - yoff));
		    isFirst = false;
		} else {
		    p.lineTo(xscale*(double)i, yscale*(val - yoff));
		}
	    }
	}
	addShape(p);
	addColor(curColor);
	repaint();
    }

    public void actionPerformed(ActionEvent e) {
	if (e.getSource() == btnAutoscale) {
	    setAutoscale(btnAutoscale.isSelected());
	} else if (e.getSource() == btnRef) {
	    if (btnRef.isSelected()) {
		takeReference();
	    } else {
		clearReference();
	    }
	} else if (e.getSource() == btnDiff) {
	    setShowDiff(btnDiff.isSelected());
	} else if (e.getSource() == btnXRange) {
	    xRangeCount = 2;
	} else if (e.getSource() == btnYRange) {
	    yRangeCount = 2;
	} else if (e.getSource() == btnYZero) {
	    autozero = true;
	} else if (e.getSource() == tfPowerRef) {
	    powerRef = Float.parseFloat(tfPowerRef.getText());
	} else if (e.getSource() == tfPowerMin) {
	    powerMin = Double.parseDouble(tfPowerMin.getText());
	    setYRange(powerMin, powerMax);
	} else if (e.getSource() == tfPowerMax) {
	    powerMax = Double.parseDouble(tfPowerMax.getText());
	    setYRange(powerMin, powerMax);
	} else {
	    super.actionPerformed(e);
	}
    }

    public void handleXRange(double xmin, double xmax) {
	//System.out.println("SpectrumDiagram.handleXRange(" + xmin + ", " + xmax + ")");
	if (filterPanel != null) {
	    filterPanel.setFrequencyRange((float)xmin, (float)xmax);
	}
    }

    public void handleYRange(double ymin, double ymax) {
	//System.out.println("SpectrumDiagram.handleYRange(" + ymin + ", " + ymax + ")");
	btnAutoscale.setSelected(false);
	autoscale = false;
	setYRange(ymin, ymax);
    }

}
