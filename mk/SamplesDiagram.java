package mk;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.util.Arrays;
import java.util.Vector;

public class SamplesDiagram extends Diagram {

    //protected static final float OFFSET       = 165.0F;

    protected FilterSamplesPanel   filterPanel  = null;
    protected Color                curColor     = new Color(255,   0, 0, 255);
    protected Color                refColor     = new Color(  0, 255, 0, 255);
    protected JToggleButton        btnAutoscale = null;
    protected JButton              btnXRange    = null;
    protected JButton              btnYRange    = null;
    protected JButton              btnYZero     = null;
    protected boolean              autoscale    = true;
    protected boolean              autozero     = false;
    protected JTextField           tfSampleMin  = null;
    protected JTextField           tfSampleMax  = null;
    protected short                sampleMin    = -2048;
    protected short                sampleMax    =  2048;
    protected short[]              rawSamples   = null;

    public SamplesDiagram(int width, int height, int left, int right, int bottom, int top) {
	super(width, height, left, right, bottom, top);
	btnAutoscale = createToggleButton("A", autoscale);
	btnXRange    = createSimpleButton("X");
	btnYRange    = createSimpleButton("Y");
	btnYZero     = createSimpleButton("0");
	createLabel("Min:");
	tfSampleMin  = createTextField("" + sampleMin, true, 6);
	createLabel("Max:");
	tfSampleMax  = createTextField("" + sampleMax, true, 6);
	createInputField();
	xRangeFlag = true;
	yRangeFlag = true;
    }

    public void setFilterPanel(FilterSamplesPanel panel) { filterPanel = panel; }

    public boolean getAutoscale() { return autoscale; }

    public void setAutoscale(boolean flag) {
	autoscale = flag;
    }

    public void setSamples(ReplySamples samples) {
	int   i;
	short val;
	if (rawSamples == null) {
	    rawSamples = Arrays.copyOf(samples.samples, samples.samples.length);
	} else {
	    for (i = 0; i < rawSamples.length; i++) {
		rawSamples[i] = samples.samples[i];
	    }
	}
	clearShapes();
	clearColors();
	setXRange(0, 4095);
	// If autoscale is used, recalculate the lowest and highest y-values.
	if (autoscale || autozero) {
	    short yminr, ymaxr;
	    val = rawSamples[0];
	    yminr = val;
	    ymaxr = val;
	    for (i = 1; i < rawSamples.length; i++) {
		val = rawSamples[i];
		if (val > ymaxr) ymaxr = val;
		if (val < yminr) yminr = val;
	    }
	    if (autoscale) {
		if (yminr == ymaxr) {
		    setYRange(yminr-1, ymaxr+1);
		} else {
		    setYRange(yminr, ymaxr);
		}
		sampleMin = (short)yMin;
		sampleMax = (short)yMax;
		tfSampleMin.setText("" + sampleMin);
		tfSampleMax.setText("" + sampleMax);
	    }
	    if (autozero) {
		sampleMin = yminr;
		sampleMax = ymaxr;
		setYRange(sampleMin, sampleMax);
		tfSampleMin.setText("" + sampleMin);
		tfSampleMax.setText("" + sampleMax);
		autozero = false;
	    }
	}
	// calculate the scaling parameters
	double xscale = (double)width/(double)rawSamples.length;
	double yoff   = yMin;
	double yscale = (double)height/(yMax - yMin);
	GeneralPath p;
	// Create the raw samples plot.
	p = new GeneralPath();
	val = rawSamples[0];
	p.moveTo(0.0, yscale*(val - yoff));
	for (i = 1; i < rawSamples.length; i++) {
	    val = rawSamples[i];
	    p.lineTo(xscale*(double)i, yscale*(val - yoff));
	}
	addShape(p);
	addColor(curColor);
	repaint();
    }

    public void actionPerformed(ActionEvent e) {
	if (e.getSource() == btnAutoscale) {
	    setAutoscale(btnAutoscale.isSelected());
	} else if (e.getSource() == btnXRange) {
	    xRangeCount = 2;
	} else if (e.getSource() == btnYRange) {
	    yRangeCount = 2;
	} else if (e.getSource() == btnYZero) {
	    autozero = true;
	} else if (e.getSource() == tfSampleMin) {
	    sampleMin = Short.parseShort(tfSampleMin.getText());
	    setYRange(sampleMin, sampleMax);
	} else if (e.getSource() == tfSampleMax) {
	    sampleMax = Short.parseShort(tfSampleMax.getText());
	    setYRange(sampleMin, sampleMax);
	} else {
	    super.actionPerformed(e);
	}
    }

    public void handleYRange(double ymin, double ymax) {
	//System.out.println("SamplesDiagram.handleYRange(" + ymin + ", " + ymax + ")");
	btnAutoscale.setSelected(false);
	autoscale = false;
	setYRange(ymin, ymax);
    }

}
