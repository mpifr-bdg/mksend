package mk;

import java.io.*;
import java.net.*;
import java.nio.*;

public class ReplyPing {

    public char tag;
    public char code;
    public int  payload;

    public ReplyPing() {
    }

    public boolean receive(DataInputStream sock) {
	try {
	    this.tag     = (char)sock.readByte();
	    this.code    = (char)sock.readByte();
	    this.payload = sock.readInt();
	    System.out.println("reply: tag=" + this.tag + ", code=" + this.code + ", payload=" + this.payload);
	    return true;
	} catch (IOException e) {
            System.err.println("Could not receive a PING reply");
	    e.printStackTrace();
            return false;
	}
    }
}
