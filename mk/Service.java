package mk;


import java.io.*;
import java.net.*;
import java.nio.*;


public class Service {

    public static final int PORT = 6000;

    protected boolean          connected = false;
    protected Socket           sock      = null;
    protected DataOutputStream out       = null;
    protected DataInputStream  in        = null;
    protected Request          request   = new Request();

    protected Service() {
    }

    public synchronized boolean isConnected() { return connected; }

    public synchronized boolean connect(String host, int port)
    {
	if (connected) return true;
	try {
	    sock = new Socket(host, port);
	    out = new DataOutputStream(new BufferedOutputStream(sock.getOutputStream()));
	    in  = new DataInputStream(new BufferedInputStream(sock.getInputStream()));
	    connected = true;
	} catch (UnknownHostException e) {
            System.err.println("Unknown host: " + host);
            return false;
	} catch (IOException e) {
            System.err.println("Could not get a connection to: " + host + ":" + port);
            return false;
        }
	return connected;
    }

    public synchronized void disconnect()
    {
	if (!connected) return;
	try {
	    out.close();
	    in.close();
	    sock.close();
	    connected = false;
	} catch (IOException e) {
            System.err.println("Could not close the connection");
	}
    }

    public synchronized ReplyPing sendPing(ReplyPing reply)
    {
	if (!connected) return null;
	request.ping();
	if (request.send(out)) {
	    if (reply == null) reply = new ReplyPing();
	    if (reply.receive(in)) {
		return reply;
	    } else {
		return null;
	    }
	} else {
	    return null;
	}
    }

    public synchronized ReplyFeatures sendGetFeatures(ReplyFeatures reply)
    {
	if (!connected) return null;
	request.getFeatures();
	if (request.send(out)) {
	    if (reply == null) reply = new ReplyFeatures();
	    if (reply.receive(in)) {
		return reply;
	    } else {
		return null;
	    }
	} else {
	    return null;
	}
    }

    public synchronized ReplySpectrum sendGetSpectrum(ReplySpectrum reply,
						      float frequency,
						      int count,
						      int binning)
    {
	if (!connected) return null;
	request.getSpectrum(frequency, count, binning);
	if (request.send(out)) {
	    if (reply == null) reply = new ReplySpectrum();
	    if (reply.receive(in)) {
		return reply;
	    } else {
		return null;
	    }
	} else {
	    return null;
	}
    }

    public synchronized ReplySamples sendGetSamples(ReplySamples reply,
						    int feed)
    {
	if (!connected) return null;
	request.getSamples(feed);
	if (request.send(out)) {
	    if (reply == null) reply = new ReplySamples();
	    if (reply.receive(in)) {
		return reply;
	    } else {
		return null;
	    }
	} else {
	    return null;
	}
    }

}
