package mk;

import java.io.*;
import java.net.*;
import java.nio.*;

public class ReplySamples {

    public char     tag;
    public char     code;
    public int      payload;
    public int      feed;
    public int      block;
    public int      nsamples;
    public int      nscis;
    public short[]  samples; // processed (mapped) raw data
    public long[]   scis;    // processed (mapped) raw data
    
    public ReplySamples() {
    }

    public void alloc(int nsamples, int nscis) {
	if (nsamples == 0) return;
	if (samples == null) {
	    samples = new short[nsamples];
	} else if (samples.length < nsamples) {
	    samples = new short[nsamples];
	}
	if (scis == null) {
	    scis = new long[nscis];
	} else if (scis.length < nscis) {
	    scis = new long[nscis];
	}
    }

    public boolean receive(DataInputStream sock) {
	try {
	    this.tag         = (char)sock.readByte();
	    this.code        = (char)sock.readByte();
	    this.payload     = sock.readInt();
	    if (tag != 'R') {
		return false;
	    }
	    this.feed        = sock.readInt();
	    this.block       = sock.readInt();
	    this.nsamples    = sock.readInt();
	    this.nscis       = sock.readInt();
	    /*
	    System.out.println("reply: cmd=" + this.cmd
			       + ", err=" + this.err
			       + ", crate=" + this.crate
			       + ", board=" + this.board
			       + ", frequency=[" + this.freq_low + " ... " + this.freq_high
			       + "], count=" + this.count
			       + ", binning=" + this.binning);
	    */
	    alloc(this.nsamples, this.nscis);
	    for (int i = 0; i < nsamples; i++) {
		this.samples[i] = sock.readShort();
		//System.out.println("samples[" + i + "] = " + this.samples[i]);
	    }
	    for (int i = 0; i < nscis; i++) {
		this.scis[i] = sock.readLong();
		//System.out.println("samples[" + i + "] = " + this.samples[i]);
	    }
	    return true;
	} catch (IOException e) {
            System.err.println("Could not receive a GET_SAMPLES reply");
	    e.printStackTrace();
            return false;
	}
    }

}

