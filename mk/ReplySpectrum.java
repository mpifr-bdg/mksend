package mk;

import java.io.*;
import java.net.*;
import java.nio.*;

public class ReplySpectrum {

    public char     tag;
    public char     code;
    public int      payload;
    public int      nreceived;
    public int      nsaturated;
    public float    freq_low;    // the lowest frequency (spectrum[0])
    public float    freq_high;   // the highest frequency (spectrum[count-1])
    public int      count;       // count contains the number of channels in the processed raw data
    public int      binning;     // the number of raw data channels used to compute one sample
    public float[]  spectrum;    // processed (mapped) raw data
    
    public ReplySpectrum() {
    }

    public void alloc(int count) {
	if (count == 0) return;
	if (spectrum == null) {
	    spectrum = new float[count];
	} else if (spectrum.length < count) {
	    spectrum = new float[count];
	}
    }

    public boolean receive(DataInputStream sock) {
	try {
	    this.tag         = (char)sock.readByte();
	    this.code        = (char)sock.readByte();
	    this.payload     = sock.readInt();
	    if (tag != 'S') {
		return false;
	    }
	    this.nreceived   = sock.readInt();
	    this.nsaturated  = sock.readInt();
	    this.freq_low    = sock.readFloat();
	    this.freq_high   = sock.readFloat();
	    this.count       = sock.readInt();
	    this.binning     = sock.readInt();
	    /*
	    System.out.println("reply: cmd=" + this.cmd
			       + ", err=" + this.err
			       + ", crate=" + this.crate
			       + ", board=" + this.board
			       + ", frequency=[" + this.freq_low + " ... " + this.freq_high
			       + "], count=" + this.count
			       + ", binning=" + this.binning);
	    */
	    alloc(this.count);
	    for (int i = 0; i < count; i++) {
		this.spectrum[i] = sock.readFloat();
		//System.out.println("spectrum[" + i + "] = " + this.spectrum[i]);
	    }
	    return true;
	} catch (IOException e) {
            System.err.println("Could not receive a GET_SPECTRUM reply");
	    e.printStackTrace();
            return false;
	}
    }

}

