package mk;

import java.io.*;
import java.net.*;
import java.nio.*;

public class ReplyFeatures {

    public char  tag;
    public char  code;
    public int   payload;
    public short mode;
    // spectrum related features
    public int   nspectra;
    public int   nchannels;
    public int   ncounters;
    public float bandwidth;
    // samples related features
    public int   nfeeds;
    public int   nblocks;
    public int   nsamples;
    public int   nbits;
    public int   nscis;

    public ReplyFeatures() {
    }

    public boolean receive(DataInputStream sock) {
	try {
	    this.tag         = (char)sock.readByte();
	    this.code        = (char)sock.readByte();
	    this.payload     = sock.readInt();
	    if (tag != 'F') {
		return false;
	    }
	    mode = sock.readShort();
	    // spectrum related features
	    nspectra = sock.readInt();
	    nchannels = sock.readInt();
	    ncounters = sock.readInt();
	    bandwidth = sock.readFloat();
	    // samples related features
	    nfeeds = sock.readInt();
	    nblocks = sock.readInt();
	    nsamples = sock.readInt();
	    nbits = sock.readInt();
	    nscis = sock.readInt();
	    return true;
	} catch (IOException e) {
            System.err.println("Could not receive a GET_FEATURES reply");
	    e.printStackTrace();
            return false;
	}
    }
}

