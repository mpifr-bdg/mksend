package mk;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.util.Vector;

public class BufferedDiagram extends Diagram {

    protected BufferedImage buffer1  = null;
    protected BufferedImage buffer2  = null;

    public BufferedDiagram(int width, int height) {
	super(width, height, 0, 0, 0, 0);
    }

    public BufferedDiagram(int width, int height, int left, int right, int bottom, int top) {
	super(width, height, left, right, bottom, top);
    }


    public BufferedImage getBuffer() {
	if (buffer1 == null) {
	    Image buf = createImage(width, height);
	    if (buf instanceof BufferedImage) {
		buffer1 = (BufferedImage)buf;
	    }
	}
	if (buffer2 == null) {
	    Image buf = createImage(width, height);
	    if (buf instanceof BufferedImage) {
		buffer2 = (BufferedImage)buf;
	    }
	}
	return buffer1;
    }

    public void shiftVertical() {
	WritableRaster wr1 = buffer1.getRaster();
	WritableRaster wr2 = buffer2.getRaster();
	wr2.setRect(0, 1, wr1);
	wr1.setRect(0, 0, wr2);
    }

    public void paintComponent(Graphics g) {
	super.paintComponent(g);
	g.drawImage(buffer1, leftMargin, topMargin, null);
    }

}
