package mk;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.util.Arrays;
import java.util.Vector;

import java.text.*;

public class Monitor extends JFrame implements ActionListener {

    protected String  cfgHost              = "localhost";
    protected int     cfgPort              = Service.PORT;
    protected long    cfgDelay             = 200;
    protected boolean cfgWithTouch         = false;
    protected int     cfgPanelWidthSingle  = 512;
    protected int     cfgPanelWidthDouble  = 256;
    protected int     cfgPanelHeightSingle = 256;
    protected int     cfgPanelHeightDouble = 128;
    protected int     cfgFilterCount       = 512;
    protected int     cfgFilterBinning     = 1;
    protected int     cfgFeed              = 0;

    protected NumberFormat numFormat2D0 = null;
    protected NumberFormat numFormat6D0 = null;
    protected NumberFormat numFormatD0  = null;
    protected NumberFormat numFormatD1  = null;
    protected NumberFormat numFormatD3  = null;
    protected NumberFormat numFormatE3  = null;

    // spectrum related stuff
    protected Service              specService         = null;
    protected ConnectionPanel      specConnectionPanel = new ConnectionPanel("Connection Spectrum");
    protected FilterSpectrumPanel  specFilterPanel     = new FilterSpectrumPanel();
    protected SpectrumDiagram      specDiagram         = null;
    protected ReplyFeatures        specFeatures        = new ReplyFeatures();
    protected boolean              specAvailable       = false;
    protected ReplySpectrum        spectrum            = new ReplySpectrum();
    protected Thread               specMessageThread   = null;
    protected Thread               specDiagramThread   = null;

    // samples related stuff
    protected Service              sampService         = null;
    protected ConnectionPanel      sampConnectionPanel = new ConnectionPanel("Connection Samples");
    protected FilterSamplesPanel   sampFilterPanel     = new FilterSamplesPanel();
    protected SamplesDiagram       sampDiagram         = null;
    protected ReplyFeatures        sampFeatures        = new ReplyFeatures();
    protected boolean              sampAvailable       = false;
    protected ReplySamples         samples             = new ReplySamples();
    protected Thread               sampMessageThread   = null;
    protected Thread               sampDiagramThread   = null;

    protected ReplyPing            ping                = new ReplyPing();

    // setup and features
    protected GridBagPanel         mainPanel           = new GridBagPanel();
    protected InfoPanel            infoPanel           = new InfoPanel();
    protected JButton              btnHelp             = null;
    protected JTabbedPane          cardPanel           = new JTabbedPane();


    public Monitor(String title) {
        super(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	specService = new Service();
	sampService = new Service();
    }

    protected void setHost(String host) { cfgHost = host; }

    protected void setPort(int port) { cfgPort = port; }

    protected void setDelay(long delay) { cfgDelay = delay; }

    protected void setGUISize(String tag) {
	if (tag.equals("s") || tag.equals("small")) {
	    cfgPanelWidthSingle  = 384;
	    cfgPanelWidthDouble  = 136;
	    cfgPanelHeightSingle = 240;
	    cfgPanelHeightDouble = 100;
	    cfgFilterCount       = 4096;
	    cfgFilterBinning     = 16;
	} else if (tag.equals("l") || tag.equals("large")) {
	    cfgPanelWidthSingle  = 1024;
	    cfgPanelWidthDouble  = 476;
	    cfgPanelHeightSingle = 550;
	    cfgPanelHeightDouble = 256;
	    cfgFilterCount       = 8192;
	    cfgFilterBinning     = 8;
	} else {
	    cfgPanelWidthSingle  = 512;
	    cfgPanelWidthDouble  = 204;
	    cfgPanelHeightSingle = 256;
	    cfgPanelHeightDouble = 120;
	    cfgFilterCount       = 8192;
	    cfgFilterBinning     = 8;
	}
    }

    protected void setWithTouch(boolean withTouch) {
	cfgWithTouch = withTouch;
    }

    protected void createNumberFormats() {
	numFormat2D0 = NumberFormat.getInstance();
	numFormat2D0.setMinimumFractionDigits(0);
	numFormat2D0.setMaximumFractionDigits(0);
	numFormat2D0.setMinimumIntegerDigits(2);
	numFormat2D0.setMaximumIntegerDigits(2);
	numFormat2D0.setGroupingUsed(false);

	numFormat6D0 = NumberFormat.getInstance();
	numFormat6D0.setMinimumFractionDigits(0);
	numFormat6D0.setMaximumFractionDigits(0);
	numFormat6D0.setMinimumIntegerDigits(6);
	numFormat6D0.setMaximumIntegerDigits(6);
	numFormat6D0.setGroupingUsed(false);

	numFormatD0 = NumberFormat.getInstance();
	numFormatD0.setMinimumFractionDigits(0);
	numFormatD0.setMaximumFractionDigits(0);
	numFormatD0.setGroupingUsed(false);

	numFormatD1 = NumberFormat.getInstance();
	numFormatD1.setMinimumFractionDigits(1);
	numFormatD1.setMaximumFractionDigits(1);
	numFormatD1.setGroupingUsed(false);

	numFormatD3 = NumberFormat.getInstance();
	numFormatD3.setMinimumFractionDigits(3);
	numFormatD3.setMaximumFractionDigits(3);
	numFormatD3.setGroupingUsed(false);

	numFormatE3 = NumberFormat.getInstance();
	if (numFormatE3 instanceof DecimalFormat) {
	    ((DecimalFormat)numFormatE3).applyPattern("#.###E0");
	}
    }

    protected JButton createHelpButton() {
	btnHelp = new JButton("?");
	btnHelp.setMargin(new Insets(0,0,0,0));
	btnHelp.setMinimumSize(new Dimension(25, 25));
	btnHelp.setPreferredSize(new Dimension(25, 25));
	btnHelp.setMaximumSize(new Dimension(25, 25));
	btnHelp.addActionListener(this);
	return btnHelp;
    }

    // ##########################################################
    // ### spectrum related stuff
    // ##########################################################
    protected class SpectrumMessageLoop implements Runnable {
	public ReplySpectrum    spectrum = new ReplySpectrum();
	public SpectrumMessageLoop() {}
	public void run() {
	    for ( ; ; ) {
		ReplySpectrum nspectrum = specService.sendGetSpectrum(spectrum, specFilterPanel.getFrequency(), specFilterPanel.getCount(), specFilterPanel.getBinning());
		if ((nspectrum != null) && (nspectrum.tag == 'S')) spectrum = provideSpectrum(nspectrum);
		try { Thread.sleep(cfgDelay); }
		catch (InterruptedException e) { return; }
		catch (Exception e) {}
	    }
	}

    }

    protected class SpectrumDiagramLoop implements Runnable {
	public ReplySpectrum spectrum = new ReplySpectrum();
	public SpectrumDiagramLoop() {}
	public void run() {
	    for ( ; ; ) {
		ReplySpectrum dspectrum = consumeSpectrum(spectrum);
		if (dspectrum == null) {
		    try { Thread.sleep(cfgDelay); }
		    catch (InterruptedException e) { return; }
		    catch (Exception e) {}
		    continue;
		}
		if (Thread.interrupted()) return;
		if (dspectrum != null) {
		    spectrum = dspectrum;
		    try {
			if (spectrum.count != 0) {
			    specFilterPanel.setSpectrum(spectrum);
			    infoPanel.setSpectrum(spectrum);
			    specDiagram.setSpectrum(spectrum);
			}
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		    try { Thread.sleep(cfgDelay); }
		    catch (InterruptedException e) { return; }
		    catch (Exception e) {}
		}
	    }
	}
    }

    protected ReplySpectrum provideSpectrum(ReplySpectrum spectrum) {
	synchronized (this) {
	    ReplySpectrum ospectrum = this.spectrum;
	    this.spectrum = spectrum;
	    this.specAvailable = true;
	    return ospectrum;
	}
    }

    protected ReplySpectrum consumeSpectrum(ReplySpectrum spectrum) {
	synchronized (this) {
	    if (!specAvailable) return null;
	    ReplySpectrum ospectrum = this.spectrum;
	    this.spectrum = spectrum;
	    this.specAvailable = false;
	    return ospectrum;
	}
    }

    protected JPanel createSpectrumPanel(int width, int height) {
	specDiagram = new SpectrumDiagram(width, height, Diagram.LEFT, Diagram.RIGHT, Diagram.BOTTOM + 25, Diagram.TOP);
	specDiagram.enableXAxis(true, true, numFormatD1);
	specDiagram.enableYAxis(true, true, numFormatD1);
	specDiagram.setFilterPanel(specFilterPanel);
	JPanel specPanel = new JPanel();
	specPanel.setBorder(BorderFactory.createTitledBorder("Spectrum"));
	specPanel.add(specDiagram);
	return specPanel;
    }

    // ##########################################################
    // ### samples related stuff
    // ##########################################################
    protected JPanel createSamplesPanel(int width, int height) {
	sampDiagram = new SamplesDiagram(width, height, Diagram.LEFT, Diagram.RIGHT, Diagram.BOTTOM + 25, Diagram.TOP);
	sampDiagram.enableXAxis(true, true, numFormatD1);
	sampDiagram.enableYAxis(true, true, numFormatD1);
	sampDiagram.setFilterPanel(sampFilterPanel);
	JPanel sampPanel = new JPanel();
	sampPanel.setBorder(BorderFactory.createTitledBorder("Samples"));
	sampPanel.add(sampDiagram);
	return sampPanel;
    }

    protected class SamplesMessageLoop implements Runnable {
	public ReplySamples    samples = new ReplySamples();
	public SamplesMessageLoop() {}
	public void run() {
	    for ( ; ; ) {
		ReplySamples nsamples = sampService.sendGetSamples(samples, sampFilterPanel.getFeed());
		if ((nsamples != null) && (nsamples.tag == 'R')) samples = provideSamples(nsamples);
		try { Thread.sleep(cfgDelay); }
		catch (InterruptedException e) { return; }
		catch (Exception e) {}
	    }
	}

    }

    protected class SamplesDiagramLoop implements Runnable {
	public ReplySamples samples = new ReplySamples();
	public SamplesDiagramLoop() {}
	public void run() {
	    for ( ; ; ) {
		ReplySamples dsamples = consumeSamples(samples);
		if (dsamples == null) {
		    try { Thread.sleep(cfgDelay); }
		    catch (InterruptedException e) { return; }
		    catch (Exception e) {}
		    continue;
		}
		if (Thread.interrupted()) return;
		if (dsamples != null) {
		    samples = dsamples;
		    try {
			if (samples.nsamples != 0) {
			    sampFilterPanel.setSamples(samples);
			    infoPanel.setSamples(samples);
			    sampDiagram.setSamples(samples);
			}
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		    try { Thread.sleep(cfgDelay); }
		    catch (InterruptedException e) { return; }
		    catch (Exception e) {}
		}
	    }
	}
    }

    protected ReplySamples provideSamples(ReplySamples samples) {
	synchronized (this) {
	    ReplySamples osamples = this.samples;
	    this.samples = samples;
	    this.sampAvailable = true;
	    return osamples;
	}
    }

    protected ReplySamples consumeSamples(ReplySamples samples) {
	synchronized (this) {
	    if (!sampAvailable) return null;
	    ReplySamples osamples = this.samples;
	    this.samples = samples;
	    this.sampAvailable = false;
	    return osamples;
	}
    }


    // ##########################################################
    // ### common stuff
    // ##########################################################
    public void createGUI() {
	createNumberFormats();
	int row = 0;
	int col = 0;

	Diagram.setTouch(cfgWithTouch);
	
	specConnectionPanel.createGUI(cfgWithTouch);
	specConnectionPanel.setHost(cfgHost);
	specConnectionPanel.setPort(cfgPort);
	specConnectionPanel.setStartStopHandler(this);
       	specFilterPanel.createGUI(cfgWithTouch);
	specFilterPanel.setCount(cfgFilterCount);
	specFilterPanel.setBinning(cfgFilterBinning);

	sampConnectionPanel.createGUI(cfgWithTouch);
	sampConnectionPanel.setHost(cfgHost);
	sampConnectionPanel.setPort(cfgPort);
	sampConnectionPanel.setStartStopHandler(this);
	sampFilterPanel.createGUI(cfgWithTouch);
	sampFilterPanel.setFeed(cfgFeed);

	infoPanel.createGUI();
	JPanel  specPanel = createSpectrumPanel(cfgPanelWidthSingle, cfgPanelHeightSingle);
	JPanel  sampPanel = createSamplesPanel(cfgPanelWidthSingle, cfgPanelHeightSingle);
	JButton btnHelp   = createHelpButton();

	GridBagPanel card;

	card = new GridBagPanel();
	card.grid(specPanel,     0, 0, "tlr");
	cardPanel.addTab("Spectrum", card);
	card = new GridBagPanel();
	card.grid(sampPanel,     0, 0, "tlr");
	cardPanel.addTab("Samples", card);

	// first line: spectrum related stuff
	mainPanel.grid(specConnectionPanel,      0, 0, 2, 1, "l");
	mainPanel.grid(specFilterPanel,          2, 0, 1, 1, "lr");
	mainPanel.grid(btnHelp,                  3, 0, 1, 1, "tr");
	// second line: samples related stuff
	mainPanel.grid(sampConnectionPanel,      0, 1, 2, 1, "l");
	mainPanel.grid(sampFilterPanel,          2, 1, 1, 1, "lr");
	//third line: info and stacked diagram panels
	mainPanel.grid(infoPanel,                0, 2, 1, 1, "tblr");
	mainPanel.grid(cardPanel,                1, 2, 3, 1, "tblr");

	getContentPane().add(mainPanel, BorderLayout.CENTER);
        pack();
        setVisible(true);
    }

    protected void handleStartStopSpectrum() {
	Dimension size = getSize();
	//System.out.println("Monitor.createGUI(): size = " + size.width + " x " + size.height);
	if (specService.isConnected()) {
	    // RUNNING -> STOPPED
	    try {
		if (specMessageThread != null) {
		    specMessageThread.interrupt();
		    specMessageThread.join();
		    specMessageThread = null;
		}
		if (specDiagramThread != null) {
		    specDiagramThread.interrupt();
		    specDiagramThread.join();
		    specDiagramThread = null;
		}
		specService.disconnect();
		specConnectionPanel.isStopped();
	    } catch (Exception ex) {}
	} else {
	    // STOPPED -> RUNNING
	    try {
		String connHost = specConnectionPanel.getHost();
		int    connPort = specConnectionPanel.getPort();
		if (specService.connect(connHost, connPort)) {
		    ping = specService.sendPing(ping);
		    specFeatures = specService.sendGetFeatures(specFeatures);
		    if (specFeatures != null) {
			infoPanel.setFeatures(specFeatures);
		    }
		    specDiagramThread = new Thread(new SpectrumDiagramLoop());
		    specDiagramThread.start();
		    specMessageThread = new Thread(new SpectrumMessageLoop());
		    specMessageThread.start();
		    specConnectionPanel.isStarted();
		}
	    } catch (Exception ex) {}
	}
    }

    protected void handleStartStopSamples() {
	Dimension size = getSize();
	//System.out.println("Monitor.createGUI(): size = " + size.width + " x " + size.height);
	if (sampService.isConnected()) {
	    // RUNNING -> STOPPED
	    try {
		if (sampMessageThread != null) {
		    sampMessageThread.interrupt();
		    sampMessageThread.join();
		    sampMessageThread = null;
		}
		if (sampDiagramThread != null) {
		    sampDiagramThread.interrupt();
		    sampDiagramThread.join();
		    sampDiagramThread = null;
		}
		sampService.disconnect();
		sampConnectionPanel.isStopped();
	    } catch (Exception ex) {}
	} else {
	    // STOPPED -> RUNNING
	    try {
		String connHost = sampConnectionPanel.getHost();
		int    connPort = sampConnectionPanel.getPort();
		if (sampService.connect(connHost, connPort)) {
		    ping = sampService.sendPing(ping);
		    sampFeatures = sampService.sendGetFeatures(sampFeatures);
		    if (sampFeatures != null) {
			infoPanel.setFeatures(sampFeatures);
		    }
		    sampDiagramThread = new Thread(new SamplesDiagramLoop());
		    sampDiagramThread.start();
		    sampMessageThread = new Thread(new SamplesMessageLoop());
		    sampMessageThread.start();
		    sampConnectionPanel.isStarted();
		}
	    } catch (Exception ex) {}
	}
    }

    public void actionPerformed(ActionEvent e) {
	if (e.getSource() == specConnectionPanel.getStartStopButton()) {
	    handleStartStopSpectrum();
	} else if (e.getSource() == sampConnectionPanel.getStartStopButton()) {
	    handleStartStopSamples();
	} else if (e.getSource() == btnHelp) {
	    HelpDialog.showHelp(" Monitor", "Monitor.html");
	}
    }

    static protected void usage() {
	System.out.println("usage: java mon.Monitor ( -help | <options> )");
	System.out.println("  -help");
	System.out.println("     shows this text");
	System.out.println("the following options are available:");
	System.out.println("  -host <host>");
	System.out.println("     sets the initial hostname in the connection panel (default localhost)");
	System.out.println("  -port <port>");
	System.out.println("     sets the initial port number in the connection panel (default " + Service.PORT + ")");
	System.out.println("  -delay <delay>");
	System.out.println("     sets the delay between each communication and GUI update ([ms], default 50 ms)");
	System.out.println("  -size ( small | medium | large )");
	System.out.println("     specifies the size of the GUI:");
	System.out.println("     small  : the main diagrams will show 400 values, fits on a 800x480 pixel display");
	System.out.println("     medium : the main diagrams will show 512 values, needs about 900*500 pixels");
	System.out.println("     large  : the main diagrams will show 1024 values");
    }

    public static void main(String[] args) {
        final Monitor monitor = new Monitor(" Monitor");
	int idx = 0;
	monitor.setHost("localhost");
	monitor.setPort(Service.PORT);
	monitor.setGUISize("l");
	if ((args.length == 1) && (args[0].equals("-help"))) {
	    usage();
	    return;
	}
	while (idx < args.length) {
	    if (args[idx].equals("-host")) {
		idx++;
		if (idx < args.length) {
		    monitor.setHost(args[idx]);
		    idx++;
		}
	    } else if (args[idx].equals("-port")) {
		idx++;
		if (idx < args.length) {
		    try {
			monitor.setPort(Integer.parseInt(args[idx]));
		    } catch (Exception e) {
		    }
		    idx++;
		}
	    } else if (args[idx].equals("-delay")) {
		idx++;
		if (idx < args.length) {
		    try {
			monitor.setDelay(Long.parseLong(args[idx]));
		    } catch (Exception e) {
		    }
		    idx++;
		}
	    } else if (args[idx].equals("-size")) {
		idx++;
		if (idx < args.length) {
		    monitor.setGUISize(args[idx]);
		    idx++;
		}
	    } else if (args[idx].equals("-touch")) {
		idx++;
		monitor.setWithTouch(true);
	    } else {
		idx++;
	    }
	}
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    monitor.createGUI();
                }
            });
    }


}
