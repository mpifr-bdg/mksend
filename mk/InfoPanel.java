package mk;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;


public class InfoPanel extends GridBagPanel {

    // Spectrum feature related informations
    protected JTextField   tfNSpectra       = null;
    protected JTextField   tfNChannels      = null;
    protected JTextField   tfNCounters      = null;
    protected JTextField   tfBandwidth      = null;
    // Samples feature related informations
    protected JTextField   tfNFeeds         = null;
    protected JTextField   tfNBlocks        = null;
    protected JTextField   tfNSamples       = null;
    protected JTextField   tfNBits          = null;
    protected JTextField   tfNScis          = null;
    // Spectrum related informations
    protected JTextField   tfNReceived      = null;
    protected JTextField   tfNSaturated     = null;
    protected JTextField   tfFreqLow        = null;
    protected JTextField   tfFreqHigh       = null;
    protected JTextField   tfCount          = null;
    protected JTextField   tfBinning        = null;
    // Samples related informations
    protected JTextField   tfFeed           = null;

    public InfoPanel() {
	super("Infos");
    }

    protected JTextField createTextField(String content) {
	JTextField tf = new JTextField(content);
	tf.setEditable(false);
	tf.setColumns(7);
	tf.setHorizontalAlignment(JTextField.RIGHT);
	return tf;
    }

    public void createGUI() {
	int row, col;

	super.createGUI();
	// Spectrum feature related informations
	tfNSpectra   = createTextField("999");
	tfNChannels  = createTextField("999");
	tfNCounters  = createTextField("999");
	tfBandwidth  = createTextField("9999");
	// Samples feature related informations
	tfNFeeds     = createTextField("999");
	tfNBlocks    = createTextField("999");
	tfNSamples   = createTextField("9999");
	tfNBits      = createTextField("99");
	tfNScis      = createTextField("99");
	// Spectrum related informations
	tfNReceived  = createTextField("999999");
	tfNSaturated = createTextField("999999");
	tfFreqLow    = createTextField("9999.9");
	tfFreqHigh   = createTextField("9999.9");
	tfCount      = createTextField("99999");
	tfBinning    = createTextField("99");
	// Samples related informations
	tfFeed       = createTextField("999");

	row = 0; col = 0;
	// Spectrum feature related informations
	grid(new JLabel("# Spectra:"),   col++, row, "r"); grid(tfNSpectra,   col++, row, "lr"); row++; col = 0;
	grid(new JLabel("# Channels:"),  col++, row, "r"); grid(tfNChannels,  col++, row, "lr"); row++; col = 0;
	grid(new JLabel("# Counters:"),  col++, row, "r"); grid(tfNCounters,  col++, row, "lr"); row++; col = 0;
	grid(new JLabel("Bandwidth:"),   col++, row, "r"); grid(tfBandwidth,  col++, row, "lr"); grid(new JLabel("MHz"), col++, row, "l"); row++; col = 0;
	// Samples feature related informations
	grid(new JLabel("# Feeds:"),     col++, row, "r"); grid(tfNFeeds,     col++, row, "lr"); row++; col = 0;
	grid(new JLabel("# Blocks:"),    col++, row, "r"); grid(tfNBlocks,    col++, row, "lr"); row++; col = 0;
	grid(new JLabel("# Samples:"),   col++, row, "r"); grid(tfNSamples,   col++, row, "lr"); row++; col = 0;
	grid(new JLabel("# Bits:"),      col++, row, "r"); grid(tfNBits,      col++, row, "lr"); row++; col = 0;
	grid(new JLabel("# SCI:"),       col++, row, "r"); grid(tfNScis,      col++, row, "lr"); row++; col = 0;
	// Spectrum related informations
	grid(new JLabel("# Received:"),  col++, row, "r"); grid(tfNReceived,  col++, row, "lr"); row++; col = 0;
	grid(new JLabel("# Saturated:"), col++, row, "r"); grid(tfNSaturated, col++, row, "lr"); row++; col = 0;
	grid(new JLabel("Freq Low:"),    col++, row, "r"); grid(tfFreqLow,    col++, row, "lr"); grid(new JLabel("MHz"), col++, row, "l"); row++; col = 0;
	grid(new JLabel("Freq High:"),   col++, row, "r"); grid(tfFreqHigh,   col++, row, "lr"); grid(new JLabel("MHz"), col++, row, "l"); row++; col = 0;
	grid(new JLabel("Count:"),       col++, row, "r"); grid(tfCount,      col++, row, "lr"); row++; col = 0;
	grid(new JLabel("Binning:"),     col++, row, "r"); grid(tfBinning,    col++, row, "lr"); row++; col = 0;
	// Samples related informations
	grid(new JLabel("Feed:"),        col++, row, "r"); grid(tfFeed,       col++, row, "lr"); row++; col = 0;
	// the lower part is a resizable JPanel
	setDynamicRow(row);
	grid(new JPanel(), col++, row, "tblr");
    }

    public void setFeatures(ReplyFeatures features) {
	if (features.nspectra != 0) {
	    // spectrum related features
	    tfNSpectra.setText("" + features.nspectra);
	    tfNChannels.setText("" + features.nchannels);
	    tfNCounters.setText("" + features.ncounters);
	    tfBandwidth.setText(numFormatD1.format(features.bandwidth));
	}
	if (features.nfeeds != 0) {
	    // samples related features
	    tfNFeeds.setText("" + features.nfeeds);
	    tfNBlocks.setText("" + features.nblocks);
	    tfNSamples.setText("" + features.nsamples);
	    tfNBits.setText("" + features.nbits);
	    tfNScis.setText("" + features.nscis);
	}
    }

    public void setSpectrum(ReplySpectrum spectrum) {
	tfNReceived.setText("" + spectrum.nreceived);
	tfNSaturated.setText("" + spectrum.nsaturated);
	tfFreqLow.setText(numFormatD1.format(spectrum.freq_low));
	tfFreqHigh.setText(numFormatD1.format(spectrum.freq_high));
	tfCount.setText("" + spectrum.count);
	tfBinning.setText("" + spectrum.binning);
    }

    public void setSamples(ReplySamples samples) {
	tfFeed.setText("" + samples.feed);
    }

}

