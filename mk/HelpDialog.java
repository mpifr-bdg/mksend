package mk;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.HyperlinkEvent;
import javax.swing.text.html.HTMLFrameHyperlinkEvent;
import javax.swing.text.html.HTMLDocument;

import java.util.Stack;

import java.net.URL;

public class HelpDialog extends JDialog implements HyperlinkListener, ActionListener {

    protected JPanel      panel      = null;
    protected JPanel      btnPanel   = null;
    protected JButton     btnMain    = null;
    protected JButton     btnBack    = null;
    protected JEditorPane helpView   = null;
    protected JScrollPane helpScroll = null;
    protected URL         urlMain    = null;
    protected Stack<URL>  urlStack   = new Stack<URL>();

    private HelpDialog() {
        super((JFrame)null, false);
    }

    public void createGUI() {
	panel = new JPanel();
	panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
	// create the button panel
	btnPanel = new JPanel();
	btnPanel.setLayout(new FlowLayout(FlowLayout.LEADING, 0, 0));
	btnMain = new JButton("M");
	btnMain.setMargin(new Insets(0,0,0,0));
	btnMain.setMinimumSize(new Dimension(25, 25));
	btnMain.setPreferredSize(new Dimension(25, 25));
	btnMain.setMaximumSize(new Dimension(25, 25));
	btnMain.addActionListener(this);
	btnBack = new JButton("<");
	btnBack.setMargin(new Insets(0,0,0,0));
	btnBack.setMinimumSize(new Dimension(25, 25));
	btnBack.setPreferredSize(new Dimension(25, 25));
	btnBack.setMaximumSize(new Dimension(25, 25));
	btnBack.addActionListener(this);
	btnPanel.add(btnMain);
	btnPanel.add(btnBack);

        // create the help panel
        helpView = new JEditorPane();
	helpView.setEditable(false);
	helpView.addHyperlinkListener(this);
        JScrollPane helpScroll = new JScrollPane(helpView);
        //helpScroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
        helpScroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
	helpScroll.setPreferredSize(new Dimension(750, 350));
	helpScroll.setMinimumSize(new Dimension(10, 10));

	// create the overall layout
	panel.add(btnPanel);
	panel.add(helpScroll);
	setContentPane(panel);
        pack();
    }

    public void show(String title, String node) {
	setTitle(title);
	urlStack.clear();
	urlMain = HelpDialog.class.getResource("html/" + node);
	if (urlMain != null) {
	    try {
		System.out.println("show(): show " + urlMain + " node");
		urlStack.push(urlMain);
		helpView.setPage(urlMain);
	    } catch (java.io.IOException e) {
		System.err.println("Attempted to read a bad URL: " + urlMain);
	    }
	} else {
	    System.err.println("Couldn't find file: " + node);
	}
	setVisible(true);
    }

    public void hyperlinkUpdate(HyperlinkEvent e) {
	if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
	    JEditorPane pane = (JEditorPane) e.getSource();
	    if (e instanceof HTMLFrameHyperlinkEvent) {
		HTMLFrameHyperlinkEvent  evt = (HTMLFrameHyperlinkEvent)e;
		HTMLDocument doc = (HTMLDocument)helpView.getDocument();
		System.out.println("hyperlinkUpdate(): processing a HTMLFrameHyperlinkEvent");
		doc.processHTMLFrameHyperlinkEvent(evt);
	    } else {
		try {
		    System.out.println("hyperlinkUpdate(): process a " + e.getURL() + " event");
		    urlStack.push(e.getURL());
		    helpView.setPage(e.getURL());
		} catch (Throwable t) {
		    t.printStackTrace();
		}
	    }
	}
    }

    protected void handleMain() {
	if (urlMain == null) return;
	urlStack.clear();
	try {
	    System.out.println("show(): show " + urlMain + " node");
	    urlStack.push(urlMain);
	    helpView.setPage(urlMain);
	} catch (java.io.IOException e) {
	    System.err.println("Attempted to read a bad URL: " + urlMain);
	}
    }

    protected void handleBack() {
	if (urlStack.size() <= 1) return;
	urlStack.pop();
	URL url = urlStack.peek();
	try {
	    System.out.println("show(): show " + url + " node");
	    helpView.setPage(url);
	} catch (java.io.IOException e) {
	    System.err.println("Attempted to read a bad URL: " + url);
	}
    }

    public void actionPerformed(ActionEvent e) {
	if (e.getSource() == btnMain) {
	    handleMain();
	} else if (e.getSource() == btnBack) {
	    handleBack();
	}
    }

    static private HelpDialog helpDialog = null;

    static public void showHelp(String title, String node) {
	if (helpDialog == null) {
	    helpDialog = new HelpDialog();
	    helpDialog.createGUI();
	}
	helpDialog.show(title, node);
    }
}
