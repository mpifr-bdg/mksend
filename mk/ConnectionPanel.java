package mk;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

public class ConnectionPanel extends Panel implements MouseListener {

    protected JTextField   tfHost           = null;
    protected JTextField   tfPort           = null;
    protected JButton      btnStartStop     = null;
    //protected JButton      btnHelp          = null;

    public ConnectionPanel(String label) {
	super(label);
    }

    public void createGUI(boolean withTouch) {
	super.createGUI();
	setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
	tfHost = new JTextField("localhost");
	tfHost.setColumns(8);
	if (withTouch) {
	    tfHost.setEditable(false);
	    tfHost.addMouseListener(this);
	}
	//tfHost.addActionListener(this);
	tfPort = new JTextField("" + Service.PORT);
	tfPort.setColumns(4);
	if (withTouch) {
	    tfPort.setEditable(false);
	    tfPort.addMouseListener(this);
	}
	btnStartStop = createButton("Start");
	btnStartStop.setMinimumSize(new Dimension(60,25));
	btnStartStop.setPreferredSize(new Dimension(60,25));
	btnStartStop.setMaximumSize(new Dimension(60,25));
	//btnHelp = createButton("?");
	//btnHelp.setMinimumSize(new Dimension(25,25));
	//btnHelp.setPreferredSize(new Dimension(25,25));
	//btnHelp.setMaximumSize(new Dimension(25,25));
	//btnHelp.addActionListener(this);
	// Host + Port + Start/Stop
	add(new JLabel("Host:"));
	add(tfHost);
	add(new JLabel("Port:"));
	add(tfPort);
	add(btnStartStop);
	//add(btnHelp);
    }

    public void setStartStopHandler(ActionListener l) {
	btnStartStop.addActionListener(l);
    }

    public JButton getStartStopButton() { return btnStartStop; }

    public void isStarted() {
	btnStartStop.setText("Stop");
    }

    public void isStopped() {
	btnStartStop.setText("Start");
    }

    public String getHost() {
	return tfHost.getText();
    }

    public void setHost(String host) {
	tfHost.setText(host);
    }

    public int getPort() {
	try {
	    return Integer.parseInt(tfPort.getText());
	} catch (Exception e) {
	    return Service.PORT;
	}
    }

    public void setPort(int port) {
	tfPort.setText("" + port);
    }

    public void actionPerformed(ActionEvent e) {
	if (e.getSource() == tfHost) {
	    setHost(AlphaLowerCaseDialog.enterString("Host name", tfHost.getText()));
	    //} else if (e.getSource() == btnHelp) {
	    //HelpDialog.showHelp("Connection Panel", "ConnectionPanel.html");
	}
    }

    public void mouseEntered(MouseEvent me) { }

    public void mouseExited(MouseEvent me) { }

    public void mouseClicked(MouseEvent me) {
	if (me.getSource() == tfHost) {
	    setHost(AlphaLowerCaseDialog.enterString("Host name", tfHost.getText()));
	} else if (me.getSource() == tfPort) {
	    setPort(NumberDialog.enterFixnum("Port number", tfPort.getText()));
	}
    }

    public void mousePressed(MouseEvent me) { }

    public void mouseReleased(MouseEvent me) { }

}

