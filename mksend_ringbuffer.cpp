#include <unistd.h>
#include <chrono>
#include <functional>

#include "spead2/common_memory_allocator.h"

#include "ascii_header.h"

#include "mksend_ringbuffer.h"

namespace mksend
{

  ringbuffer::ringbuffer(key_t key,
			 psrdada_cpp::MultiLog &mlog,
			 std::shared_ptr<options> opts,
			 std::vector<std::unique_ptr<mksend::stream> > &streams)
    :
    psrdada_cpp::DadaInputStream<ringbuffer>(key, mlog, *this),
    opts(opts),
    streams(streams)
  {
    //infinite = true;
    //timestamp = opts->sample_start;
    _quiet = opts->quiet;
  }

  ringbuffer::~ringbuffer()
  {
  }

  /*
    The header contains the first timestamp of the first SPEAD heap in the first slot.
    It is necessary to extract the value in order to overwrite an optional value given
    as program parameter or configuration file key/value pair.
   */
  void ringbuffer::on_connect(psrdada_cpp::RawBytes& block)
  {
    std::size_t timestamp;   // timestamp for the heaps inside the current group

    // set the last character to '\0' (paranoia)
    block.ptr()[block.used_bytes() - 1] = '\0';
    if (ascii_header_get(block.ptr(), SAMPLE_START_KEY, "%ld", &timestamp) == 1)
      {
	opts->items[opts->indices[0]].value = timestamp;
      }
  }

  void ringbuffer::handle_end_of_transmission()
  {
    spead2::bug_compat_mask bug_compat = opts->pyspead ? spead2::BUG_COMPAT_PYSPEAD_0_5_2 : 0;
    spead2::flavour         f(spead2::maximum_version, 64, 48, bug_compat);
    std::size_t             iout;

    // Put an end heap into each output stream
    if (opts->network_mode == FULL_NETWORK_MODE)
      {
	for (iout = 0; iout < streams.size(); iout++)
	  {
	    spead2::send::heap  *h = new spead2::send::heap(f);
	    h->add_end();
	    streams.at(iout)->get_stream().async_send_heap(*h, [] (const boost::system::error_code &ec, spead2::item_pointer_t bytes_transferred) { (void)ec; (void)bytes_transferred; });
	    streams.at(iout)->flush();
	    delete h;
	  }
      }
    stop();
  }

  void ringbuffer::handle_send_heaps(psrdada_cpp::RawBytes& block)
  {
    std::size_t                bytes_to_read = block.used_bytes();
    std::uint8_t              *data = (std::uint8_t*)block.ptr();
    spead2::s_item_pointer_t  *sci = NULL;
    std::size_t                heap_group = opts->heap_group;
    std::size_t                nheaps = bytes_to_read/(opts->heap_size + opts->nsci*sizeof(spead2::s_item_pointer_t));
    std::size_t                iheap = 0;
    std::size_t                istr;
    int                        iitem;
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();

    if (nheaps%(heap_group*streams.size()) != 0)
      {
	std::cerr << "ERROR: the number of heaps inside a ringbuffer slot is not a multiple of heap_group ("
		  << heap_group
		  << ") and number of output streams ("
		  << streams.size()
		  << "), please adjust the PSR_DADA ringbuffer slot size!"
		  << std::endl;
	exit(-1);
      }
    if (!_quiet)
      {
	std::cout << "processing slot containing " << nheaps << " heaps, payload size " << opts->heap_size << " Bytes, including " << opts->nsci << " side-channel items" << std::endl;
      }
    sci = (spead2::s_item_pointer_t *)(data + bytes_to_read) - (nheaps*opts->nsci);
    while (iheap != nheaps)
      {
	// the next stream index to use is equal to _iout (it starts with 0 for the first heap of the first slot)
	// get a free heap from this stream, block until one is available
	_iout = (_iheap/heap_group)%streams.size();
	std::size_t  nr = streams.at(_iout)->get_free_heap();
	if (nr == stream::NO_HEAP_AVAILABLE) break;
	// copy the item pointer values from the side-channel
	opts->update_side_channel_items(sci, iheap);
	// Update the items in the available heap (immediate items and payload)
	for (iitem = 0; iitem < opts->nitems; iitem++)
	  {
	    if (opts->items[iitem].type == PAYLOAD_ITEM_TYPE)
	      { // add/modify the payload to a heap (should be the last item of a heap)
		streams.at(_iout)->set_item(nr,                           // free heap for stream iout
					    opts->items[iitem].id,        // id
					    data + opts->heap_size*iheap, // pointer to the payload
					    opts->heap_size);             // payload size
	      }
	    else
	      { // add/modify an immediate item to a heap
		streams.at(_iout)->set_item(nr,                        // free heap for stream iout
					    opts->items[iitem].id,     // id
					    opts->items[iitem].value); // immediate value
		//usleep(100000);
	      }
	  }
	// send the heap
	if (!_quiet)
	  {
	    std::cout << "sending heap " << iheap << " at mcast " << _iout << " timestamp " << opts->items[0].id << " " << opts->items[0].value << std::endl;
	  }
	if (!streams.at(_iout)->send_heap(nr))
	  {
	    std::cerr << "ERROR sending heap " << nr << " asyncronously vi stream " << _iout << std::endl;
	  }
        total_nheaps  += 1;
        total_payload += opts->heap_size;
	// update the index items values for the following heap (post-increment)
	opts->update_index_items();
	// update heap and stream indices
	iheap++;
	_iheap++;
	//_iout = (_iout + 1)%(streams.size());
      }
    // flush all output streams
    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
    for (istr = 0; istr < streams.size(); istr++)
      {
	streams.at(istr)->flush();
      }
    std::chrono::high_resolution_clock::time_point t3 = std::chrono::high_resolution_clock::now();
    total_send  += std::chrono::duration_cast<std::chrono::nanoseconds>( t3 - t1 ).count();
    total_flush += std::chrono::duration_cast<std::chrono::nanoseconds>( t3 - t2 ).count();
  }

  void ringbuffer::on_next(psrdada_cpp::RawBytes& block)
  {
    if (block.used_bytes() == block.total_bytes())
      { // keep going on if a slot is completely filled
	handle_send_heaps(block);
      }
    else
      { // stop transmission if a slot is not fully used
	handle_end_of_transmission();
      }
  }

#define MEMORY_SIZE   1024*1024
  
  void ringbuffer::simulate()
  {
    std::cout << "simulate()" << std::endl;
    // some calculated values
    std::size_t                               nheaps = opts->heap_count*opts->ngroups_data;
    std::size_t                               memsize = (opts->heap_size + opts->nsci*sizeof(spead2::s_item_pointer_t))*nheaps;
    std::cout << "  nheaps " << nheaps << " memsize " << memsize << std::endl;
    // first allocate some memory
    std::shared_ptr<spead2::mmap_allocator>   memallocator = std::make_shared<spead2::mmap_allocator>(0, true);
    spead2::memory_allocator::pointer         mptr = memallocator->allocate(memsize, NULL);;
    psrdada_cpp::RawBytes                    *block = new psrdada_cpp::RawBytes((char*)mptr.get(), memsize, memsize);
    std::size_t                               bytes_to_read = block->used_bytes();
    std::uint8_t                             *data = (std::uint8_t*)block->ptr();
    spead2::s_item_pointer_t                 *sci = (spead2::s_item_pointer_t *)(data + bytes_to_read) - (nheaps*opts->nsci);
    // initial value item pointer values;
    int                                       i;
    std::size_t                               islot;
    std::size_t                               j, k;
    spead2::s_item_pointer_t                  ts_next = opts->sample_start; 
    spead2::s_item_pointer_t                  ts_step;
    spead2::s_item_pointer_t                  items[MAX_ITEMS];

    // fill the memory with an artificial header (we need a valid SAMPLE_START_KEY key/value pair)
    memset(data, 0, memsize);
    ascii_header_set((char*)data, SAMPLE_START_KEY, "%ld", ts_next);
    on_connect(*block);
    // get the difference between successive timestamps
    for (i = 0; i < opts->nitems; i++)
      {
	if (opts->items[i].type != SERIAL_ITEM_TYPE) continue;
	ts_step = opts->items[i].step;
      }
    // create an infinite number of artificial slots
    for (i = 0; i < MAX_ITEMS; i++)
      {
	items[i] = (spead2::s_item_pointer_t)(42*(i + 1));
      }
    std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
    //for (islot = 0; islot < opts->nslots; islot++) {
    islot = 0;
    while ((opts->nslots == 0) || (islot < opts->nslots)) {
      for (j = 0; j < nheaps/opts->heap_count; j++)
	{
	  items[0] = ts_next;
	  for (k = 0; k < opts->heap_count; k++)
	    {
	      std::size_t idx;
	      idx = (j*opts->heap_count + k)*opts->nsci;
	      for (i = 1; i < opts->nsci; i++)
		{
		  items[i] += 1;
		}
	      for (i = 0; i < opts->nsci; i++)
		{
		  sci[idx + i] = items[i];
		}
	      idx = (j*opts->heap_count + k)*opts->heap_size;
	      //memset(data + idx, (char)(j*opts->heap_count + k), opts->heap_size);
	    }
	  ts_next += ts_step;
	}
      on_next(*block);
      total_nslots += 1;
      islot++;
    }
    std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
    total_duration = std::chrono::duration_cast<std::chrono::nanoseconds>( t2 - t1 ).count();
    std::cout << "simulation run: " << total_nslots << " " << total_nheaps << " " << total_payload << " " << total_duration << " " << total_send << " " << total_flush << std::endl;
  }

}
