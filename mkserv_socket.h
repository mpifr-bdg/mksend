#ifndef mkserv_socket_h
#define mkserv_socket_h

#include <inttypes.h>
#include <pthread.h>

//#define WITH_TYPE_TAGS

#define MKTAG(c1,c2) ((((int16_t)c1) << 8) + (int16_t)c2)

namespace mkserv {

  const int MAX_PACKET_SIZE     = 32*1024;
  const int PORT                = 6000;
#ifdef WITH_TYPE_TAGS
  // char           's' 'S'
  // int8           'b' 'B'
  // int16          'c' 'C'
  // int32          'i' 'I'
  // int64          'e' 'E'
  // uint8          'f' 'F'
  // uint16         'g' 'G'
  // uint32         'h' 'H'
  // uint64         'i' 'I'
  // float          'f' 'F'
  // double         'd' 'D'
  // float complex  'c' 'C'
  // double complex 'm' 'M'
  constexpr int16_t SINGLE_CHAR_DT      = MKTAG('s', '1');
  constexpr int16_t MULTI_CHAR_DT       = MKTAG('S', '1');
  constexpr int16_t SINGLE_INT8_DT      = MKTAG('i', '1');
  constexpr int16_t MULTI_INT8_DT       = MKTAG('I', '1');
  constexpr int16_t SINGLE_INT16_DT     = MKTAG('i', '2');
  constexpr int16_t MULTI_INT16_DT      = MKTAG('I', '2');
  constexpr int16_t SINGLE_INT32_DT     = MKTAG('i', '4');
  constexpr int16_t MULTI_INT32_DT      = MKTAG('I', '4');
  constexpr int16_t SINGLE_INT64_DT     = MKTAG('i', '8');
  constexpr int16_t MULTI_INT64_DT      = MKTAG('I', '8');
  constexpr int16_t SINGLE_UINT8_DT     = MKTAG('u', '1');
  constexpr int16_t MULTI_UINT8_DT      = MKTAG('U', '1');
  constexpr int16_t SINGLE_UINT16_DT    = MKTAG('u', '2');
  constexpr int16_t MULTI_UINT16_DT     = MKTAG('U', '2');
  constexpr int16_t SINGLE_UINT32_DT    = MKTAG('u', '4');
  constexpr int16_t MULTI_UINT32_DT     = MKTAG('U', '4');
  constexpr int16_t SINGLE_UINT64_DT    = MKTAG('u', '8');
  constexpr int16_t MULTI_UINT64_DT     = MKTAG('U', '8');
  constexpr int16_t SINGLE_FLOAT32_DT   = MKTAG('f', '4');
  constexpr int16_t MULTI_FLOAT32_DT    = MKTAG('F', '4');
  constexpr int16_t SINGLE_FLOAT64_DT   = MKTAG('f', '8');
  constexpr int16_t MULTI_FLOAT64_DT    = MKTAG('F', '8');
  constexpr int16_t SINGLE_COMPLEX32_DT = MKTAG('c', '4');
  constexpr int16_t MULTI_COMPLEX32_DT  = MKTAG('C', '4');
  constexpr int16_t SINGLE_COMPLEX64_DT = MKTAG('c', '8');
  constexpr int16_t MULTI_COMPLEX64_DT  = MKTAG('C', '8');
#endif /* WITH_TYPE_TAGS */
  
  class socket {
  protected:
    int        _lsock;             // Dieser Socket wird von Listen() verwendet
    int        _sock;              // Dieser Socket wird zur eigentlichen Kommunikation verwendet (nach Accept() und Open()
    size_t     _buffer_size;       // Die allokierte Groesses des Puffers (mit einer bestimmten Granularitaet)
    char      *_buffer;            // Der eigentliche Puffer
    size_t     _buffer_used;       // Der aktuelle Fuellstand des Puffers
    size_t     _buffer_read_pos;   // Die aktuelle Leseposition, wird nach jedem Get() veraendert
    size_t     _buffer_write_pos;  // Die aktuelle Schreibposition, wird nach jedem Put() veraendert
  public:
    socket();
    socket(int sock);
    ~socket();
    inline int lfd() { return _lsock; };
    inline int cfd() { return _sock;  };
    // open a client socket connection
    int connect(const char *name, int port, int timeout = 0);
    // open a server socket connection
    int listen(int port);
    // connect to a client
    int accept();
    void finish_server();
    void finish_client();
    void start_writing();
    int send();
    int receive(size_t n);
    void start_reading();
    // writing single values into a message (direct value)
#ifdef WITH_TYPE_TAGS
    inline int write(char     value) { return put(SINGLE_CHAR_DT,    (int8_t)value);       };
    inline int write(int16_t  value) { return put(SINGLE_INT16_DT,   value);               };
    inline int write(int32_t  value) { return put(SINGLE_INT32_DT,   value);               };
    inline int write(int64_t  value) { return put(SINGLE_INT64_DT,   value);               };
    inline int write(uint64_t value) { return put(SINGLE_UINT64_DT,  value);               };
    inline int write(float    value) { return put(SINGLE_FLOAT32_DT, *((int32_t*)&value)); };
#else
    inline int write(char     value) { return put((int8_t)value);       };
    inline int write(int16_t  value) { return put(value);               };
    inline int write(int32_t  value) { return put(value);               };
    inline int write(int64_t  value) { return put(value);               };
    inline int write(uint64_t value) { return put(value);               };
    inline int write(float    value) { return put(*((int32_t*)&value)); };
#endif /* WITH_TYPE_TAGS */
    // reading single values from a message (reference)
#ifdef WITH_TYPE_TAGS
    inline int read(char     &value) { return get(SINGLE_CHAR_DT,    (int8_t*)&value);  };
    inline int read(int16_t  &value) { return get(SINGLE_INT16_DT,   &value);           };
    inline int read(int32_t  &value) { return get(SINGLE_INT32_DT,   &value);           };
    inline int read(int64_t  &value) { return get(SINGLE_INT64_DT,   &value);           };
    inline int read(uint64_t &value) { return get(SINGLE_UINT64_DT,  &value);           };
    inline int read(float    &value) { return get(SINGLE_FLOAT32_DT, (int32_t*)&value); };
#else
    inline int read(char     &value) { return get((int8_t*)&value);  };
    inline int read(int16_t  &value) { return get(&value);           };
    inline int read(int32_t  &value) { return get(&value);           };
    inline int read(int64_t  &value) { return get(&value);           };
    inline int read(uint64_t &value) { return get(&value);           };
    inline int read(float    &value) { return get((int32_t*)&value); };
#endif /* WITH_TYPE_TAGS */
    // writing multiple values into a message
#ifdef WITH_TYPE_TAGS
    inline int write(char     *values, size_t count) { return put(MULTI_CHAR_DT,    (int8_t*)values,  count); };
    inline int write(int16_t  *values, size_t count) { return put(MULTI_INT16_DT,   values,           count); };
    inline int write(int32_t  *values, size_t count) { return put(MULTI_INT32_DT,   values,           count); };
    inline int write(int64_t  *values, size_t count) { return put(MULTI_INT64_DT,   values,           count); };
    inline int write(uint64_t *values, size_t count) { return put(MULTI_UINT64_DT,  values,           count); };
    inline int write(float    *values, size_t count) { return put(MULTI_FLOAT32_DT, (int32_t*)values, count); };
#else
    inline int write(char     *values, size_t count) { return put((int8_t*)values,  count); };
    inline int write(int16_t  *values, size_t count) { return put(values,           count); };
    inline int write(int32_t  *values, size_t count) { return put(values,           count); };
    inline int write(int64_t  *values, size_t count) { return put(values,           count); };
    inline int write(uint64_t *values, size_t count) { return put(values,           count); };
    inline int write(float    *values, size_t count) { return put((int32_t*)values, count); };
#endif /* WITH_TYPE_TAGS */
    // reading multiple values from a message
#ifdef WITH_TYPE_TAGS
    inline int read(char      *values, size_t count) { return get(MULTI_CHAR_DT,    (int8_t*)values,  count); };
    inline int read(int16_t   *values, size_t count) { return get(MULTI_INT16_DT,   values,           count); };
    inline int read(int32_t   *values, size_t count) { return get(MULTI_INT32_DT,   values,           count); };
    inline int read(int64_t   *values, size_t count) { return get(MULTI_INT64_DT,   values,           count); };
    inline int read(uint64_t  *values, size_t count) { return get(MULTI_UINT64_DT,  values,           count); };
    inline int read(float     *values, size_t count) { return get(MULTI_FLOAT32_DT, (int32_t*)values, count); };
#else
    inline int read(char      *values, size_t count) { return get((int8_t*)values,  count); };
    inline int read(int16_t   *values, size_t count) { return get(values,           count); };
    inline int read(int32_t   *values, size_t count) { return get(values,           count); };
    inline int read(int64_t   *values, size_t count) { return get(values,           count); };
    inline int read(uint64_t  *values, size_t count) { return get(values,           count); };
    inline int read(float     *values, size_t count) { return get((int32_t*)values, count); };
#endif /* WITH_TYPE_TAGS */
  protected:
    int reserve_space(size_t size);
    // 8-bit values
    int dput(int8_t  value);
    int dget(int8_t *value);
#ifdef WITH_TYPE_TAGS
    int put(int16_t tag, int8_t   value);
    int put(int16_t tag, int8_t  *values, size_t count);
    int get(int16_t tag, int8_t  *value);
    int get(int16_t tag, int8_t  *values, size_t count);
#else
    int put(int8_t   value);
    int put(int8_t  *values, size_t count);
    int get(int8_t  *value);
    int get(int8_t  *values, size_t count);
#endif /* WITH_TYPE_TAGS */
    // 16-bit values
    int dput(int16_t  value);
    int dget(int16_t *value);
#ifdef WITH_TYPE_TAGS
    int put(int16_t tag, int16_t  value);
    int put(int16_t tag, int16_t *values, size_t count);
    int get(int16_t tag, int16_t *value);
    int get(int16_t tag, int16_t *values, size_t count);
#else
    int put(int16_t  value);
    int put(int16_t *values, size_t count);
    int get(int16_t *value);
    int get(int16_t *values, size_t count);
#endif /* WITH_TYPE_TAGS */
    // 32-bit values
    int dput(int32_t  value);
    int dget(int32_t *value);
#ifdef WITH_TYPE_TAGS
    int put(int16_t tag, int32_t  value);
    int put(int16_t tag, int32_t *values, size_t count);
    int get(int16_t tag, int32_t *value);
    int get(int16_t tag, int32_t *values, size_t count);
#else
    int put(int32_t  value);
    int put(int32_t *values, size_t count);
    int get(int32_t *value);
    int get(int32_t *values, size_t count);
#endif /* WITH_TYPE_TAGS */
    // 64-bit values
    int dput(int64_t  value);
    int dget(int64_t *value);
    int dput(uint64_t  value);
    int dget(uint64_t *value);
#ifdef WITH_TYPE_TAGS
    int put(int16_t tag, int64_t  value);
    int put(int16_t tag, int64_t *values, size_t count);
    int get(int16_t tag, int64_t *value);
    int get(int16_t tag, int64_t *values, size_t count);
    int put(int16_t tag, uint64_t  value);
    int put(int16_t tag, uint64_t *values, size_t count);
    int get(int16_t tag, uint64_t *value);
    int get(int16_t tag, uint64_t *values, size_t count);
#else
    int put(int64_t  value);
    int put(int64_t *values, size_t count);
    int get(int64_t *value);
    int get(int64_t *values, size_t count);
    int put(uint64_t  value);
    int put(uint64_t *values, size_t count);
    int get(uint64_t *value);
    int get(uint64_t *values, size_t count);
#endif /* WITH_TYPE_TAGS */
  };
  
}

#endif /* mkserv_socket_h */
