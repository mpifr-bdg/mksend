#ifndef mksend_stream_h
#define mksend_stream_h

#include <iostream>
#include <limits>
#include <cstdint>
#include <cstdlib>
#include <vector>

#include <boost/interprocess/sync/interprocess_semaphore.hpp>

#include <spead2/common_endian.h>
#include <spead2/common_thread_pool.h>
#include <spead2/common_defines.h>
#include <spead2/common_flavour.h>
#include <spead2/send_heap.h>
#include <spead2/send_stream.h>

#include "mksend_heap.h"

namespace mksend
{

  class stream
  {
  public:
    static const std::size_t NO_HEAP_AVAILABLE = std::numeric_limits<std::size_t>::max();
  protected:
    std::unique_ptr<spead2::send::stream>        _stream;
    std::size_t                                  _nbheaps;
    boost::interprocess::interprocess_semaphore  _sem;
    std::mutex                                   _mutex;
    std::vector<std::unique_ptr<mksend::heap> >  _heaps;
    std::vector<bool>                            _used;
    std::size_t                                  _head;
    spead2::item_pointer_t                       _id_next;
    spead2::item_pointer_t                       _id_step;
  public:
    stream(std::unique_ptr<spead2::send::stream> stream, std::size_t nbheaps);
    ~stream();
    void set_cnt_sequence(spead2::item_pointer_t next, spead2::item_pointer_t step);
    spead2::send::stream &get_stream();
    void create_heaps(const spead2::flavour &f);
    std::size_t get_free_heap();
    //std::size_t get_free_heap(const boost::posix_time::ptime &abs_time);
    void set_item(std::size_t nr, spead2::s_item_pointer_t id, spead2::s_item_pointer_t value);
    void set_item(std::size_t nr, spead2::s_item_pointer_t id, const std::uint8_t  *ptr, std::size_t length);
    bool send_heap(std::size_t nr);
    void heap_finished(std::size_t nr);
    void flush();
  };
  
}

#endif /* mksend_stream_h */
