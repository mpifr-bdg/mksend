/* gcc -c mkserv_logging.cpp */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <syslog.h>
#include <errno.h>

#include "mkserv_logging.h"

namespace mkserv {

  static char gApplication[256] = "monFFTS";

  void set_application(const char *name)
  {
    strncpy(gApplication, name, sizeof(gApplication));
    gApplication[sizeof(gApplication) - 1] = '\0';
  }

  void error(const char* filename,
	     unsigned linenumber,
	     const char* text)
  {
    int errornumber = errno;
    //if (errno == 0) return;
    perror(text);
    openlog(gApplication, LOG_PERROR | LOG_PID, LOG_LOCAL5);
    syslog(LOG_ERR,"[ERROR] %s %i: errno tells: %s, %s",
	   filename, linenumber,
	   strerror(errornumber),
	   text);
    closelog();
  }

}
