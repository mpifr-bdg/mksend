

#include <string.h>

#include <iostream>     // std::cout
#include <fstream>      // std::ifstream

#include "ascii_header.h"

#include "mksend_options.h"


namespace mksend
{

  /*
  template<typename T>
  static po::typed_value<T> *make_opt(T &var)
  {
    return po::value<T>(&var)->default_value(var);
  }
  */

  template<typename T>
  static po::typed_value<T> *make_opt(T &var)
  {
    return po::value<T>(&var);
  }

  /*
  static po::typed_value<bool> *make_opt(bool &var)
  {
    return po::bool_switch(&var)->default_value(var);
  }
  */

  static po::typed_value<bool> *make_opt(bool &var)
  {
    return po::bool_switch(&var);
  }

  options::options()
  {
    std::size_t i;

    header = new char[DADA_DEFAULT_HEADER_SIZE + DADA_DEFAULT_HEADER_SIZE];
    for (i = 0; i < DADA_DEFAULT_HEADER_SIZE + DADA_DEFAULT_HEADER_SIZE; i++) header[i] = '\0';
    header[DADA_DEFAULT_HEADER_SIZE+1] = ASCII_HEADER_SENTINEL;
  }

  options::~options()
  {
    delete header;
  }

  void options::usage(std::ostream &o)
  {
    if (!ready)
      {
	ready = true;
	create_args();
      }
    o << "Usage: mksend [options] { Multicast-IP }\n";
    o << desc;
  }
  
  void options::parse_args(int argc, const char **argv)
  {
    if (!ready)
      {
	ready = true;
	create_args();
      }
    all.add(desc);
    all.add(hidden);
    try
      {
	po::store(po::command_line_parser(argc, argv)
		  .style(po::command_line_style::default_style & ~po::command_line_style::allow_guessing)
		  .options(all)
		  .positional(positional)
		  .run(), vm);
	po::notify(vm);
	if (vm.count("help"))
	  {
	    usage(std::cout);
	    std::exit(0);
	  }
	/*
	if (!vm.count("destination"))
	  throw po::error("At least one IP is required");
	*/
	if (vm.count("destination"))
	  {
	    destinations_opt = vm["destination"].as<std::vector<std::string>>();
	    if (destinations_opt.size() != 0)
	      {
		bool is_first = true;
		for (std::vector<std::string>::iterator ipi = destinations_opt.begin(); ipi != destinations_opt.end(); ++ipi)
		  {
		    if (!is_first)
		      {
			destinations_str += ',';
		      }
		    destinations_str += *ipi;
		    is_first = false;
		  }
	      }
	  }
	//if (hdrname != "")
	if (vm.count(HEADER_OPT) != 0)
	  { // read the given template header file and adapt some configuration parameters
	    //hdrname = vm[HEADER_OPT].as<std::string>();
	    //std::cout << "try to load file " << hdrname << std::endl;
	    load_header();
	  }
	apply_header(); // use values from header or default values
	return;
      }
    catch (po::error &e)
      {
	std::cerr << e.what() << '\n';
	usage(std::cerr);
	std::exit(2);
      }
  }

  void options::load_header()
  {
    std::ifstream   is(hdrname, std::ifstream::binary);

    if (is)
      {
	// get length of file:
	is.seekg (0, is.end);
	std::size_t length = is.tellg();
	is.seekg (0, is.beg);
	if (length > DADA_DEFAULT_HEADER_SIZE)
	  {
	    std::cerr << "options::load_header(), given file " << hdrname << " contains more than " << DADA_DEFAULT_HEADER_SIZE << " characters, ognoring this file." << std::endl;
	    return;
	  }
	//std::cout << "options::load_header(), loading file " << hdrname << std::endl;
	is.read(header, length);
	if (!is)
	  {
	    std::cerr << "error: only " << is.gcount() << " could be read" << std::endl;
	  }
	is.close();
      }
    else
      {
	std::cerr << "WARNING: header file " << hdrname << " does not exist." << std::endl;
      }
  }

  void options::set_start_time(int64_t timestamp)
  {
    double epoch = sync_epoch + timestamp / sample_clock;
    double integral;
    double fractional = modf(epoch, &integral);
    struct timeval tv;
    time_t start_utc;
    struct tm *nowtm;
    char tbuf[64];
    char utc_string[128];

    tv.tv_sec = integral;
    tv.tv_usec = (int) (fractional*1e6);
    start_utc = tv.tv_sec;
    nowtm = gmtime(&start_utc);
    strftime(tbuf, sizeof(tbuf), DADA_TIMESTR, nowtm);
    snprintf(utc_string, sizeof(utc_string), "%s.%06ld", tbuf, tv.tv_usec);
    ascii_header_set(header, SAMPLE_START_KEY, "%ld", timestamp);
    if (!check_header())
      {
	std::cerr << "ERROR, storing " << SAMPLE_START_KEY << " with value " << timestamp << " in header failed due to size restrictions. -> incomplete header due to clipping" << std::endl;
      }
    ascii_header_set(header, UTC_START_KEY, "%s", utc_string);
    if (!check_header())
      {
	std::cerr << "ERROR, storing " << UTC_START_KEY << " with value " << utc_string << " in header failed due to size restrictions. -> incomplete header due to clipping" << std::endl;
      }
  }

  /*
   * all SPEAD heap item pointers 
   */
  void options::update_indices()
  {
    int i;
    bool ok = true;
    int  payload_count = 0;

    nindices = 0;
    heap_count = 1;
    for (i = 0; i < nitems; i++)
      {
	if (items[i].id_used_type == DEFAULT_USED) {
	  std::cerr << "ERROR: please specify the item pointer id for item " << (i+1) << " (ITEM" << (i+1) << "_ID)\n";
	  ok = false;
	}
	if (items[i].step != ITEM_STEP_DEF)
	  { // first index, serial number (timestamp), assume INDEX == 1, ignore SCI
	    items[i].index  = 1;
	    items[i].sci    = 0;
	    items[i].type   = SERIAL_ITEM_TYPE;
	    items[i].count  = 0;
	    items[i].lindex = 0;
	    if (items[i].values.size() > 0)
	      {
		items[i].value = items[i].values.at(0);
	      }
	    else
	      {
		items[i].value  = 0;
	      }
	    // register serial number as first index component
	    indices[0] = i;
	    if (nindices < 1) nindices = 1;
	  }
	else if (items[i].sci != ITEM_SCI_DEF)
	  { // side-channel item, ignore LIST, STEP and INDEX
	    items[i].list   = "";
	    items[i].step   = 0;
	    items[i].index  = 0;
	    items[i].values.clear();
	    items[i].type   = SIDE_CHANNEL_ITEM_TYPE;
	    items[i].count  = 0;
	    items[i].lindex = items[i].sci-1; // store the 0-based index as local index
	    items[i].value  = 0;
	  }
	else if ((items[i].values.size() > 1) && (items[i].index != ITEM_INDEX_DEF))
	  { // list of values, list must contain more than one value, used as index component (1-based), ignore STEP and SCI
	    if (items[i].list_used_type == DEFAULT_USED) {
	      std::cerr << "ERROR: please specify the allowed item values (ITEM" << (i+1) << "_LIST)\n";
	      ok = false;
	    }
	    items[i].step   = 0;
	    items[i].sci    = 0;
	    items[i].type   = INDEX_ITEM_TYPE;
	    items[i].count  = items[i].values.size();
	    items[i].lindex = 0;
	    items[i].value  = items[i].values.at(0);
	    indices[items[i].index-1] = i;
	    if (nindices <= (int)(items[i].index-1)) nindices = (items[i].index-1) + 1;
	    heap_count *= items[i].count;
	  }
	else if ((items[i].values.size() == 1) && (items[i].index == ITEM_INDEX_DEF))
	  { // fixed value, list must contain only one value, ignore STEP and SCI
	    items[i].step   = 0;
	    items[i].sci    = 0;
	    items[i].type   = FIXED_ITEM_TYPE;
	    items[i].count  = 0;
	    items[i].lindex = 0;
	    items[i].value  = items[i].values.at(0);
	  }
	else
	  { // plain data (FFT, samples, ...), ignore INDEX
	    payload_count++;
	    items[i].index  = 0;
	    items[i].type   = PAYLOAD_ITEM_TYPE;
	    items[i].count  = 0;
	    items[i].lindex = 0;
	    items[i].value  = heap_size;
	  }
	//std::cout << "item[" << i << "] = " << items[i].id << " " << items[i].type << std::endl;
      }
    if (payload_count != 1) {
      std::cerr << "ERROR: cannot work without any payload or more than one payloads, please check the number of items with only IDXi_ID specified.\n";
      ok = false;
    }
    if (!ok) {
      std::cerr << "ABORTING: please change your program options and/or the configuration file\n";
      exit(-1);
    }
  }

  void options::update_side_channel_items(const spead2::s_item_pointer_t *sci, std::size_t heap_index)
  {
    int  i;

    for (i = 0; i < nitems; i++)
      {
	if (items[i].type != SIDE_CHANNEL_ITEM_TYPE) continue;
	items[i].value = sci[heap_index*nsci + items[i].lindex]; // lindex is 0-based, see update_indices()
      }
  }

  void options::update_index_items()
  {
    int  i;
    bool carry = true;

    for (i = (nindices-1); i >= 1; i--)
      {
	int  index = indices[i];
	carry = (items[index].lindex == (items[index].count-1));
	items[index].lindex = (items[index].lindex + 1)%items[index].count;
	items[index].value = items[index].values.at(items[index].lindex);
	if (!carry) break;
      }
    if ((nindices >= 1) && carry)
      {
	int  index = indices[0];
	items[index].value += items[index].step;
      }
  }

  /*
    Parse an option given as default (string value), header file entry or program option and converts it into
    an integer number. It is allowed to use decimals, hexadecimals (prefix 0x) or binary (prefix 0b) notation.
    We have to distinguish three cases:
    1. The parameter is specified as program option, this value does overwrite the default value and the value given in the header file.
    2. The parameter is set in the header file but not as a program option, this value does overwrite the default value and it is stored in the header file (unchanged).
    3. Only the default value is given, this value is stored in the header file (unchanged).
  */
  USED_TYPE options::finalize_parameter(std::string &val, const char *opt, const char *key)
  {
    USED_TYPE ut;
    if (!quiet) std::cout << "finalize_parameter(" << val << ", " << opt << ", " << key << ")";
    if (vm.count(opt) != 0) { // Check if the parameter is given as program option
      // -> use the program option value already given in val and store it in the header file
      ut = OPTION_USED;
      if (!quiet) std::cout << " option for " << opt << " is " << val;
      if (key[0] != '\0') {
	if (val.length() == 0) {
	  ascii_header_set(header, key, "unset");
	  if (!quiet) std::cout << "  header becomes unset" << '\n';
	} else {
	  ascii_header_set(header, key, "%s", val.c_str());
	  if (!quiet) std::cout << "  header becomes " << val << '\n';
	}
	if (!check_header()) {
	  std::cerr << "ERROR, storing " << key << " with value " << val << " in header failed due to size restrictions. -> incomplete header due to clipping" << '\n';
	}
      }
    } else if (key[0] != '\0') { // Check if a value is given in the header file
      char sval[1024];
      if (ascii_header_get(header, key, "%s", sval) != -1) {
	if (strcmp(sval, "unset") == 0) { // check if the value in the header is unset
	  // -> use the default value already given in val
	  ut = DEFAULT_USED;
	  if (!quiet) std::cout << " header unset, default for " << opt << " is " << val;
	} else {
	  // -> use the value from the header file
	  ut = CONFIG_USED;
	  val = sval;
	  if (!quiet) std::cout << " header for " << opt << " is " << val;
	}
      } else {
	ut = DEFAULT_USED;
	if (!quiet) std::cout << " default for " << opt << " is " << val;
      }
    }
    if (!quiet) std::cout << " -> " << val << '\n';
    return ut;
  }

  bool options::parse_fixnum(int &val, std::string &val_str)
  {
    try {
      if (val_str.compare(0,2,"0x") == 0)
	{
	  val = std::stoi(std::string(val_str.begin() + 2, val_str.end()), nullptr, 16);
	}
      else if (val_str.compare(0,2,"0b") == 0)
	{
	  val = std::stoi(std::string(val_str.begin() + 2, val_str.end()), nullptr, 2);
	}
      else
	{
	  val = std::stoi(val_str, nullptr, 10);
	}
      return true;
    } catch (std::exception& e) {
      return false;
    }
  }
  
  bool options::parse_fixnum(spead2::s_item_pointer_t &val, std::string &val_str)
  {
    try {
      if (val_str.compare(0,2,"0x") == 0)
	{
	  val = std::stol(std::string(val_str.begin() + 2, val_str.end()), nullptr, 16);
	}
      else if (val_str.compare(0,2,"0b") == 0)
	{
	  val = std::stol(std::string(val_str.begin() + 2, val_str.end()), nullptr, 2);
	}
      else
	{
	  val = std::stol(val_str, nullptr, 10);
	}
      return true;
    } catch (std::exception& e) {
      return false;
    }
  }
  
  bool options::parse_fixnum(std::size_t &val, std::string &val_str)
  {
    try {
      if (val_str.compare(0,2,"0x") == 0)
	{
	  val = std::stol(std::string(val_str.begin() + 2, val_str.end()), nullptr, 16);
	}
      else if (val_str.compare(0,2,"0b") == 0)
	{
	  val = std::stol(std::string(val_str.begin() + 2, val_str.end()), nullptr, 2);
	}
      else
	{
	  val = std::stol(val_str, nullptr, 10);
	}
      return true;
    } catch (std::exception& e) {
      return false;
    }
  }

  USED_TYPE options::parse_parameter(std::string &val, const char *opt, const char *key)
  {
    USED_TYPE ut = finalize_parameter(val, opt, key);
    //if (!quiet) std::cout << opt << "/" << key << " = " << val << '\n';
    return ut;
  }

  USED_TYPE options::parse_parameter(int &val, std::string &val_str, const char *opt, const char *key)
  {
    USED_TYPE ut = finalize_parameter(val_str, opt, key);
    // we have the final value in val_str -> convert it into an integer number according to an optional prefix
    if (!parse_fixnum(val, val_str))
      {
	std::cerr << "Exception: cannot convert " << val_str << " into int for option " << opt << "/" << key << '\n';
	// put the default value already given in val into the header (the current value cannot be converted into std::size_t)
	if (key[0] != '\0') {
	  ascii_header_set(header, key, "%d", val);
	  //std::cout << "  header becomes " << val << '\n';
	  if (!check_header()) {
	    std::cerr << "ERROR, storing " << key << " with value " << val << " in header failed due to size restrictions. -> incomplete header due to clipping" << '\n';
	  }
	}
      }
    //if (!quiet) std::cout << opt << "/" << key << " = " << val << '\n';
    return ut;
  }

  USED_TYPE options::parse_parameter(std::size_t &val, std::string &val_str, const char *opt, const char *key)
  {
    USED_TYPE ut = finalize_parameter(val_str, opt, key);
    // we have the final value in val_str -> convert it into an integer number according to an optional prefix
    if (!parse_fixnum(val, val_str))
      {
	std::cerr << "Exception: cannot convert " << val_str << " into int for option " << opt << "/" << key << '\n';
	// put the default value already given in val into the header (the current value cannot be converted into std::size_t)
	if (key[0] != '\0') {
	  ascii_header_set(header, key, "%ld", val);
	  //std::cout << "  header becomes " << val << '\n';
	  if (!check_header()) {
	    std::cerr << "ERROR, storing " << key << " with value " << val << " in header failed due to size restrictions. -> incomplete header due to clipping" << '\n';
	  }
	}
      }
    //if (!quiet) std::cout << opt << "/" << key << " = " << val << '\n';
    return ut;
  }

  USED_TYPE options::parse_parameter(double &val, std::string &val_str, const char *opt, const char *key)
  {
    USED_TYPE ut = finalize_parameter(val_str, opt, key);
    // we have the final value in val_str -> convert it into an integer number according to an optional prefix
    try {
      val = std::stod(val_str, nullptr);
    } catch (std::exception& e) {
      std::cerr << "Exception: " << e.what() << " cannot convert " << val_str << " into double for option " << opt << "/" << key << '\n';
      // put the default value already given in val into the header (the current value cannot be converted into std::size_t)
      if (key[0] != '\0') {
	ascii_header_set(header, key, "%f", val);
	//std::cout << "  header becomes " << val << '\n';
	if (!check_header()) {
	  std::cerr << "ERROR, storing " << key << " with value " << val << " in header failed due to size restrictions. -> incomplete header due to clipping" << '\n';
	}
      }
    }
    //if (!quiet) std::cout << opt << "/" << key << " = " << val << '\n';
    return ut;
  }

  /*
    Parse a list of integer values where each value is either a direct value or a range definition.
    All integer values (direct or in a range definition) are either a decimal, hexadecimal or binary number.
    The syntax for an integer list is:
    <list> := "unset" | ( <element> { "," <element> } ) .
    <element> := <number> | ( <first> ":" <last> ":" <step> ) .
    <number>  := <decimal> | <hexadecimal> | <binary> .
   */
  USED_TYPE options::parse_parameter(std::vector<spead2::s_item_pointer_t> &val, std::string &val_str, const char *opt, const char *key)
  {
    std::string::size_type str_from = 0, str_to, str_length;

    USED_TYPE ut = finalize_parameter(val_str, opt, key);
    // we have the final value in val_str -> convert it into an list of integer number according to an optional prefix and with range support
    val.clear();
    str_length = val_str.length();
    while(str_from < str_length + 1) {
      str_to = val_str.find_first_of(",", str_from);
      if(str_to == std::string::npos) str_to = str_length;
      if(str_to == str_from) break;
      std::string               el_str(val_str.data() + str_from, str_to - str_from);
      std::string::size_type    el_from = 0, el_to, el_length;
      spead2::s_item_pointer_t  vals[3];
      int                       nparts = 0;
      el_length = el_str.length();
      while((el_from < el_length + 1) && (nparts != 3)) {
	el_to = el_str.find_first_of(":", el_from);
	if(el_to == std::string::npos) el_to = el_length;
	if(el_to == el_from) break;
	std::string hel(el_str.data() + el_from, el_to - el_from);
	if (!parse_fixnum(vals[nparts], hel)) {
	  std::cerr << "Exception: cannot convert " << hel << " at position " << str_from << " into spead2::s_item_pointer_t for option " << opt << "/" << key << '\n';
	  nparts = 0;
	  break;
	}
	nparts++;
	el_from = el_to + 1;
      }
      if (nparts < 3) vals[2] = 1;           // <first> ':' <last>  => <step> := 1
      if (nparts < 2) vals[1] = vals[0] + 1; // <first>             => <step> := 1, <last> := <first> + 1
      if (!quiet) std::cout << "  sequence from " << vals[0] << " to " << vals[1] << " (excluding) with step " << vals[2] << '\n';
      while (vals[0] < vals[1]) {
	val.push_back(vals[0]);
	vals[0] += vals[2];
      }
      str_from = str_to + 1;
    }
    /*
    std::cout << "item value list:";
    std::size_t i;
    for (i = 0; i < val.size(); i++)
      {
	std::cout << " " << val.at(i) << "->" << i;
      }
    std::cout << '\n';
    */
    return ut;
  }

  USED_TYPE options::parse_parameter(std::vector<std::size_t> &val, std::string &val_str, const char *opt, const char *key)
  {
    std::string::size_type str_from = 0, str_to, str_length;

    USED_TYPE ut = finalize_parameter(val_str, opt, key);
    // we have the final value in val_str -> convert it into an list of integer number according to an optional prefix and with range support
    val.clear();
    str_length = val_str.length();
    while(str_from < str_length + 1) {
      str_to = val_str.find_first_of(",", str_from);
      if(str_to == std::string::npos) str_to = str_length;
      if(str_to == str_from) break;
      std::string               el_str(val_str.data() + str_from, str_to - str_from);
      std::string::size_type    el_from = 0, el_to, el_length;
      std::size_t               vals[3];
      int                       nparts = 0;
      el_length = el_str.length();
      while((el_from < el_length + 1) && (nparts != 3)) {
	el_to = el_str.find_first_of(":", el_from);
	if(el_to == std::string::npos) el_to = el_length;
	if(el_to == el_from) break;
	std::string hel(el_str.data() + el_from, el_to - el_from);
	if (!parse_fixnum(vals[nparts], hel)) {
	  std::cerr << "Exception: cannot convert " << hel << " at position " << str_from << " into std::size_t for option " << opt << "/" << key << '\n';
	  nparts = 0;
	  break;
	}
	nparts++;
	el_from = el_to + 1;
      }
      if (nparts < 3) vals[2] = 1;           // <first> ':' <last>  => <step> := 1
      if (nparts < 2) vals[1] = vals[0] + 1; // <first>             => <step> := 1, <last> := <first> + 1
      if (!quiet) std::cout << "  sequence from " << vals[0] << " to " << vals[1] << " (excluding) with step " << vals[2] << '\n';
      while (vals[0] < vals[1]) {
	val.push_back(vals[0]);
	vals[0] += vals[2];
      }
      str_from = str_to + 1;
    }
    /*
    std::cout << "item value list:";
    std::size_t i;
    for (i = 0; i < val.size(); i++)
      {
	std::cout << " " << val.at(i) << "->" << i;
      }
    std::cout << '\n';
    */
    return ut;
  }

  USED_TYPE options::parse_parameter(std::vector<std::string> &val, std::string &val_str, const char *opt, const char *key)
  {
    std::string::size_type str_from = 0, str_to, str_length;

    USED_TYPE ut = finalize_parameter(val_str, opt, key);
    // we have the final value in val_str -> convert it into an list of IPs allowing "i.j.k.l+n" notation
    val.clear();
    str_length = val_str.length();
    while(str_from < str_length + 1) {
      str_to = val_str.find_first_of(",", str_from);
      if(str_to == std::string::npos) str_to = str_length;
      if(str_to == str_from) break;
      std::string               el_str(val_str.data() + str_from, str_to - str_from);
      std::string::size_type    el_from = 0, el_to, el_length;
      int                       vals[6];
      int                       nparts = 0;
      el_length = el_str.length();
      while((el_from < el_length + 1) && (nparts != 6)) {
	el_to = el_str.find_first_of(".+:", el_from);
	if(el_to == std::string::npos) el_to = el_length;
	if(el_to == el_from) break;
	std::string hel(el_str.data() + el_from, el_to - el_from);
	if (!parse_fixnum(vals[nparts], hel)) {
	  std::cerr << "Exception: cannot convert " << hel << " at position " << str_from << " into std::size_t for option " << opt << "/" << key << '\n';
	  nparts = 0;
	  break;
	}
	nparts++;
	el_from = el_to + 1;
      }
      str_from = str_to + 1;
      if (nparts == 0) continue;
      if (nparts < 6) vals[5] = 1;
      if (nparts < 5) vals[4] = 0;
      if (!quiet) std::cout << "  IP sequence for " << vals[0] << "." << vals[1] << "." << vals[2] << "." << vals[3] << "+" << vals[4] << ":" << vals[5] << '\n';
      vals[4] += 1; // the n means up to l+n _including_
      while (vals[4] > 0) {
	std::string ip_str;
	char ip_adr[256];
	snprintf(ip_adr, sizeof(ip_adr), "%d.%d.%d.%d", vals[0], vals[1], vals[2], vals[3]);
	ip_str = ip_adr;
	val.push_back(ip_str);
	vals[3] += vals[5];
	vals[4] -= vals[5];
      }
    }
    /*
    std::cout << "item value list:";
    std::size_t i;
    for (i = 0; i < val.size(); i++)
      {
	std::cout << " " << val.at(i) << "->" << i;
      }
    std::cout << '\n';
    */
    return ut;
  }

  bool options::check_header()
  {
    bool result = (header[DADA_DEFAULT_HEADER_SIZE+1] == ASCII_HEADER_SENTINEL);
    header[DADA_DEFAULT_HEADER_SIZE] = '\0';
    header[DADA_DEFAULT_HEADER_SIZE+1] = ASCII_HEADER_SENTINEL;
    return result;
  }

  void options::create_args()
  {
    int i;

    desc.add_options()
      ("help",             "show this text")
      // optional header file contain configuration options and additional information
      (HEADER_OPT,         make_opt(hdrname),             HEADER_DESC)
      // some flags
      (QUIET_OPT,          make_opt(quiet),               QUIET_DESC)
      (PYSPEAD_OPT,        make_opt(pyspead),             PYSPEAD_DESC)
      // some options, default values should be ok to use, will _not_ go into header
      (PACKET_OPT,         make_opt(packet_str),          PACKET_DESC)
      (BUFFER_OPT,         make_opt(buffer_str),          BUFFER_DESC)
      (NTHREADS_OPT,       make_opt(threads_str),         NTHREADS_DESC)
      (NHEAPS_OPT,         make_opt(heaps_str),           NHEAPS_DESC)
      ("memcpy-nt",        make_opt(memcpy_nt),           "Use non-temporal memcpy")
      // DADA ringbuffer related stuff
      (DADA_MODE_OPT,      make_opt(dada_mode_str),       DADA_MODE_DESC)
      (DADA_KEY_OPT,       make_opt(dada_key),            DADA_KEY_DESC)
      (NGROUPS_DATA_OPT,   make_opt(ngroups_data_str),    NGROUPS_DATA_DESC)
      (NSLOTS_OPT,         make_opt(nslots_str),          NSLOTS_DESC)
      // network configuration
      (NETWORK_MODE_OPT,   make_opt(network_mode_str),    NETWORK_MODE_DESC)
#if SPEAD2_USE_IBV
      (IBV_IF_OPT,         make_opt(ibv_if),              IBV_IF_DESC)
      (IBV_VECTOR_OPT,     make_opt(ibv_comp_vector_str), IBV_VECTOR_DESC)
      (IBV_MAX_POLL_OPT,   make_opt(ibv_max_poll_str),    IBV_MAX_POLL_DESC)
#endif
      (UDP_IF_OPT,         make_opt(udp_if),              UDP_IF_DESC)
      (NHOPS_OPT,          make_opt(nhops_str),           NHOPS_DESC)
      (NWRATE_OPT,         make_opt(nwrate_str),          NWRATE_DESC)
      (BURST_SIZE_OPT,     make_opt(burst_size_str),      BURST_SIZE_DESC)
      (BURST_RATIO_OPT,    make_opt(burst_ratio_str),     BURST_RATIO_DESC)
      (PORT_OPT,           make_opt(port),                PORT_DESC)
      (SYNC_EPOCH_OPT,     make_opt(sync_epoch_str),      SYNC_EPOCH_DESC)
      (SAMPLE_CLOCK_OPT,   make_opt(sample_clock_str),    SAMPLE_CLOCK_DESC)
      (SAMPLE_START_OPT,   make_opt(sample_start_str),    SAMPLE_START_DESC)
      (HEAP_SIZE_OPT,      make_opt(heap_size_str),       HEAP_SIZE_DESC)
      (HEAP_ID_START_OPT,  make_opt(heap_id_start_str),   HEAP_ID_START_DESC)
      (HEAP_ID_OFFSET_OPT, make_opt(heap_id_offset_str),  HEAP_ID_OFFSET_DESC)
      (HEAP_ID_STEP_OPT,   make_opt(heap_id_step_str),    HEAP_ID_STEP_DESC)
      (HEAP_GROUP_OPT,     make_opt(heap_group_str),      HEAP_GROUP_DESC)
      (NSCI_OPT,           make_opt(nsci_str),            NSCI_DESC)
      ;
    // SPEAD heap items
    desc.add_options()
      (NITEMS_OPT,     make_opt(nitems_str),              NITEMS_DESC)
      ;
    for (i = 0; i < MAX_ITEMS; i++)
      {
	char olabel[32];
	char odesc[255];
	snprintf(olabel, sizeof(olabel) - 1, ITEM_ID_OPT,  i+1);
	snprintf(odesc,  sizeof(odesc) - 1,  ITEM_ID_DESC, i+1);
	desc.add_options()(olabel, make_opt(items[i].id_str), odesc);
	snprintf(olabel, sizeof(olabel) - 1, ITEM_LIST_OPT,  i+1);
	snprintf(odesc,  sizeof(odesc) - 1,  ITEM_LIST_DESC, i+1);
	desc.add_options()(olabel, make_opt(items[i].list), odesc);
	snprintf(olabel, sizeof(olabel) - 1, ITEM_STEP_OPT, i+1);
	snprintf(odesc,  sizeof(odesc) - 1,  ITEM_STEP_DESC, i+1);
	desc.add_options()(olabel, make_opt(items[i].step_str), odesc);
	snprintf(olabel, sizeof(olabel) - 1, ITEM_INDEX_OPT, i+1);
	snprintf(odesc,  sizeof(odesc) - 1,  ITEM_INDEX_DESC, i+1);
	desc.add_options()(olabel, make_opt(items[i].index_str), odesc);
	snprintf(olabel, sizeof(olabel) - 1, ITEM_SCI_OPT, i+1);
	snprintf(odesc,  sizeof(odesc) - 1,  ITEM_SCI_DESC, i+1);
	desc.add_options()(olabel, make_opt(items[i].sci_str), odesc);
      }

    hidden.add_options()
      // network configuration
      (DESTINATIONS_OPT,      po::value<std::vector<std::string>>()->composing(), DESTINATIONS_DESC);
    positional.add("destination", -1);
  }

  /*
   * Each parameter can be specified as default value, program option and header value. If a command line option is given,
   * it takes precedence over a header value which takes precedence over the default value.
   * Only program parameters which have an influence on the behaviour can have a header key value and are stored in the
   * header copied into the ringbuffer.
   */
  void options::apply_header()
  {
    int            i;

    /* the following options should have sufficient default values */
    parse_parameter(packet,          packet_str, PACKET_OPT, PACKET_KEY);
    parse_parameter(buffer,          buffer_str, BUFFER_OPT, BUFFER_KEY);
    parse_parameter(threads,         threads_str, NTHREADS_OPT, NTHREADS_KEY);
    parse_parameter(heaps,           heaps_str, NHEAPS_OPT, NHEAPS_KEY);
    /* The following options describe the DADA ringbuffer use */
    parse_parameter(dada_mode,       dada_mode_str, DADA_MODE_OPT, DADA_MODE_KEY);
    parse_parameter(dada_key,                       DADA_KEY_OPT, DADA_KEY_KEY);
    parse_parameter(ngroups_data,    ngroups_data_str, NGROUPS_DATA_OPT, NGROUPS_DATA_KEY);
    parse_parameter(nslots,          nslots_str, NSLOTS_OPT, NSLOTS_KEY);
    /* The following options describe the connection to the F-Engines (network) */
    parse_parameter(network_mode,    network_mode_str, NETWORK_MODE_OPT, NETWORK_MODE_KEY);
#if SPEAD2_USE_IBV
    parse_parameter(ibv_if,          IBV_IF_OPT, IBV_IF_KEY);
    parse_parameter(ibv_comp_vector, ibv_comp_vector_str, IBV_VECTOR_OPT, IBV_VECTOR_KEY);
    parse_parameter(ibv_max_poll,    ibv_max_poll_str, IBV_MAX_POLL_OPT, IBV_MAX_POLL_KEY);
#endif
    parse_parameter(udp_if,          UDP_IF_OPT, UDP_IF_KEY);
    parse_parameter(nhops,           nhops_str, NHOPS_OPT, NHOPS_KEY);
    parse_parameter(nwrate,          nwrate_str, NWRATE_OPT, NWRATE_KEY);
    parse_parameter(burst_size,      burst_size_str, BURST_SIZE_OPT, BURST_SIZE_KEY);
    parse_parameter(burst_ratio,     burst_ratio_str, BURST_RATIO_OPT, BURST_RATIO_KEY);
    parse_parameter(port,            PORT_OPT, PORT_KEY);
    parse_parameter(sample_clock,    sample_clock_str, SAMPLE_CLOCK_OPT, SAMPLE_CLOCK_KEY);
    parse_parameter(sample_start,    sample_start_str, SAMPLE_START_OPT, SAMPLE_START_KEY);
    parse_parameter(sync_epoch,      sync_epoch_str, SYNC_EPOCH_OPT, SYNC_EPOCH_KEY);
    parse_parameter(heap_size,       heap_size_str, HEAP_SIZE_OPT, HEAP_SIZE_KEY);
    parse_parameter(heap_id_start,   heap_id_start_str, HEAP_ID_START_OPT, HEAP_ID_START_KEY);
    parse_parameter(heap_id_offset,  heap_id_offset_str, HEAP_ID_OFFSET_OPT, HEAP_ID_OFFSET_KEY);
    parse_parameter(heap_id_step,    heap_id_step_str, HEAP_ID_STEP_OPT, HEAP_ID_STEP_KEY);
    parse_parameter(heap_group,      heap_group_str, HEAP_GROUP_OPT, HEAP_GROUP_KEY);
    parse_parameter(nsci,            nsci_str, NSCI_OPT, NSCI_KEY);
    parse_parameter(nitems,          nitems_str, NITEMS_OPT, NITEMS_KEY);
    if (nitems > MAX_ITEMS) nitems = MAX_ITEMS;
    for (i = 0; i < nitems; i++)
      {
	char iopt[32];
	char ikey[32];
	snprintf(iopt, sizeof(iopt) - 1, ITEM_ID_OPT, i+1);
	snprintf(ikey, sizeof(ikey) - 1, ITEM_ID_KEY, i+1);
	items[i].id_used_type = parse_parameter(items[i].id, items[i].id_str, iopt, ikey);
	snprintf(iopt, sizeof(iopt) - 1, ITEM_LIST_OPT, i+1);
	snprintf(ikey, sizeof(ikey) - 1, ITEM_LIST_KEY, i+1);
	items[i].list_used_type = parse_parameter(items[i].values, items[i].list, iopt, ikey);
	snprintf(iopt, sizeof(iopt) - 1, ITEM_STEP_OPT, i+1);
	snprintf(ikey, sizeof(ikey) - 1, ITEM_STEP_KEY, i+1);
	items[i].step_used_type = parse_parameter(items[i].step, items[i].step_str, iopt, ikey);
	snprintf(iopt, sizeof(iopt) - 1, ITEM_INDEX_OPT, i+1);
	snprintf(ikey, sizeof(ikey) - 1, ITEM_INDEX_KEY, i+1);
	items[i].index_used_type = parse_parameter(items[i].index, items[i].index_str, iopt, ikey);
	snprintf(iopt, sizeof(iopt) - 1, ITEM_SCI_OPT, i+1);
	snprintf(ikey, sizeof(ikey) - 1, ITEM_SCI_KEY, i+1);
	items[i].sci_used_type = parse_parameter(items[i].sci, items[i].sci_str, iopt, ikey);
      }
    update_indices();
    parse_parameter(destinations, destinations_str, DESTINATIONS_OPT, DESTINATIONS_KEY);
  }

}
