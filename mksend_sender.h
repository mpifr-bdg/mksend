#ifndef mkrecv_receiver_h
#define mkrecv_receiver_h

#include <vector>
#include <thread>
#include <atomic>

#include <boost/asio.hpp>

#include <spead2/common_thread_pool.h>
#include <spead2/common_defines.h>
#include <spead2/send_stream.h>

#include "mksend_options.h"
#include "mksend_ringbuffer.h"
#include "mksend_stream.h"

namespace po = boost::program_options;
namespace asio = boost::asio;

namespace mksend
{

  class sender
  {
  protected:
    static sender     *instance;
  protected:
    psrdada_cpp::MultiLog                           mlog;
    std::shared_ptr<mksend::options>                opts = NULL;
    std::shared_ptr<mksend::ringbuffer>             ringbuffer = NULL;
    //std::shared_ptr<mkrecv::allocator>              allocator = NULL;
    std::shared_ptr<spead2::send::stream_config>    sconfig = NULL;
    std::vector<std::unique_ptr<mksend::stream> >   streams;
    std::shared_ptr<spead2::thread_pool>            thread_pool = NULL;

  public:
    sender();
    virtual ~sender();
    int execute(int argc, const char **argv);
    std::shared_ptr<mksend::options> create_options();
    std::shared_ptr<mksend::ringbuffer> create_ringbuffer();
    //std::unique_ptr<mkrecv::stream> create_stream();
  public:
    static void request_stop();
  protected:
    std::unique_ptr<spead2::send::stream> make_stream(std::vector<std::string>::iterator destination);
  };

}

#endif /* mkrecv_receiver_h */
