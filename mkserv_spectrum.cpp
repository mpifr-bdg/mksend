/* gcc -c mkserv_spectrum.cpp */
/* gcc -o mkspectrum mkserv_spectrum.cpp mkserv_message.cpp mkserv_socket.cpp mkserv_logging.cpp -lstdc++ -lpthread */
/* ./mkspectrum */
/* ./mkspectrum  -h localhost -p 6000 -f 0.0 -c 64 -b 4 */

#include <stdio.h>
#include <string.h>
#include <iostream>     // std::cout

#include <thread>
#include <atomic>
#include <vector>

#include "mkserv_logging.h"
#include "mkserv_message.h"

//#define SIMULATION
#define RINGBUFFER

#ifdef RINGBUFFER
#include "boost/program_options.hpp"
#include "boost/lexical_cast.hpp"

namespace po = boost::program_options;

const size_t SUCCESS = 0;
const size_t ERROR_IN_COMMAND_LINE = 1;
const size_t ERROR_UNHANDLED_EXCEPTION = 2;

template<typename T>
static po::typed_value<T> *make_opt(T &var)
{
  return po::value<T>(&var);
}

#endif /* RINGBUFFER */

typedef struct {
  std::string  host;
  int          port;         // server port (default is mkserv::PORT)
  float        frequency;    // center frequency of the spectrum part in MHz
  int          count;
  int          binning;
} setup_t;

static void send_spectrum(mkserv::socket &sock, setup_t *setup)
{
  mkserv::message_header      msg_hdr;
  mkserv::spectrum_request    spec_req(&msg_hdr);
  mkserv::spectrum_reply      spec_rep(&msg_hdr);

  spec_req.create(setup->frequency, setup->count, setup->binning);
  if (!spec_req.send_message(sock)) exit(2);
  if (!msg_hdr.receive_header(sock)) exit(3);
  if (msg_hdr.tag() == mkserv::ERROR_TAG) {
    mkserv::error_reply  err_rep(&msg_hdr);
    if (!err_rep.receive_body(sock)) exit(4);
    std::cerr << "ERROR: cannot send a SPECTRUM message, received an ERROR reply: " << err_rep.err_code() << ":" << err_rep.err_flags() << std::endl;
    return;
  }
  if (!spec_rep.receive_body(sock)) exit(5);
  std::cout << "reply: " << msg_hdr.tag() << msg_hdr.code() << " " << msg_hdr.payload() << std::endl;
  std::cout << "nreceived: " << spec_rep.nreceived() << std::endl;
  std::cout << "nsaturated: " << spec_rep.nsaturated() << std::endl;
  std::cout << "freq_low: " << spec_rep.freq_low() << std::endl;
  std::cout << "freq_high: " << spec_rep.freq_high() << std::endl;
  std::cout << "count: " << spec_rep.count() << std::endl;
  std::cout << "binning: " << spec_rep.binning() << std::endl;
  for (int32_t i = 0; i < spec_rep.count(); i++) {
    std::cout << "spectrum[" << i << "] = " << spec_rep.spectrum(i) << std::endl;
  }
}

static void send_exit(mkserv::socket &sock)
{
  mkserv::message_header      msg_hdr;

  msg_hdr.create(mkserv::EXIT_TAG, mkserv::REQUEST_CODE, 0);
  if (!msg_hdr.send_header(sock)) exit(2);
  if (!msg_hdr.receive_header(sock)) exit(3);
  std::cout << "reply: " << msg_hdr.tag() << msg_hdr.code() << " " << msg_hdr.payload() << std::endl;
}

int main(int argc, char *argv[])
{
  mkserv::set_application("mkspectrum");

  static setup_t    setup;
  mkserv::socket    sock;

  setup.host = "localhost";
  setup.port      = mkserv::PORT;
  setup.frequency = 0.0;
  setup.count     = 64;
  setup.binning   = 4;

#ifdef RINGBUFFER
  // program option declaration
  namespace po = boost::program_options;
  po::options_description desc("Options");
  desc.add_options()("help", "Print help messages");
  desc.add_options()("host",      make_opt(setup.host), "The server host");
  desc.add_options()("port",      make_opt(setup.port), "The server port");
  desc.add_options()("frequency", make_opt(setup.frequency), "The central frequency of the spectrum part in MHz");
  desc.add_options()("count",     make_opt(setup.count), "The number of mapped spectrum channels (result)");
  desc.add_options()("binning",   make_opt(setup.binning), "The number of raw channels in a mapped channel");
  po::variables_map vm;
  try {
    po::store(po::parse_command_line(argc, argv, desc), vm);
    if (vm.count("help")) {
      std::cout << "Read a PFB spectrum from a server." << std::endl << desc << std::endl;
      return SUCCESS;
    }
    po::notify(vm);
  } catch (po::error &e) {
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
    std::cerr << desc << std::endl;
    return ERROR_IN_COMMAND_LINE;
  }
#endif /* RINGBUFFER */

  if (!sock.connect(setup.host.c_str(), setup.port)) exit(1);
  std::cout << "sock fd = " << sock.cfd() << std::endl;
  send_spectrum(sock, &setup);
  send_exit(sock);
  sock.finish_client();
}
