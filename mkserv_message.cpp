/* gcc -c mkserv_message.cpp */
#include "mkserv_message.h"

namespace mkserv {

  // #######################################################
  // # class message_block
  // # provides the basic message block class
  // #######################################################
  message_block::message_block() : _length(0)
  {
  }

  message_block::~message_block()
  {
  }
  
  void message_block::add(size_t size)
  {
#ifdef WITH_TYPE_TAGS
    _length += sizeof(int16_t); // parameter tag
#endif
    _length += size;            // parameter itself
  }
  
  void message_block::add(size_t size, size_t count)
  {
#ifdef WITH_TYPE_TAGS
    _length += sizeof(int16_t); // parameter tag
    _length += sizeof(int32_t); // element count
#endif
    _length += count*size;      // elements itself
  }

  void message_block::init()
  {
    _length = 0;
  }
  
  int message_block::write(socket &sock)
  {
    return 1;
  }

  int message_block::read(socket &sock)
  {
    return 1;
  }

  // #######################################################
  // # class message_header
  // # provides the basic message header class
  // #######################################################
  message_header::message_header() : _msg_tag('?'), _msg_code('?'), _payload(0)
  {
    // calculate message header length
    init();
    add(sizeof(_msg_tag));
    add(sizeof(_msg_code));
    add(sizeof(_payload));
  }

  message_header::~message_header()
  {
  }

  void message_header::create(char tag, char code, int32_t payload)
  {
    // fill message header members
    _msg_tag  = tag;
    _msg_code = code;
    _payload  = payload;
  }
  
  int message_header::write(socket &sock)
  {
    // write all message header members into the socket
    int flag = 1;
    flag = flag && sock.write(_msg_tag);
    flag = flag && sock.write(_msg_code);
    flag = flag && sock.write(_payload);
    return flag;
  }
  
  int message_header::read(socket &sock)
  {
    // read all message header members from the socket
    int flag = 1;
    flag = flag && sock.read(_msg_tag);
    flag = flag && sock.read(_msg_code);
    flag = flag && sock.read(_payload);
    return flag;
  }
  
  int message_header::send_header(socket &sock)
  {
    // send only the message header into a socket (the payload is not sent!)
    int flag = 1;
    sock.start_writing();
    flag = flag && write(sock);
    return flag && sock.send();
  }
  
  int message_header::receive_header(socket &sock)
  {
    // receive only a message header from a socket (the payload is not read in!)
    int flag = 1;
    flag = flag && sock.receive(length());
    sock.start_reading();
    return flag && read(sock);
  }


  // #######################################################
  // # class message_body
  // #######################################################
  message_body::message_body(message_header *hdr) : _hdr(hdr)
  {
  }

  message_body::~message_body()
  {
  }

  int message_body::send_message(socket &sock)
  {
    // send the whole message (header and body)
    int flag = 1;
    sock.start_writing();
    flag = flag && _hdr->write(sock);
    flag = flag && write(sock);
    return flag && sock.send();
  }
  
  int message_body::receive_body(socket &sock)
  {
    // receive the payload of the message (the header is already read, needed for dispatching)
    int flag = 1;
    sock.receive(_hdr->payload());
    sock.start_reading();
    flag = flag && read(sock);
    return flag;
  }
 

  // #######################################################
  // # class error_reply
  // #######################################################
  error_reply::error_reply(message_header *hdr) : message_body(hdr)
  {
    _err_code  = 0;
    _err_flags = 0;
  }
  
  error_reply::~error_reply()
  {
  }
  
  void error_reply::create(int32_t err_code, int32_t err_flags)
  {
    // fill message body members
    _err_code  = err_code;
    _err_flags = err_flags;
    // calculate message body length
    init();
    add(sizeof(_err_code));
    add(sizeof(_err_flags));
    // initialize the header (tag, code, payload)
    _hdr->create(ERROR_TAG, REPLY_CODE, length());
  }
  
  int error_reply::write(socket &sock)
  {
    int flag = 1;
    flag = flag && sock.write(_err_code);
    flag = flag && sock.write(_err_flags);
    return flag;
  }
  
  int error_reply::read(socket &sock)
  {
    int flag = 1;
    flag = flag && sock.read(_err_code);
    flag = flag && sock.read(_err_flags);
    return flag;
  }


  // #######################################################
  // # class ping_request
  // #######################################################
  ping_request::ping_request(message_header *hdr) : message_body(hdr)
  {
  }
  
  ping_request::~ping_request()
  {
  }
  
  void ping_request::create()
  {
    // fill message body members
    // calculate message body length
    init();
    // initialize the header (tag, code, payload)
    _hdr->create(PING_TAG, REQUEST_CODE, length());
  }
  
  
  // #######################################################
  // # class ping_reply
  // #######################################################
  ping_reply::ping_reply(message_header *hdr) : message_body(hdr)
  {
  }
  
  ping_reply::~ping_reply()
  {
  }
  
  void ping_reply::create()
  {
    // fill message body members
    // calculate message body length
    init();
    // initialize the header (tag, code, payload)
    _hdr->create(PING_TAG, REPLY_CODE, length());
  }
  

  // #######################################################
  // # class features_request
  // #######################################################
  features_request::features_request(message_header *hdr) : message_body(hdr)
  {
    _mode = DUPLICATES_MODE;
  }

  features_request::~features_request()
  {
  }
  
  void features_request::create(int16_t mode)
  {
    // fill message body members
    _mode = mode;
    // calculate message body length
    init();
    add(sizeof(_mode));
    // initialize the header (tag, code, payload)
    _hdr->create(FEATURES_TAG, REQUEST_CODE, length());
  }
    
  int features_request::write(socket &sock)
  {
    int flag = 1;
    flag = flag && sock.write(_mode);
    return flag;
  }
      
  int features_request::read(socket &sock)
  {
    int flag = 1;
    flag = flag && sock.read(_mode);
    return flag;
  }


  // #######################################################
  // # class features_reply
  // #######################################################
  features_reply::features_reply(message_header *hdr) :  message_body(hdr)
  {
    _mode = DUPLICATES_MODE;
    _nspectra  = 0;
    _nchannels = 0;
    _ncounters = 0;
    _bandwidth = 0.0;
    _nfeeds    = 0;
    _nblocks   = 0;
    _nsamples  = 0;
    _nbits     = 0;
    _nscis     = 0;
  }
    
  features_reply::~features_reply()
  {
  }

  void features_reply::create(int16_t mode,
			      int32_t nspectra,
			      int32_t nchannels,
			      int32_t ncounters,
			      float bandwidth,
			      int32_t nfeeds,
			      int32_t nblocks,
			      int32_t nsamples,
			      int32_t nbits,
			      int32_t nscis)
  {
    // fill message body members
    _mode      = mode;
    _nspectra  = nspectra;
    _nchannels = nchannels;
    _ncounters = ncounters;
    _bandwidth = bandwidth;
    _nfeeds    = nfeeds;
    _nblocks   = nblocks;
    _nsamples  = nsamples;
    _nbits     = nbits;
    _nscis     = nscis;
    create();
  }
  
  void features_reply::create()
  {
    // calculate message body length
    init();
    add(sizeof(_mode));
    add(sizeof(_nspectra));
    add(sizeof(_nchannels));
    add(sizeof(_ncounters));
    add(sizeof(_bandwidth));
    add(sizeof(_nfeeds));
    add(sizeof(_nblocks));
    add(sizeof(_nsamples));
    add(sizeof(_nbits));
    add(sizeof(_nscis));
    // initialize the header (tag, code, payload)
    _hdr->create(FEATURES_TAG, REPLY_CODE, length());
  }
  
  int features_reply::write(socket &sock)
  {
    int flag = 1;
    flag = flag && sock.write(_mode);
    flag = flag && sock.write(_nspectra);
    flag = flag && sock.write(_nchannels);
    flag = flag && sock.write(_ncounters);
    flag = flag && sock.write(_bandwidth);
    flag = flag && sock.write(_nfeeds);
    flag = flag && sock.write(_nblocks);
    flag = flag && sock.write(_nsamples);
    flag = flag && sock.write(_nbits);
    flag = flag && sock.write(_nscis);
    return flag;
  }
  
  int features_reply::read(socket &sock)
  {
    int flag = 1;
    flag = flag && sock.read(_mode);
    flag = flag && sock.read(_nspectra);
    flag = flag && sock.read(_nchannels);
    flag = flag && sock.read(_ncounters);
    flag = flag && sock.read(_bandwidth);
    flag = flag && sock.read(_nfeeds);
    flag = flag && sock.read(_nblocks);
    flag = flag && sock.read(_nsamples);
    flag = flag && sock.read(_nbits);
    flag = flag && sock.read(_nscis);
    return flag;
  }


  // #######################################################
  // # class spectrum_request
  // #######################################################
  spectrum_request::spectrum_request(message_header *hdr) : message_body(hdr)
  {
    _frequency = 0.0;
    _count     = 0;
    _binning   = 0;
  }
  
  spectrum_request::~spectrum_request()
  {
  }

  void spectrum_request::create(float frequency, int32_t count, int32_t binning)
  {
    // fill message body members
    _frequency = frequency;
    _count     = count;
    _binning   = binning;
    // calculate message body length
    init();
    add(sizeof(_frequency));
    add(sizeof(_count));
    add(sizeof(_binning));
    // initialize the header (tag, code, payload)
    _hdr->create(SPECTRUM_TAG, REQUEST_CODE, length());
  }

  int spectrum_request::write(socket &sock)
  {
    int flag = 1;
    flag = flag && sock.write(_frequency);
    flag = flag && sock.write(_count);
    flag = flag && sock.write(_binning);
    return flag;
  }

  int spectrum_request::read(socket &sock)
  {
    int flag = 1;
    flag = flag && sock.read(_frequency);
    flag = flag && sock.read(_count);
    flag = flag && sock.read(_binning);
    return flag;
  }
  
  
  // #######################################################
  // # class spectrum_reply
  // #######################################################
  spectrum_reply::spectrum_reply(message_header *hdr) : message_body(hdr)
  {
    _nreceived = 0;
    _nsaturated = 0;
    _freq_low = 0.0;
    _freq_high = 0.0;
    _count = 0;
    _binning = 0;
  }

  spectrum_reply::~spectrum_reply()
  {
  }

  void spectrum_reply::create(int32_t nreceived, int32_t nsaturated, float freq_low, float freq_high, int32_t count, int32_t binning)
  {
    // fill message body members
    _nreceived  = nreceived;
    _nsaturated = nsaturated;
    _freq_low   = freq_low;
    _freq_high  = freq_high;
    _count      = count;
    _binning    = binning;
    // calculate message body length
    init();
    add(sizeof(_nreceived));
    add(sizeof(_nsaturated));
    add(sizeof(_freq_low));
    add(sizeof(_freq_high));
    add(sizeof(_count));
    add(sizeof(_binning));
    add(sizeof(float), _count);
    // initialize the header (tag, code, payload)
    _hdr->create(SPECTRUM_TAG, REPLY_CODE, length());
  }
			
  int spectrum_reply::write(socket &sock)
  {
    int flag = 1;
    flag = flag && sock.write(_nreceived);
    flag = flag && sock.write(_nsaturated);
    flag = flag && sock.write(_freq_low);
    flag = flag && sock.write(_freq_high);
    flag = flag && sock.write(_count);
    flag = flag && sock.write(_binning);
    flag = flag && sock.write(_spectrum, _count);
    return flag;
  }
			
  int spectrum_reply::read(socket &sock)
  {
    int flag = 1;
    flag = flag && sock.read(_nreceived);
    flag = flag && sock.read(_nsaturated);
    flag = flag && sock.read(_freq_low);
    flag = flag && sock.read(_freq_high);
    flag = flag && sock.read(_count);
    flag = flag && sock.read(_binning);
    flag = flag && sock.read(_spectrum, _count);
    return flag;
  }


  // #######################################################
  // # class samples_request
  // #######################################################
  samples_request::samples_request(message_header *hdr) : message_body(hdr)
  {
    _mode   = CONTINUOUS_MODE;
    _feed   = 0;
  }
  
  samples_request::~samples_request()
  {
  }

  void samples_request::create(int16_t mode, int32_t feed)
  {
    // fill message body members
    _mode  = mode;
    _feed  = feed;
    // calculate message body length
    init();
    add(sizeof(_mode));
    add(sizeof(_feed));
    // initialize the header (tag, code, payload)
    _hdr->create(SAMPLES_TAG, REQUEST_CODE, length());
  }

  int samples_request::write(socket &sock)
  {
    int flag = 1;
    flag = flag && sock.write(_mode);
    flag = flag && sock.write(_feed);
    return flag;
  }

  int samples_request::read(socket &sock)
  {
    int flag = 1;
    flag = flag && sock.read(_mode);
    flag = flag && sock.read(_feed);
    return flag;
  }
  
  
  // #######################################################
  // # class samples_reply
  // #######################################################
  samples_reply::samples_reply(message_header *hdr) : message_body(hdr)
  {
    _feed     = 0;
    _block    = 0;
    _nsamples = 0;
    _nscis    = 0;
  }

  samples_reply::~samples_reply()
  {
  }

  void samples_reply::create(int32_t feed, int32_t block, int32_t nsamples, int32_t nscis)
  {
    // fill message body members
    _feed     = feed;
    _block    = block;
    _nsamples = nsamples;
    _nscis    = nscis;
    // calculate message body length
    init();
    add(sizeof(_feed));
    add(sizeof(_block));
    add(sizeof(_nsamples));
    add(sizeof(_nscis));
    add(sizeof(int16_t),  _nsamples);
    add(sizeof(uint64_t), _nscis);
    // initialize the header (tag, code, payload)
    _hdr->create(SAMPLES_TAG, REPLY_CODE, length());
  }
			
  int samples_reply::write(socket &sock)
  {
    int flag = 1;
    flag = flag && sock.write(_feed);
    flag = flag && sock.write(_block);
    flag = flag && sock.write(_nsamples);
    flag = flag && sock.write(_nscis);
    flag = flag && sock.write(_samples, _nsamples);
    flag = flag && sock.write(_scis,    _nscis);
    return flag;
  }
			
  int samples_reply::read(socket &sock)
  {
    int flag = 1;
    flag = flag && sock.read(_feed);
    flag = flag && sock.read(_block);
    flag = flag && sock.read(_nsamples);
    flag = flag && sock.read(_nscis);
    flag = flag && sock.read(_samples, _nsamples);
    flag = flag && sock.read(_scis,    _nscis);
    return flag;
  }



}
