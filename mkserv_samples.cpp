/* gcc -c mkserv_samples.cpp */
/* gcc -o mksamples mkserv_samples.cpp mkserv_message.cpp mkserv_socket.cpp mkserv_logging.cpp -lstdc++ -lpthread */
/* ./mksamples -h localhost -p 6000 -f 0 */

#include <stdio.h>
#include <string.h>
#include <iostream>     // std::cout

#include <thread>
#include <atomic>
#include <vector>

#include "mkserv_logging.h"
#include "mkserv_message.h"

//#define SIMULATION
#define RINGBUFFER

#ifdef RINGBUFFER
#include "boost/program_options.hpp"
#include "boost/lexical_cast.hpp"

namespace po = boost::program_options;

const size_t SUCCESS = 0;
const size_t ERROR_IN_COMMAND_LINE = 1;
const size_t ERROR_UNHANDLED_EXCEPTION = 2;

template<typename T>
static po::typed_value<T> *make_opt(T &var)
{
  return po::value<T>(&var);
}

#endif /* RINGBUFFER */

typedef struct {
  std::string  host;
  int          port;         // server port (default is mkserv::PORT)
  int          mode;
  int          feed;
} setup_t;

static void send_samples(mkserv::socket &sock, setup_t *setup)
{
  mkserv::message_header      msg_hdr;
  mkserv::samples_request     samp_req(&msg_hdr);
  mkserv::samples_reply       samp_rep(&msg_hdr);

  samp_req.create(setup->mode, setup->feed);
  if (!samp_req.send_message(sock)) exit(2);
  if (!msg_hdr.receive_header(sock)) exit(3);
  if (msg_hdr.tag() == mkserv::ERROR_TAG) {
    mkserv::error_reply  err_rep(&msg_hdr);
    if (!err_rep.receive_body(sock)) exit(4);
    std::cerr << "ERROR: cannot send a SAMPLES message, received an ERROR reply: " << err_rep.err_code() << ":" << err_rep.err_flags() << std::endl;
    return;
  }
  if (!samp_rep.receive_body(sock)) exit(5);
  std::cout << "reply: " << msg_hdr.tag() << msg_hdr.code() << " " << msg_hdr.payload() << std::endl;
  std::cout << "feed: " << samp_rep.feed() << std::endl;
  std::cout << "block: " << samp_rep.block() << std::endl;
  std::cout << "nsamples: " << samp_rep.nsamples() << std::endl;
  std::cout << "nscis: " << samp_rep.nscis() << std::endl;
  for (int32_t i = 0; i < samp_rep.nsamples(); i++) {
    std::cout << "sample[" << i << "] = " << samp_rep.samples(i) << std::endl;
  }
  for (int32_t i = 0; i < samp_rep.nscis(); i++) {
    std::cout << "sci[" << i << "] = " << samp_rep.scis(i) << std::endl;
  }
}

static void send_exit(mkserv::socket &sock)
{
  mkserv::message_header      msg_hdr;

  msg_hdr.create(mkserv::EXIT_TAG, mkserv::REQUEST_CODE, 0);
  if (!msg_hdr.send_header(sock)) exit(2);
  if (!msg_hdr.receive_header(sock)) exit(3);
  std::cout << "reply: " << msg_hdr.tag() << msg_hdr.code() << " " << msg_hdr.payload() << std::endl;
}

int main(int argc, char *argv[])
{
  mkserv::set_application("mksamples");

  static setup_t    setup;
  mkserv::socket    sock;

  setup.host = "localhost";
  setup.port = mkserv::PORT + 1;
  setup.mode = mkserv::DUPLICATES_MODE;
  setup.feed = 0;

#ifdef RINGBUFFER
  // program option declaration
  namespace po = boost::program_options;
  po::options_description desc("Options");
  desc.add_options()("help", "Print help messages");
  desc.add_options()("host", make_opt(setup.host), "The server host");
  desc.add_options()("port", make_opt(setup.port), "The server port");
  desc.add_options()("feed", make_opt(setup.feed), "The feed number (starting with 0)");
  po::variables_map vm;
  try {
    po::store(po::parse_command_line(argc, argv, desc), vm);
    if (vm.count("help")) {
      std::cout << "Read raw samples from a server." << std::endl << desc << std::endl;
      return SUCCESS;
    }
    po::notify(vm);
  } catch (po::error &e) {
    std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
    std::cerr << desc << std::endl;
    return ERROR_IN_COMMAND_LINE;
  }
#endif /* RINGBUFFER */
  if (!sock.connect(setup.host.c_str(), setup.port)) exit(1);
  std::cout << "sock fd = " << sock.cfd() << std::endl;
  send_samples(sock, &setup);
  send_exit(sock);
  sock.finish_client();
}
