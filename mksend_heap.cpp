#include "mksend_heap.h"


namespace mksend
{

  spead2::s_item_pointer_t  heap::_mask = 0L;

  heap::heap(const spead2::flavour &f) : _flavour(f)
  {
    _mask = (1L << (spead2::s_item_pointer_t)f.get_heap_address_bits()) - 1L;
  }

  heap::~heap()
  {
    if (_heap != NULL)
      {
	delete _heap;
	_heap = NULL;
      }
    _nbitems = 0;
  }

  void heap::create_heap()
  {
    if (_heap != NULL) return;
    _heap = new spead2::send::heap(_flavour);
  }

  void heap::set_item(spead2::s_item_pointer_t id, spead2::s_item_pointer_t value)
  {
    int i;

    value &= _mask;
    // ensure that a heap does exist
    create_heap();
    // check if there is already an existing item with the same ID
    for (i = 0; i < _nbitems; i++)
      {
	if (_ids[i] == id)
	  {
	    spead2::send::item   &item = _heap->get_item(_handles[i]);
	    if (item.is_inline)
	      {
		item.data.immediate = value;
		return;
	      }
	    else
	      {
		std::cerr << "PANIC: try to change a non-immediate to an immediate item, not foreseen!" << std::endl;
	      }
	  }
      }
    // we need to add a new item
    _ids[_nbitems] = id;
    _handles[_nbitems] = _heap->add_item(id, value);
    _nbitems++;
  }

  void heap::set_item(spead2::s_item_pointer_t id, const std::uint8_t  *ptr, std::size_t length)
  {
    int i;

    // ensure that a heap does exist
    create_heap();
    // check if there is already an existing item with the same ID
    for (i = 0; i < _nbitems; i++)
      {
	if (_ids[i] == id)
	  {
	    spead2::send::item   &item = _heap->get_item(_handles[i]);
	    if (!item.is_inline)
	      {
		item.data.buffer.ptr = ptr;
		item.data.buffer.length = length;
		return;
	      }
	    else
	      {
		std::cerr << "PANIC: try to change an immediate to a non-immediate item, not foreseen!" << std::endl;
	      }
	  }
      }
    // we need to add a new item
    _ids[_nbitems] = id;
    _handles[_nbitems] = _heap->add_item(id, ptr, length, false);
    _nbitems++;
  }

}
