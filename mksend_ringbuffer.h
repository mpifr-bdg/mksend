#ifndef mksend_ringbuffer_h
#define mksend_ringbuffer_h

#include <memory>

#include "psrdada_cpp/dada_input_stream.hpp"
#include "psrdada_cpp/raw_bytes.hpp"
#include "psrdada_cpp/multilog.hpp"
#include "psrdada_cpp/common.hpp"

#include <spead2/send_stream.h>


#include "mksend_options.h"
#include "mksend_heap.h"
#include "mksend_stream.h"

namespace mksend
{

  class heap_data_deleter {
  public:
    heap_data_deleter() {}
    // do _not_ delete the heap data since it is some memory inside a PSR_DADA ringbuffer slot
    template <class T> void operator()(T* p) {}
  };

  class ringbuffer : public psrdada_cpp::DadaInputStream<ringbuffer>
  {
  protected:
    std::shared_ptr<mksend::options>               opts = NULL;
    bool                                           _quiet = false;
    std::size_t                                    nbytes;
    //bool                                           infinite;
    std::vector<std::unique_ptr<mksend::stream> > &streams;
    //std::size_t                                    stridx = 0; // index of the stream for the _next_ heap
    std::size_t                                    _iheap = 0;
    std::size_t                                    _iout = 0;
    //std::size_t                                    heap_size;   // size of a heap in bytes
    //std::size_t                                    heap_count;  // number of heaps inside one group (same timestamp)
    //std::size_t                                    timestamp;   // timestamp for the heaps inside the current group
    //std::size_t                                    slot_capacity; // number of heap groups inside a ringbuffer slot
    std::size_t                                    total_nslots = 0;
    std::size_t                                    total_nheaps = 0;
    std::size_t                                    total_payload = 0;
    std::size_t                                    total_duration = 0;
    std::size_t                                    total_send = 0;
    std::size_t                                    total_flush = 0;
  public:
    ringbuffer(key_t key, psrdada_cpp::MultiLog &mlog, std::shared_ptr<mksend::options> opts, std::vector<std::unique_ptr<mksend::stream> > &streams);
    ~ringbuffer();
    void on_connect(psrdada_cpp::RawBytes& block);
    void on_next(psrdada_cpp::RawBytes& block);
    void simulate();
    void init(psrdada_cpp::RawBytes& block) {on_connect(block); };
    bool operator()(psrdada_cpp::RawBytes& block) {  on_next(block); return false; }
  protected:
    void handle_end_of_transmission();
    void handle_send_heaps(psrdada_cpp::RawBytes& block);
  };


}

#endif /* mksend_ringbuffer_h */
