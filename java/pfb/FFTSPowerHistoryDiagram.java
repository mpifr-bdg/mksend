package pfb;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.util.Arrays;
import java.util.Vector;

import java.text.*;

public class FFTSPowerHistoryDiagram extends FFTSDiagram {

    protected Color            colorI       = new Color(255, 0,   0, 255);
    protected Color            colorQ       = new Color(  0, 0, 255, 255);
    protected JButton          btnIQ        = null;
    protected JToggleButton    btnAutoscale = null;
    protected JButton          btnClear     = null;
    protected JToggleButton    btnOn        = null;
    protected JToggleButton    btnOff       = null;
    protected JButton          btnYRange    = null;
    protected boolean          autoscale    = true;
    protected boolean          selI         = true;
    protected boolean          onFlag       = true;
    protected boolean          offFlag      = true;
    protected int              hlen         = 0;
    protected float[]          historyI     = null;
    protected float[]          historyQ     = null;

    public FFTSPowerHistoryDiagram(int width, int height, int left, int right, int bottom, int top) {
	super(width, height, left, right, bottom, top);
	btnIQ        = createSimpleButton("I");
	btnAutoscale = createToggleButton("A", autoscale);
	btnClear     = createSimpleButton("C");
	//btnOn        = createToggleButton("On", getOnFlag());
	//btnOff       = createToggleButton("Off", getOffFlag());
	btnYRange    = createSimpleButton("Y");
	createInputField();
	historyI = new float[width];
 	historyQ = new float[width];
	yRangeFlag = true;
    }

    public boolean getAutoscale() { return autoscale; }

    public void setAutoscale(boolean flag) {
	autoscale = flag;
    }

    public boolean getOnFlag() { return onFlag; }

    public void setOnFlag(boolean flag) { onFlag = flag; }

    public boolean getOffFlag() { return offFlag; }

    public void setOffFlag(boolean flag) { offFlag = flag; }

    public void reset() { hlen = 0; }

    public void setSpectrum(FFTSReplySpectrum spectrum) {
	int    i;
	double val;
	if ((spectrum.phase == 1) && !offFlag) return;
	if ((spectrum.phase == 2) && !onFlag) return;
	//System.out.println("FFTSPowerHistoryDiagram.setSpectrum() : tp = " + spectrum.tpI + ", " + spectrum.tpQ);
	if (hlen < historyI.length) {
	    historyI[hlen] = spectrum.tpI;
	    historyQ[hlen] = spectrum.tpQ;
	    hlen++;
	} else {
	    for (i = 0; i < historyI.length - 1; i++) {
		historyI[i] = historyI[i + 1];
		historyQ[i] = historyQ[i + 1];
	    }
	    historyI[hlen - 1] = spectrum.tpI;
	    historyQ[hlen - 1] = spectrum.tpQ;
	}
	clearShapes();
	clearColors();
	if (hlen < 2) return;
	float[] hist = null;
	if (selI) {
	    hist = historyI;
	} else {
	    hist = historyQ;
	}
	// If autoscale is used, recalculate the lowest and highest y-values.
	if (autoscale) {
	    double yminr, ymaxr;
	    val = hist[0];
	    yminr = val;
	    ymaxr = val;
	    for (i = 0; i < hlen; i++) {
		val = hist[i];
		if (val > ymaxr) ymaxr = val;
		if (val < yminr) yminr = val;
	    }
	    if (yminr == ymaxr) {
		setYRange(yminr - 1, ymaxr + 1);
	    } else {
		setYRange(yminr, ymaxr);
	    }
	}
	setXRange(-hlen + 1, 0);
	// calculate the scaling parameters
	double xscale = (double)width/(double)(hlen - 1);
	double yoff   = yMin;
	double yscale = (double)height/(yMax - yMin);
	GeneralPath p;
	// Create the power history plot
	p = new GeneralPath();
	val = hist[0];
	p.moveTo(0.0, yscale*(val - yoff));
	for (i = 1; i < hlen; i++) {
	    val = hist[i];
	    p.lineTo(xscale*(double)i,
		     yscale*(val - yoff));
	}
	addShape(p);
	if (selI) {
	    addColor(colorI);
	} else {
	    addColor(colorQ);
	}
	repaint();
    }

    public void actionPerformed(ActionEvent e) {
	if (e.getSource() == btnIQ) {
	    if (selI) {
		// I -> Q
		selI = false;
		btnIQ.setText("Q");
	    } else {
		// Q -> I
		selI = true;
		btnIQ.setText("I");
	    }
	} else if (e.getSource() == btnAutoscale) {
	    setAutoscale(btnAutoscale.isSelected());
	} else if (e.getSource() == btnClear) {
	    reset();
	} else if (e.getSource() == btnOn) {
	    setOnFlag(btnOn.isSelected());
	} else if (e.getSource() == btnOff) {
	    setOffFlag(btnOff.isSelected());
	} else if (e.getSource() == btnYRange) {
	    yRangeCount = 2;
	} else {
	    super.actionPerformed(e);
	}
    }

    public void handleYRange(double ymin, double ymax) {
	//System.out.println("FFTSPowerHistoryDiagram.handleYRange(" + ymin + ", " + ymax + ")");
	btnAutoscale.setSelected(false);
	autoscale = false;
	setYRange(ymin, ymax);
    }

}
