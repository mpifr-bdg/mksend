package pfb;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.text.*;

import pfb.FFTSGridBagPanel;

public class NumberDialog extends JDialog implements ActionListener {

    protected FFTSGridBagPanel panel        = null;

    protected boolean          isFlonum     = true;

    protected Font             tfFont       = null;
    protected Font             btnFont      = null;

    protected boolean          textOk       = false;
    protected JTextField       textField    = null;

    protected JPanel           dRow1        = null;
    protected JPanel           dRow2        = null;
    protected JPanel           dRow3        = null;
    protected JPanel           dRow4        = null;

    protected JButton          btnDigit0    = null;
    protected JButton          btnDigit1    = null;
    protected JButton          btnDigit2    = null;
    protected JButton          btnDigit3    = null;
    protected JButton          btnDigit4    = null;
    protected JButton          btnDigit5    = null;
    protected JButton          btnDigit6    = null;
    protected JButton          btnDigit7    = null;
    protected JButton          btnDigit8    = null;
    protected JButton          btnDigit9    = null;

    protected JButton          btnMinus     = null;
    protected JButton          btnDecimal   = null;
    protected JButton          btnBackSpace = null;

    protected JSeparator       sep          = null;

    protected JPanel           btnPanel     = null;
    protected JButton          btnOk        = null;
    protected JButton          btnCancel    = null;

    protected NumberDialog() {
        super((JFrame)null, true);
    }

    protected void createGUI() {
	tfFont  = new Font("Dialog", Font.PLAIN, 18);
	btnFont = new Font("Dialog", Font.BOLD, 18);
	panel = new FFTSGridBagPanel();
	textField    = new JTextField();
	textField.setFont(tfFont);
	//textField.setEditable(false);
	btnDigit0    = createSimpleButton("0");
	btnDigit1    = createSimpleButton("1");
	btnDigit2    = createSimpleButton("2");
	btnDigit3    = createSimpleButton("3");
	btnDigit4    = createSimpleButton("4");
	btnDigit5    = createSimpleButton("5");
	btnDigit6    = createSimpleButton("6");
	btnDigit7    = createSimpleButton("7");
	btnDigit8    = createSimpleButton("8");
	btnDigit9    = createSimpleButton("9");

	btnMinus     = createSimpleButton("-");
	btnDecimal   = createSimpleButton(".");
	btnBackSpace = createSimpleButton("Del");

	dRow1 = new JPanel();
	dRow1.add(btnDigit7);
	dRow1.add(btnDigit8);
	dRow1.add(btnDigit9);
	dRow1.add(btnMinus);
	dRow2 = new JPanel();
	dRow2.add(btnDigit4);
	dRow2.add(btnDigit5);
	dRow2.add(btnDigit6);
	dRow3 = new JPanel();
	dRow3.add(btnDigit1);
	dRow3.add(btnDigit2);
	dRow3.add(btnDigit3);
	dRow4 = new JPanel();
	dRow4.add(btnDigit0);
	dRow4.add(btnDecimal);
	dRow4.add(btnBackSpace);

	sep = new JSeparator();
	sep.setMinimumSize(new Dimension(5, 5));
	sep.setPreferredSize(new Dimension(5, 5));

	btnPanel     = new JPanel();
	btnOk        = new JButton("Ok");
	btnOk.addActionListener(this);
	btnCancel    = new JButton("Cancel");
	btnCancel.addActionListener(this);
	btnPanel.add(btnOk);
	btnPanel.add(btnCancel);
    }

    protected void layoutKeys() {
	panel.grid(textField,        0, 0, "rl");
	panel.grid(new JPanel(),     0, 1, "c");
	panel.grid(dRow1,            0, 2, "l");
	panel.grid(dRow2,            0, 3, "l");
	panel.grid(dRow3,            0, 4, "l");
	panel.grid(dRow4,            0, 5, "l");
	panel.grid(new JPanel(),     0, 6, "c");
	panel.grid(sep,              0, 7, "rltb");
	panel.grid(new JPanel(),     0, 8, "c");
	panel.grid(btnPanel,         0, 9, "l");
	setContentPane(panel);
        pack();
    }

    public void enableFixnum() {
	isFlonum = false;
	btnDecimal.setEnabled(false);
    }

    public void enableFlonum() {
	isFlonum = true;
	btnDecimal.setEnabled(true);
    }

    protected JPanel createSpace() {
	JPanel panel = new JPanel();
	panel.setMinimumSize(new Dimension(25, 40));
	panel.setPreferredSize(new Dimension(25, 40));
	return panel;
    }

    protected JButton createSimpleButton(String label) {
	JButton btn = new JButton(label);
	btn.setFont(btnFont);
	btn.setMargin(new Insets(0,0,0,0));
	btn.setMinimumSize(new Dimension(50, 40));
	btn.setPreferredSize(new Dimension(50, 40));
	//btn.setMaximumSize(new Dimension(rightMargin, 30));
	btn.addActionListener(this);
	return btn;
    }

    public boolean show(String title, String text) {
	setTitle(title);
	textField.setText(text);
	textOk = false;
	setVisible(true);
	return textOk;
    }

    public String getText() {
	if (textOk) {
	    return textField.getText();
	} else {
	    return null;
	}
    }

    public void actionPerformed(ActionEvent e) {
	if (e.getSource() == btnDigit0) {
	    textField.setText(textField.getText() + "0");
	} else if (e.getSource() == btnDigit1) {
	    textField.setText(textField.getText() + "1");
	} else if (e.getSource() == btnDigit2) {
	    textField.setText(textField.getText() + "2");
	} else if (e.getSource() == btnDigit3) {
	    textField.setText(textField.getText() + "3");
	} else if (e.getSource() == btnDigit4) {
	    textField.setText(textField.getText() + "4");
	} else if (e.getSource() == btnDigit5) {
	    textField.setText(textField.getText() + "5");
	} else if (e.getSource() == btnDigit6) {
	    textField.setText(textField.getText() + "6");
	} else if (e.getSource() == btnDigit7) {
	    textField.setText(textField.getText() + "7");
	} else if (e.getSource() == btnDigit8) {
	    textField.setText(textField.getText() + "8");
	} else if (e.getSource() == btnDigit9) {
	    textField.setText(textField.getText() + "9");
	} else if (e.getSource() == btnMinus) {
	    textField.setText(textField.getText() + "-");
	} else if (e.getSource() == btnDecimal) {
	    textField.setText(textField.getText() + ".");
	} else if (e.getSource() == btnBackSpace) {
	    String str = textField.getText();
	    if (str.length() != 0) {
		textField.setText(str.substring(0, str.length() - 1));
	    }
	} else if (e.getSource() == btnOk) {
	    textOk = true;
	    setVisible(false);
	} else if (e.getSource() == btnCancel) {
	    textOk = false;
	    setVisible(false);
	}
    }

    static private NumberDialog dialog = null;

    static public double enterFlonum(String title, String num) {
	if (dialog == null) {
	    dialog = new NumberDialog();
	    dialog.createGUI();
	    dialog.layoutKeys();
	}
	dialog.enableFlonum();
	boolean ok = dialog.show(title, num);
	try {
	    if (ok) {
		return Double.parseDouble(dialog.getText());
	    } else {
		return Double.parseDouble(num);
	    }
	} catch (Exception e) {
	    try {
		return Double.parseDouble(num);
	    } catch (Exception e2) {
		return 0.0;
	    }
	}
    }

    static public int enterFixnum(String title, String num) {
	if (dialog == null) {
	    dialog = new NumberDialog();
	    dialog.createGUI();
	    dialog.layoutKeys();
	}
	dialog.enableFixnum();
	boolean ok = dialog.show(title, num);
	try {
	    if (ok) {
		return Integer.parseInt(dialog.getText());
	    } else {
		return Integer.parseInt(num);
	    }
	} catch (Exception e) {
	    try {
		return Integer.parseInt(num);
	    } catch (Exception e2) {
		return 0;
	    }
	}
    }


}
