package pfb;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.util.Arrays;
import java.util.Vector;

import java.text.*;

public class FFTSPowerDiagram extends FFTSDiagram {

    protected static final int MAX_BOARDS  = 32;

    protected Color             colorI       = new Color(255, 0,   0, 255);
    protected Color             colorQ       = new Color(  0, 0, 255, 255);
    protected JButton           btnIQ        = null;
    protected boolean           selI         = true;
    protected float[]           valuesI      = new float[MAX_BOARDS];
    protected float[]           valuesQ      = new float[MAX_BOARDS];
    protected boolean[]         valid        = new boolean[MAX_BOARDS];

    public FFTSPowerDiagram(int width, int height, int left, int right, int bottom, int top) {
	super(width, height, left, right, bottom, top);
	clearValues();
	setXGridRange(1, MAX_BOARDS);
	btnIQ = createSimpleButton("I");
	btnIQ.setPreferredSize(new Dimension(rightMargin, 20));
    }

    public void clearValues() {
	for (int i = 0; i < valuesI.length; i++) {
	    valuesI[i] = 0.0F;
	    valuesQ[i] = 0.0F;
	    valid[i] = false;
	}
    }

    public void setSpectrum(FFTSReplySpectrum spectrum) {
	if (spectrum.board >= valuesI.length) return;
	clearShapes();
	clearColors();
	valuesI[spectrum.board - 1] = spectrum.tpI;
	valuesQ[spectrum.board - 1] = spectrum.tpQ;
	valid[spectrum.board - 1] = true;
	int i;
	// Skalierung der X-Achse:
	double xscale = ((double)width)/(double)(valuesI.length + 2);
	double xoff   = xscale;
	double bsize  = 0.4*xscale; // 1/2 Breite eines Balkens
	// Skalierung der Y-Achse:
	boolean found = false;
	double ymin = 0.0;
	double ymax = 0.0;
	if (selI) {
	    for (i = 0; i < valuesI.length; i++) {
		if (!valid[i]) continue;
		if (!found) {
		    ymin = valuesI[i];
		    ymax = valuesI[i];
		    found = true;
		}
		if (valuesI[i] < ymin) ymin = valuesI[i];
		if (valuesI[i] > ymax) ymax = valuesI[i];
	    }
	} else {
	    for (i = 0; i < valuesQ.length; i++) {
		if (!valid[i]) continue;
		if (!found) {
		    ymin = valuesQ[i];
		    ymax = valuesQ[i];
		    found = true;
		}
		if (valuesQ[i] < ymin) ymin = valuesQ[i];
		if (valuesQ[i] > ymax) ymax = valuesQ[i];
	    }
	}

	double yscale = ymax/((double)height);
	if (yscale == 0.0) yscale = 1.0;
	for (i = 0; i < valuesI.length; i++) {
	    if (!valid[i]) continue;
	    Rectangle2D.Double rect;
	    if (selI) {
		rect = new Rectangle2D.Double(
					      xoff + xscale*((double)i) - bsize,
					      0.0,
					      2.0*bsize,
					      valuesI[i]/yscale
					      );
		addShape(rect);
		addColor(colorI);
	    } else {
		rect = new Rectangle2D.Double(
					      xoff + xscale*((double)i) - bsize,
					      0.0,
					      2.0*bsize,
					      valuesQ[i]/yscale
					      );
		addShape(rect);
		addColor(colorQ);
	    }
	}
	setXRange(0.0, valuesI.length + 1);
	setYRange(0.0, ymax);
	repaint();
    }

    public void actionPerformed(ActionEvent e) {
	if (e.getSource() == btnIQ) {
	    if (selI) {
		// I -> Q
		selI = false;
		btnIQ.setText("Q");
	    } else {
		// Q -> I
		selI = true;
		btnIQ.setText("I");
	    }
	}
    }

}
