package pfb;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.util.Arrays;
import java.util.Vector;

public class FFTSOnOffDiagram extends FFTSDiagram {

    protected FFTSFilterPanel  filterPanel  = null;
    protected Color            curColor     = new Color(255,   0, 0, 255);
    protected JToggleButton    btnAutoscale = null;
    protected JButton          btnXRange    = null;
    protected JButton          btnYRange    = null;
    protected boolean          autoscale    = true;
    protected float[]          onSpectrum   = null;
    protected float[]          offSpectrum  = null;
    protected float[]          resSpectrum  = null;

    public FFTSOnOffDiagram(int width, int height, int left, int right, int bottom, int top) {
	super(width, height, left, right, bottom, top);
	btnAutoscale = createToggleButton("A", autoscale);
	btnXRange    = createSimpleButton("X");
	btnYRange    = createSimpleButton("Y");
	createInputField();
	xRangeFlag = true;
	yRangeFlag = true;
    }

    public void setFilterPanel(FFTSFilterPanel panel) { filterPanel = panel; }

    public boolean getAutoscale() { return autoscale; }

    public void setAutoscale(boolean flag) {
	autoscale = flag;
    }

    public void setSpectrum(FFTSReplySpectrum spectrum) {
	int    i;
	double val;
	if (spectrum.phase == 1) {
	    if ((offSpectrum == null) || (offSpectrum.length != spectrum.count)) {
		offSpectrum = new float[spectrum.count];
	    }
	    for (i = 0; i < offSpectrum.length; i++) {
		offSpectrum[i] = spectrum.spectrum[i];
	    }
	} else if (spectrum.phase == 2) {
	    if ((onSpectrum == null) || (onSpectrum.length != spectrum.count)) {
		onSpectrum = new float[spectrum.count];
	    } else {
		for (i = 0; i < onSpectrum.length; i++) {
		    onSpectrum[i] = spectrum.spectrum[i];
		}
	    }
	}
	if (spectrum.phase != 2) return;
	if (offSpectrum == null) return;
	if (onSpectrum == null) return;
	if (offSpectrum.length != onSpectrum.length) return;
	clearShapes();
	clearColors();
	setXRange(spectrum.freq_low, spectrum.freq_high);
	// calculate (on - off)/off
	if ((resSpectrum == null) || (resSpectrum.length != onSpectrum.length)) {
	    resSpectrum = new float[onSpectrum.length];
	}
	for (i = 0; i < resSpectrum.length; i++) {
	    resSpectrum[i] = (onSpectrum[i] - offSpectrum[i])/offSpectrum[i];
	}
	// If autoscale is used, recalculate the lowest and highest y-values.
	if (autoscale) {
	    double yminr, ymaxr;
	    val = resSpectrum[1];
	    yminr = val;
	    ymaxr = val;
	    for (i = 1; i < resSpectrum.length; i++) {
		val = resSpectrum[i];
		if (val > ymaxr) ymaxr = val;
		if (val < yminr) yminr = val;
	    }
	    if (yminr == ymaxr) {
		setYRange(yminr - 1, ymaxr + 1);
	    } else {
		setYRange(yminr, ymaxr);
	    }
	}
	// calculate the scaling parameters
	double xscale = ((double)width)/(double)resSpectrum.length;
	double yoff = yMin;
	double yscale = (double)height/(yMax - yMin);
	// Create the on/off plot.
	GeneralPath p = new GeneralPath();
	p.moveTo(0.0, yscale*(resSpectrum[0] - yoff));
	for (i = 1; i < resSpectrum.length; i++) {
	    p.lineTo(xscale*(double)i,
		     yscale*(resSpectrum[i] - yoff));
	}
	addShape(p);
	addColor(curColor);
	repaint();
    }

    public void actionPerformed(ActionEvent e) {
	if (e.getSource() == btnAutoscale) {
	    setAutoscale(btnAutoscale.isSelected());
	} else if (e.getSource() == btnXRange) {
	    xRangeCount = 2;
	} else if (e.getSource() == btnYRange) {
	    yRangeCount = 2;
	} else {
	    super.actionPerformed(e);
	}
    }

    public void handleXRange(double xmin, double xmax) {
	//System.out.println("FFTSOnOffDiagram.handleXRange(" + xmin + ", " + xmax + ")");
	if (filterPanel != null) {
	    filterPanel.setFrequencyRange((float)xmin, (float)xmax);
	}
    }

    public void handleYRange(double ymin, double ymax) {
	//System.out.println("FFTSOnOffDiagram.handleYRange(" + ymin + ", " + ymax + ")");
	btnAutoscale.setSelected(false);
	autoscale = false;
	setYRange(ymin, ymax);
    }

}
