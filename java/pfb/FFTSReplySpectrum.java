package pfb;

import java.io.*;
import java.net.*;
import java.nio.*;

public class FFTSReplySpectrum {
    public static final short FLAG_TIME_ERR       =  (1 << 0);
    public static final short FLAG_TRANSMIT_ERR   =  (1 << 1);

    public static final short FLAG_OVERFLOW_WOLA  =  (1 << 0);
    public static final short FLAG_OVERFLOW_FFT   =  (1 << 1);
    public static final short FLAG_OVERFLOW_TRICK =  (1 << 2);
    public static final short FLAG_OVERFLOW_INT   =  (1 << 3);
    public static final short FLAG_OVERFLOW_ADC   =  (1 << 4);
    public static final short FLAG_OVERFLOW_COPY  =  (1 << 5);
    public static final short FLAG_OVERFLOW_ETH   =  (1 << 6);
    public static final short FLAG_OVERFLOW_CLK   =  (1 << 7);

    public static final short FLAG_EXT_BLANK_SYNC =  (1 << 0);

    public char     tag;
    public char     code;
    public int      payload;
    public int      nreceived;
    public int      nsaturated;
    public short    crate;
    public short    board;
    public float    freq_low;    // the lowest frequency (spectrum[0])
    public float    freq_high;   // the highest frequency (spectrum[count-1])
    public int      count;       // count contains the number of channels in the processed raw data
    public int      binning;     // the number of raw data channels used to compute one sample
    public int      dcnt;        // dcnt
    public int      dumpcnt;     // dumpcnt
    public int      intCnt;      // intCnt
    public int      day;         // dayofyear + 1000*dummy1 ???
    public int      second;      // hour*100*100 + min*100 + sec
    public int      microsecond; // microsec
    public int      timeSync;    // synctime
    public int      timeBlank;   // blanktime
    public short    bandwidth;   // bandwidth
    public short    channels;    // channels
    public short    tempBoard;   // tempBoard
    public short    tempFPGA;    // tempFPGA
    public short    tempADC0;    // tempADC0
    public short    tempADC1;    // tempADC1
    public short    phase;       // phaseCur OR phase
    public short    phaseCount;  // numofphases OR numofphases2
    public short    flagsError;  // timeErr, transmitErr OR transmitErr2
    public short    flagsI;      // IovfWOLA, IovfFFT, IovfTrick, IovfInt, IovfADC, IovfCopy, IovfEth, IovfClk
    public short    flagsQ;      // QovfWOLA, QovfFFT, QovfTrick, QovfInt, QovfADC, QovfCopy, QovfEth, QovfClk
    public short    flagsSystem; // BlankSync OR BlankSync2
    public int[]    adcHistI;    // adcLev[*].I
    public int[]    adcHistQ;    // adcLev[*].Q
    public float    tpI;         // 10.0*log10(totPwrI/tpCntI) + tpOffsetLSB/256
    public float    tpQ;         // 10.0*log10(totPwrQ/tpCntQ) + tpOffsetLSB/256
    public float[]  spectrum;    // processed (mapped) raw data
    
    public FFTSReplySpectrum() {
	adcHistI = new int[32];
	adcHistQ = new int[32];
    }

    public void alloc(int count) {
	if (count == 0) return;
	if (spectrum == null) {
	    spectrum = new float[count];
	} else if (spectrum.length < count) {
	    spectrum = new float[count];
	}
    }

    public boolean receive(DataInputStream sock) {
	try {
	    this.tag         = (char)sock.readByte();
	    this.code        = (char)sock.readByte();
	    this.payload     = sock.readInt();
	    if (tag != 'S') {
		return false;
	    }
	    this.nreceived   = sock.readInt();
	    this.nsaturated  = sock.readInt();
	    this.crate       = 1;
	    this.board       = 1;
	    this.freq_low    = sock.readFloat();
	    this.freq_high   = sock.readFloat();
	    this.count       = sock.readInt();
	    this.binning     = sock.readInt();
	    /*
	    System.out.println("reply: cmd=" + this.cmd
			       + ", err=" + this.err
			       + ", crate=" + this.crate
			       + ", board=" + this.board
			       + ", frequency=[" + this.freq_low + " ... " + this.freq_high
			       + "], count=" + this.count
			       + ", binning=" + this.binning);
	    */
	    // read the header from the stream
	    this.dcnt        = 0;     // dcnt
	    this.dumpcnt     = 0;     // dumpcnt
	    this.intCnt      = 0;     // intCnt
	    //System.out.println("crate=" + this.crate + ", board=" + this.board + ", dcnt=" + this.dcnt + ", dumpcnt=" + this.dumpcnt + ", intCnt=" + this.intCnt);
	    this.day         = 0;     // dayofyear + 1000*dummy1 ???
	    this.second      = 0;     // hour*100*100 + min*100 + sec
	    this.microsecond = 0;     // microsec
	    //System.out.println("day=" + this.day + ", second=" + this.second + ", microsecond=" + this.microsecond);
	    this.timeSync    = 0;     // synctime
	    this.timeBlank   = 0;     // blanktime
	    //System.out.println("timeSync=" + this.timeSync + ", timeBlank=" + this.timeBlank);
	    this.bandwidth   = 1350;   // bandwidth
	    this.channels    = 16;     // channels
	    //System.out.println("bandwidth=" + this.bandwidth + " Mhz, channels=" + (1 << this.channels));
	    this.tempBoard   = 0;   // tempBoard
	    this.tempFPGA    = 0;   // tempFPGA
	    this.tempADC0    = 0;   // tempADC0
	    this.tempADC1    = 0;   // tempADC1
	    //System.out.println("tempBoard=" + (float)this.tempBoard/32 + ", tempFPGA=" + (float)this.tempFPGA/32 + ", tempADC0=" + (float)this.tempADC0/32 + ", tempADC1=" + (float)this.tempADC1/32);
	    this.phase       = 1;   // phaseCur OR phase
	    this.phaseCount  = 1;   // numofphases OR numofphases2
	    this.flagsError  = 0;   // timeErr, transmitErr OR transmitErr2
	    this.flagsI      = 0;   // IovfWOLA, IovfFFT, IovfTrick, IovfInt, IovfADC, IovfCopy, IovfEth, IovfClk
	    this.flagsQ      = 0;   // QovfWOLA, QovfFFT, QovfTrick, QovfInt, QovfADC, QovfCopy, QovfEth, QovfClk
	    this.flagsSystem = 0;   // BlankSync OR BlankSync2
	    for (int i = 0; i < 32; i++) {
		this.adcHistI[i] = 0; // adcLev[*].I
		//System.out.println("adcHist[" + i + "] I=" + this.adcHistI[i] + ", Q=" + this.adcHistQ[i]);
	    }
	    for (int i = 0; i < 32; i++) {
		this.adcHistQ[i] = 0; // adcLev[*].Q
		//System.out.println("adcHist[" + i + "] I=" + this.adcHistI[i] + ", Q=" + this.adcHistQ[i]);
	    }
	    this.tpI = 0.0F;           // 10.0*log10(totPwrI/tpCntI) + tpOffsetLSB/256
	    this.tpQ = 0.0F;           // 10.0*log10(totPwrQ/tpCntQ) + tpOffsetLSB/256
	    alloc(this.count);
	    for (int i = 0; i < count; i++) {
		this.spectrum[i] = sock.readFloat();
		//System.out.println("spectrum[" + i + "] = " + this.spectrum[i]);
	    }
	    return true;
	} catch (IOException e) {
            System.err.println("Could not receive a GET_SPECTRUM reply");
	    e.printStackTrace();
            return false;
	}
    }

}

