package pfb;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.text.*;

import pfb.FFTSGridBagPanel;

public class AlphaLowerCaseDialog extends NumberDialog {

    protected JPanel           aRow1        = null;
    protected JPanel           aRow2        = null;
    protected JPanel           aRow3        = null;
    protected JPanel           aRow4        = null;

    protected JButton          btnAlphaA    = null;
    protected JButton          btnAlphaB    = null;
    protected JButton          btnAlphaC    = null;
    protected JButton          btnAlphaD    = null;
    protected JButton          btnAlphaE    = null;
    protected JButton          btnAlphaF    = null;
    protected JButton          btnAlphaG    = null;
    protected JButton          btnAlphaH    = null;
    protected JButton          btnAlphaI    = null;
    protected JButton          btnAlphaJ    = null;
    protected JButton          btnAlphaK    = null;
    protected JButton          btnAlphaL    = null;
    protected JButton          btnAlphaM    = null;
    protected JButton          btnAlphaN    = null;
    protected JButton          btnAlphaO    = null;
    protected JButton          btnAlphaP    = null;
    protected JButton          btnAlphaQ    = null;
    protected JButton          btnAlphaR    = null;
    protected JButton          btnAlphaS    = null;
    protected JButton          btnAlphaT    = null;
    protected JButton          btnAlphaU    = null;
    protected JButton          btnAlphaV    = null;
    protected JButton          btnAlphaW    = null;
    protected JButton          btnAlphaX    = null;
    protected JButton          btnAlphaY    = null;
    protected JButton          btnAlphaZ    = null;

    protected JButton          btnSpace     = null;

    private AlphaLowerCaseDialog() {
        super();
    }

    protected void createGUI() {
	super.createGUI();
	btnAlphaA = createSimpleButton("a");
	btnAlphaB = createSimpleButton("b");
	btnAlphaC = createSimpleButton("c");
	btnAlphaD = createSimpleButton("d");
	btnAlphaE = createSimpleButton("e");
	btnAlphaF = createSimpleButton("f");
	btnAlphaG = createSimpleButton("g");
	btnAlphaH = createSimpleButton("h");
	btnAlphaI = createSimpleButton("i");
	btnAlphaJ = createSimpleButton("j");
	btnAlphaK = createSimpleButton("k");
	btnAlphaL = createSimpleButton("l");
	btnAlphaM = createSimpleButton("m");
	btnAlphaN = createSimpleButton("n");
	btnAlphaO = createSimpleButton("o");
	btnAlphaP = createSimpleButton("p");
	btnAlphaQ = createSimpleButton("q");
	btnAlphaR = createSimpleButton("r");
	btnAlphaS = createSimpleButton("s");
	btnAlphaT = createSimpleButton("t");
	btnAlphaU = createSimpleButton("u");
	btnAlphaV = createSimpleButton("v");
	btnAlphaW = createSimpleButton("w");
	btnAlphaX = createSimpleButton("x");
	btnAlphaY = createSimpleButton("y");
	btnAlphaZ = createSimpleButton("z");

	btnSpace     = createSimpleButton("Space");
	btnSpace.setMinimumSize(new Dimension(300, 40));
	btnSpace.setPreferredSize(new Dimension(300, 40));

	aRow1 = new JPanel();
	aRow1.add(btnAlphaQ);
	aRow1.add(btnAlphaW);
	aRow1.add(btnAlphaE);
	aRow1.add(btnAlphaR);
	aRow1.add(btnAlphaT);
	aRow1.add(btnAlphaY);
	aRow1.add(btnAlphaU);
	aRow1.add(btnAlphaI);
	aRow1.add(btnAlphaO);
	aRow1.add(btnAlphaP);
	aRow2 = new JPanel();
	aRow2.add(createSpace());
	aRow2.add(btnAlphaA);
	aRow2.add(btnAlphaS);
	aRow2.add(btnAlphaD);
	aRow2.add(btnAlphaF);
	aRow2.add(btnAlphaG);
	aRow2.add(btnAlphaH);
	aRow2.add(btnAlphaJ);
	aRow2.add(btnAlphaK);
	aRow2.add(btnAlphaL);
	aRow3 = new JPanel();
	aRow3.add(createSpace());
	aRow3.add(createSpace());
	aRow3.add(btnAlphaZ);
	aRow3.add(btnAlphaX);
	aRow3.add(btnAlphaC);
	aRow3.add(btnAlphaV);
	aRow3.add(btnAlphaB);
	aRow3.add(btnAlphaN);
	aRow3.add(btnAlphaM);
	aRow4 = new JPanel();
	aRow4.add(createSpace());
	aRow4.add(createSpace());
	aRow4.add(createSpace());
	aRow4.add(createSpace());
	aRow4.add(createSpace());
	aRow4.add(btnSpace);
    }

    protected void layoutKeys() {
	panel.grid(textField,        0, 0, 2, 1, "rl");
	panel.grid(new JPanel(),     0, 1, "c");
	panel.grid(aRow1,            0, 2, "l");
	panel.grid(dRow1,            1, 2, "l");
	panel.grid(aRow2,            0, 3, "l");
	panel.grid(dRow2,            1, 3, "l");
	panel.grid(aRow3,            0, 4, "l");
	panel.grid(dRow3,            1, 4, "l");
	panel.grid(aRow4,            0, 5, "l");
	panel.grid(dRow4,            1, 5, "l");
	panel.grid(new JPanel(),     0, 6, "c");
	panel.grid(sep,              0, 7, 2, 1, "rltb");
	panel.grid(new JPanel(),     0, 8, "c");
	panel.grid(btnPanel,         0, 9, 2, 1, "l");
	setContentPane(panel);
        pack();
    }

    public void actionPerformed(ActionEvent e) {
	if (e.getSource() == btnAlphaA) {
	    textField.setText(textField.getText() + "a");
	} else if (e.getSource() == btnAlphaB) {
	    textField.setText(textField.getText() + "b");
	} else if (e.getSource() == btnAlphaC) {
	    textField.setText(textField.getText() + "c");
	} else if (e.getSource() == btnAlphaD) {
	    textField.setText(textField.getText() + "d");
	} else if (e.getSource() == btnAlphaE) {
	    textField.setText(textField.getText() + "e");
	} else if (e.getSource() == btnAlphaF) {
	    textField.setText(textField.getText() + "f");
	} else if (e.getSource() == btnAlphaG) {
	    textField.setText(textField.getText() + "g");
	} else if (e.getSource() == btnAlphaH) {
	    textField.setText(textField.getText() + "h");
	} else if (e.getSource() == btnAlphaI) {
	    textField.setText(textField.getText() + "i");
	} else if (e.getSource() == btnAlphaJ) {
	    textField.setText(textField.getText() + "j");
	} else if (e.getSource() == btnAlphaK) {
	    textField.setText(textField.getText() + "k");
	} else if (e.getSource() == btnAlphaL) {
	    textField.setText(textField.getText() + "l");
	} else if (e.getSource() == btnAlphaM) {
	    textField.setText(textField.getText() + "m");
	} else if (e.getSource() == btnAlphaN) {
	    textField.setText(textField.getText() + "n");
	} else if (e.getSource() == btnAlphaO) {
	    textField.setText(textField.getText() + "o");
	} else if (e.getSource() == btnAlphaP) {
	    textField.setText(textField.getText() + "p");
	} else if (e.getSource() == btnAlphaQ) {
	    textField.setText(textField.getText() + "q");
	} else if (e.getSource() == btnAlphaR) {
	    textField.setText(textField.getText() + "r");
	} else if (e.getSource() == btnAlphaS) {
	    textField.setText(textField.getText() + "s");
	} else if (e.getSource() == btnAlphaT) {
	    textField.setText(textField.getText() + "t");
	} else if (e.getSource() == btnAlphaU) {
	    textField.setText(textField.getText() + "u");
	} else if (e.getSource() == btnAlphaV) {
	    textField.setText(textField.getText() + "v");
	} else if (e.getSource() == btnAlphaW) {
	    textField.setText(textField.getText() + "w");
	} else if (e.getSource() == btnAlphaX) {
	    textField.setText(textField.getText() + "x");
	} else if (e.getSource() == btnAlphaY) {
	    textField.setText(textField.getText() + "y");
	} else if (e.getSource() == btnAlphaZ) {
	    textField.setText(textField.getText() + "z");
	} else if (e.getSource() == btnDecimal) {
	    textField.setText(textField.getText() + ".");
	} else if (e.getSource() == btnSpace) {
	    textField.setText(textField.getText() + " ");
	} else {
	    super.actionPerformed(e);
	}
    }

    static private AlphaLowerCaseDialog dialog = null;

    static public String enterString(String title, String text) {
	if (dialog == null) {
	    dialog = new AlphaLowerCaseDialog();
	    dialog.createGUI();
	    dialog.layoutKeys();
	}
	if (dialog.show(title, text)) {
	    return dialog.getText();
	} else {
	    return text;
	}
    }

}
