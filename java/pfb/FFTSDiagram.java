package pfb;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.text.*;

import java.util.Vector;

public class FFTSDiagram extends JPanel implements ActionListener, MouseListener {

    public static final int INSET  = 4;
    public static final int LEFT   = 50;
    public static final int RIGHT  = 30;
    public static final int BOTTOM = 16;
    public static final int TOP    = 8;

    public static final int XMIN_TAG = 1;
    public static final int XMAX_TAG = 2;
    public static final int YMIN_TAG = 3;
    public static final int YMAX_TAG = 4;

    static protected final int       TICK_HDIST    = 40;
    static protected final int       TICK_VDIST    = 20;
    static protected final int       TICK_SIZE     = 4;
    static protected final double    CHAR_HEIGHT   = 8.0;
    static protected final double    CHAR_WIDTH    = 7.0;

    protected static boolean withTouch = false;

    //protected FlowLayout      diaLayout = null;
    protected BorderLayout    diaLayout = null;
    //protected GridLayout      btnLayout = null;
    protected BoxLayout       btnLayout = null;
    protected JPanel          btnPanel  = null;
    protected JPanel          infoPanel = null;
    protected JTextField      inputTF   = null;
    protected int             inputTag  = 0;

    protected int             width           = 0;
    protected int             height          = 0;
    protected int             leftMargin      = 0;
    protected int             rightMargin     = 0;
    protected int             bottomMargin    = 0;
    protected int             topMargin       = 0;
    protected boolean         axisFlag        = false;
    protected Font            lFont           = null;
    protected boolean         xAxisFlag       = false;
    protected boolean         xGridFlag       = false;
    protected double          xGridMinDelta   = 0.0;
    protected boolean         xGridRangeFlag  = false;
    protected double          xGridRangeMin   = 0.0;
    protected double          xGridRangeMax   = 0.0;
    protected NumberFormat    xLabelFormat    = null;
    protected double          xMin            = 0.0;
    protected double          xMax            = 0.0;
    protected double          xGridDelta      = 0.0;
    protected double          xGridMin        = 0.0;
    protected double          xGridMax        = 0.0;
    protected boolean         xRangeFlag      = false;
    protected int             xRangeCount     = 0;
    protected double          xRangeMin       = 0.0;
    protected double          xRangeMax       = 0.0;
    protected boolean         yAxisFlag       = false;
    protected boolean         yGridFlag       = false;
    protected double          yGridMinDelta   = 0.0;
    protected boolean         yGridRangeFlag  = false;
    protected double          yGridRangeMin   = 0.0;
    protected double          yGridRangeMax   = 0.0;
    protected NumberFormat    yLabelFormat    = null;
    protected double          yMin            = 0.0;
    protected double          yMax            = 0.0;
    protected double          yGridDelta      = 0.0;
    protected double          yGridMin        = 0.0;
    protected double          yGridMax        = 0.0;
    protected boolean         yRangeFlag      = false;
    protected int             yRangeCount     = 0;
    protected double          yRangeMin       = 0.0;
    protected double          yRangeMax       = 0.0;
    protected Vector<Shape>   shapes          = new Vector<Shape>();
    protected Vector<Color>   colors          = new Vector<Color>();
    protected AffineTransform ftrans          = new AffineTransform();

    protected NumberFormat numFormat2D0 = null;
    protected NumberFormat numFormat6D0 = null;
    protected NumberFormat numFormatD0  = null;
    protected NumberFormat numFormatD1  = null;
    protected NumberFormat numFormatD2  = null;
    protected NumberFormat numFormatD3  = null;
    protected NumberFormat numFormatE3  = null;

    public FFTSDiagram(int width, int height) {
	this(width, height, 0, 0, 0, 0);
    }

    public FFTSDiagram(int width, int height, int left, int right, int bottom, int top) {
	double fm00 = 1.0, fm01 = 0.0, fm02 = 0.0;
	double fm10 = 0.0, fm11 = 1.0, fm12 = 0.0;
	Dimension size = new Dimension(width + left + right, height + bottom + top);
	setSize(size);
	setPreferredSize(size);
	setMinimumSize(size);
	setMaximumSize(size);
	this.width = width;
	this.height = height;
	leftMargin = left;
	rightMargin = right;
	bottomMargin = bottom;
	topMargin = top;
	fm00 =  1.0;
	fm01 =  0.0;
	fm02 =  left;
	fm10 =  0.0;
	fm11 = -1.0;
	fm12 =  top + height;
	ftrans.setTransform(fm00, fm10, fm01, fm11, fm02, fm12);
	lFont = new Font("Monospaced", Font.PLAIN, (int)CHAR_HEIGHT);
	// number formats
	numFormat2D0 = NumberFormat.getInstance();
	numFormat2D0.setMinimumFractionDigits(0);
	numFormat2D0.setMaximumFractionDigits(0);
	numFormat2D0.setMinimumIntegerDigits(2);
	numFormat2D0.setMaximumIntegerDigits(2);
	numFormat2D0.setGroupingUsed(false);

	numFormat6D0 = NumberFormat.getInstance();
	numFormat6D0.setMinimumFractionDigits(0);
	numFormat6D0.setMaximumFractionDigits(0);
	numFormat6D0.setMinimumIntegerDigits(6);
	numFormat6D0.setMaximumIntegerDigits(6);
	numFormat6D0.setGroupingUsed(false);

	numFormatD0 = NumberFormat.getInstance();
	numFormatD0.setMinimumFractionDigits(0);
	numFormatD0.setMaximumFractionDigits(0);
	numFormatD0.setGroupingUsed(false);

	numFormatD1 = NumberFormat.getInstance();
	numFormatD1.setMinimumFractionDigits(1);
	numFormatD1.setMaximumFractionDigits(1);
	numFormatD1.setGroupingUsed(false);

	numFormatD2 = NumberFormat.getInstance();
	numFormatD2.setMinimumFractionDigits(2);
	numFormatD2.setMaximumFractionDigits(2);
	numFormatD2.setGroupingUsed(false);

	numFormatD3 = NumberFormat.getInstance();
	numFormatD3.setMinimumFractionDigits(3);
	numFormatD3.setMaximumFractionDigits(3);
	numFormatD3.setGroupingUsed(false);

	numFormatE3 = NumberFormat.getInstance();
	if (numFormatE3 instanceof DecimalFormat) {
	    ((DecimalFormat)numFormatE3).applyPattern("#.###E0");
	}
	addMouseListener(this);
    }

    protected void createLayout() {
	if (diaLayout != null) return;
	diaLayout = new BorderLayout();
	setLayout(diaLayout);
    }

    protected void createButtonPanel() {
	if (btnPanel != null) return;
	createLayout();
	btnPanel = new JPanel();
	btnPanel.setOpaque(false);
	//btnLayout = new GridLayout(0, 1);
	btnLayout = new BoxLayout(btnPanel, BoxLayout.PAGE_AXIS);
	btnPanel.setLayout(btnLayout);
	//add(btnPanel);
	add(btnPanel, BorderLayout.LINE_END);
    }

    protected void createInfoPanel() {
	if (infoPanel != null) return;
	createLayout();
	infoPanel = new JPanel();
	//infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.LINE_AXIS));
	infoPanel.setLayout(new FlowLayout(FlowLayout.LEADING, 0, 0));
	add(infoPanel, BorderLayout.PAGE_END);
    }

    protected JButton createSimpleButton(String label) {
	createButtonPanel();
	JButton btn = new JButton(label);
	btn.setMargin(new Insets(0,0,0,0));
	//btn.setMinimumSize(new Dimension(rightMargin, 20));
	//btn.setPreferredSize(new Dimension(rightMargin, 20));
	//btn.setMinimumSize(new Dimension(rightMargin, 20));
	btn.setMinimumSize(new Dimension(rightMargin, 30));
	btn.setPreferredSize(new Dimension(rightMargin, 30));
	btn.setMaximumSize(new Dimension(rightMargin, 30));
	btn.addActionListener(this);
	btnPanel.add(btn);
	return btn;
    }

    protected JToggleButton createToggleButton(String label, boolean flag) {
	createButtonPanel();
	JToggleButton btn = new JToggleButton(label, flag);
	btn.setMargin(new Insets(0,0,0,0));
	//btn.setMinimumSize(new Dimension(rightMargin, 20));
	//btn.setPreferredSize(new Dimension(rightMargin, 20));
	//btn.setMaximumSize(new Dimension(rightMargin, 20));
	btn.setMinimumSize(new Dimension(rightMargin, 30));
	btn.setPreferredSize(new Dimension(rightMargin, 30));
	btn.setMaximumSize(new Dimension(rightMargin, 30));
	btn.addActionListener(this);
	btnPanel.add(btn);
	return btn;
    }

    protected JLabel createLabel(String text) {
	createInfoPanel();
	JLabel label = new JLabel(text);
	infoPanel.add(label);
	return label;
    }

    protected JTextField createTextField(String text, boolean editable, int ncol) {
	createInfoPanel();
	JTextField field = new JTextField(text);
	field.setColumns(ncol);
	if (editable) {
	    if (withTouch) {
		field.setEditable(false);
		field.addMouseListener(this);
	    } else {
		field.addActionListener(this);
	    }
	} else {
	    field.setEditable(false);
	}
	infoPanel.add(field);
	return field;
    }

    protected JTextField createInputField() {
	if (withTouch) return null;
	createLayout();
	if (inputTF == null) {
	    inputTF = new JTextField();
	    inputTF.addActionListener(this);
	    inputTF.setVisible(false);
	    add(inputTF, BorderLayout.PAGE_START);
	}
	return inputTF;
    }

    protected void showInput(String text, int tag) {
	//System.out.println("FFTSDiagram.showInput(" + text + ", " + tag + ")");
	if (withTouch) {
	    double value;
	    switch (tag) {
	    case XMIN_TAG:
		value = NumberDialog.enterFlonum("X-Range: lower Value", text);
		handleXRange(Math.min(value, xMax), Math.max(value, xMax));
		break;
	    case XMAX_TAG:
		value = NumberDialog.enterFlonum("X-Range: upper Value", text);
		handleXRange(Math.min(value, xMin), Math.max(value, xMin));
		break;
	    case YMIN_TAG:
		value = NumberDialog.enterFlonum("Y-Range: lower Value", text);
		handleYRange(Math.min(value, yMax), Math.max(value, yMax));
		break;
	    case YMAX_TAG:
		value = NumberDialog.enterFlonum("Y-Range: upper Value", text);
		handleYRange(Math.min(value, yMin), Math.max(value, yMin));
		break;
	    default:;
	    }
	} else {
	    if (inputTF == null) return;
	    inputTF.setText(text);
	    inputTF.setVisible(true);
	    inputTag = tag;
	    //System.out.println("  inputTF.isVisible() = " + inputTF.isVisible());
	    validate();
	}
    }

    public void enableXAxis(boolean axis, boolean grid, NumberFormat format) {
	xAxisFlag = axis;
	xGridFlag = grid;
	xLabelFormat = format;
    }

    public void enableXAxis(boolean axis, boolean grid, double minDelta, NumberFormat format) {
	xAxisFlag = axis;
	xGridFlag = grid;
	xGridMinDelta = minDelta;
	xLabelFormat = format;
    }

    public void setXGridRange(double xmin, double xmax) {
	xGridRangeFlag = true;
	xGridRangeMin = xmin;
	xGridRangeMax = xmax;
    }

    public void setXRange(double xmin, double xmax) {
	this.xMin = xmin;
	this.xMax = xmax;
	xGridDelta = niceNumber((xMax - xMin)/(width/TICK_HDIST), false);
	if (xGridDelta < xGridMinDelta) xGridDelta = xGridMinDelta;
	if (xGridRangeFlag) {
	    xGridMin = xGridRangeMin;
	    xGridMax = xGridRangeMax;
	} else {
	    xGridMin = Math.ceil(xMin/xGridDelta)*xGridDelta;
	    xGridMax = Math.floor(xMax/xGridDelta)*xGridDelta;
	}
	//System.out.println("FFTSDiagram.setXRange(" + xmin + "," + xmax + ")");
	//System.out.println("  => grid = " + xGridDelta + ", " + xGridMin + " .. " + xGridMax);
    }

    public void enableYAxis(boolean axis, boolean grid, NumberFormat format) {
	yAxisFlag = axis;
	yGridFlag = grid;
	yLabelFormat = format;
    }

    public void enableYAxis(boolean axis, boolean grid, double minDelta, NumberFormat format) {
	yAxisFlag = axis;
	yGridFlag = grid;
	yGridMinDelta = minDelta;
	yLabelFormat = format;
    }

    public void setYGridRange(double ymin, double ymax) {
	yGridRangeFlag = true;
	yGridRangeMin = ymin;
	yGridRangeMax = ymax;
    }

    public void setYRange(double ymin, double ymax) {
	this.yMin = ymin;
	this.yMax = ymax;
	yGridDelta = niceNumber((yMax - yMin)/(height/TICK_VDIST), false);
	if (yGridDelta < yGridMinDelta) yGridDelta = yGridMinDelta;
	if (yGridRangeFlag) {
	    yGridMin = yGridRangeMin;
	    yGridMax = yGridRangeMax;
	} else {
	    yGridMin = Math.ceil(yMin/yGridDelta)*yGridDelta;
	    yGridMax = Math.floor(yMax/yGridDelta)*yGridDelta;
	}
	//System.out.println("FFTSDiagram.setYRange(" + ymin + "," + ymax + ")");
	//System.out.println("  => grid = " + yGridDelta + ", " + yGridMin + " .. " + yGridMax);
    }

    public void clearShapes() { shapes.clear(); }

    public void addShape(Shape shape) { shapes.add(shape); }

    public void clearColors() { colors.clear(); }

    public void addColor(Color color) { colors.add(color); }

    protected void paintXAxis(Graphics2D g2) {
	if (!xAxisFlag) return;
	// Beim Zeichnen der Achse wird wegen der Beschriftung ohne Transformation gearbeitet!
	// Es wird die optionale X-Achse gezeichnet (nur die Achse!)
	g2.setColor(Color.BLACK);
	g2.drawLine(leftMargin, topMargin + height, leftMargin + width - 1, topMargin + height);
	// Es wird die Achsenbeschriftung (inklusiver der Ticks) gezeichnet
	if (xLabelFormat != null) {
	    String label;
	    g2.setColor(Color.BLACK);
	    if (!xGridRangeFlag || (xMin >= xGridRangeMin)) {
		label = xLabelFormat.format(xMin);
		g2.drawString(label,
			      leftMargin - (int)(0.5*CHAR_WIDTH*(double)label.length()),
			      topMargin + height + (int)(1.8*CHAR_HEIGHT));
	    }
	    if (!xGridRangeFlag || (xMax <= xGridRangeMax)) {
		label = xLabelFormat.format(xMax);
		g2.drawString(label,
			      leftMargin + width - (int)(0.5*CHAR_WIDTH*(double)label.length()),
			      topMargin + height + (int)(1.8*CHAR_HEIGHT));
	    }
	    if (xGridDelta != 0.0) {
		for (double x = xGridMin; x <= xGridMax; x += xGridDelta) {
		    label = xLabelFormat.format(x);
		    double hx = (x - xMin)*(double)width/(xMax - xMin);
		    if (!xGridRangeFlag && ((int)hx < TICK_HDIST)) continue;
		    if (!xGridRangeFlag && (width - (int)hx < TICK_HDIST)) continue;
		    g2.drawLine(leftMargin + (int)hx,
				topMargin + height,
				leftMargin + (int)hx,
				topMargin + height + TICK_SIZE);
		    g2.drawString(label,
				  leftMargin + (int)hx - (int)(0.5*CHAR_WIDTH*(double)label.length()),
				  topMargin + height + (int)(1.8*CHAR_HEIGHT));
		}
	    }
	}
	// Es wird ein (optionales) Gitter (vertikale Linien) gezeichnet
	if (xGridFlag && (xGridDelta != 0.0)) {
	    g2.setColor(Color.GRAY);
	    for (double x = xGridMin; x <= xGridMax; x += xGridDelta) {
		double hx = (x - xMin)*(double)width/(xMax - xMin);
		g2.drawLine(leftMargin + (int)hx,
			    topMargin,
			    leftMargin + (int)hx,
			    topMargin + height);
	    }
	}
    }

    protected void paintYAxis(Graphics2D g2) {
	if (!yAxisFlag) return;
	// Beim Zeichnen der Achse wird wegen der Beschriftung ohne Transformation gearbeitet!
	// Es wird die optionale Y-Achse gezeichnet (nur die Achse!)
	g2.setColor(Color.BLACK);
	g2.drawLine(leftMargin, topMargin, leftMargin, topMargin + height);
	// Es wird die Achsenbeschriftung (inklusiver der Ticks) gezeichnet
	if (yLabelFormat != null) {
	    String label;
	    g2.setColor(Color.BLACK);
	    if (!yGridRangeFlag || (yMin >= yGridRangeMin)) {
		label = yLabelFormat.format(yMin);
		g2.drawString(label,
			      leftMargin - TICK_SIZE - (int)(CHAR_WIDTH*(double)label.length()),
			      topMargin + height + (int)(0.7*CHAR_HEIGHT));
	    }
	    if (!yGridRangeFlag || (yMax <= yGridRangeMax)) {
		label = yLabelFormat.format(yMax);
		g2.drawString(label,
			      leftMargin - TICK_SIZE - (int)(CHAR_WIDTH*(double)label.length()),
			      topMargin + (int)(0.7*CHAR_HEIGHT));
	    }
	    if (yGridDelta != 0.0) {
		for (double y = yGridMin; y <= yGridMax; y += yGridDelta) {
		    label = yLabelFormat.format(y);
		    double hy = (y - yMin)*(double)height/(yMax - yMin);
		    g2.drawLine(leftMargin - TICK_SIZE,
				topMargin + height - (int)hy,
				leftMargin,
				topMargin + height - (int)hy);
		    if (!yGridRangeFlag && ((int)hy < TICK_VDIST)) continue;
		    if (!yGridRangeFlag && (height - (int)hy < TICK_VDIST)) continue;
		    g2.drawString(label,
				  leftMargin - TICK_SIZE - (int)(CHAR_WIDTH*(double)label.length()),
				  topMargin + height - (int)hy + (int)(0.7*CHAR_HEIGHT));
		}
	    }
	}
	// Es wird ein (optionales) Gitter (horizontale Linien) gezeichnet
	if (yGridFlag && (yGridDelta != 0.0)) {
	    g2.setColor(Color.GRAY);
	    for (double y = yGridMin; y <= yGridMax; y += yGridDelta) {
		double hy = (y - yMin)*(double)height/(yMax - yMin);
		g2.drawLine(leftMargin,
			    topMargin + height - (int)hy,
			    leftMargin + width,
			    topMargin + height - (int)hy);
	    }
	}
    }

    public void paintComponent(Graphics g) {
	super.paintComponent(g);       
	Graphics2D g2 = (Graphics2D)g;
	// Get the current transform
	AffineTransform saveAT = g2.getTransform();
	// Render optional axis labels
	paintXAxis(g2);
	paintYAxis(g2);
	// Perform transformation
	g2.transform(ftrans);
	// Render
	if (colors.size() == 0) {
	    g2.setColor(Color.BLACK);
	} else {
	    g2.setColor(colors.elementAt(0));
	}
	for (int i = 0; i < shapes.size(); i++) {
	    Shape s = shapes.elementAt(i);
	    if (shapes.size() == colors.size()) {
		g2.setColor(colors.elementAt(i));
	    }
	    if (s instanceof Rectangle2D.Double) {
		g2.fill(s);
	    } else {
		g2.draw(s);
	    }
	}
	// Restore original transform
	g2.setTransform(saveAT);
    }  

    public static double niceNumber(double x, boolean r) {
        int    exp = (int)(Math.floor(Math.log10(x)));
        double f   = x/Math.pow(10, exp);
        double nf;
        if (r) {
            if (f < 1.5) {
                nf = 1.0;
            } else if (f < 3.0) {
                nf = 2.0;
            } else if (f < 7.0) {
                nf = 5.0;
            } else {
                nf = 10.0;
            }
        } else {
            if (f <= 1.0) {
                nf = 1.0;
            } else if (f <= 2.0) {
                nf = 2.0;
            } else if (f <= 5.0) {
                nf = 5.0;
            } else {
                nf = 10.0;
            }
        }
        return nf*Math.pow(10.0, exp);
    }

    public void handleXRange(double xmin, double xmax) {
	//System.out.println("FFTSDiagram.handleXRange(" + xmin + ", " + xmax + ")");
    }

    public void handleYRange(double ymin, double ymax) {
	//System.out.println("FFTSDiagram.handleYRange(" + ymin + ", " + ymax + ")");
    }

    public void actionPerformed(ActionEvent e) {
	if (e.getSource() == inputTF) {
	    try {
		double value = Double.parseDouble(inputTF.getText());
		inputTF.setVisible(false);
		validate();
		switch (inputTag) {
		case XMIN_TAG:
		    handleXRange(Math.min(value, xMax), Math.max(value, xMax));
		    break;
		case XMAX_TAG:
		    handleXRange(Math.min(value, xMin), Math.max(value, xMin));
		    break;
		case YMIN_TAG:
		    handleYRange(Math.min(value, yMax), Math.max(value, yMax));
		    break;
		case YMAX_TAG:
		    handleYRange(Math.min(value, yMin), Math.max(value, yMin));
		    break;
		default:;
		}
	    } catch (Exception ex) {
		System.err.println("Could not convert " + inputTF.getText() + " into a floating point number");
		ex.printStackTrace();
	    }
	}
    }

    public void mouseEntered(MouseEvent me) { }

    public void mouseExited(MouseEvent me) { }

    public void mouseClicked(MouseEvent me) {
	Point p = me.getPoint();
	//System.out.println(p);
	if ((inputTF != null) && (inputTF.isVisible())) {
	    inputTF.setVisible(false);
	}
	if (yRangeFlag && (p.x <= leftMargin) && (p.y <= (topMargin + height)))  {
	    // Es wurde rechts von der y-Achse geklickt
	    // -> Es soll der kleinste oder der groesste Y-Wert (y-Skalierung) gesetzt werden
	    if (p.y < (topMargin + height/2)) {
		// Es wurde oberhalb der Mitte der y-Achse geklickt
		// -> Es soll der groesste y-Wert gesetzt werden
		if (yLabelFormat != null) {
		    showInput(yLabelFormat.format(yMax), YMAX_TAG);
		} else {
		    showInput("" + yMax, YMAX_TAG);
		}
	    } else {
		// Es wurde unterhalb der Mitte der y-Achse geklickt
		// -> Es soll der kleinste y-Wert gesetzt werden
		if (yLabelFormat != null) {
		    showInput(yLabelFormat.format(yMin), YMIN_TAG);
		} else {
		    showInput("" + yMin, YMIN_TAG);
		}
	    }
	    xRangeCount = 0;
	    yRangeCount = 0;
	} else if (xRangeFlag && (p.y > (height + topMargin))) {
	    // Es wurde unterhalb der x-Achse geklickt
	    // -> Es soll der kleinste oder groesste X-Wert (x-Ausschnitt) gesetzt werden
	    if (p.x < (leftMargin + width/2)) {
		// Es wurde links von der Mitte der x-Achse geklickt
		// -> Es soll der kleinste x-Wert (linke Begrenzung) gesetzt werden
		if (xLabelFormat != null) {
		    showInput(xLabelFormat.format(xMin), XMIN_TAG);
		} else {
		    showInput("" + xMin, XMIN_TAG);
		}
	    } else {
		// Es wurde rechts von der Mitte der x-Achse geklickt
		// -> Es soll der groesste x-Wert (rechte Begrenzung) gesetzt werden
		if (xLabelFormat != null) {
		    showInput(xLabelFormat.format(xMax), XMAX_TAG);
		} else {
		    showInput("" + xMax, XMAX_TAG);
		}
	    }
	    xRangeCount = 0;
	    yRangeCount = 0;
	} else if (xRangeCount != 0) {
	    double x = xMin + (xMax - xMin)*(double)(p.x - leftMargin)/(double)width;
	    if ((x < xMin) || (x > xMax)) return;
	    if (xRangeCount == 2) {
		xRangeMin = x;
		xRangeCount--;
	    } else if (xRangeCount == 1) {
		xRangeMax = x;
		xRangeCount--;
		handleXRange(Math.min(xRangeMin, xRangeMax), Math.max(xRangeMin, xRangeMax));
	    }
	} else if (yRangeCount != 0) {
	    double y = yMin + (yMax - yMin)*(double)(topMargin + height - p.y)/(double)height;
	    if ((y < yMin) || (y > yMax)) return;
	    if (yRangeCount == 2) {
		yRangeMin = y;
		yRangeCount--;
	    } else if (yRangeCount == 1) {
		yRangeMax = y;
		yRangeCount--;
		handleYRange(Math.min(yRangeMin, yRangeMax), Math.max(yRangeMin, yRangeMax));
	    }
	}
    }

    public void mousePressed(MouseEvent me) { }

    public void mouseReleased(MouseEvent me) { }

    static public void setTouch(boolean flag) {
	withTouch = flag;
    }


}

