package pfb;

import java.io.*;
import java.net.*;
import java.nio.*;

public class FFTSRequest {

    public static final short CMD_PING             = 1;
    public static final short CMD_GET_FEATURES     = 2;
    public static final short CMD_GET_SPECTRUM     = 3;
    public static final short CMD_GET_POWER        = 4;
    public static final short CMD_GET_SAMPLES      = 5;

    public char  tag;
    public char  code;
    public short cmd;
    public short crate;
    public short board;
    public float frequency;
    public int   count;
    public int   binning;
    public int   feed;

    public FFTSRequest() {
    }

    public void ping() {
	this.tag       = 'P';
	this.code      = '?';
	this.cmd       = CMD_PING;
	this.crate     = 0;
	this.board     = 0;
	this.frequency = 0.0F;
	this.count     = 0;
	this.binning   = 0;
    }

    public void getFeatures() {
	this.cmd       = CMD_GET_FEATURES;
	this.crate     = 0;
	this.board     = 0;
	this.frequency = 0.0F;
	this.count     = 0;
	this.binning   = 0;
    }

    public void getSpectrum(short crate, short board, float frequency, int count, int binning) {
	this.tag       = 'S';
	this.code      = '?';
	this.cmd       = CMD_GET_SPECTRUM;
	this.crate     = crate;
	this.board     = board;
	this.frequency = frequency;
	this.count     = count;
	this.binning   = binning;
    }

    public void getPower(short crate, short board) {
	this.cmd       = CMD_GET_POWER;
	this.crate     = crate;
	this.board     = board;
	this.frequency = 0.0F;
	this.count     = 0;
	this.binning   = 0;
    }

    public void getSamples(int feed) {
	this.tag       = 'R';
	this.code      = '?';
	this.cmd       = CMD_GET_SAMPLES;
	this.crate     = 0;
	this.board     = 0;
	this.frequency = 0.0F;
	this.count     = 0;
	this.binning   = 0;
	this.feed      = feed;
    }

    public boolean send(DataOutputStream sock) {
	try {
	    if (tag == 'P') {
		sock.writeByte(tag);
		sock.writeByte(code);
		sock.writeInt(0);
	    } else if (tag == 'S') {
		sock.writeByte(tag);
		sock.writeByte(code);
		sock.writeInt(12);
		sock.writeFloat(frequency);
		sock.writeInt(count);
		sock.writeInt(binning);
	    } else if (tag == 'R') {
		sock.writeByte(tag);
		sock.writeByte(code);
		sock.writeShort(1);
		sock.writeInt(feed);
	    }
	    sock.flush();
	    return true;
	} catch (IOException e) {
            System.err.println("Could not send a request");
	    e.printStackTrace();
	    return false;
	}
    }

}
