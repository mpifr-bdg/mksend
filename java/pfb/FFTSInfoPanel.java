package pfb;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;


public class FFTSInfoPanel extends FFTSGridBagPanel {

    protected JTextField   tfCounter        = new JTextField("99999999");
    protected JTextField   tfBandwidth      = new JTextField("9999");
    protected JTextField   tfChannels       = new JTextField("999999");
    protected JTextField   tfSync           = new JTextField("99999.999");
    protected JTextField   tfBlank          = new JTextField("99999.999");
    protected JTextField   tfPhase          = new JTextField("9/9");
    protected JTextField   tfMode           = new JTextField("Unknown");
    protected JTextField   tfTime           = new JTextField("HH:MM:SS.SSSSSS");
    protected JTextField   tfIntTime        = new JTextField("99999.999");
    protected JTextField   tfTempBoard      = new JTextField("99.9");
    protected JTextField   tfTempFPGA       = new JTextField("99.9");
    protected JTextField   tfTempADC0       = new JTextField("99.9");
    protected JTextField   tfTempADC1       = new JTextField("99.9");

    public FFTSInfoPanel() {
	super("Infos");
    }

    public void createGUI() {
	int row, col;

	super.createGUI();

	tfCounter.setEditable(false);
	tfBandwidth.setEditable(false);
	tfChannels.setEditable(false);
	tfSync.setEditable(false);
	tfBlank.setEditable(false);
	tfPhase.setEditable(false);
	tfMode.setEditable(false);
	tfTime.setEditable(false);
	tfIntTime.setEditable(false);
	tfTempBoard.setEditable(false);
	tfTempFPGA.setEditable(false);
	tfTempADC0.setEditable(false);
	tfTempADC1.setEditable(false);

	tfCounter.setColumns(7);
	tfBandwidth.setColumns(7);
	tfChannels.setColumns(7);
	tfSync.setColumns(7);
	tfBlank.setColumns(7);
	tfPhase.setColumns(7);
	tfMode.setColumns(7);
	tfIntTime.setColumns(7);
	tfTime.setColumns(7);
	tfTempBoard.setColumns(7);
	tfTempFPGA.setColumns(7);
	tfTempADC0.setColumns(7);
	tfTempADC1.setColumns(7);

	tfCounter.setHorizontalAlignment(JTextField.RIGHT);
	tfBandwidth.setHorizontalAlignment(JTextField.RIGHT);
	tfChannels.setHorizontalAlignment(JTextField.RIGHT);
	tfSync.setHorizontalAlignment(JTextField.RIGHT);
	tfBlank.setHorizontalAlignment(JTextField.RIGHT);
	tfPhase.setHorizontalAlignment(JTextField.RIGHT);
	tfMode.setHorizontalAlignment(JTextField.RIGHT);
	tfIntTime.setHorizontalAlignment(JTextField.RIGHT);
	tfTime.setHorizontalAlignment(JTextField.RIGHT);
	tfTempBoard.setHorizontalAlignment(JTextField.RIGHT);
	tfTempFPGA.setHorizontalAlignment(JTextField.RIGHT);
	tfTempADC0.setHorizontalAlignment(JTextField.RIGHT);
	tfTempADC1.setHorizontalAlignment(JTextField.RIGHT);

	row = 0; col = 0;
	//grid(new JLabel("Irig-B:"),     col++, row, "r");
	grid(tfTime,                    col++, row, 3, 1, "rl");
	row++; col = 0;
	grid(new JLabel("Nr:"),         col++, row, "r");
	grid(tfCounter,                 col++, row, "lr");
	row++; col = 0;
	grid(new JLabel("BW"),          col++, row, "r");
	grid(tfBandwidth,               col++, row, "lr");
	grid(new JLabel("MHz"),         col++, row, "l");
	row++; col = 0;
	grid(new JLabel("# Ch:"),       col++, row, "r");
	grid(tfChannels,                col++, row, "lr");
	row++; col = 0;
	grid(new JLabel("Sync:"),       col++, row, "r");
	grid(tfSync,                    col++, row, "lr");
	grid(new JLabel("ms"),          col++, row, "l");
	row++; col = 0;
	grid(new JLabel("Blank:"),      col++, row, "r");
	grid(tfBlank,                   col++, row, "lr");
	grid(new JLabel("ms"),          col++, row, "l");
	row++; col = 0;
	grid(new JLabel("Int:"),        col++, row, "r");
	grid(tfIntTime,                 col++, row, "lr");
	grid(new JLabel("ms"),          col++, row, "l");
	row++; col = 0;
	grid(new JLabel("Phase:"),      col++, row, "r");
	grid(tfPhase,                   col++, row, "lr");
	row++; col = 0;
	//grid(new JLabel("Mode:"),       col++, row, "r");
	//grid(tfMode,                    col++, row, "lr");
	//row++; col = 0;
	grid(new JLabel("Board:"), col++, row, "r");
	grid(tfTempBoard,               col++, row, "lr");
	grid(new JLabel("C"),           col++, row, "l");
	row++; col = 0;
	grid(new JLabel("FPGA:"),  col++, row, "r");
	grid(tfTempFPGA,                col++, row, "lr");
	grid(new JLabel("C"),           col++, row, "l");
	row++; col = 0;
	grid(new JLabel("ADC0:"),  col++, row, "r");
	grid(tfTempADC0,                col++, row, "lr");
	grid(new JLabel("C"),           col++, row, "l");
	row++; col = 0;
	grid(new JLabel("ADC1:"),  col++, row, "r");
	grid(tfTempADC1,                col++, row, "lr");
	grid(new JLabel("C"),           col++, row, "l");
	row++; col = 0;
	setDynamicRow(row);
	grid(new JPanel(),              col++, row, "tblr");
    }

    public void setSpectrum(FFTSReplySpectrum spectrum) {
	tfCounter.setText("" + spectrum.dumpcnt);
	tfBandwidth.setText("" + spectrum.bandwidth);
	tfChannels.setText("" + (1 << spectrum.channels));
	tfSync.setText(numFormatD3.format(((float)spectrum.timeSync)/1000.0F));
	tfBlank.setText(numFormatD3.format(((float)spectrum.timeBlank)/1000.0F));
	tfPhase.setText("" + spectrum.phase + "/" + spectrum.phaseCount);
	if ((spectrum.flagsSystem & FFTSReplySpectrum.FLAG_EXT_BLANK_SYNC) != 0) {
	    tfMode.setText("External");
	} else {
	    tfMode.setText("Internal");
	}
	//protected JLabel       lIntTime        = new JLabel("999999.999");
	tfTime.setText(numFormat2D0.format(spectrum.second/10000)
		       + ":"
		       + numFormat2D0.format((spectrum.second/100)%100)
		       + ":"
		       + numFormat2D0.format(spectrum.second%100)
		       + "."
		       + numFormat6D0.format(spectrum.microsecond));
	tfIntTime.setText(numFormatD3.format((float)(spectrum.intCnt)*(float)(1 << spectrum.channels)/(float)(spectrum.bandwidth)/1000.0F));
	tfTempBoard.setText(numFormatD1.format((float)spectrum.tempBoard/32.0));
	tfTempFPGA.setText(numFormatD1.format((float)spectrum.tempFPGA/32.0));
	tfTempADC0.setText(numFormatD1.format((float)spectrum.tempADC0/32.0));
	tfTempADC1.setText(numFormatD1.format((float)spectrum.tempADC1/32.0));
    }

}

