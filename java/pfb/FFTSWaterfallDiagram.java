package pfb;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.awt.image.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.util.Arrays;
import java.util.Vector;

import java.text.*;

public class FFTSWaterfallDiagram extends FFTSBufferedDiagram {

    protected static int[] HEAT_CM = new int[] {
	0, 0, 0, 3, 1, 0, 6, 2, 0, 10, 3, 0, 13, 4, 0,
	16, 5, 0, 20, 6, 0, 23, 7, 0, 27, 8, 0, 30, 9, 0,
	33, 10, 0, 37, 11, 0, 40, 12, 0, 43, 13, 0, 47, 14, 0,
	50, 15, 0, 54, 16, 0, 57, 17, 0, 60, 18, 0, 64, 19, 0,
	67, 20, 0, 70, 21, 0, 74, 22, 0, 77, 23, 0, 81, 24, 0,
	84, 25, 0, 87, 26, 0, 91, 27, 0, 94, 28, 0, 97, 29, 0,
	101, 30, 0, 104, 31, 0, 108, 32, 0, 110, 33, 0, 112, 34, 0,
	114, 35, 0, 117, 36, 0, 119, 37, 0, 121, 38, 0, 124, 39, 0,
	126, 40, 0, 128, 41, 0, 131, 42, 0, 133, 43, 0, 135, 44, 0,
	138, 45, 0, 140, 46, 0, 142, 47, 0, 145, 48, 0, 147, 49, 0,
	149, 50, 0, 151, 51, 0, 154, 52, 0, 156, 53, 0, 158, 54, 0,
	161, 55, 0, 163, 56, 0, 165, 57, 0, 168, 58, 0, 170, 59, 0,
	172, 60, 0, 175, 61, 0, 177, 62, 0, 179, 63, 0, 182, 64, 0,
	183, 65, 0, 184, 66, 0, 186, 67, 0, 187, 68, 0, 188, 69, 0,
	190, 70, 0, 191, 71, 0, 192, 72, 0, 194, 73, 0, 195, 74, 0,
	196, 75, 0, 198, 76, 0, 199, 77, 0, 200, 78, 0, 202, 79, 0,
	203, 80, 0, 204, 81, 0, 206, 82, 0, 207, 83, 0, 208, 84, 0,
	210, 85, 0, 211, 86, 0, 212, 87, 0, 214, 88, 0, 215, 89, 0,
	216, 90, 0, 218, 91, 0, 219, 92, 0, 220, 93, 0, 222, 94, 0,
	223, 95, 0, 225, 96, 0, 225, 97, 0, 226, 98, 0, 226, 99, 0,
	227, 100, 0, 228, 101, 0, 228, 102, 0, 229, 103, 0, 230, 104, 0,
	230, 105, 0, 231, 106, 0, 232, 107, 0, 232, 108, 0, 233, 109, 0,
	234, 110, 0, 234, 111, 0, 235, 112, 0, 236, 113, 0, 236, 114, 0,
	237, 115, 0, 238, 116, 0, 238, 117, 0, 239, 118, 0, 240, 119, 0,
	240, 120, 0, 241, 121, 0, 242, 122, 0, 242, 123, 0, 243, 124, 0,
	244, 125, 0, 244, 126, 0, 245, 127, 0, 246, 128, 0, 246, 129, 0,
	246, 130, 0, 246, 131, 0, 247, 132, 0, 247, 133, 0, 247, 134, 0,
	247, 135, 0, 248, 136, 0, 248, 137, 0, 248, 138, 0, 249, 139, 0,
	249, 140, 0, 249, 141, 0, 249, 142, 0, 250, 143, 0, 250, 144, 0,
	250, 145, 0, 251, 146, 0, 251, 147, 0, 251, 148, 0, 251, 149, 0,
	252, 150, 0, 252, 151, 0, 252, 152, 0, 253, 153, 0, 253, 154, 0,
	253, 155, 0, 253, 156, 0, 254, 157, 0, 254, 158, 0, 254, 159, 0,
	255, 160, 0, 255, 161, 1, 255, 162, 2, 255, 163, 3, 255, 164, 4,
	255, 165, 5, 255, 166, 6, 255, 167, 7, 255, 168, 8, 255, 169, 9,
	255, 170, 10, 255, 171, 12, 255, 172, 13, 255, 173, 14, 255, 174, 15,
	255, 175, 16, 255, 176, 17, 255, 177, 18, 255, 178, 19, 255, 179, 20,
	255, 180, 21, 255, 181, 22, 255, 182, 24, 255, 183, 25, 255, 184, 26,
	255, 185, 27, 255, 186, 28, 255, 187, 29, 255, 188, 30, 255, 189, 31,
	255, 190, 32, 255, 191, 33, 255, 192, 35, 255, 193, 38, 255, 194, 41,
	255, 195, 44, 255, 196, 48, 255, 197, 51, 255, 198, 54, 255, 199, 58,
	255, 200, 61, 255, 201, 64, 255, 202, 68, 255, 203, 71, 255, 204, 74,
	255, 205, 78, 255, 206, 81, 255, 207, 84, 255, 208, 88, 255, 209, 91,
	255, 210, 94, 255, 211, 97, 255, 212, 101, 255, 213, 104, 255, 214, 107,
	255, 215, 111, 255, 216, 114, 255, 217, 117, 255, 218, 121, 255, 219, 124,
	255, 220, 127, 255, 221, 131, 255, 222, 134, 255, 223, 137, 255, 224, 141,
	255, 225, 144, 255, 226, 148, 255, 227, 152, 255, 228, 155, 255, 229, 159,
	255, 230, 163, 255, 231, 166, 255, 232, 170, 255, 233, 174, 255, 234, 177,
	255, 235, 181, 255, 236, 185, 255, 237, 188, 255, 238, 192, 255, 239, 196,
	255, 240, 199, 255, 241, 203, 255, 242, 207, 255, 243, 210, 255, 244, 214,
	255, 245, 218, 255, 246, 221, 255, 247, 225, 255, 248, 229, 255, 249, 232,
	255, 250, 236, 255, 251, 240, 255, 252, 243, 255, 253, 247, 255, 254, 251,
	255, 255, 255
    };

    protected static int[] RAINBOW_CM = new int[] {
	0, 0, 4, 0, 1, 10, 0, 2, 16, 0, 3, 21, 0, 4, 27,
	0, 5, 32, 0, 6, 38, 0, 7, 43, 0, 8, 49, 0, 9, 55,
	0, 10, 61, 0, 11, 66, 0, 12, 72, 0, 13, 77, 0, 14, 83,
	0, 15, 88, 0, 16, 93, 0, 18, 96, 0, 20, 100, 0, 22, 104,
	0, 25, 108, 0, 27, 111, 0, 29, 115, 0, 31, 119, 0, 34, 123,
	0, 36, 126, 0, 38, 130, 0, 40, 134, 0, 43, 138, 0, 45, 142,
	0, 47, 146, 0, 49, 150, 0, 52, 154, 0, 54, 158, 0, 57, 162,
	0, 59, 166, 0, 62, 171, 0, 64, 175, 0, 67, 180, 0, 69, 184,
	0, 72, 189, 0, 74, 193, 0, 77, 197, 0, 79, 201, 0, 82, 206,
	0, 84, 210, 0, 87, 214, 0, 89, 217, 0, 92, 220, 0, 95, 221,
	0, 97, 222, 0, 100, 224, 0, 102, 225, 0, 105, 226, 0, 107, 228,
	0, 110, 229, 0, 112, 231, 0, 115, 231, 0, 117, 232, 0, 120, 232,
	0, 122, 233, 0, 125, 234, 0, 127, 234, 0, 130, 235, 0, 133, 236,
	0, 135, 235, 0, 137, 235, 0, 139, 234, 0, 141, 234, 0, 143, 233,
	0, 145, 233, 0, 148, 232, 0, 150, 232, 0, 152, 229, 0, 154, 227,
	0, 156, 225, 0, 158, 223, 0, 161, 220, 0, 163, 218, 0, 165, 216,
	0, 167, 214, 0, 168, 211, 1, 170, 207, 2, 172, 204, 3, 174, 200,
	3, 176, 196, 4, 178, 193, 5, 180, 189, 6, 182, 185, 6, 184, 182,
	7, 186, 178, 8, 188, 175, 9, 190, 171, 9, 192, 167, 10, 194, 164,
	11, 196, 160, 12, 198, 156, 14, 199, 152, 16, 200, 148, 19, 202, 144,
	21, 203, 140, 24, 205, 136, 26, 206, 132, 29, 208, 128, 31, 209, 123,
	33, 210, 119, 36, 212, 115, 38, 213, 111, 41, 215, 107, 43, 216, 103,
	46, 218, 99, 48, 219, 95, 51, 221, 90, 54, 222, 87, 58, 223, 84,
	61, 225, 81, 65, 226, 77, 69, 227, 74, 72, 228, 71, 76, 230, 67,
	80, 231, 64, 83, 232, 61, 87, 233, 57, 90, 234, 54, 94, 236, 51,
	98, 237, 47, 101, 238, 44, 105, 239, 41, 109, 241, 37, 113, 241, 35,
	117, 242, 33, 121, 243, 31, 126, 244, 28, 130, 245, 26, 134, 246, 24,
	139, 247, 21, 143, 248, 19, 147, 248, 17, 152, 249, 14, 156, 250, 12,
	160, 251, 10, 165, 252, 7, 169, 253, 5, 173, 254, 3, 178, 255, 0,
	181, 255, 0, 185, 255, 0, 189, 255, 0, 192, 254, 0, 196, 254, 0,
	200, 254, 0, 203, 253, 0, 207, 253, 0, 211, 253, 0, 214, 252, 0,
	218, 252, 0, 222, 252, 0, 225, 251, 0, 229, 251, 0, 233, 251, 0,
	237, 250, 0, 238, 249, 0, 239, 248, 0, 240, 247, 0, 242, 246, 0,
	243, 245, 0, 244, 243, 0, 245, 242, 0, 247, 241, 0, 247, 240, 0,
	247, 239, 0, 248, 237, 0, 248, 236, 0, 248, 235, 0, 249, 234, 0,
	249, 233, 0, 250, 231, 0, 250, 229, 0, 250, 227, 0, 250, 224, 0,
	251, 222, 0, 251, 220, 0, 251, 217, 0, 252, 215, 0, 252, 212, 0,
	252, 210, 0, 253, 208, 0, 253, 205, 0, 253, 203, 0, 254, 201, 0,
	254, 198, 0, 254, 196, 0, 255, 193, 0, 255, 190, 0, 255, 186, 0,
	255, 182, 0, 255, 178, 0, 255, 174, 0, 255, 170, 0, 255, 166, 0,
	255, 162, 0, 255, 158, 0, 255, 154, 0, 255, 150, 0, 255, 146, 0,
	255, 142, 0, 255, 138, 0, 255, 134, 0, 255, 130, 0, 255, 124, 0,
	255, 118, 0, 255, 112, 0, 255, 106, 0, 255, 100, 0, 255, 94, 0,
	255, 88, 0, 255, 81, 0, 255, 75, 0, 255, 69, 0, 255, 63, 0,
	255, 57, 0, 255, 51, 0, 255, 45, 0, 255, 39, 0, 255, 32, 1,
	255, 36, 5, 255, 40, 9, 255, 44, 13, 255, 48, 18, 255, 52, 22,
	255, 56, 26, 255, 60, 30, 255, 64, 35, 255, 68, 42, 255, 72, 50,
	255, 76, 58, 255, 80, 66, 255, 84, 74, 255, 88, 82, 255, 92, 90,
	255, 96, 98, 255, 105, 107, 255, 115, 117, 255, 124, 126, 255, 134, 136,
	255, 144, 146, 255, 153, 155, 255, 163, 165, 255, 173, 175, 255, 184, 186,
	255, 196, 197, 255, 208, 209, 255, 219, 220, 255, 231, 232, 255, 243, 243,
	255, 255, 255
    };

    protected FFTSFilterPanel  filterPanel  = null;
    protected JToggleButton    btnAutoscale = null;
    protected JButton          btnRestart   = null;
    protected JToggleButton    btnSelectOn  = null;
    protected JToggleButton    btnSelectOff = null;
    protected JButton          btnXRange    = null;
    protected boolean          onFlag       = true;
    protected boolean          offFlag      = true;
    protected boolean          autoscale    = true;
    protected int              refCount     = 16;
    protected int              refCountLeft = 16;
    protected JTextField       tfAmpRange   = null;
    protected JTextField       tfRefCount   = null;
    protected JTextField       tfAmpMin     = null;
    protected JTextField       tfAmpMax     = null;
    protected float            xscale       = 1.0F;
    protected NumberFormat     zValueFormat = null;
    protected float            zmin         = -0.1F;
    protected float            zmax         =  0.1F;
    protected float            rmin         = 0.0F;
    protected float            rmax         = 0.0F;
    protected float[]          refSpectrum  = null;
    protected float[]          rawSpectrum  = null;
    protected float[]          resSpectrum  = null;
    protected int[]            colormap     = new int[256];

    public FFTSWaterfallDiagram(int width, int height, int left, int right, int bottom, int top) {
	super(width, height, left, right, bottom, top);
	btnAutoscale = createToggleButton("A", autoscale);
	btnRestart   = createSimpleButton("C");
	//btnSelectOn  = createToggleButton("On", onFlag);
	//btnSelectOff = createToggleButton("Off", offFlag);
	btnXRange    = createSimpleButton("X");
	createLabel("Value:");
	tfAmpRange   = createTextField("[0.000 ... 9.999]", false, 12);
	createLabel("Ref Count:");
	tfRefCount = createTextField("" + refCount, true, 3);
	createLabel("Color:");
	tfAmpMin = createTextField("" + zmin, true, 4);
	tfAmpMax = createTextField("" + zmax, true, 4);
	createInputField();
	xRangeFlag = true;
	createColorMap(HEAT_CM);
 	//createColorMap(RAINBOW_CM);
    }

    public void setFilterPanel(FFTSFilterPanel panel) { filterPanel = panel; }

    protected void createColorMap(int[] cm) {
	int idx = 0;
	if ((colormap == null) || (colormap.length != 256)) {
	    colormap = new int[256];
	}
	for (idx = 0; idx < 256; idx++) {
	    colormap[idx] = 255*65536 + 255*256 + 255;
	}
	for (idx = 0; idx < 128; idx++) {
	    int red   = cm[6*idx + 0];
	    int green = cm[6*idx + 1];
	    int blue  = cm[6*idx + 2];
	    colormap[128 + idx] = red*65536 + green*256 + blue;
	    colormap[128 - idx] = red*65536 + green*256 + blue;
	    //System.out.println("colormap[" + idx + "] = " + red + ", " + green + ", " + blue);
	}
    }

    public void setZFormat(NumberFormat f) { zValueFormat = f; }

    public float getRowMin() { return rmin; }

    public float getRowMax() { return rmax; }

    public float getZMin() { return zmin; }

    public void setZMin(float z) {
	zmin = z;
	tfAmpMin.setText("" + z);
    }

    public float getZMax() { return zmax; }

    public void setZMax(float z) {
	zmax = z;
	tfAmpMax.setText("" + z);
    }

    public int getRefCount() {
	return refCount;
    }

    public void setRefCount(int count) {
	refCount = count;
	tfRefCount.setText("" + count);
	handleRestart();
    }

    public void setSpectrum(FFTSReplySpectrum spectrum) {
	//System.out.println("FFTSWaterfallDiagram.setSpectrum(..): refCount=" + refCount + ", refCountLeft=" + refCountLeft + ", spectrum.count=" + spectrum.count);
	if (refCountLeft != 0) {
	    // Es werden gerade Off-Spektren aufaddiert
	    if (spectrum.phase != 1) {
		//System.out.println("  spectrum.phase != 1 => Abbruch");
		return; // Kein Off-Spektrum -> Abbruch
	    }
	    if (refSpectrum == null) {
		// Wir haben noch kein Off-Spectrum -> Kopieren des Off Spektrums
		//System.out.println("  Wir haben noch kein Off-Spectrum -> Kopieren des Off Spektrums");
		refSpectrum = Arrays.copyOf(spectrum.spectrum, spectrum.count);
	    } else if (refSpectrum.length != spectrum.count) {
		// Die Anzahl an Samples passt nicht -> Neuanfang
		//System.out.println("  Die Anzahl an Samples passt nicht -> Neuanfang");
		refSpectrum = Arrays.copyOf(spectrum.spectrum, spectrum.count);
		refCountLeft = refCount;
	    } else {
		//System.out.println("  Es wird das aktuelle Off Spektrum addiert");
		for (int i = 0; i < refSpectrum.length; i++) {
		    refSpectrum[i] += spectrum.spectrum[i];
		}
	    }
	    refCountLeft--;
	    if (refCountLeft == 0) {
		// Wir haben alle gewuenschten Off Spektren
		//System.out.println("  Wir haben alle gewuenschten Off Spektren -> skalieren des Spektrums");
		for (int i = 0; i < refSpectrum.length; i++) {
		    refSpectrum[i] /= (float)refCount;
		}
	    }
	    return;
	}
	// Wir haben ein Referenzspektrum
	if ((rawSpectrum == null) || (rawSpectrum.length != spectrum.count)) {
	    //System.out.println("  Es wird das aktuelle Spektrum kopiert");
	    rawSpectrum = Arrays.copyOf(spectrum.spectrum, spectrum.count);
	} else {
	    //System.out.println("  Es wird das aktuelle Spektrum verwendet");
	    for (int i = 0; i < rawSpectrum.length; i++) {
		rawSpectrum[i] = spectrum.spectrum[i];
	    }
	}
	if (refCountLeft != 0) return;
	if (refSpectrum == null) return;
	if (rawSpectrum == null) return;
	if (refSpectrum.length != rawSpectrum.length) {
	    //System.out.println("  refSpectrum.length(" + refSpectrum.length + ") != rawSpectrum.length(" + rawSpectrum.length + ") -> Restart");
	    handleRestart();
	    return;
	}
	if ((spectrum.phase == 1) && !offFlag) return;
	if ((spectrum.phase == 2) && !onFlag) return;

	int i;
	// calculate (on - off)/off
	if ((resSpectrum == null) || (resSpectrum.length != rawSpectrum.length)) {
	    resSpectrum = new float[rawSpectrum.length];
	}
	for (i = 0; i < resSpectrum.length; i++) {
	    resSpectrum[i] = (rawSpectrum[i] - refSpectrum[i])/refSpectrum[i];
	}
	xscale = (float)width/(float)resSpectrum.length;
	xscale = (float)Math.min(xscale, 1.0);
	rmin = resSpectrum[0];
	rmax = resSpectrum[0];
	for (i = 0; i < resSpectrum.length; i++) {
	    if (resSpectrum[i] < rmin) rmin = resSpectrum[i];
	    if (resSpectrum[i] > rmax) rmax = resSpectrum[i];
	}
	BufferedImage buf = getBuffer();
	shiftVertical();
	for (i = 0; i < resSpectrum.length; i++) {
	    int x = (int)(xscale*(float)i);
	    if ((x < 0) || (x >= width)) continue;
	    int ci = (int)((float)colormap.length*(resSpectrum[i] - zmin)/(zmax - zmin));
	    if (ci < 0) {
		buf.setRGB(x, 0, 255*65536 + 255*256 + 255);
	    } else if (ci >= colormap.length) {
		buf.setRGB(x, 0, 255*65536 + 255*256 + 255);
	    } else {
		buf.setRGB(x, 0, colormap[ci]);
	    }
	}
	for (i = 0; i < resSpectrum.length; i++) {
	    int x = (int)(xscale*(float)i);
	    if ((x < 0) || (x >= width)) continue;
	    int ci = (int)((float)colormap.length*(float)i/(float)resSpectrum.length);
	    if (ci < 0) {
		buf.setRGB(x, height - 1, 255*65536 + 255*256 + 255);
	    } else if (ci >= colormap.length) {
		buf.setRGB(x, height - 1, 255*65536 + 255*256 + 255);
	    } else {
		buf.setRGB(x, height - 1, colormap[ci]);
	    }
	}
	setXRange(spectrum.freq_low, spectrum.freq_high);
	setYRange(-height + 1, 0);
	tfAmpRange.setText("["
			   + zValueFormat.format(rmin)
			   + " .. "
			   + zValueFormat.format(rmax)
			   + "]");
	repaint();
    }

    protected void handleRestart() {
	//System.out.println("FFTSWaterfallDiagram.handleRestart()");
	refCountLeft = refCount;
	if (refSpectrum != null) {
	    for (int i = 0; i < refSpectrum.length; i++) {
		refSpectrum[i] = 0.0F;
	    }
	}
	BufferedImage buf = getBuffer();
	Graphics g2 = buf.getGraphics();
	g2.setColor(Color.BLACK);
	g2.fillRect(0, 0, width, height);
	repaint();
    }

    public void actionPerformed(ActionEvent e) {
	try {
	    if (e.getSource() == btnAutoscale) {
		autoscale = btnAutoscale.isSelected();
	    } else if (e.getSource() == btnRestart) {
		handleRestart();
	    } else if (e.getSource() == btnSelectOn) {
		onFlag = btnSelectOn.isSelected();
	    } else if (e.getSource() == btnSelectOff) {
		offFlag = btnSelectOff.isSelected();
	    } else if (e.getSource() == tfRefCount) {
		setRefCount(Integer.parseInt(tfRefCount.getText()));
	    } else if (e.getSource() == btnXRange) {
		xRangeCount = 2;
	    } else if (e.getSource() == tfAmpMin) {
		zmin = Float.parseFloat(tfAmpMin.getText());
	    } else if (e.getSource() == tfAmpMax) {
		zmax = Float.parseFloat(tfAmpMax.getText());
	    } else {
		super.actionPerformed(e);
	    }
	} catch (Exception ex) {
	    ex.printStackTrace();
	}
    }

    public void mouseClicked(MouseEvent me) {
	if (me.getSource() == tfRefCount) {
	    setRefCount(NumberDialog.enterFixnum("Reference Count", tfRefCount.getText()));
	} else if (me.getSource() == tfAmpMin) {
	    setZMin((float)NumberDialog.enterFlonum("Lower Amplitude Value", tfAmpMin.getText()));
	} else if (me.getSource() == tfAmpMax) {
	    setZMax((float)NumberDialog.enterFlonum("Upper Amplitude Value", tfAmpMax.getText()));
	} else {
	    super.mouseClicked(me);
	}
    }

    public void handleXRange(double xmin, double xmax) {
	//System.out.println("FFTSWaterfallDiagram.handleXRange(" + xmin + ", " + xmax + ")");
	if (filterPanel != null) {
	    filterPanel.setFrequencyRange((float)xmin, (float)xmax);
	}
    }


}
