package pfb;

import java.io.*;
import java.net.*;
import java.nio.*;

public class FFTSReplyFeatures {

    public char  tag;
    public char  code;
    public int   payload;
    public short crate_count;
    public short board_count;

    public FFTSReplyFeatures() {
    }

    public boolean receive(DataInputStream sock) {
	this.tag         = 'F';
	this.code        = '?';
	this.payload     = 8;
	this.crate_count = 1;
	this.board_count = 1;
	//System.out.println("reply: cmd=" + this.cmd + ", err=" + this.err + ", crate_count=" + this.crate_count + ", board_count=" + this.board_count);
	return true;
    }
}

