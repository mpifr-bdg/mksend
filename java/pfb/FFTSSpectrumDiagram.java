package pfb;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.util.Arrays;
import java.util.Vector;

public class FFTSSpectrumDiagram extends FFTSDiagram {

    //protected static final float OFFSET       = 165.0F;

    protected FFTSFilterPanel  filterPanel  = null;
    protected Color            curColor     = new Color(255,   0, 0, 255);
    protected Color            refColor     = new Color(  0, 255, 0, 255);
    protected JToggleButton    btnAutoscale = null;
    protected JToggleButton    btnRef       = null;
    protected JButton          btnXRange    = null;
    protected JButton          btnYRange    = null;
    protected JButton          btnYZero     = null;
    protected boolean          autoscale    = true;
    protected boolean          autozero     = false;
    protected JTextField       tfPowerRef   = null;
    protected JTextField       tfPowerMin   = null;
    protected JTextField       tfPowerMax   = null;
    protected float            powerRef     = 165.0F;
    protected double           powerMin     = -10.0;
    protected double           powerMax     =  10.0;
    protected float[]          rawSpectrum  = null;
    protected float[]          refSpectrum  = null;

    public FFTSSpectrumDiagram(int width, int height, int left, int right, int bottom, int top) {
	super(width, height, left, right, bottom, top);
	btnAutoscale = createToggleButton("A", autoscale);
	btnRef       = createToggleButton("R", false);
	btnXRange    = createSimpleButton("X");
	btnYRange    = createSimpleButton("Y");
	btnYZero     = createSimpleButton("0");
	createLabel("Ref:");
	tfPowerRef  = createTextField("" + powerRef, true, 6);
	createLabel("Min:");
	tfPowerMin  = createTextField("" + powerMin, true, 6);
	createLabel("Max:");
	tfPowerMax  = createTextField("" + powerMax, true, 6);
	createInputField();
	xRangeFlag = true;
	yRangeFlag = true;
    }

    public void setFilterPanel(FFTSFilterPanel panel) { filterPanel = panel; }

    public boolean getAutoscale() { return autoscale; }

    public void setAutoscale(boolean flag) {
	autoscale = flag;
    }

    public void clearReference() {
	refSpectrum = null;
    }

    public void takeReference() {
	refSpectrum = Arrays.copyOf(rawSpectrum, rawSpectrum.length);
    }

    public void setSpectrum(FFTSReplySpectrum spectrum) {
	int    i;
	double val;
	if ((refSpectrum != null) && (refSpectrum.length != spectrum.count)) refSpectrum = null;
	if ((rawSpectrum == null) || (rawSpectrum.length != spectrum.count)) {
	    rawSpectrum = Arrays.copyOf(spectrum.spectrum, spectrum.count);
	} else {
	    for (i = 0; i < rawSpectrum.length; i++) {
		rawSpectrum[i] = spectrum.spectrum[i];
	    }
	}
	clearShapes();
	clearColors();
	setXRange(spectrum.freq_low, spectrum.freq_high);
	// If autoscale is used, recalculate the lowest and highest y-values.
	if (autoscale || autozero) {
	    double yminr, ymaxr;
	    val = rawSpectrum[0];
	    yminr = val;
	    ymaxr = val;
	    for (i = 1; i < rawSpectrum.length; i++) {
		val = rawSpectrum[i];
		if (val > ymaxr) ymaxr = val;
		if (val < yminr) yminr = val;
	    }
	    if (refSpectrum != null) {
		for (i = 0; i < refSpectrum.length; i++) {
		    val = refSpectrum[i];
		    if (val > ymaxr) ymaxr = val;
		    if (val < yminr) yminr = val;
		}
	    }
	    if (autoscale) {
		if (yminr == ymaxr) {
		    setYRange(10.0*Math.log10(yminr) - powerRef - 1, 10.0*Math.log10(ymaxr) - powerRef + 1);
		} else {
		    setYRange(10.0*Math.log10(yminr) - powerRef, 10.0*Math.log10(ymaxr) - powerRef);
		}
		powerMin = yMin;
		powerMax = yMax;
		tfPowerMin.setText(numFormatD1.format((float)powerMin));
		tfPowerMax.setText(numFormatD1.format((float)powerMax));
	    }
	    if (autozero) {
		powerRef = (float)(10.0*Math.log10(ymaxr));
		powerMin = 10.0*Math.log10(yminr) - powerRef - 3.0;
		powerMax = 10.0*Math.log10(ymaxr) - powerRef + 3.0;
		setYRange(powerMin, powerMax);
		tfPowerRef.setText(numFormatD2.format(powerRef));
		tfPowerMin.setText(numFormatD2.format((float)powerMin));
		tfPowerMax.setText(numFormatD2.format((float)powerMax));
		autozero = false;
	    }
	}
	// calculate the scaling parameters
	double xscale = (double)width/(double)rawSpectrum.length;
	double yoff   = yMin;
	double yscale = (double)height/(yMax - yMin);
	GeneralPath p;
	// Create the optinal reference spectrum plot.
	if (refSpectrum != null) {
	    p = new GeneralPath();
	    val = 10.0*Math.log10(refSpectrum[0]) - powerRef;
	    p.moveTo(0.0, yscale*(val - yoff));
	    for (i = 1; i < refSpectrum.length; i++) {
		val = 10.0*Math.log10(refSpectrum[i]) - powerRef;
		p.lineTo(xscale*(double)i, yscale*(val - yoff));
	    }
	    addShape(p);
	    addColor(refColor);
	}
	// Create the raw spectrum plot.
	p = new GeneralPath();
	val = 10.0*Math.log10(rawSpectrum[0]) - powerRef;
	p.moveTo(0.0, yscale*(val - yoff));
	for (i = 1; i < rawSpectrum.length; i++) {
	    val = 10.0*Math.log10(rawSpectrum[i]) - powerRef;
	    p.lineTo(xscale*(double)i, yscale*(val - yoff));
	}
	addShape(p);
	addColor(curColor);
	repaint();
    }

    public void actionPerformed(ActionEvent e) {
	if (e.getSource() == btnAutoscale) {
	    setAutoscale(btnAutoscale.isSelected());
	} else if (e.getSource() == btnRef) {
	    if (btnRef.isSelected()) {
		takeReference();
	    } else {
		clearReference();
	    }
	} else if (e.getSource() == btnXRange) {
	    xRangeCount = 2;
	} else if (e.getSource() == btnYRange) {
	    yRangeCount = 2;
	} else if (e.getSource() == btnYZero) {
	    autozero = true;
	} else if (e.getSource() == tfPowerRef) {
	    powerRef = Float.parseFloat(tfPowerRef.getText());
	} else if (e.getSource() == tfPowerMin) {
	    powerMin = Double.parseDouble(tfPowerMin.getText());
	    setYRange(powerMin, powerMax);
	} else if (e.getSource() == tfPowerMax) {
	    powerMax = Double.parseDouble(tfPowerMax.getText());
	    setYRange(powerMin, powerMax);
	} else {
	    super.actionPerformed(e);
	}
    }

    public void handleXRange(double xmin, double xmax) {
	//System.out.println("FFTSSpectrumDiagram.handleXRange(" + xmin + ", " + xmax + ")");
	if (filterPanel != null) {
	    filterPanel.setFrequencyRange((float)xmin, (float)xmax);
	}
    }

    public void handleYRange(double ymin, double ymax) {
	//System.out.println("FFTSSpectrumDiagram.handleYRange(" + ymin + ", " + ymax + ")");
	btnAutoscale.setSelected(false);
	autoscale = false;
	setYRange(ymin, ymax);
    }

}
