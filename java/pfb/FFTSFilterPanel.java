package pfb;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.util.Arrays;
import java.util.Vector;

import java.text.*;

public class FFTSFilterPanel extends FFTSPanel implements MouseListener {

    protected FFTSReplySpectrum   spectrum     = null;
    protected short               nCrates      = 0;
    protected short               crate        = 1;
    protected short               nBoards      = 0;
    protected short               board        = 1;
    protected float               frequency    = 2000.0F;
    protected int                 count        = 512;
    protected int                 binning      = 1;

    protected JTextField          tfBoard      = null;
    protected JLabel              lBoardCount  = null;
    protected JTextField          tfFrequency  = null;
    protected JTextField          tfCount      = null;
    protected JTextField          tfBinning    = null;
    protected JButton             btnFull      = null;
    //protected JButton             btnZoomIn    = null;
    //protected JButton             btnZoomOut   = null;
    protected JButton             btnMoveLeft  = null;
    protected JButton             btnMoveRight = null;

    public FFTSFilterPanel() {
	super("Filter");
    }

    public void createGUI(boolean withTouch) {
	super.createGUI();
	setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
	tfBoard = new JTextField("" + board);
	tfBoard.setColumns(2);
	if (withTouch) {
	    tfBoard.setEditable(false);
	    tfBoard.addMouseListener(this);
	} else {
	    tfBoard.addActionListener(this);
	}
	lBoardCount = new JLabel("[1 .." + nBoards + "]");
	tfFrequency = new JTextField("" + frequency);
	tfFrequency.setColumns(5);
	if (withTouch) {
	    tfFrequency.setEditable(false);
	    tfFrequency.addMouseListener(this);
	} else {
	    tfFrequency.addActionListener(this);
	}
	tfCount = new JTextField("" + count);
	tfCount.setColumns(4);
	if (withTouch) {
	    tfCount.setEditable(false);
	    tfCount.addMouseListener(this);
	} else {
	    tfCount.addActionListener(this);
	}
	tfBinning = new JTextField("" + binning);
	tfBinning.setColumns(3);
	if (withTouch) {
	    tfBinning.setEditable(false);
	    tfBinning.addMouseListener(this);
	} else {
	    tfBinning.addActionListener(this);
	}
	btnFull      = createButton("F");
	btnFull.setPreferredSize(new Dimension(30,25));
	//btnZoomIn    = createButton("+");
	//btnZoomIn.setPreferredSize(new Dimension(30,25));
	//btnZoomOut   = createButton("-");
	//btnZoomOut.setPreferredSize(new Dimension(30,25));
	btnMoveLeft  = createButton("<");
	btnMoveLeft.setPreferredSize(new Dimension(30,25));
	btnMoveRight = createButton(">");
	btnMoveRight.setPreferredSize(new Dimension(30,25));
	// Board + Frequency + Count + Binning + Range
	add(new JLabel("Board:"));
	add(tfBoard);
	//add(lBoardCount);
	add(new JLabel("Freq:"));
	add(tfFrequency);
	add(new JLabel("Count:"));
	add(tfCount);
	add(new JLabel("Bin:"));
	add(tfBinning);
	add(btnFull);
	//add(btnZoomIn);
	//add(btnZoomOut);
	add(btnMoveLeft);
	add(btnMoveRight);
    }

    public short getCrateCount() { return nCrates; }

    public void setCrateCount(short nCrates) {
	this.nCrates = nCrates;
    }

    public short getBoardCount() { return nBoards; }

    public void setBoardCount(short nBoards) {
	this.nBoards = nBoards;
	lBoardCount.setText("[1 .." + nBoards + "]");
    }

    public short getCrate() { return crate; }
    public short getBoard() { return board; }
    public float getFrequency() { return frequency; }
    public int getCount() { return count; }
    public int getBinning() { return binning; }

    public void setCount(int count) {
	this.count = count;
	tfCount.setText("" + count);
    }

    public void setBinning(int binning) {
	this.binning = binning;
	tfBinning.setText("" + binning);
    }

    public void full() {
	if (spectrum == null) return;
	frequency = 0.0F;
	count = 1 << spectrum.channels;
	binning = 1;
	tfFrequency.setText("" + frequency);
	tfCount.setText("" + count);
	tfBinning.setText("" + binning);
    }

    public void zoomIn() {
	if (binning > 1) {
	    binning = binning/2;
	    tfBinning.setText("" + binning);
	}
    }

    public void zoomOut() {
	binning = binning*2;
	tfBinning.setText("" + binning);
    }

    public void moveLower() {
	if (spectrum == null) return;
	float freq = frequency - 0.25F*(spectrum.freq_high - spectrum.freq_low);
	if (freq < 0.0) freq = 0.0F;
	frequency = freq;
	tfFrequency.setText(numFormatD1.format(frequency));
    }
    
    public void moveHigher() {
	if (spectrum == null) return;
	float freq = frequency + 0.25F*(spectrum.freq_high - spectrum.freq_low);
	if (freq > spectrum.bandwidth) freq = spectrum.bandwidth;
	frequency = freq;
	tfFrequency.setText(numFormatD1.format(frequency));
    }

    public void setFrequencyRange(float lower, float higher) {
	if (spectrum == null) return;
	//System.out.println("FFTSFilterPanel.setFrequencyRange(" + lower + ", " + higher + ")");
	int   sCount   = 1 << spectrum.channels;
	float sChWidth = spectrum.bandwidth/(float)sCount;
	int   fCount   = count;
	int   fBinning = binning;
	//System.out.println("  sCount   = " + sCount);
	//System.out.println("  sChWidth = " + sChWidth);
	//System.out.println("  fBinning = " + fBinning);
	float nfreq    = 0.5F*(lower + higher);
	int   nbinning = binning;
	int   ncount   = (int)((higher - lower)/sChWidth)/nbinning;
	while ((ncount < 1024) && (nbinning > 1)) {
	    nbinning = nbinning - 1;
	    ncount  = (int)((higher - lower)/sChWidth)/nbinning;
	}
	//System.out.println("  freq    = " + nfreq);
	//System.out.println("  count   = " + ncount);
	//System.out.println("  binning = " + nbinning);
	frequency = nfreq;
	tfFrequency.setText(numFormatD1.format(frequency));
	count = ncount;
	tfCount.setText("" + count);
	binning = nbinning;
	tfBinning.setText("" + binning);
    }

    public void setLowerFrequency(float lower) {
	//System.out.println("FFTSFilterPanel.setLowerFrequency(" + lower + ")");
	if (spectrum == null) return;
	setFrequencyRange(Math.min(lower, spectrum.freq_high), Math.max(lower, spectrum.freq_high));
    }
    
    public void setHigherFrequency(float higher) {
	//System.out.println("FFTSFilterPanel.setHigherFrequency(" + higher + ")");
	if (spectrum == null) return;
	setFrequencyRange(Math.min(higher, spectrum.freq_low), Math.max(higher, spectrum.freq_low));
    }
    
    public void actionPerformed(ActionEvent e) {
	if (e.getSource() == tfBoard) {
	    try {
		board = Short.parseShort(tfBoard.getText());
	    } catch (Exception ex) {}
	} else if (e.getSource() == tfFrequency) {
	    try {
		frequency = Float.parseFloat(tfFrequency.getText());
	    } catch (Exception ex) {}
	} else if (e.getSource() == tfCount) {
	    try {
		count = Integer.parseInt(tfCount.getText());
	    } catch (Exception ex) {}
	} else if (e.getSource() == tfBinning) {
	    try {
		binning = Integer.parseInt(tfBinning.getText());
	    } catch (Exception ex) {}
	} else if (e.getSource() == btnFull) {
	    full();
	    //} else if (e.getSource() == btnZoomIn) {
	    //zoomIn();
	    //} else if (e.getSource() == btnZoomOut) {
	    //zoomOut();
	} else if (e.getSource() == btnMoveLeft) {
	    moveLower();
	} else if (e.getSource() == btnMoveRight) {
	    moveHigher();
	}
    }

    public void mouseEntered(MouseEvent me) { }

    public void mouseExited(MouseEvent me) { }

    public void mouseClicked(MouseEvent me) {
	if (me.getSource() == tfBoard) {
	    int b = NumberDialog.enterFixnum("Board number", tfBoard.getText());
	    tfBoard.setText("" + b);
	    this.board = (short)b;
	} else if (me.getSource() == tfFrequency) {
	    float nfreq = (float)NumberDialog.enterFlonum("Center frequency", tfFrequency.getText());
	    frequency = nfreq;
	    tfFrequency.setText(numFormatD1.format(frequency));
	} else if (me.getSource() == tfCount) {
	    setCount(NumberDialog.enterFixnum("Sample count", tfCount.getText()));
	} else if (me.getSource() == tfBinning) {
	    setBinning(NumberDialog.enterFixnum("Sample bining", tfBinning.getText()));
	}
    }

    public void mousePressed(MouseEvent me) { }

    public void mouseReleased(MouseEvent me) { }

    public void setSpectrum(FFTSReplySpectrum spectrum) {
	this.spectrum = spectrum;
    }

}
