package pfb;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.util.Arrays;
import java.util.Vector;

import java.text.*;

public class FFTSMonitor extends JFrame implements ActionListener {

    protected String  cfgHost              = "localhost";
    protected int     cfgPort              = FFTSService.PORT;
    protected long    cfgDelay             = 200;
    protected boolean cfgWithTouch         = false;
    protected int     cfgPanelWidthSingle  = 512;
    protected int     cfgPanelWidthDouble  = 256;
    protected int     cfgPanelHeightSingle = 256;
    protected int     cfgPanelHeightDouble = 128;
    protected int     cfgFilterCount       = 512;
    protected int     cfgFilterBinning     = 1;

    protected FFTSService           service           = null;
    protected FFTSReplyPing         ping              = new FFTSReplyPing();
    protected FFTSReplyFeatures     features          = new FFTSReplyFeatures();
    protected boolean               spectrumFlag      = true;
    protected short                 boardnr           = 1;
    protected FFTSReplySpectrum     spectrum          = new FFTSReplySpectrum();
    protected boolean               spectrumAvailable = false;

    protected NumberFormat numFormat2D0 = null;
    protected NumberFormat numFormat6D0 = null;
    protected NumberFormat numFormatD0  = null;
    protected NumberFormat numFormatD1  = null;
    protected NumberFormat numFormatD3  = null;
    protected NumberFormat numFormatE3  = null;

    // setup and features
    protected FFTSGridBagPanel        mainPanel           = new FFTSGridBagPanel();

    protected FFTSConnectionPanel     connectionPanel     = new FFTSConnectionPanel();
    protected FFTSFilterPanel         filterPanel         = new FFTSFilterPanel();
    protected FFTSInfoPanel           infoPanel           = new FFTSInfoPanel();
    protected FFTSSpectrumDiagram     spectrumDiagram     = null;
    protected FFTSHistogramDiagram    histogramDiagram    = null;
    protected FFTSPowerDiagram        powerDiagram        = null;
    protected FFTSPowerHistoryDiagram powerHistoryDiagram = null;
    protected FFTSOnOffDiagram        onOffDiagram        = null;
    protected FFTSWaterfallDiagram    waterfallDiagram    = null;
    protected JButton                 btnHelp             = null;

    protected JTabbedPane             cardPanel           = new JTabbedPane();

    protected Thread                  spectrumThread      = null;
    protected Thread                  diagramThread       = null;

    protected class SpectrumLoop implements Runnable {

	public FFTSReplySpectrum    spectrum = new FFTSReplySpectrum();

	public SpectrumLoop() {}

	public void run() {
	    for ( ; ; ) {
		long              beforeTime = System.currentTimeMillis();
		FFTSReplySpectrum nspectrum = null;
		short             board;
		int               count;
		if (filterPanel.getBoardCount() == 1) {
		    // Es gibt nur ein Board
		    // -> Es wird versucht von diesem Board ein neues Spektrum zu holen
		    board = filterPanel.getBoard();
		    count = filterPanel.getCount();
		} else if (spectrumFlag) {
		    // Es gibt mehr als ein Board und das Spektrum flag ist gesetzt
		    // -> Es wird versucht ein Spektrum des vom Nutzer angegebenen Boards zu bekommen
		    board = filterPanel.getBoard();
		    count = filterPanel.getCount();
		    spectrumFlag = false;
		} else {
		    // Es gibt mehr als ein Board und das Spektrum flag ist nicht gesetzt
		    // -> Es wird versucht den Header eines anderen Boards zu holen
		    if (boardnr == filterPanel.getBoard()) {
			// Die Board-Nummer geht von 1 bis filterPanel.getBoardCount()!
			// Die folgnde Berechnung (skippen der gewaehlten Nummer) ist korrekt!
			boardnr = (short)(boardnr%filterPanel.getBoardCount() + 1);
		    }
		    board = boardnr;
		    count = 0;
		    // Nun wird zur naechsten Boardnummer gegangen
		    boardnr = (short)(boardnr%filterPanel.getBoardCount() + 1);
		    spectrumFlag = true;
		}

		nspectrum = service.sendGetSpectrum(spectrum,
						    filterPanel.getCrate(),
						    board,
						    filterPanel.getFrequency(),
						    count,
						    filterPanel.getBinning());

		if ((nspectrum != null) && (nspectrum.tag == 'S')) {
		    spectrum = provideSpectrum(nspectrum);
		}
		try {
		    /*
		    long afterTime = System.currentTimeMillis();
		    long delay = cfgDelay - (afterTime - beforeTime);
		    //System.out.println("SpectrumLoop: delay=" + delay);
		    if (delay > 0) {
			Thread.sleep(delay);
		    }
		    */
		    Thread.sleep(cfgDelay);
		} catch (InterruptedException e) {
		    //We've been interrupted: no more messages.
		    return;
		} catch (Exception e) {}
	    }
	}

    }

    protected class DiagramLoop implements Runnable {

	public FFTSReplySpectrum spectrum = new FFTSReplySpectrum();

	public DiagramLoop() {}

	public void run() {
	    for ( ; ; ) {
		long              beforeTime = System.currentTimeMillis();
		FFTSReplySpectrum dspectrum = consumeSpectrum(spectrum);
		if (dspectrum == null) {
		    try {
			long afterTime = System.currentTimeMillis();
			long delay = cfgDelay - (afterTime - beforeTime);
			if (delay > 0) {
			    Thread.sleep(delay);
			}
			Thread.sleep(cfgDelay);
		    } catch (InterruptedException e) {
			return;
		    } catch (Exception e) {}
		    continue;
		}
		if (Thread.interrupted()) {
		    //We've been interrupted: no more crunching.
		    return;
		}
		if (dspectrum != null) {
		    spectrum = dspectrum;
		    try {
			if (spectrum.count != 0) {
			    filterPanel.setSpectrum(spectrum);
			    infoPanel.setSpectrum(spectrum);
			    spectrumDiagram.setSpectrum(spectrum);
			    //histogramDiagram.setSpectrum(spectrum);
			    //powerDiagram.setSpectrum(spectrum);
			    //powerHistoryDiagram.setSpectrum(spectrum);
			    //onOffDiagram.setSpectrum(spectrum);
			    waterfallDiagram.setSpectrum(spectrum);
			} else {
			    powerDiagram.setSpectrum(spectrum);
			}
		    } catch (Exception e) {
			e.printStackTrace();
		    }
		    try {
			/*
			  long afterTime = System.currentTimeMillis();
			  long delay = cfgDelay - (afterTime - beforeTime);
			  if (delay > 0) {
			  Thread.sleep(delay);
			  }
			*/
			Thread.sleep(cfgDelay);
		    } catch (InterruptedException e) {
			return;
		    } catch (Exception e) {}
		}
	    }
	}
    }

    public FFTSMonitor(String title) {
        super(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	service = FFTSService.getInstance();
    }

    protected FFTSReplySpectrum provideSpectrum(FFTSReplySpectrum spectrum) {
	synchronized (this) {
	    FFTSReplySpectrum ospectrum = this.spectrum;
	    this.spectrum = spectrum;
	    this.spectrumAvailable = true;
	    return ospectrum;
	}
    }

    protected FFTSReplySpectrum consumeSpectrum(FFTSReplySpectrum spectrum) {
	synchronized (this) {
	    if (!spectrumAvailable) return null;
	    FFTSReplySpectrum ospectrum = this.spectrum;
	    this.spectrum = spectrum;
	    this.spectrumAvailable = false;
	    return ospectrum;
	}
    }

    protected void setHost(String host) { cfgHost = host; }

    protected void setPort(int port) { cfgPort = port; }

    protected void setDelay(long delay) { cfgDelay = delay; }

    protected void setGUISize(String tag) {
	if (tag.equals("s") || tag.equals("small")) {
	    cfgPanelWidthSingle  = 384;
	    cfgPanelWidthDouble  = 136;
	    cfgPanelHeightSingle = 240;
	    cfgPanelHeightDouble = 100;
	    cfgFilterCount       = 4096;
	    cfgFilterBinning     = 16;
	} else if (tag.equals("l") || tag.equals("large")) {
	    cfgPanelWidthSingle  = 1024;
	    cfgPanelWidthDouble  = 476;
	    cfgPanelHeightSingle = 550;
	    cfgPanelHeightDouble = 256;
	    cfgFilterCount       = 8192;
	    cfgFilterBinning     = 8;
	} else {
	    cfgPanelWidthSingle  = 512;
	    cfgPanelWidthDouble  = 204;
	    cfgPanelHeightSingle = 256;
	    cfgPanelHeightDouble = 120;
	    cfgFilterCount       = 8192;
	    cfgFilterBinning     = 8;
	}
    }

    protected void setWithTouch(boolean withTouch) {
	cfgWithTouch = withTouch;
    }

    protected void createNumberFormats() {
	numFormat2D0 = NumberFormat.getInstance();
	numFormat2D0.setMinimumFractionDigits(0);
	numFormat2D0.setMaximumFractionDigits(0);
	numFormat2D0.setMinimumIntegerDigits(2);
	numFormat2D0.setMaximumIntegerDigits(2);
	numFormat2D0.setGroupingUsed(false);

	numFormat6D0 = NumberFormat.getInstance();
	numFormat6D0.setMinimumFractionDigits(0);
	numFormat6D0.setMaximumFractionDigits(0);
	numFormat6D0.setMinimumIntegerDigits(6);
	numFormat6D0.setMaximumIntegerDigits(6);
	numFormat6D0.setGroupingUsed(false);

	numFormatD0 = NumberFormat.getInstance();
	numFormatD0.setMinimumFractionDigits(0);
	numFormatD0.setMaximumFractionDigits(0);
	numFormatD0.setGroupingUsed(false);

	numFormatD1 = NumberFormat.getInstance();
	numFormatD1.setMinimumFractionDigits(1);
	numFormatD1.setMaximumFractionDigits(1);
	numFormatD1.setGroupingUsed(false);

	numFormatD3 = NumberFormat.getInstance();
	numFormatD3.setMinimumFractionDigits(3);
	numFormatD3.setMaximumFractionDigits(3);
	numFormatD3.setGroupingUsed(false);

	numFormatE3 = NumberFormat.getInstance();
	if (numFormatE3 instanceof DecimalFormat) {
	    ((DecimalFormat)numFormatE3).applyPattern("#.###E0");
	}
    }

    protected JPanel createSpectrumPanel(int width, int height) {
	spectrumDiagram = new FFTSSpectrumDiagram(width, height,
						  FFTSDiagram.LEFT, FFTSDiagram.RIGHT,
						  FFTSDiagram.BOTTOM + 25, FFTSDiagram.TOP);
	spectrumDiagram.enableXAxis(true, true, numFormatD1);
	spectrumDiagram.enableYAxis(true, true, numFormatD1);
	spectrumDiagram.setFilterPanel(filterPanel);
	JPanel spectrumPanel = new JPanel();
	spectrumPanel.setBorder(BorderFactory.createTitledBorder("Spectrum"));
	spectrumPanel.add(spectrumDiagram);
	return spectrumPanel;
    }

    protected JPanel createHistogramPanel(int width, int height) {
	histogramDiagram = new FFTSHistogramDiagram(width, height,
						    FFTSDiagram.LEFT, FFTSDiagram.RIGHT,
						    FFTSDiagram.BOTTOM, FFTSDiagram.TOP);
	histogramDiagram.enableXAxis(true, false, numFormatD1);
	histogramDiagram.setXGridRange(-0.5, 0.5);
	histogramDiagram.enableYAxis(true, true, numFormatD0);
	JPanel histogramPanel = new JPanel();
	histogramPanel.setBorder(BorderFactory.createTitledBorder("ADC Histogram"));
	histogramPanel.add(histogramDiagram);
	return histogramPanel;
    }

    protected JPanel createPowerPanel(int width, int height) {
	powerDiagram = new FFTSPowerDiagram(width, height,
					    FFTSDiagram.LEFT, FFTSDiagram.RIGHT,
					    FFTSDiagram.BOTTOM, FFTSDiagram.TOP);
	powerDiagram.enableXAxis(true, false, numFormatD0);
	powerDiagram.enableYAxis(true, true, numFormatD1);
	JPanel powerPanel = new JPanel();
	powerPanel.setBorder(BorderFactory.createTitledBorder("Total Power"));
	powerPanel.add(powerDiagram);
	return powerPanel;
    }

    protected JPanel createPowerHistoryPanel(int width, int height) {
	powerHistoryDiagram = new FFTSPowerHistoryDiagram(width, height,
							  FFTSDiagram.LEFT, FFTSDiagram.RIGHT,
							  FFTSDiagram.BOTTOM, FFTSDiagram.TOP);
	powerHistoryDiagram.enableXAxis(true, true, 1.0, numFormatD0);
	powerHistoryDiagram.enableYAxis(true, true, numFormatD1);
	JPanel powerHistoryPanel = new JPanel();
	powerHistoryPanel.setBorder(BorderFactory.createTitledBorder("Total Power History"));
	powerHistoryPanel.add(powerHistoryDiagram);
	return powerHistoryPanel;
    }

    protected JPanel createOnOffPanel(int width, int height) {
	onOffDiagram = new FFTSOnOffDiagram(width, height,
					    FFTSDiagram.LEFT, FFTSDiagram.RIGHT,
					    FFTSDiagram.BOTTOM, FFTSDiagram.TOP);
	onOffDiagram.enableXAxis(true, true, numFormatD1);
	onOffDiagram.enableYAxis(true, true, numFormatD3);
	onOffDiagram.setFilterPanel(filterPanel);
	JPanel onOffPanel = new JPanel();
	onOffPanel.setBorder(BorderFactory.createTitledBorder("On-Off"));
	onOffPanel.add(onOffDiagram);
	return onOffPanel;
    }

    protected JPanel createWaterfallPanel(int width, int height) {
	waterfallDiagram = new FFTSWaterfallDiagram(width, height,
						    FFTSDiagram.LEFT, FFTSDiagram.RIGHT,
						    FFTSDiagram.BOTTOM + 25, FFTSDiagram.TOP);
	waterfallDiagram.enableXAxis(true, false, numFormatD1);
	waterfallDiagram.enableYAxis(true, false, numFormatD0);
	waterfallDiagram.setZFormat(numFormatD3);
	waterfallDiagram.setFilterPanel(filterPanel);
	JPanel waterfallPanel = new JPanel();
	waterfallPanel.setBorder(BorderFactory.createTitledBorder("Waterfall"));
	waterfallPanel.add(waterfallDiagram);
	return waterfallPanel;
    }

    protected JButton createHelpButton() {
	btnHelp = new JButton("?");
	btnHelp.setMargin(new Insets(0,0,0,0));
	btnHelp.setMinimumSize(new Dimension(25, 25));
	btnHelp.setPreferredSize(new Dimension(25, 25));
	btnHelp.setMaximumSize(new Dimension(25, 25));
	btnHelp.addActionListener(this);
	return btnHelp;
    }

    public void createGUI() {
	createNumberFormats();
	int row = 0;
	int col = 0;

	FFTSDiagram.setTouch(cfgWithTouch);
	connectionPanel.createGUI(cfgWithTouch);
	connectionPanel.setHost(cfgHost);
	connectionPanel.setPort(cfgPort);
	connectionPanel.setStartStopHandler(this);
	filterPanel.createGUI(cfgWithTouch);
	filterPanel.setCount(cfgFilterCount);
	filterPanel.setBinning(cfgFilterBinning);
	infoPanel.createGUI();
	JPanel spectrumPanel     = createSpectrumPanel(cfgPanelWidthSingle, cfgPanelHeightSingle);
	//JPanel spectrumPanel     = createSpectrumPanel(cfgPanelWidthSingle, cfgPanelHeightDouble);
	//JPanel histogramPanel    = createHistogramPanel(cfgPanelWidthDouble, cfgPanelHeightDouble);
	//JPanel powerPanel        = createPowerPanel(cfgPanelWidthDouble, cfgPanelHeightDouble);
	//JPanel powerHistoryPanel = createPowerHistoryPanel(cfgPanelWidthSingle, cfgPanelHeightDouble);
	//JPanel onOffPanel        = createOnOffPanel(cfgPanelWidthSingle, cfgPanelHeightDouble);
	JPanel waterfallPanel    = createWaterfallPanel(cfgPanelWidthSingle, cfgPanelHeightSingle);
	JButton btnHelp          = createHelpButton();

	FFTSGridBagPanel card;

	card = new FFTSGridBagPanel();
	card.grid(spectrumPanel,     0, 0, "tlr");
	//card.grid(histogramPanel,    0, 1,       "tl");
	//card.grid(powerPanel,        1, 1,       "tl");
	cardPanel.addTab("Spectrum", card);

	//card = new FFTSGridBagPanel();
	//card.grid(powerHistoryPanel, 0, 0, "tlr");
	//card.grid(onOffPanel,        0, 1, "tlr");
	//cardPanel.addTab("Power & On/Off", card);

	card = new FFTSGridBagPanel();
	card.grid(waterfallPanel,    0, 0, "tlr");
	cardPanel.addTab("Waterfall", card);

	mainPanel.grid(connectionPanel,  0, 0, 2, 1, "l");
	mainPanel.grid(filterPanel,      2, 0, 1, 1, "l");
	mainPanel.grid(btnHelp,          3, 0, 1, 1, "tr");
	mainPanel.grid(infoPanel,        0, 1, 1, 1, "tblr");
	mainPanel.grid(cardPanel,        1, 1, 3, 1, "tblr");

	getContentPane().add(mainPanel, BorderLayout.CENTER);
        pack();
        setVisible(true);
    }

    protected void handleStartStop() {
	Dimension size = getSize();
	//System.out.println("FFTSMonitor.createGUI(): size = " + size.width + " x " + size.height);
	if (service.isConnected()) {
	    // RUNNING -> STOPPED
	    try {
		if (spectrumThread != null) {
		    spectrumThread.interrupt();
		    spectrumThread.join();
		    spectrumThread = null;
		}
		if (diagramThread != null) {
		    diagramThread.interrupt();
		    diagramThread.join();
		    diagramThread = null;
		}
		service.disconnect();
		connectionPanel.isStopped();
	    } catch (Exception ex) {}
	} else {
	    // STOPPED -> RUNNING
	    try {
		String connHost = connectionPanel.getHost();
		int    connPort = connectionPanel.getPort();
		if (service.connect(connHost, connPort)) {
		    ping = service.sendPing(ping);
		    features = service.sendGetFeatures(features);
		    if (features != null) {
			short nCrates = features.crate_count;
			short nBoards = features.board_count;
			filterPanel.setBoardCount(nBoards);
		    }
		    diagramThread = new Thread(new DiagramLoop());
		    diagramThread.start();
		    spectrumThread = new Thread(new SpectrumLoop());
		    spectrumThread.start();
		    connectionPanel.isStarted();
		}
	    } catch (Exception ex) {}
	}
    }

    public void actionPerformed(ActionEvent e) {
	if (e.getSource() == connectionPanel.getStartStopButton()) {
	    handleStartStop();
	} else if (e.getSource() == btnHelp) {
	    FFTSHelpDialog.showHelp("FFTS Monitor", "FFTSMonitor.html");
	}
    }

    static protected void usage() {
	System.out.println("usage: java mon.FFTSMonitor ( -help | <options> )");
	System.out.println("  -help");
	System.out.println("     shows this text");
	System.out.println("the following options are available:");
	System.out.println("  -host <host>");
	System.out.println("     sets the initial hostname in the connection panel (default localhost)");
	System.out.println("  -port <port>");
	System.out.println("     sets the initial port number in the connection panel (default " + FFTSService.PORT + ")");
	System.out.println("  -delay <delay>");
	System.out.println("     sets the delay between each communication and GUI update ([ms], default 50 ms)");
	System.out.println("  -size ( small | medium | large )");
	System.out.println("     specifies the size of the GUI:");
	System.out.println("     small  : the main diagrams will show 400 values, fits on a 800x480 pixel display");
	System.out.println("     medium : the main diagrams will show 512 values, needs about 900*500 pixels");
	System.out.println("     large  : the main diagrams will show 1024 values");
    }

    public static void main(String[] args) {
        final FFTSMonitor monitor = new FFTSMonitor("FFTS Monitor");
	int idx = 0;
	monitor.setHost("localhost");
	monitor.setPort(FFTSService.PORT);
	monitor.setGUISize("m");
	if ((args.length == 1) && (args[0].equals("-help"))) {
	    usage();
	    return;
	}
	while (idx < args.length) {
	    if (args[idx].equals("-host")) {
		idx++;
		if (idx < args.length) {
		    monitor.setHost(args[idx]);
		    idx++;
		}
	    } else if (args[idx].equals("-port")) {
		idx++;
		if (idx < args.length) {
		    try {
			monitor.setPort(Integer.parseInt(args[idx]));
		    } catch (Exception e) {
		    }
		    idx++;
		}
	    } else if (args[idx].equals("-delay")) {
		idx++;
		if (idx < args.length) {
		    try {
			monitor.setDelay(Long.parseLong(args[idx]));
		    } catch (Exception e) {
		    }
		    idx++;
		}
	    } else if (args[idx].equals("-size")) {
		idx++;
		if (idx < args.length) {
		    monitor.setGUISize(args[idx]);
		    idx++;
		}
	    } else if (args[idx].equals("-touch")) {
		idx++;
		monitor.setWithTouch(true);
	    } else {
		idx++;
	    }
	}
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    monitor.createGUI();
                }
            });
    }


}
