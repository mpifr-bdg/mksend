package pfb;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.util.Arrays;
import java.util.Vector;

import java.text.*;

public class FFTSPanel extends JPanel implements ActionListener {
    
    protected NumberFormat numFormat2D0 = null;
    protected NumberFormat numFormat6D0 = null;
    protected NumberFormat numFormatD0  = null;
    protected NumberFormat numFormatD1  = null;
    protected NumberFormat numFormatD2  = null;
    protected NumberFormat numFormatD3  = null;
    protected NumberFormat numFormatE3  = null;

    public FFTSPanel() {
    }

    public FFTSPanel(String title) {
	setBorder(BorderFactory.createTitledBorder(title));
    }

    protected JButton createButton(String label) {
	JButton btn = new JButton(label);
	btn.setMargin(new Insets(2,4,2,4));
	btn.addActionListener(this);
	return btn;
    }

    protected JToggleButton createToggleButton(String label, boolean flag) {
	JToggleButton btn = new JToggleButton(label, flag);
	btn.setMargin(new Insets(2,4,2,4));
	btn.addActionListener(this);
	return btn;
    }

    public void createGUI() {
	numFormat2D0 = NumberFormat.getInstance();
	numFormat2D0.setMinimumFractionDigits(0);
	numFormat2D0.setMaximumFractionDigits(0);
	numFormat2D0.setMinimumIntegerDigits(2);
	numFormat2D0.setMaximumIntegerDigits(2);
	numFormat2D0.setGroupingUsed(false);

	numFormat6D0 = NumberFormat.getInstance();
	numFormat6D0.setMinimumFractionDigits(0);
	numFormat6D0.setMaximumFractionDigits(0);
	numFormat6D0.setMinimumIntegerDigits(6);
	numFormat6D0.setMaximumIntegerDigits(6);
	numFormat6D0.setGroupingUsed(false);

	numFormatD0 = NumberFormat.getInstance();
	numFormatD0.setMinimumFractionDigits(0);
	numFormatD0.setMaximumFractionDigits(0);
	numFormatD0.setGroupingUsed(false);

	numFormatD1 = NumberFormat.getInstance();
	numFormatD1.setMinimumFractionDigits(1);
	numFormatD1.setMaximumFractionDigits(1);
	numFormatD1.setGroupingUsed(false);

	numFormatD2 = NumberFormat.getInstance();
	numFormatD2.setMinimumFractionDigits(2);
	numFormatD2.setMaximumFractionDigits(2);
	numFormatD2.setGroupingUsed(false);

	numFormatD3 = NumberFormat.getInstance();
	numFormatD3.setMinimumFractionDigits(3);
	numFormatD3.setMaximumFractionDigits(3);
	numFormatD3.setGroupingUsed(false);

	numFormatE3 = NumberFormat.getInstance();
	if (numFormatE3 instanceof DecimalFormat) {
	    ((DecimalFormat)numFormatE3).applyPattern("#.###E0");
	}
    }

    public void actionPerformed(ActionEvent e) {
    }

}
