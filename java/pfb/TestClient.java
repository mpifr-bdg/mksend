package pfb;

import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.awt.GraphicsEnvironment;
import java.awt.GraphicsDevice;
import java.awt.GraphicsConfiguration;
import java.awt.geom.AffineTransform;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.io.*;
import java.net.*;
import java.nio.*;

public class TestClient {

    public static void main(String[] args) throws IOException {
	FFTSService service = FFTSService.getInstance();
	service.connect(args[0], FFTSService.PORT);
	service.sendPing(null);
	service.sendGetFeatures(null);
	FFTSReplySpectrum spectrum = new FFTSReplySpectrum();
	for (int i=0; i < 100; i++) {
	    service.sendGetSpectrum(spectrum, (short)1, (short)1, 2000.0F, 64, 4);
	    try {
		Thread.sleep(50);
	    } catch (Exception e) {}
	}
	service.disconnect();
    }
}

