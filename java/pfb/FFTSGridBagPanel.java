package pfb;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.util.Arrays;
import java.util.Vector;

import java.text.*;

public class FFTSGridBagPanel extends FFTSPanel {

    protected int dynamicColumn = -1;
    protected int dynamicRow    = -1;

    public FFTSGridBagPanel() {
	setLayout(new GridBagLayout());
    }

    public FFTSGridBagPanel(String title) {
	super(title);
	setLayout(new GridBagLayout());
    }

    public void setDynamicColumn(int col) { dynamicColumn = col; }

    public void setDynamicRow(int row) { dynamicRow = row; }

    public void grid(Component comp, int col, int row, int colspan, int rowspan, String sticky) {
        GridBagConstraints   cons = new GridBagConstraints();
        cons.insets = new Insets(0, 0, 0, 0);
        int stickySet = 0;
        if (sticky.indexOf('r') != -1) stickySet += 8;
        if (sticky.indexOf('l') != -1) stickySet += 4;
        if (sticky.indexOf('t') != -1) stickySet += 2;
        if (sticky.indexOf('b') != -1) stickySet += 1;
        if ((stickySet & 15) == 15) {
            cons.fill = GridBagConstraints.BOTH;
        } else if ((stickySet & 12) == 12) {
            cons.fill = GridBagConstraints.HORIZONTAL;
        } else if ((stickySet & 3) == 3) {
            cons.fill = GridBagConstraints.VERTICAL;
        } else {
            cons.fill = GridBagConstraints.NONE;
        }
        switch (stickySet) {
        case  0: /* c    */ cons.anchor = GridBagConstraints.CENTER; break;
        case  1: /* b    */ cons.anchor = GridBagConstraints.PAGE_END; break;
        case  2: /* t    */ cons.anchor = GridBagConstraints.PAGE_START; break;
        case  3: /* tb   */ cons.anchor = GridBagConstraints.CENTER; break;
        case  4: /* l    */ cons.anchor = GridBagConstraints.LINE_START; break;
        case  5: /* lb   */ cons.anchor = GridBagConstraints.LAST_LINE_START; break;
        case  6: /* tl   */ cons.anchor = GridBagConstraints.FIRST_LINE_START; break;
        case  7: /* tbl  */ cons.anchor = GridBagConstraints.LINE_START; break;
        case  8: /* r    */ cons.anchor = GridBagConstraints.LINE_END; break;
        case  9: /* br   */ cons.anchor = GridBagConstraints.LAST_LINE_END; break;
        case 10: /* tr   */ cons.anchor = GridBagConstraints.FIRST_LINE_END; break;
        case 11: /* tbr  */ cons.anchor = GridBagConstraints.LINE_END; break;
        case 12: /* lr   */ cons.anchor = GridBagConstraints.CENTER; break;
        case 13: /* blr  */ cons.anchor = GridBagConstraints.PAGE_END; break;
        case 14: /* tlr  */ cons.anchor = GridBagConstraints.PAGE_START; break;
        case 15: /* tblr */ cons.anchor = GridBagConstraints.CENTER; break;
        }
        cons.gridx = col;
        cons.gridy = row;
        cons.gridwidth = colspan;
        cons.gridheight = rowspan;
	if (dynamicRow == row) cons.weighty = 1.0;
	if (dynamicColumn == col) cons.weightx = 1.0;
        add(comp, cons);
    }

    public void grid(Component comp, int col, int row, String sticky) {
        grid(comp, col, row, 1, 1, sticky);
    }

}

