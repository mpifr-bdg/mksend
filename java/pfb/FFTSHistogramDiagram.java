package pfb;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

import java.util.Arrays;
import java.util.Vector;

import java.text.*;

public class FFTSHistogramDiagram extends FFTSDiagram {

    protected Color             colorI       = new Color(255, 0,   0, 255);
    protected Color             colorQ       = new Color(  0, 0, 255, 255);
    protected JButton           btnIQ        = null;
    protected boolean           selI         = true;
    

    public FFTSHistogramDiagram(int width, int height, int left, int right, int bottom, int top) {
	super(width, height, left, right, bottom, top);
	btnIQ = createSimpleButton("I");
   }

    public void setSpectrum(FFTSReplySpectrum spectrum) {
	if (spectrum.count == 0) return;
	clearShapes();
	clearColors();
	int   i;
	// Skalierung der X-Achse:
	double xscale = ((double)width)/(double)(spectrum.adcHistI.length + 2);
	double xoff   = xscale;
	double bsize  = 0.4*xscale; // 1/2 Breite eines Balkens
	// Skalierung der Y-Achse:
	double ymax;
	if (selI) {
	    ymax = spectrum.adcHistI[0];
	    for (i = 0; i < spectrum.adcHistI.length; i++) {
		if (spectrum.adcHistI[i] > ymax) ymax = spectrum.adcHistI[i];
	    }
	} else {
	    ymax = spectrum.adcHistQ[0];
	    for (i = 0; i < spectrum.adcHistQ.length; i++) {
		if (spectrum.adcHistQ[i] > ymax) ymax = spectrum.adcHistQ[i];
	    }
	}
	double yscale = ymax/((double)height);
	if (yscale == 0.0) yscale = 1.0;
	for (i = 0; i < spectrum.adcHistI.length; i++) {
	    Rectangle2D.Double rect;
	    if (selI && (spectrum.adcHistI[i] != 0)) {
		double h = spectrum.adcHistI[i]/yscale;
		if (h < 1.0) h = 1.0;
		rect = new Rectangle2D.Double(
					      xoff + xscale*((double)i) - bsize,
					      0.0,
					      2.0*bsize,
					      h);
		addShape(rect);
		addColor(colorI);
	    }
	    if (!selI && spectrum.adcHistQ[i] != 0) {
		double h = spectrum.adcHistQ[i]/yscale;
		if (h < 1.0) h = 1.0;
		rect = new Rectangle2D.Double(
					      xoff + xscale*((double)i) - bsize,
					      0.0,
					      2.0*bsize,
					      h);
		addShape(rect);
		addColor(colorQ);
	    }
	}
	setXRange(-0.5 - 1.0/(double)(spectrum.adcHistQ.length + 2), 0.5 + 1.0/(double)(spectrum.adcHistQ.length + 2));
	setYRange(0.0, ymax);
	repaint();
    }

    public void actionPerformed(ActionEvent e) {
	if (e.getSource() == btnIQ) {
	    if (selI) {
		// I -> Q
		selI = false;
		btnIQ.setText("Q");
	    } else {
		// Q -> I
		selI = true;
		btnIQ.setText("I");
	    }
	}
    }


}
