package pfb;


import java.io.*;
import java.net.*;
import java.nio.*;


public class FFTSService {

    public static final int PORT = 6000;

    protected boolean          connected = false;
    protected Socket           sock      = null;
    protected DataOutputStream out       = null;
    protected DataInputStream  in        = null;
    protected FFTSRequest      request   = new FFTSRequest();

    protected FFTSService() {
    }

    public synchronized boolean isConnected() { return connected; }

    public synchronized boolean connect(String host, int port)
    {
	if (connected) return true;
	try {
	    sock = new Socket(host, port);
	    out = new DataOutputStream(new BufferedOutputStream(sock.getOutputStream()));
	    in  = new DataInputStream(new BufferedInputStream(sock.getInputStream()));
	    connected = true;
	} catch (UnknownHostException e) {
            System.err.println("Unknown host: " + host);
            return false;
	} catch (IOException e) {
            System.err.println("Could not get a connection to: " + host + ":" + port);
            return false;
        }
	return connected;
    }

    public synchronized void disconnect()
    {
	if (!connected) return;
	try {
	    out.close();
	    in.close();
	    sock.close();
	    connected = false;
	} catch (IOException e) {
            System.err.println("Could not close the connection");
	}
    }

    public synchronized FFTSReplyPing sendPing(FFTSReplyPing reply)
    {
	if (!connected) return null;
	request.ping();
	if (request.send(out)) {
	    if (reply == null) reply = new FFTSReplyPing();
	    if (reply.receive(in)) {
		return reply;
	    } else {
		return null;
	    }
	} else {
	    return null;
	}
    }

    public synchronized FFTSReplyFeatures sendGetFeatures(FFTSReplyFeatures reply)
    {
	if (!connected) return null;
	request.getFeatures();
	if (request.send(out)) {
	    if (reply == null) reply = new FFTSReplyFeatures();
	    if (reply.receive(in)) {
		return reply;
	    } else {
		return null;
	    }
	} else {
	    return null;
	}
    }

    public synchronized FFTSReplySpectrum sendGetSpectrum(FFTSReplySpectrum reply,
							  short crate,
							  short board,
							  float frequency,
							  int count,
							  int binning)
    {
	if (!connected) return null;
	request.getSpectrum(crate, board, frequency, count, binning);
	if (request.send(out)) {
	    if (reply == null) reply = new FFTSReplySpectrum();
	    if (reply.receive(in)) {
		return reply;
	    } else {
		return null;
	    }
	} else {
	    return null;
	}
    }

    public synchronized FFTSReplySamples sendGetSpectrum(FFTSReplySamples reply,
							 int feed)
    {
	if (!connected) return null;
	request.getSamples(feed);
	if (request.send(out)) {
	    if (reply == null) reply = new FFTSReplySamples();
	    if (reply.receive(in)) {
		return reply;
	    } else {
		return null;
	    }
	} else {
	    return null;
	}
    }

    protected static FFTSService instance = null;

    static public FFTSService getInstance()
    {
	if (instance == null) {
	    instance = new FFTSService();
	}
	return instance;
    }

}
