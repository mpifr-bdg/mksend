#ifndef mksend_options_h
#define mksend_options_h

#include <iostream>
#include <cstdint>
#include <cstdlib>
#include <vector>

#include <boost/program_options.hpp>

#include <spead2/common_features.h>
#include <spead2/send_udp.h>
#if SPEAD2_USE_IBV
# include <spead2/send_udp_ibv.h>
#endif

#include "dada_def.h"

#define MAX_ITEMS      16

#define DADA_TIMESTR "%Y-%m-%d-%H:%M:%S"


/* Configuration file option */
#define HEADER_OPT         "header"
#define HEADER_DESC        "name of a template header file which may include configuration options"
#define HEADER_DEF         ""

/* Flags, therefore all default values are false */
#define QUIET_OPT          "quiet"
#define QUIET_DESC         "Only show total of heaps received"

#define PYSPEAD_OPT        "pyspead"
#define PYSPEAD_DESC       "Be bug-compatible with PySPEAD"

/* The following options should have sufficient default values */
#define PACKET_OPT         "packet-size"
#define PACKET_KEY         "PACKET_SIZE"
#define PACKET_DESC        "Maximum packet size to use for UDP"

#define BUFFER_OPT         "buffer-size"
#define BUFFER_KEY         "BUFFER_SIZE"
#define BUFFER_DESC        "Socket buffer size"

#define NTHREADS_OPT       "nthreads"
#define NTHREADS_KEY       "NTHREADS"
#define NTHREADS_DESC      "Number of worker threads"

#define NHEAPS_OPT         "nheaps"
#define NHEAPS_KEY         "NHEAPS"
#define NHEAPS_DESC        "Maximum number of active heaps"

/* The following options describe the DADA ringbuffer use */

#define DADA_MODE_OPT      "dada-mode"
#define DADA_MODE_KEY      "DADA_MODE"
#define DADA_MODE_DESC     "dada mode (0 = artificial data, 1 = data from dada ringbuffer)"
#define NO_DADA_MODE       0
#define FULL_DADA_MODE     1

#define DADA_KEY_OPT       "dada-key"
#define DADA_KEY_KEY       "DADA_KEY"
#define DADA_KEY_DESC      "PSRDADA ring buffer key"

#define NGROUPS_DATA_OPT   "ngroups-data"
#define NGROUPS_DATA_KEY   "NGROUPS_DATA"
#define NGROUPS_DATA_DESC  "Number of groups (heaps with the same timestamp) going into the data space."

#define NSLOTS_OPT         "nslots"
#define NSLOTS_KEY         "NSLOTS"
#define NSLOTS_DESC        "Number of slots send via network during simulation, 0 means infinite."

/* The following options describe the connection to the network */

#define NETWORK_MODE_OPT   "network-mode"
#define NETWORK_MODE_KEY   "NETWORK_MODE"
#define NETWORK_MODE_DESC  "network mode (0 = no network, 1 = full network support)"
#define NO_NETWORK_MODE    0
#define FULL_NETWORK_MODE  1
#define NETWORK_MODE_DEF   FULL_NETWORK_MODE

#if SPEAD2_USE_IBV
#define IBV_IF_OPT         "ibv-if"
#define IBV_IF_KEY         "IBV_IF"
#define IBV_IF_DESC        "Interface address for ibverbs"

#define IBV_VECTOR_OPT     "ibv-vector"
#define IBV_VECTOR_KEY     "IBV_VECTOR"
#define IBV_VECTOR_DESC    "Interrupt vector (-1 for polled)"

#define IBV_MAX_POLL_OPT   "ibv-max-poll"
#define IBV_MAX_POLL_KEY   "IBV_MAX_POLL"
#define IBV_MAX_POLL_DESC  "Maximum number of times to poll in a row"
#define IBV_MAX_POLL_DEF   spead2::send::udp_ibv_stream::default_max_poll
#endif

#define UDP_IF_OPT         "udp-if"
#define UDP_IF_KEY         "UDP_IF"
#define UDP_IF_DESC        "UDP interface"

#define NHOPS_OPT          "nhops"
#define NHOPS_KEY          "NHOPS"
#define NHOPS_DESC         "Maximum number of hops"

#define NWRATE_OPT         "rate"
#define NWRATE_KEY         "RATE"
#define NWRATE_DESC        "Network use rate"

#define BURST_SIZE_OPT     "burst-size"
#define BURST_SIZE_KEY     "BURST_SIZE"
#define BURST_SIZE_DESC    "Size of a network burst [Bytes]"

#define BURST_RATIO_OPT    "burst-ratio"
#define BURST_RATIO_KEY    "BURST_RATIO"
#define BURST_RATIO_DESC   "I do not know what this means."

#define PORT_OPT           "port"
#define PORT_KEY           "PORT"
#define PORT_DESC          "Port number"

#define DESTINATIONS_OPT   "destination"
#define DESTINATIONS_KEY   "MCAST_DESTINATIONS"
#define DESTINATIONS_DESC  "destinations"

/* The following options describe the timing information */
#define SYNC_EPOCH_OPT     "sync-epoch"
#define SYNC_EPOCH_KEY     "SYNC_TIME"
#define SYNC_EPOCH_DESC    "the ADC sync epoch"
#define SYNC_EPOCH_DEF     0.0

#define SAMPLE_CLOCK_OPT   "sample-clock"
#define SAMPLE_CLOCK_KEY   "SAMPLE_CLOCK"
#define SAMPLE_CLOCK_DESC  "virtual sample clock used for calculations"
#define SAMPLE_CLOCK_DEF   1750000000.0

#define SAMPLE_START_OPT   "sample-start"
#define SAMPLE_START_KEY   "SAMPLE_CLOCK_START"
#define SAMPLE_START_DESC  "first timestamp of the first SPEAD heap in the first slot"
#define SAMPLE_START_DEF   0

#define UTC_START_KEY      "UTC_START"

/* It is possible to specify the heap size and use it as a filter, otherwise the first heap is used to determine this size. */
#define HEAP_SIZE_OPT      "heap-size"
#define HEAP_SIZE_KEY      "HEAP_SIZE"
#define HEAP_SIZE_DESC     "The heap size used for checking incomming heaps."

#define HEAP_ID_START_OPT  "heap-id-start"
#define HEAP_ID_START_KEY  "HEAP_ID_START"
#define HEAP_ID_START_DESC "First used heap id"

#define HEAP_ID_OFFSET_OPT  "heap-id-offset"
#define HEAP_ID_OFFSET_KEY  "HEAP_ID_OFFSET"
#define HEAP_ID_OFFSET_DESC "offset of heap id (different streams)"

#define HEAP_ID_STEP_OPT   "heap-id-step"
#define HEAP_ID_STEP_KEY   "HEAP_ID_STEP"
#define HEAP_ID_STEP_DESC  "difference between two heap ids (same stream)"

#define HEAP_GROUP_OPT     "heap-group"
#define HEAP_GROUP_KEY     "HEAP_GROUP"
#define HEAP_GROUP_DESC    "number of consecutive heaps going into the same stream"

#define NSCI_OPT           "nsci"
#define NSCI_KEY           "NSCI"
#define NSCI_DESC          "Number of item pointers in the side-channel"

/* It is possible to add a specific number of item pointer values to each heap:
 * ITEM1_ID     0x1001          # item #1 ID (16 bits)
 * ITEM1_LIST   42              # fixed value (each heap will have the same value)
 * ITEM1_STEP   unset           # value is fixed
 * ITEM1_INDEX  unset           # this item is fixed
 * ITEM1_SCI    unset           # no side channel item

 * ITEM2_ID     0x1002          # item #2 ID
 * ITEM2_LIST   0,256,512,768   # The frequency band list
 * ITEM2_STEP   unset           # value is fixed
 * ITEM2_INDEX  2               # this item is used as second index inside a group, same semantics as in mkrecv!
 * ITEM2_SCI    unset           # no side channel item

 * ITEM3_ID     0x1003          # item #3 ID
 * ITEM3_LIST   unset           # first value of a serial number
 * ITEM3_STEP   42              # the difference between successive values
 * ITEM3_INDEX  1               # the first index is always build using the timestamp (serial number)
 * ITEM3_SCI    unset           # no side channel item

 * ITEM4_ID     0x1004          # item #4 ID
 * ITEM4_LIST   unset           # no explicite value given
 * ITEM4_STEP   unset           # value is fixed
 * ITEM4_INDEX  unset           # no index
 * ITEM4_SCI    1               # it is the first item in the side-channel data (inside each ringbuffer slot)

 * ITEM5_ID     0x1005          # item #5 ID
 * ITEM5_LIST   unset           # no explicite value given
 * ITEM5_STEP   unset           # value is fixed
 * ITEM5_INDEX  unset           # no index
 * ITEM5_SCI    unset           # no side-channel item
 */

/*
 *  LIST   STEP   INDEX   SCI    =>  Type
 *
 *  unset  set    ignore  ignore     start value from ringbuffer header, step size, index must be equal to 1, serial number
 *  set    set    ignore  ignore     start value, list must contain only one value, step size, index must be equal to 1, serial number
 *
 *  ignore ignore ignore  set        value is take from side-channel, index is 1-based, 0 == unset
 *
 *  set    unset  set     unset      list of values, list must contain more than one value, used as index component (1-based)
 *
 *  set    unset  unset   unset      fixed value, list must contain only one value
 *
 *  unset  unset  ignore  unset      plain data (FFT, samples, ...)
 *
 * LIST, unset  => number of values equal to 0
 * STEP, unset  => step size equal to 0
 * INDEX, unset => value is equal to 0, external 1-based index
 * SCI, unset   => value is equal to 0, external 1-based index
 *
 *
 * IF STEP == set THEN
 *    first index, serial number (timestamp), assume INDEX == 1, ignore SCI
 * ELSEIF SCI == set THEN
 *    side-channel item, ignore LIST, STEP and INDEX
 * ELSEIF LIST == set AND INDEX == set THEN
 *    list of values, list must contain more than one value, used as index component (1-based), ignore STEP and SCI
 * ELSEIF LIST == set AND INDEX == unset THEN
 *    fixed value, list must contain only one value, ignore STEP and SCI
 * ELSE
 *    plain data (FFT, samples, ...), ignore INDEX
 * ENDIF
 */

typedef enum {SERIAL_ITEM_TYPE, INDEX_ITEM_TYPE, FIXED_ITEM_TYPE, SIDE_CHANNEL_ITEM_TYPE, PAYLOAD_ITEM_TYPE} ITEM_TYPE;

#define NITEMS_OPT        "nitems"
#define NITEMS_KEY        "NITEMS"
#define NITEMS_DESC       "Number of item pointers in a SPEAD heap"

#define ITEM_ID_OPT       "item%d-id"
#define ITEM_ID_KEY       "ITEM%d_ID"
#define ITEM_ID_DESC      "SPEAD heap item pointer %d, Id"

#define ITEM_LIST_OPT     "item%d-list"
#define ITEM_LIST_KEY     "ITEM%d_LIST"
#define ITEM_LIST_DESC    "SPEAD heap item pointer %d, list of values"

#define ITEM_STEP_OPT     "item%d-step"
#define ITEM_STEP_KEY     "ITEM%d_STEP"
#define ITEM_STEP_DESC    "SPEAD heap item pointer %d, difference between successive values"
#define ITEM_STEP_DEF     0

#define ITEM_INDEX_OPT    "item%d-index"
#define ITEM_INDEX_KEY    "ITEM%d_INDEX"
#define ITEM_INDEX_DESC   "SPEAD heap item pointer %d, index component"
#define ITEM_INDEX_DEF    0

#define ITEM_SCI_OPT      "item%d-sci"
#define ITEM_SCI_KEY      "ITEM%d_SCI"
#define ITEM_SCI_DESC     "SPEAD heap item pointer %d, side-channel index"
#define ITEM_SCI_DEF      0

namespace po = boost::program_options;

namespace mksend
{

  static constexpr char  ASCII_HEADER_SENTINEL = 4;
  typedef enum {DEFAULT_USED, CONFIG_USED, OPTION_USED} USED_TYPE;

  class item_options
  {
  public:
    std::string                           id_str          = "0";
    USED_TYPE                             id_used_type    = DEFAULT_USED;
    std::size_t                           id              =  0;  // ITEMi_ID
    std::string                           step_str        = "0";
    USED_TYPE                             step_used_type  = DEFAULT_USED;
    std::size_t                           step            =  0;  // ITEMi_STEP
    std::string                           index_str       = "0";
    USED_TYPE                             index_used_type = DEFAULT_USED;
    std::size_t                           index           =  0;  // ITEMi_INDEX, 1-based!
    std::string                           sci_str         = "0";
    USED_TYPE                             sci_used_type   = DEFAULT_USED;
    std::size_t                           sci             =  0;  // ITEMi_SCI, 1-based!
    std::string                           list            = "";  // ITEMi_LIST
    USED_TYPE                             list_used_type  = DEFAULT_USED;
    std::vector<spead2::s_item_pointer_t> values;
    // the following members are used to compute the current/next variable item values (indices)
    ITEM_TYPE                             type   = FIXED_ITEM_TYPE;
    std::size_t                           count  = 0; // number of values for this index
    std::size_t                           lindex = 0; // local index [0 .. (count-1)]
    spead2::s_item_pointer_t              value  = 0; // current value, updated due to index calculation
  };

  class options
  {
  protected:
    bool                      ready              = false;
  public:
    // optional header file contain configuration options and additional information
    std::string               hdrname            = HEADER_DEF;
    // some flags
    bool                      quiet              = false;
    bool                      pyspead            = false;
    // some options, default values should be ok to use, will _not_ go into header
    std::string               packet_str         = "1472";
    std::size_t               packet             =  1472;
    std::string               buffer_str         = "524288";
    std::size_t               buffer             =  524288;
    std::string               threads_str        = "1";
    int                       threads            =  1;
    std::string               heaps_str          = "4";
    std::size_t               heaps              =  4;
    bool                      memcpy_nt          = false;
    // DADA ringbuffer related stuff
    std::string               dada_mode_str      = "1";
    std::size_t               dada_mode          =  1;
    std::string               dada_key           = "dada";
    std::string               ngroups_data_str    = "64";
    std::size_t               ngroups_data        =  64;
    std::string               nslots_str          = "0";
    std::size_t               nslots              = 0;
    // network configuration
    std::string               network_mode_str   = "1";
    std::size_t               network_mode       =  1;
#if SPEAD2_USE_IBV
    std::string               ibv_if              = "";
    std::string               ibv_comp_vector_str = "0";
    int                       ibv_comp_vector     =  0;
    std::string               ibv_max_poll_str    = "10";
    int                       ibv_max_poll        =  10;
#endif
    std::string               udp_if             = "";
    std::string               nhops_str          = "1";
    int                       nhops              =  1;
    std::string               nwrate_str         = "0.0";
    double                    nwrate             =  0.0;
    std::string               burst_size_str     = "65536";
    std::size_t               burst_size         =  65536;
    std::string               burst_ratio_str    = "1.05";
    double                    burst_ratio        =  1.05;
    std::string               port               = "";
    std::string               destinations_str   = "";
    std::vector<std::string>  destinations_opt;
    std::vector<std::string>  destinations;
    std::string               sync_epoch_str     = "0.0";
    double                    sync_epoch         =  0.0;
    std::string               sample_clock_str   = "1750000000.0";
    double                    sample_clock       =  1750000000.0;
    std::string               sample_start_str   = "0";
    std::size_t               sample_start       =  0;
    std::string               heap_size_str      = "0";
    std::size_t               heap_size          =  0;
    std::string               heap_id_start_str  = "1";
    std::size_t               heap_id_start      =  1;
    std::string               heap_id_offset_str = "1";
    std::size_t               heap_id_offset     =  1;
    std::string               heap_id_step_str   = "1";
    std::size_t               heap_id_step       =  1;
    std::string               heap_group_str     = "1";
    std::size_t               heap_group         =  1;
    std::string               heap_count_str     = "1";
    std::size_t               heap_count         =  1;
    std::string               nsci_str           = "0";
    int                       nsci               =  0;
    // item pointer definitions
    std::string               nitems_str         = "0";
    int                       nitems             = 0;
    item_options              items[MAX_ITEMS];
    // Index definitions for mapping a heap to an index
    int                       nindices           = 0;
    int                       indices[MAX_ITEMS]; // ordering: slowest index/timestamp == 0, fastest index == (nindices-1)
    // heap filter mechanism
    char                     *header          = NULL;
  protected:
    po::options_description              desc;
    po::options_description              hidden;
    po::options_description              all;
    po::positional_options_description   positional;
    po::variables_map                    vm;

  public:
    options();
    virtual ~options();
    void usage(std::ostream &o);
    void parse_args(int argc, const char **argv);
    void load_header();
    void set_start_time(int64_t timestamp);
    void update_indices();
    void update_side_channel_items(const spead2::s_item_pointer_t *sci, std::size_t heap_index); // called _before_ using items
    void update_index_items();                                           // called _after_ using items
    bool check_header();
    virtual void create_args();
    virtual void apply_header();
  protected:
    USED_TYPE finalize_parameter(std::string &val_str, const char *opt, const char *key);
    bool parse_fixnum(int &val, std::string &val_str);
    bool parse_fixnum(spead2::s_item_pointer_t &val, std::string &val_str);
    bool parse_fixnum(std::size_t &val, std::string &val_str);
    USED_TYPE parse_parameter(std::string &val, const char *opt, const char *key);
    USED_TYPE parse_parameter(int &val, std::string &val_str, const char *opt, const char *key);
    USED_TYPE parse_parameter(std::size_t &val, std::string &val_str, const char *opt, const char *key);
    USED_TYPE parse_parameter(double &val, std::string &val_str, const char *opt, const char *key);
    USED_TYPE parse_parameter(bool &val, std::string &val_str, const char *opt, const char *key);
    USED_TYPE parse_parameter(std::vector<spead2::s_item_pointer_t> &val, std::string &val_str, const char *opt, const char *key);
    USED_TYPE parse_parameter(std::vector<std::size_t> &val, std::string &val_str, const char *opt, const char *key);
    USED_TYPE parse_parameter(std::vector<std::string> &val, std::string &val_str, const char *opt, const char *key);
  };

}

#endif /* mksend_options_h */
