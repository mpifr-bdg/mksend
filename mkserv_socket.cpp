/* gcc -c mkserv_socket.cpp */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include <endian.h>
#include <byteswap.h>

#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "mkserv_socket.h"
#include "mkserv_logging.h"

namespace mkserv {

  socket::socket() :
    _lsock(-1),
    _sock(-1),
    _buffer_size(0),
    _buffer(NULL),
    _buffer_used(0),
    _buffer_read_pos(0),
    _buffer_write_pos(0)
  {
  }

  socket::socket(int sock) :
    _lsock(-1),
    _sock(sock),
    _buffer_size(0),
    _buffer(NULL),
    _buffer_used(0),
    _buffer_read_pos(0),
    _buffer_write_pos(0)
  {
  }

  socket::~socket()
  {
    finish_server();
    finish_client();
  }


  int socket::listen(int port)
  {
    struct sockaddr_in host;
    int                sockopt = 1;

    /* open socket */
    _lsock = ::socket(AF_INET, SOCK_STREAM, 0);
    if (_lsock == -1) {
      error(__FILE__, __LINE__, "error creating socket");
      return 0;
    }
    /* allow reuse of address */
    sockopt = 1;
    if (setsockopt(_lsock, SOL_SOCKET, SO_REUSEADDR, (char *)&sockopt, sizeof(sockopt)) != 0) {
      error(__FILE__, __LINE__, "socket option error");
      close(_lsock);
      _lsock = -1;
      return 0;
    }
    /* set close on exec flag */
    if (fcntl(_lsock, F_SETFD, 1) == -1) {
      error(__FILE__, __LINE__, "fcntl error");
      close(_lsock);
      _lsock = -1;
      return 0;
    }
    /* bind socket */
    memset((char *)&host, 0, sizeof(host));
    host.sin_family      = AF_INET;
    host.sin_port        = htons (port);
    host.sin_addr.s_addr = htonl (INADDR_ANY);
    if (bind(_lsock, (struct sockaddr *) &host, sizeof (host)) < 0) {
      error(__FILE__, __LINE__, "error binding socket");
      close(_lsock);
      _lsock = -1;
      return 0;
    }
    /* listen on socket */
    if (::listen(_lsock, 8) < 0) {
      error(__FILE__, __LINE__, "error listening on socket");
      close(_lsock);
      _lsock = -1;
      return 0;
    }
    return 1;
  }

  int socket::accept()
  {
    int                optlen = 0;
    int                sockopt = 1;
    struct sockaddr    client;
    socklen_t          clientSize = sizeof(client);
    int                csock;

    /* wait for connection */
    if((csock = ::accept (_lsock, &client, &clientSize)) == -1) {
      error(__FILE__, __LINE__, "error connecting to client");
      return 0;
    }
    /* set maximum packet length */
    optlen = sizeof(sockopt);
    sockopt = MAX_PACKET_SIZE;
    if (setsockopt(csock, SOL_SOCKET, SO_RCVBUF, (char *)&sockopt, sizeof(sockopt)) != 0 ||
	setsockopt(csock, SOL_SOCKET, SO_SNDBUF, (char *)&sockopt, sizeof(sockopt)) != 0) {
      error(__FILE__, __LINE__, "socket option error");
      close(csock);
      return 0;
    }
    /* NODELAY */
    optlen = sizeof(sockopt);
    sockopt = 1;
    /* TODO: ist csock hier richtig? */
    if (setsockopt(_lsock, IPPROTO_TCP, TCP_NODELAY, (char *)&sockopt, sizeof(sockopt)) != 0) {
      error(__FILE__, __LINE__, "socket option error");
      close(csock);
      return 0;
    }
    /* set close on exec flag */
    if (fcntl(_lsock, F_SETFD, 1) == -1) {
      error(__FILE__, __LINE__, "fcntl error");
      close(csock);
      return 0;
    }
    _sock = csock;
    return 1;
  }

  int socket::connect(const char *name, int port, int timeout)
  {
    struct sockaddr_in host;
    struct hostent    *hp;
    in_addr_t          sockServerAddr;
    int                optlen = 0;
    int                sockopt = 1;
    int                ssock = -1;
    int                flags, n, has_error;
    socklen_t          len;
    fd_set             rset, wset;
    struct timeval     tval;
    int                herrno;
    char               msg[256];

    hp = gethostbyname(name);
    if (hp == NULL) {
      herrno = errno;
      snprintf(msg, 256, "unknown host <%s>", name);
      errno = herrno;
      error(__FILE__, __LINE__, msg);
      return 0;
    }
    memcpy(&sockServerAddr, *(hp->h_addr_list), sizeof (in_addr_t));
    /* open socket */
    ssock = ::socket(AF_INET, SOCK_STREAM, 0);
    if (ssock == -1) {
      error(__FILE__, __LINE__, "error creating socket");
      return 0;
    }
    /* allow reuse of address */
    optlen = sizeof(sockopt);
    sockopt = 1;
    if (setsockopt(ssock, SOL_SOCKET, SO_REUSEADDR, (char *)&sockopt, sizeof(sockopt)) != 0) {
      error(__FILE__, __LINE__, "socket option error");
      close (ssock);
      return 0;
    }
    /* set maximum packet length */
    optlen = sizeof(sockopt);
    sockopt = MAX_PACKET_SIZE;
    if (setsockopt(ssock, SOL_SOCKET, SO_RCVBUF, (char *)&sockopt, sizeof(sockopt)) != 0 ||
	setsockopt(ssock, SOL_SOCKET, SO_SNDBUF, (char *)&sockopt, sizeof(sockopt)) != 0) {
      error(__FILE__, __LINE__, "socket option error");
      close (ssock);
      return 0;
    }
    /*
     *  NODELAY
     */
    optlen = sizeof(sockopt);
    sockopt = 1;
    if (setsockopt(ssock, IPPROTO_TCP, TCP_NODELAY, (char *)&sockopt, sizeof(sockopt)) != 0) {
      error(__FILE__, __LINE__, "socket option error");
      close (ssock);
      return 0;
    }
    /* connect to server */
    memset((char *)&host, 0, sizeof(host));
    host.sin_family      = AF_INET;
    host.sin_port        = htons (port);
    host.sin_addr.s_addr = sockServerAddr;
    flags = fcntl(ssock, F_GETFL, 0);
    fcntl(ssock, F_SETFL, flags | O_NONBLOCK);
    if ((n = ::connect(ssock, (struct sockaddr *)&host, sizeof(host)) < 0)) {
      if (errno != EINPROGRESS) {
	error(__FILE__, __LINE__, "error connecting to server");
	close (ssock);
	return 0;
      }
    }
    has_error = 0;
    if (n != 0) {
      FD_ZERO(&rset);
      FD_SET(ssock, &rset);
      wset = rset;
      tval.tv_sec = timeout;
      tval.tv_usec = 0;
      if ((n = select(ssock + 1, &rset, &wset, NULL, timeout ? &tval : NULL)) == 0) {
	close(ssock);
	errno = ETIMEDOUT;
	error(__FILE__, __LINE__, "error connecting to server");
	return 0;
      }
      if (FD_ISSET(ssock, &rset) || FD_ISSET(ssock, &wset)) {
	len = sizeof(has_error);
	if (getsockopt(ssock, SOL_SOCKET, SO_ERROR, &has_error, &len) < 0) {
	  error(__FILE__, __LINE__, "solaris pending error");
	  return 0;
	}
      } else {
	error(__FILE__, __LINE__, "select error, ssock not set");
	return 0;
      }
    }
    // restore old status flags
    fcntl(ssock, F_SETFL, flags);
    if (has_error) {
      close(ssock);
      errno = has_error;
      error(__FILE__, __LINE__, "some error occurred");
      return 0;
    }
    /* set close on exec flag */
    if (fcntl(ssock, F_SETFD, 1) == -1)
      {
	error(__FILE__, __LINE__, "fcntl error");
	close (ssock);
	return 0;
      }
    _sock = ssock;
    return 1;
  }

  void socket::finish_server()
  {
    if (_lsock != -1) {
      close(_lsock);
      _lsock = -1;
    }
  }

  void socket::finish_client()
  {
    if (_sock != -1) {
      close(_sock);
      _sock = -1;
    }
  }

  void socket::start_writing()
  {
    _buffer_write_pos = 0;
  }

  int socket::send()
  {
    size_t      nleft;
    ssize_t     nwritten;
    const char *ptr;
    
    //printf("socket::send(), _buffer_used=%ld\n", _buffer_used);
    ptr = _buffer;
    nleft = _buffer_used;
    errno = 0;
    while (nleft > 0) {
      printf("(nwritten = write(sock, ptr, nleft)) <= 0 : nleft=%ld\n", nleft);
      if ((nwritten = ::write(_sock, ptr, nleft)) <= 0) {
	if (errno != 0) error(__FILE__, __LINE__, "problem sending data");
	printf(" -> nwritten=%ld, errno=%d\n", nwritten, errno);
	if (errno == EINTR) {
	  nwritten = 0;
	} else {
	  error(__FILE__, __LINE__, "could not send a message via a socket");
	  return 0;
	}
      } else {
	if (errno != 0) error(__FILE__, __LINE__, "problem sending data");
	printf(" -> nwritten=%ld, errno=%d\n", nwritten, errno);
      }
      nleft -= nwritten;
      ptr   += nwritten;
    }
    return (nleft == 0);
  }

  int socket::receive(size_t n)
  {
    size_t   nleft;
    ssize_t  nread;
    char    *ptr;
    
    _buffer_used = 0;
    if (!reserve_space(n)) return 0;
    //printf("socket::Receive(%ld)\n", n);
    ptr = _buffer;
    nleft = n;
    errno = 0;
    while (nleft > 0) {
      printf("(nread = read(_sock, ptr, nleft)) < 0 : nleft=%ld\n", nleft);
      if ((nread = ::read(_sock, ptr, nleft)) < 0) {
	if (errno != 0) error(__FILE__, __LINE__, "problem receiving data");
	printf(" -> nread=%ld, errno=%d\n", nread, errno);
	if (errno == EINTR) {
	  nread = 0;
	} else {
	  error(__FILE__, __LINE__, "problem receiving data");
	  return 0;
	}
      } else if (nread == 0) {
	if (errno != 0) error(__FILE__, __LINE__, "problem receiving data");
	break;
      }
      printf(" -> nread=%ld, errno=%d\n", nread, errno);
      nleft -= nread;
      ptr   += nread;
      _buffer_used += nread;
    }
    if (nleft != 0) error(__FILE__, __LINE__, "could not receive all data");
    return (nleft == 0);
  }

  void socket::start_reading()
  {
    _buffer_read_pos = 0;
  }

  int socket::reserve_space(size_t size)
  {
    int   newSize;

    if (_buffer_used + size < _buffer_size) return 1; // there is enough space
    // calculate the needed space as 1KB chunk sizes
    newSize = ((_buffer_used + size + 1023)/1024)*1024;
    if (_buffer == NULL) {
      _buffer = (char *)calloc(newSize, sizeof(char));
      if (_buffer == NULL) {
	error(__FILE__, __LINE__, "could not allocate new _buffer for message");
	return 0;
      }
    } else {
      _buffer = (char *)realloc(_buffer, newSize*sizeof(char));
      if (_buffer == NULL) {
	error(__FILE__, __LINE__, "could not reallocate new _buffer for message");
	return 0;
      }
    }
    _buffer_size = newSize;
    return 1;
  }

  // ##############################################################
  // reading and writing 8-bit values (single and multiple)
  // ##############################################################
  int socket::dput(int8_t value)
  {
    if (_buffer_write_pos + sizeof(int8_t) > _buffer_size) return 0;

    _buffer[_buffer_write_pos++] = value;
    _buffer_used = _buffer_write_pos;
    return 1;
  }

  int socket::dget(int8_t *value)
  {
    if (_buffer_read_pos + sizeof(int8_t) > _buffer_used) return 0;
    *value = _buffer[_buffer_read_pos];
    _buffer_read_pos += sizeof(int8_t);
    return 1;
  }
  
#ifdef WITH_TYPE_TAGS
  // put one 8-bit value
  int socket::put(int16_t tag, int8_t value)
  {
    if (!reserve_space(sizeof(int16_t) + sizeof(int8_t))) return 0;
    dput(tag);
    dput(value);
    return 1;
  }
#else
  // put one 8-bit value
  int socket::put(int8_t value)
  {
    if (!reserve_space(sizeof(int8_t))) return 0;
    dput(value);
    return 1;
  }
#endif /* WITH_TYPE_TAGS */

#ifdef WITH_TYPE_TAGS
  // put several 8-bit values
  int socket::put(int16_t tag, int8_t *values, size_t count)
  {
    if (!reserve_space(sizeof(int16_t) + sizeof(int32_t) + count*sizeof(int8_t))) return 0;
    dput(tag);
    dput((int32_t)count);
    for (size_t i = 0; i < count; i++) dput(values[i]);
    return 1;
  }
#else
  // put several 8-bit values
  int socket::put(int8_t *values, size_t count)
  {
    if (!reserve_space(count*sizeof(int8_t))) return 0;
    for (size_t i = 0; i < count; i++) dput(values[i]);
    return 1;
  }
#endif /* WITH_TYPE_TAGS */

#ifdef WITH_TYPE_TAGS
  // get one 8-bit value
  int socket::get(int16_t tag, int8_t *value)
  {
    int16_t mtag;
    if (!dget(&mtag)) return 0;
    if (tag != mtag) {
      error(__FILE__, __LINE__, "wrong tag in message");
      return 0;
    }
    return dget(value);
  }
#else
  // get one 8-bit value
  int socket::get(int8_t *value)
  {
    return dget(value);
  }
#endif /* WITH_TYPE_TAGS */

#ifdef WITH_TYPE_TAGS
  // get several 8-bit value
  int socket::get(int16_t tag, int8_t *values, size_t count)
  {
    int16_t mtag;
    if (!dget(&mtag)) return 0;
    if (tag != mtag) {
      error(__FILE__, __LINE__, "wrong tag in message");
      return 0;
    }
    int32_t mcount;
    if (!dget(&mcount)) return 0;
    if ((int32_t)count != mcount) {
      error(__FILE__, __LINE__, "wrong number of 8-bit values in message");
      return 0;
    }
    for (size_t i = 0; i < count; i++) dget(values + i);
    return 1;
  }
#else
  // get several 8-bit value
  int socket::get(int8_t *values, size_t count)
  {
    for (size_t i = 0; i < count; i++) dget(values + i);
    return 1;
  }
#endif /* WITH_TYPE_TAGS */


  // ##############################################################
  // reading and writing 16-bit values (single and multiple)
  // ##############################################################
  int socket::dput(int16_t value)
  {
#if __BYTE_ORDER == __LITTLE_ENDIAN
    value = bswap_16(value);
#endif
    memcpy(_buffer + _buffer_write_pos, (char*)&value, sizeof(int16_t));
    _buffer_write_pos += sizeof(int16_t);
    _buffer_used = _buffer_write_pos;
    return 1;
  }

  int socket::dget(int16_t *value)
  {
    if (_buffer_read_pos + sizeof(int16_t) > _buffer_used) return 0;
    memcpy((char*)value, _buffer + _buffer_read_pos, sizeof(int16_t));
    _buffer_read_pos += sizeof(int16_t);
#if __BYTE_ORDER == __LITTLE_ENDIAN
    *value = bswap_16(*value);
#endif
    return 1;
  }
  
#ifdef WITH_TYPE_TAGS
  // put one 16-bit value (including byte swap if necessary)
  int socket::put(int16_t tag, int16_t value)
  {
    if (!reserve_space(sizeof(int16_t) + sizeof(int16_t))) return 0;
    dput(tag);
    dput(value);
    return 1;
  }
#else
  // put one 16-bit value (including byte swap if necessary)
  int socket::put(int16_t value)
  {
    if (!reserve_space(sizeof(int16_t))) return 0;
    dput(value);
    return 1;
  }
#endif /* WITH_TYPE_TAGS */

#ifdef WITH_TYPE_TAGS
  // put several 16-bit values (including byte swap if necessary)
  int socket::put(int16_t tag, int16_t *values, size_t count)
  {
    if (!reserve_space(sizeof(int16_t) + sizeof(int32_t) + count*sizeof(int16_t))) return 0;
    dput(tag);
    dput((int32_t)count);
    for (size_t i = 0; i < count; i++) dput(values[i]);
    return 1;
  }
#else
  // put several 16-bit values (including byte swap if necessary)
  int socket::put(int16_t *values, size_t count)
  {
    if (!reserve_space(count*sizeof(int16_t))) return 0;
    for (size_t i = 0; i < count; i++) dput(values[i]);
    return 1;
  }
#endif /* WITH_TYPE_TAGS */

#ifdef WITH_TYPE_TAGS
  // get one 16-bit value
  int socket::get(int16_t tag, int16_t *value)
  {
    int16_t mtag;
    if (!dget(&mtag)) return 0;
    if (tag != mtag) {
      error(__FILE__, __LINE__, "wrong tag in message");
      return 0;
    }
    return dget(value);
  }
#else
  // get one 16-bit value
  int socket::get(int16_t *value)
  {
    return dget(value);
  }
#endif /* WITH_TYPE_TAGS */

#ifdef WITH_TYPE_TAGS
  // get several 16-bit value
  int socket::get(int16_t tag, int16_t *values, size_t count)
  {
    int16_t mtag;
    if (!dget(&mtag)) return 0;
    if (tag != mtag) {
      error(__FILE__, __LINE__, "wrong tag in message");
      return 0;
    }
    int32_t mcount;
    if (!dget(&mcount)) return 0;
    if ((int32_t)count != mcount) {
      error(__FILE__, __LINE__, "wrong number of 16-bit values in message");
      return 0;
    }
    for (size_t i = 0; i < count; i++) dget(values + i);
    return 1;
  }
#else
  // get several 16-bit value
  int socket::get(int16_t *values, size_t count)
  {
    for (size_t i = 0; i < count; i++) dget(values + i);
    return 1;
  }
#endif /* WITH_TYPE_TAGS */


  // ##############################################################
  // reading and writing 32-bit values (single and multiple)
  // ##############################################################
  int socket::dput(int32_t value)
  {
#if __BYTE_ORDER == __LITTLE_ENDIAN
    value = bswap_32(value);
#endif
    memcpy(_buffer + _buffer_write_pos, (char*)&value, sizeof(int32_t));
    _buffer_write_pos += sizeof(int32_t);
    _buffer_used = _buffer_write_pos;
    return 1;
  }

  int socket::dget(int32_t *value)
  {
    if (_buffer_read_pos + sizeof(int32_t) > _buffer_used) return 0;
    memcpy((char*)value, _buffer + _buffer_read_pos, sizeof(int32_t));
    _buffer_read_pos += sizeof(int32_t);
#if __BYTE_ORDER == __LITTLE_ENDIAN
    *value = bswap_32(*value);
#endif
    return 1;
  }
  
#ifdef WITH_TYPE_TAGS
  // put one 32-bit value (including byte swap if necessary)
  int socket::put(int16_t tag, int32_t value)
  {
    if (!reserve_space(sizeof(int16_t) + sizeof(int32_t))) return 0;
    dput(tag);
    dput(value);
    return 1;
  }
#else
  // put one 32-bit value (including byte swap if necessary)
  int socket::put(int32_t value)
  {
    if (!reserve_space(sizeof(int32_t))) return 0;
    dput(value);
    return 1;
  }
#endif /* WITH_TYPE_TAGS */

#ifdef WITH_TYPE_TAGS
  // put several 32-bit values (including byte swap if necessary)
  int socket::put(int16_t tag, int32_t *values, size_t count)
  {
    if (!reserve_space(sizeof(int16_t) + sizeof(int32_t) + count*sizeof(int32_t))) return 0;
    dput(tag);
    dput((int32_t)count);
    for (size_t i = 0; i < count; i++) dput(values[i]);
    return 1;
  }
#else
  // put several 32-bit values (including byte swap if necessary)
  int socket::put(int32_t *values, size_t count)
  {
    if (!reserve_space(count*sizeof(int32_t))) return 0;
    for (size_t i = 0; i < count; i++) dput(values[i]);
    return 1;
  }
#endif /* WITH_TYPE_TAGS */

#ifdef WITH_TYPE_TAGS
  // get one 32-bit value
  int socket::get(int16_t tag, int32_t *value)
  {
    int16_t mtag;
    if (!dget(&mtag)) return 0;
    if (tag != mtag) {
      error(__FILE__, __LINE__, "wrong tag in message");
      return 0;
    }
    return dget(value);
  }
#else
  // get one 32-bit value
  int socket::get(int32_t *value)
  {
    return dget(value);
  }
#endif /* WITH_TYPE_TAGS */

#ifdef WITH_TYPE_TAGS
  // get several 32-bit value
  int socket::get(int16_t tag, int32_t *values, size_t count)
  {
    int16_t mtag;
    if (!dget(&mtag)) return 0;
    if (tag != mtag) {
      error(__FILE__, __LINE__, "wrong tag in message");
      return 0;
    }
    int32_t mcount;
    if (!dget(&mcount)) return 0;
    if ((int32_t)count != mcount) {
      error(__FILE__, __LINE__, "wrong number of 32-bit values in message");
      return 0;
    }
    for (size_t i = 0; i < count; i++) dget(values + i);
    return 1;
  }
#else
  // get several 32-bit value
  int socket::get(int32_t *values, size_t count)
  {
    for (size_t i = 0; i < count; i++) dget(values + i);
    return 1;
  }
#endif /* WITH_TYPE_TAGS */


  // ##############################################################
  // reading and writing 64-bit values (single and multiple)
  // ##############################################################
  int socket::dput(int64_t value)
  {
#if __BYTE_ORDER == __LITTLE_ENDIAN
    value = bswap_64(value);
#endif
    memcpy(_buffer + _buffer_write_pos, (char*)&value, sizeof(int64_t));
    _buffer_write_pos += sizeof(int64_t);
    _buffer_used = _buffer_write_pos;
    return 1;
  }

  int socket::dget(int64_t *value)
  {
    if (_buffer_read_pos + sizeof(int64_t) > _buffer_used) return 0;
    memcpy((char*)value, _buffer + _buffer_read_pos, sizeof(int64_t));
    _buffer_read_pos += sizeof(int64_t);
#if __BYTE_ORDER == __LITTLE_ENDIAN
    *value = bswap_64(*value);
#endif
    return 1;
  }

  int socket::dput(uint64_t value)
  {
#if __BYTE_ORDER == __LITTLE_ENDIAN
    value = bswap_64(value);
#endif
    memcpy(_buffer + _buffer_write_pos, (char*)&value, sizeof(uint64_t));
    _buffer_write_pos += sizeof(uint64_t);
    _buffer_used = _buffer_write_pos;
    return 1;
  }

  int socket::dget(uint64_t *value)
  {
    if (_buffer_read_pos + sizeof(uint64_t) > _buffer_used) return 0;
    memcpy((char*)value, _buffer + _buffer_read_pos, sizeof(uint64_t));
    _buffer_read_pos += sizeof(uint64_t);
#if __BYTE_ORDER == __LITTLE_ENDIAN
    *value = bswap_64(*value);
#endif
    return 1;
  }

#ifdef WITH_TYPE_TAGS
  // put one 64-bit value (including byte swap if necessary)
  int socket::put(int16_t tag, int64_t value)
  {
    if (!reserve_space(sizeof(int16_t) + sizeof(int64_t))) return 0;
    dput(tag);
    dput(value);
    return 1;
  }
#else
  // put one 64-bit value (including byte swap if necessary)
  int socket::put(int64_t value)
  {
    if (!reserve_space(sizeof(int64_t))) return 0;
    dput(value);
    return 1;
  }
#endif /* WITH_TYPE_TAGS */

#ifdef WITH_TYPE_TAGS
  // put several 64-bit values (including byte swap if necessary)
  int socket::put(int16_t tag, int64_t *values, size_t count)
  {
    if (!reserve_space(sizeof(int16_t) + sizeof(int32_t) + count*sizeof(int64_t))) return 0;
    dput(tag);
    dput((int32_t)count);
    for (size_t i = 0; i < count; i++) dput(values[i]);
    return 1;
  }
#else
  // put several 64-bit values (including byte swap if necessary)
  int socket::put(int64_t *values, size_t count)
  {
    if (!reserve_space(count*sizeof(int64_t))) return 0;
    for (size_t i = 0; i < count; i++) dput(values[i]);
    return 1;
  }
#endif /* WITH_TYPE_TAGS */

#ifdef WITH_TYPE_TAGS
  // get one 64-bit value
  int socket::get(int16_t tag, int64_t *value)
  {
    int16_t mtag;
    if (!dget(&mtag)) return 0;
    if (tag != mtag) {
      error(__FILE__, __LINE__, "wrong tag in message");
      return 0;
    }
    return dget(value);
  }
#else
  // get one 64-bit value
  int socket::get(int64_t *value)
  {
    return dget(value);
  }
#endif /* WITH_TYPE_TAGS */

#ifdef WITH_TYPE_TAGS
  // get several 64-bit value
  int socket::get(int16_t tag, int64_t *values, size_t count)
  {
    int16_t mtag;
    if (!dget(&mtag)) return 0;
    if (tag != mtag) {
      error(__FILE__, __LINE__, "wrong tag in message");
      return 0;
    }
    int32_t mcount;
    if (!dget(&mcount)) return 0;
    if ((int32_t)count != mcount) {
      error(__FILE__, __LINE__, "wrong number of 64-bit values in message");
      return 0;
    }
    for (size_t i = 0; i < count; i++) dget(values + i);
    return 1;
  }
#else
  // get several 64-bit value
  int socket::get(int64_t *values, size_t count)
  {
    for (size_t i = 0; i < count; i++) dget(values + i);
    return 1;
  }
#endif /* WITH_TYPE_TAGS */

#ifdef WITH_TYPE_TAGS
  // put one 64-bit value (including byte swap if necessary)
  int socket::put(int16_t tag, uint64_t value)
  {
    if (!reserve_space(sizeof(int16_t) + sizeof(uint64_t))) return 0;
    dput(tag);
    dput(value);
    return 1;
  }
#else
  // put one 64-bit value (including byte swap if necessary)
  int socket::put(uint64_t value)
  {
    if (!reserve_space(sizeof(uint64_t))) return 0;
    dput(value);
    return 1;
  }
#endif /* WITH_TYPE_TAGS */

#ifdef WITH_TYPE_TAGS
  // put several 64-bit values (including byte swap if necessary)
  int socket::put(int16_t tag, uint64_t *values, size_t count)
  {
    if (!reserve_space(sizeof(int16_t) + sizeof(int32_t) + count*sizeof(uint64_t))) return 0;
    dput(tag);
    dput((int32_t)count);
    for (size_t i = 0; i < count; i++) dput(values[i]);
    return 1;
  }
#else
  // put several 64-bit values (including byte swap if necessary)
  int socket::put(uint64_t *values, size_t count)
  {
    if (!reserve_space(count*sizeof(uint64_t))) return 0;
    for (size_t i = 0; i < count; i++) dput(values[i]);
    return 1;
  }
#endif /* WITH_TYPE_TAGS */

#ifdef WITH_TYPE_TAGS
  // get one 64-bit value
  int socket::get(int16_t tag, uint64_t *value)
  {
    int16_t mtag;
    if (!dget(&mtag)) return 0;
    if (tag != mtag) {
      error(__FILE__, __LINE__, "wrong tag in message");
      return 0;
    }
    return dget(value);
  }
#else
  // get one 64-bit value
  int socket::get(uint64_t *value)
  {
    return dget(value);
  }
#endif /* WITH_TYPE_TAGS */

#ifdef WITH_TYPE_TAGS
  // get several 64-bit value
  int socket::get(int16_t tag, uint64_t *values, size_t count)
  {
    int16_t mtag;
    if (!dget(&mtag)) return 0;
    if (tag != mtag) {
      error(__FILE__, __LINE__, "wrong tag in message");
      return 0;
    }
    int32_t mcount;
    if (!dget(&mcount)) return 0;
    if ((int32_t)count != mcount) {
      error(__FILE__, __LINE__, "wrong number of 64-bit values in message");
      return 0;
    }
    for (size_t i = 0; i < count; i++) dget(values + i);
    return 1;
  }
#else
  // get several 64-bit value
  int socket::get(uint64_t *values, size_t count)
  {
    for (size_t i = 0; i < count; i++) dget(values + i);
    return 1;
  }
#endif /* WITH_TYPE_TAGS */



}
